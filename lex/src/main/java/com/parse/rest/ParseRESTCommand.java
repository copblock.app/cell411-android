/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.rest;

import static com.parse.Parse.getURL;
import static com.parse.http.ParseHttpMethod.POST;

import androidx.annotation.CallSuper;

import com.parse.Parse;
import com.parse.encoder.NoObjectsEncoder;
import com.parse.http.ParseHttpHeader;
import com.parse.http.ParseHttpMethod;
import com.parse.http.ParseHttpRequest;
import com.parse.http.ParseSyncUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import cell411.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ParseRESTCommand
{
  public static final int DEFAULT_MAX_RETRIES = 3;
  private static final MediaType JSON = MediaType.get("application/json");
  //    public Thread newThread(@Nonnull Runnable r) {
  //      return new Thread(r, "ParseRequest.NETWORK_EXECUTOR-thread-" +
  //      mCount.getAndIncrement());
  //    }
  //  };
  //  /**
  //   * We want to use more threads than default in {@code bolts.Executors}
  //   since most of the time
  //   * the threads will be asleep waiting for data.
  //   */
  //  private static final int CPU_COUNT = Runtime.getRuntime()
  //  .availableProcessors();
  //  private static final int CORE_POOL_SIZE = CPU_COUNT * 2 + 1;
  //  private static final int MAX_POOL_SIZE = CPU_COUNT * 2 * 2 + 1;
  //  private static final long KEEP_ALIVE_TIME = 1L;
  //  private static final int MAX_QUEUE_SIZE = 128;
  //  protected static final ExecutorService NETWORK_EXECUTOR =
  //  newThreadPoolExecutor
  //  (CORE_POOL_SIZE, MAX_POOL_SIZE,
  //                                                                                  KEEP_ALIVE_TIME, TimeUnit.SECONDS,
  //                                                                                  new
  //                                                                                  LinkedBlockingQueue<Runnable>(
  //                                                                                      MAX_QUEUE_SIZE), sThreadFactory);
  public final ParseHttpMethod mMethod;
  public final URL mUrl;
  public final RequestBody body;
  private final String mSessionToken;

  public ParseRESTCommand(String httpPath, ParseHttpMethod httpMethod,
                          Map<String, ?> parameters, String sessionToken)
  {
    this(httpPath, httpMethod, encodeParams(parameters), sessionToken);
  }

  public ParseRESTCommand(String httpPath, ParseHttpMethod httpMethod,
                          JSONObject jsonParameters, String sessionToken)
  {
    Parse.checkInit();
    this.mUrl = createUrl(httpPath);
    this.body = createBody(httpMethod, jsonParameters);
    if ((body != null) && !httpMethod.needsBody()) {
      this.mMethod = POST;
    } else {
      this.mMethod = httpMethod;
    }
    this.mSessionToken = sessionToken;
  }

  static JSONObject encodeParams(Map<String, ?> params)
  {
    if (params == null) {
      return null;
    } else {
      return (JSONObject) NoObjectsEncoder.get().encode(params);
    }
  }

  public static URL createUrl(String httpPath)
  {
    // We send all parameters for GET/HEAD/DELETE requests in a post body,
    // so no need to worry about query parameters here.
    if (httpPath == null) {
      return getURL();
    }

    try {
      return new URL(getURL(), httpPath);
    } catch (MalformedURLException ex) {
      throw new RuntimeException(ex);
    }
  }

  private static RequestBody createBody(ParseHttpMethod method, JSONObject json)
  {
    if (json == null)
      return null;
    if (!method.needsBody()) {
      json.put(ParseHttpHeader.PARAMETER_METHOD_OVERRIDE, method.toString());
    }
    return new JSONRequestBody(json);
  }

  public ParseRESTCommand(Init<?> builder)
  {
    this(builder.httpPath, builder.method, builder.jsonParameters,
      builder.sessionToken);
  }

  public ParseHttpRequest newRequest()
  {
    ParseHttpRequest.Builder requestBuilder = new ParseHttpRequest.Builder();
    requestBuilder.setmMethod(mMethod).setmURL(mUrl).setBody(body);
    addHeaders(requestBuilder);
    return requestBuilder.build();
  }

  @CallSuper
  protected void addHeaders(ParseHttpRequest.Builder builder)
  {
    if(mSessionToken!=null){
      builder.addHeader(ParseHttpHeader.HEADER_SESSION_TOKEN,mSessionToken);
    }
    ParseSyncUtils.customizeRequest(builder);
  }

  /* package */ static abstract class Init<T extends Init<T>>
  {
    private String sessionToken;
    private ParseHttpMethod method = ParseHttpMethod.GET;
    private String httpPath;
    private JSONObject jsonParameters;

    private String operationSetUUID;
    private String localId;

    public T sessionToken(String sessionToken)
    {
      this.sessionToken = sessionToken;
      return self();
    }

    /* package */
    abstract T self();

    public T method(ParseHttpMethod method)
    {
      this.method = method;
      return self();
    }

    public T httpPath(String httpPath)
    {
      this.httpPath = httpPath;
      return self();
    }

    public T jsonParameters(JSONObject jsonParameters)
    {
      this.jsonParameters = jsonParameters;
      return self();
    }

    public T operationSetUUID(String operationSetUUID)
    {
      this.operationSetUUID = operationSetUUID;
      return self();
    }

    public T localId(String localId)
    {
      this.localId = localId;
      return self();
    }
  }

  public static class Builder
    extends Init<Builder>
  {
    @Override
      /* package */ Builder self()
    {
      return this;
    }

    public ParseRESTCommand build()
    {
      return new ParseRESTCommand(this);
    }
  }
}

