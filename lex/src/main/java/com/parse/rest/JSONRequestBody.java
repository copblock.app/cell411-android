package com.parse.rest;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class JSONRequestBody
  extends RequestBody
{
  static MediaType smMediaType = MediaType.get("application/json");
  final JSONObject mObject;
  private String mString;

  public JSONRequestBody(JSONObject object)
  {
    mObject = object;
  }

  @Nullable
  @Override
  public MediaType contentType()
  {
    return smMediaType;
  }

  @Override
  public void writeTo(BufferedSink sink)
  throws IOException
  {
    sink.write(getBytes());
  }

  private byte[] getBytes()
  {
    String string = toString();
    return string.getBytes(UTF_8);
  }

  @Nonnull
  @Override
  public String toString()
  {
    if (mString == null) {
      mString = mObject.toString();
    }
    return mString;
  }
}
