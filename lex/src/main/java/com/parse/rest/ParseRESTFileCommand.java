/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.rest;

import com.parse.http.ParseHttpMethod;
import com.parse.model.ParseFile;

import java.io.File;

/**
 * REST network command for creating & uploading {@link ParseFile}s.
 */
public class ParseRESTFileCommand extends ParseRESTCommand {

  private final byte[] data;
  private final String contentType;
  private final File file;

  public ParseRESTFileCommand(Builder builder) {
    super(builder);
    if (builder.file != null && builder.data != null) {
      throw new IllegalArgumentException("File and data can not be set at the same time");
    }
    this.data = builder.data;
    this.contentType = builder.contentType;
    this.file = builder.file;
  }

//  @Override
//  protected RequestBody newBody() {
//    return
//      data != null ? new ParseByteArrayHttpBody(data, contentType, this) :
//        new ParseFileHttpBody(
//          file, contentType);
//  }

  public static class Builder extends Init<Builder> {

    private byte[] data = null;
    private String contentType = null;
    private File file;

    public Builder() {
      // We only ever use ParseRESTFileCommand for file uploads, so default to POST.
      method(ParseHttpMethod.POST);
    }

    public Builder fileName(String fileName) {
      return httpPath(String.format("files/%s", fileName));
    }

    public Builder data(byte[] data) {
      this.data = data;
      return this;
    }

    public Builder contentType(String contentType) {
      this.contentType = contentType;
      return this;
    }

    public Builder file(File file) {
      this.file = file;
      return this;
    }

    @Override
      /* package */ Builder self() {
      return this;
    }

    public ParseRESTFileCommand build() {
      return new ParseRESTFileCommand(this);
    }
  }
}

