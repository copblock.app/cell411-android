package com.parse.decoder;

import com.parse.model.ParseObject;

import javax.annotation.Nullable;

import cell411.utils.collect.LazyObject;
import cell411.utils.Util;

public interface ParseDecoder
{
  LazyObject<ParseDecoder> smInstance =
    LazyObject.fromSupplier(ParseDecoder.class,ParseDecoderImpl::new);
  static ParseDecoder get()
  {
    return Util.getRef(smInstance);
  }

  ParseObject decodePointer(String className, String objectId);

  Object decode(@Nullable Object object);
}
