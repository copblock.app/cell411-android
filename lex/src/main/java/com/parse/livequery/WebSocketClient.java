package com.parse.livequery;

import android.util.Log;




import java.net.URI;
import java.util.Locale;

import javax.annotation.Nonnull;

import cell411.config.ConfigDepot;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

class WebSocketClient
  extends WebSocketListener
{

  private static final String LOG_TAG = "okhttp3WebSocketClient";

  private final WebSocketClientCallback mWebSocketClientCallback;

  private final String url;
  private final int STATUS_CODE = 1000;
  private final String CLOSING_MSG = "User invoked close";
  private WebSocket webSocket;
  private volatile SocketState state = SocketState.NONE;

  WebSocketClient(WebSocketClientCallback callback, URI hostUrl) {
    mWebSocketClientCallback = callback;
    url = hostUrl.toString();
  }

  @Override
  public void onOpen(@Nonnull WebSocket webSocket, @Nonnull Response response) {
    setState(SocketState.CONNECTED);
    mWebSocketClientCallback.onOpen();
  }

  @Override
  public void onMessage(@Nonnull WebSocket webSocket, @Nonnull String text) {
    mWebSocketClientCallback.onMessage(text);
  }

  @Override
  public void onMessage(@Nonnull WebSocket webSocket, ByteString bytes) {
    Log.w(LOG_TAG,
      String.format(Locale.US, "Socket got into inconsistent state and received %s instead.",
        bytes.toString()));
  }

  @Override
  public void onClosing(@Nonnull final WebSocket webSocket, final int code, @Nonnull
  final String reason) {
    //    mWebSocketClientCallback.onClosing(webSocket,reason);
  }

  @Override
  public void onClosed(@Nonnull WebSocket webSocket, int code, @Nonnull String reason) {
    setState(SocketState.DISCONNECTED);
    mWebSocketClientCallback.onClose();
  }

  @Override
  public void onFailure(@Nonnull WebSocket webSocket, @Nonnull Throwable t, Response response) {
    setState(SocketState.DISCONNECTED);
    mWebSocketClientCallback.onError(t);
  }

  //        @Override
  public synchronized void open() {
    if (SocketState.NONE == state) {
      // okhttp33 connects as soon as the socket is created so do it here.
      Request request = new Request.Builder().url(url).build();

      webSocket =
        ConfigDepot.getHttpClient().newWebSocket(request, this);
      setState(SocketState.CONNECTING);
    }
  }

  //        @Override
  public synchronized void close() {
    if (SocketState.NONE != state) {
      setState(SocketState.DISCONNECTING);
      webSocket.close(STATUS_CODE, CLOSING_MSG);
    }
  }

  //        @Override
  public synchronized void send(String message) {
    if (state == SocketState.CONNECTED) {
      webSocket.send(message);
    }
  }

  //        @Override
  public SocketState getState() {
    return state;
  }

  private void setState(SocketState newState) {
    synchronized (this) {
      this.state = newState;
    }
    mWebSocketClientCallback.stateChanged();
  }

}
