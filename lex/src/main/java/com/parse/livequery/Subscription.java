package com.parse.livequery;

import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

import java.util.ArrayList;
import java.util.List;

import cell411.utils.concurrent.ThreadUtil;

public class Subscription<T extends ParseObject>
  implements SubscriptionHandler<T>
{
  private final List<ObjectEventsCallback<T>> handleEventsCallbacks = new ArrayList<>();
  private final List<ErrorCallback<T>> handleErrorCallbacks = new ArrayList<>();
  private final List<HandleSubscribeCallback<T>> handleSubscribeCallbacks = new ArrayList<>();
  private final List<HandleUnsubscribeCallback<T>> handleUnsubscribeCallbacks = new ArrayList<>();

  private final int requestId;
  private final ParseQuery<T> query;

  public Subscription(int requestId, ParseQuery<T> query) {
    this.requestId = requestId;
    this.query = query;
  }

  @Override
  public Subscription<T> handleEvents(ObjectEventsCallback<T> callback) {
    handleEventsCallbacks.add(callback);
    return this;
  }

  @Override
  public Subscription<T> handleError(ErrorCallback<T> callback) {
    handleErrorCallbacks.add(callback);
    return this;
  }

  @Override
  public Subscription<T> handleSubscribe(HandleSubscribeCallback<T> callback) {
    handleSubscribeCallbacks.add(callback);
    return this;
  }

  @Override
  public Subscription<T> handleUnsubscribe(HandleUnsubscribeCallback<T> callback) {
    handleUnsubscribeCallbacks.add(callback);
    return this;
  }

  @Override
  public int getRequestId() {
    return requestId;
  }

  /* package */
  public ParseQuery<T> getQuery() {
    return query;
  }

  /* package */
  public ParseQuery.State<T> getQueryState() {
    return query.getBuilder().build();
  }

  /**
   * Tells the handler that an event has been received from the live query server.
   *
   * @param ev The event that has been received from the server.
   *           It carries the query.
   */
  protected void didReceive(ObjectEvent<T> ev) {
    Runnable messenger = ev.dispatcher(handleEventsCallbacks);
    ThreadUtil.onExec(messenger);
  }

  /**
   * Tells the handler that an error has been received from the live query server.
   *
   * @param error The error that the server has encountered.
   * @param query The query that the error occurred on.
   */
  /* package */ void didEncounter(LiveQueryException error, ParseQuery<T> query) {
    for (ErrorCallback<T> handleErrorCallback : handleErrorCallbacks) {
      handleErrorCallback.onError(query, error);
    }
  }

  /**
   * Tells the handler that a query has been successfully registered with the server.
   * - note: This may be invoked multiple times if the client disconnects/reconnects.
   *
   * @param query The query that has been subscribed.
   */
  /* package */ void didSubscribe(ParseQuery<T> query) {
    for (HandleSubscribeCallback<T> handleSubscribeCallback : handleSubscribeCallbacks) {
      handleSubscribeCallback.onSubscribe(query);
    }
  }

  /**
   * Tells the handler that a query has been successfully deregistered from the server.
   * - note: This is not called unless `unregister()` is explicitly called.
   *
   * @param query The query that has been unsubscribed.
   */
  /* package */ void didUnsubscribe(ParseQuery<T> query) {
    for (HandleUnsubscribeCallback<T> handleUnsubscribeCallback : handleUnsubscribeCallbacks) {
      handleUnsubscribeCallback.onUnsubscribe(query);
    }
  }

}
