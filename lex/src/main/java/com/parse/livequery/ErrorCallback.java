package com.parse.livequery;

import com.parse.ParseQuery;
import com.parse.model.ParseObject;

public interface ErrorCallback<T extends ParseObject> {
  void onError(ParseQuery<T> query, LiveQueryException exception);
}
