package com.parse.livequery;

import android.util.Log;

import com.parse.Parse;
import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

import cell411.json.JSONException;
import cell411.json.JSONObject;
import cell411.utils.UrlUtils;
import cell411.utils.Util;
import cell411.utils.concurrent.RunOrCall;
import cell411.utils.concurrent.Task;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.io.PrintString;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

@SuppressWarnings("unused")
public class ParseLiveQueryClient implements ExceptionHandler,
                                             WebSocketClientCallback {
  public final static XTAG TAG = new XTAG();
  private final String mApplicationId;
  private final String mClientKey;
  private final ConcurrentHashMap<Integer, Subscription<? extends ParseObject>>
    subscriptions = new ConcurrentHashMap<>();
  private final URI mUri;
  private final List<ParseLiveQueryClientCallbacks> mCallbacks =
    new ArrayList<>();
  //  private final File mLogFile;
  int mOpens = 0;
  ArrayList<String> records = new ArrayList<>();
  private WebSocketClient webSocketClient;
  private int requestIdCount = 1;
  private boolean userInitiatedDisconnect = false;
  private boolean hasReceivedConnected = false;

  //  private final OutputStream mStream;
//  private final TimeLog mLog;
  public ParseLiveQueryClient() {
    Parse.checkInit();
    mUri = getDefaultUri();
    mApplicationId = Parse.applicationId();
    mClientKey = Parse.clientKey();
  }

  private static URI getDefaultUri() {
    URL url = Parse.getURL();
    String text = url.toString();
    if(text.startsWith("https:")) {
      text=text.replace("https:","wss:");
    } else if (text.startsWith("http:")) {
      text=text.replace("http:","ws:");
    } else {
      throw new IllegalStateException("Url should be http/s.  Got " + url);
    };
    return UrlUtils.toURI(text);
  }

  public void log(CharSequence text) {

  }

  public <T extends ParseObject> Subscription<T> subscribe(ParseQuery<T> query) {
    if (query == null) {
      throw new NullPointerException("Query cannot be null");
    }
    int requestId = requestIdGenerator();
    Subscription<T> subscription = new Subscription<>(requestId, query);
    subscriptions.put(requestId, subscription);

    if (isConnected()) {
      sendSubscription(subscription);
    } else if (userInitiatedDisconnect) {
      XLog.w(TAG,
        "Warning: The client was explicitly disconnected! You must explicitly call .reconnect" +
          "() in order to process your subscriptions.");
    } else {
      connectIfNeeded();
    }

    return subscription;
  }

  public void connectIfNeeded() {
    switch (getWebSocketState()) {
      case CONNECTED:
      case CONNECTING:
      default:
        break;

      case NONE:
      case DISCONNECTING:
      case DISCONNECTED:
        reconnect();
        break;
    }
  }

  public <T extends ParseObject> void unsubscribe(final ParseQuery<T> query) {

    try (PrintString ps = new PrintString()) {
      ps.pl(query == null ? "null" : query.toJSON().toString(2));
      ps.pl("\n\n\n\n");
      //    new Exception().printStackTrace(ps);
      XLog.i(TAG, ps.toString());
    }

    if (query != null) {
      for (Subscription<? extends ParseObject> subscription : subscriptions.values()) {
        if (query.equals(subscription.getQuery())) {
          endSubscription(subscription);
        }
      }
    }
  }

  public <T extends ParseObject> void unsubscribe(final ParseQuery<T> query,
                                                  final SubscriptionHandler<T> subscriptionHandler) {
    if (query != null && subscriptionHandler != null) {
      for (Subscription<? extends ParseObject> subscription : subscriptions.values()) {
        if (query.equals(subscription.getQuery()) &&
          subscriptionHandler.equals(subscription)) {
          endSubscription(subscription);
        }
      }
    }
  }

  public int getOpens() {
    return mOpens;
  }

  public synchronized void reconnect() {
    //    XLog.i(TAG, "reconnect");
    mOpens++;
    if (webSocketClient != null) {
      webSocketClient.close();
    }

    userInitiatedDisconnect = false;
    hasReceivedConnected = false;
    webSocketClient = new WebSocketClient(this, mUri);
    webSocketClient.open();
  }

  public synchronized void disconnect() {
    if (webSocketClient != null) {
      webSocketClient.close();
      webSocketClient = null;
    }

    userInitiatedDisconnect = true;
    hasReceivedConnected = false;
  }

  public void registerListener(ParseLiveQueryClientCallbacks listener) {
    mCallbacks.add(listener);
  }

  // Private methods

  public void unregisterListener(ParseLiveQueryClientCallbacks listener) {
    mCallbacks.remove(listener);
  }

  private synchronized int requestIdGenerator() {
    return requestIdCount++;
  }

  private SocketState getWebSocketState() {
    return webSocketClient == null ? SocketState.NONE : webSocketClient.getState();
  }

  //  private void handleOperation(final String message) {
  //    try {
  //      parseMessage(message);
  //    } catch (LiveQueryException e) {
  //      throw Util.rethrow("handleOperationsAsync", e);
  //    }
  //  }

  private boolean isConnected() {
    return hasReceivedConnected && inAnyState(SocketState.CONNECTED);
  }

  private boolean inAnyState(SocketState... states) {
    return Arrays.asList(states).contains(getWebSocketState());
  }

  private void sendOperation(final ClientOperation clientOperation) {
    try {
      JSONObject jsonEncoded = clientOperation.getJSONObjectRepresentation();
      String jsonString = jsonEncoded.toString(2);
//      if (Parse.getLogLevel() <= Parse.LOG_LEVEL_DEBUG) {
//        IOUtil.stringToStream(mLog, jsonString);
//      }
      webSocketClient.send(jsonString);
    } catch (Exception e) {
      handleException("sending operation", e);
    }
  }

  public void parseMessage(String message, StackTraceElement[] stackTrace)
  throws LiveQueryException {
    try {
      JSONObject jsonObject = new JSONObject(message);
      message = jsonObject.toString(2);
      String rawOperation = jsonObject.getString("op");

      switch (rawOperation) {
        case "connected":
          hasReceivedConnected = true;
          dispatchConnected();
          int size = subscriptions.size();
          Reflect.announce(Util.format("Connected, sending %d pending subscriptions", size));
          for (Subscription<? extends ParseObject> subscription : subscriptions.values()) {
            sendSubscription(subscription);
          }
          break;
        case "redirect":
          String url = jsonObject.getString("url");
          // TODO: Handle redirect.
          Log.d(String.valueOf(TAG), "Redirect is not yet handled");
          break;
        case "subscribed":
          handleSubscribedEvent(jsonObject);
          break;
        case "unsubscribed":
          handleUnsubscribedEvent(jsonObject);
          break;
        case "enter":
        case "leave":
        case "update":
        case "create":
        case "delete":
          handleObjectEvent(jsonObject, stackTrace);
          break;
        case "error":
          handleErrorEvent(jsonObject);
          break;
        default:
          throw new LiveQueryException.InvalidResponseException(message);
      }
    } catch (JSONException e) {
      throw new LiveQueryException.InvalidResponseException(message);
    }
  }

  private String format(String s, Object... args) {
    return String.format(Locale.US, s, args);
  }

  private void dispatchConnected() {
    for (ParseLiveQueryClientCallbacks callback : mCallbacks) {
      callback.onLiveQueryClientConnected(this);
    }
  }

  private void dispatchDisconnected() {
    for (ParseLiveQueryClientCallbacks callback : mCallbacks) {
      callback.onLiveQueryClientDisconnected(this, userInitiatedDisconnect);
    }
  }

  private void dispatchServerError(LiveQueryException exc) {
    for (ParseLiveQueryClientCallbacks callback : mCallbacks) {
      callback.onLiveQueryError(this, exc);
    }
  }

  private void dispatchSocketError(Throwable reason) {
    userInitiatedDisconnect = false;

    for (ParseLiveQueryClientCallbacks callback : mCallbacks) {
      callback.onSocketError(this, reason);
    }

    dispatchDisconnected();
  }

  private <T extends ParseObject> void handleSubscribedEvent(JSONObject jsonObject)
  throws JSONException {
    final int requestId = jsonObject.getInt("requestId");
    final Subscription<T> subscription = subscriptionForRequestId(requestId);
    if (subscription != null) {
      subscription.didSubscribe(subscription.getQuery());
    }
  }

  private <T extends ParseObject> void handleUnsubscribedEvent(JSONObject jsonObject)
  throws JSONException {
    final int requestId = jsonObject.getInt("requestId");
    final Subscription<T> subscription = subscriptionForRequestId(requestId);
    if (subscription != null) {
      subscription.didUnsubscribe(subscription.getQuery());
      subscriptions.remove(requestId);
    }
  }

  private <T extends ParseObject> void handleObjectEvent(JSONObject jsonObject,
                                                         StackTraceElement[] stackTrace)
  throws JSONException {
    final int requestId = jsonObject.getInt("requestId");
    final Subscription<T> subscription = subscriptionForRequestId(requestId);

    if (subscription != null) {
      ObjectEvent<T> event = new ObjectEvent<>(subscription, jsonObject, stackTrace);
      subscription.didReceive(event);
    }
  }

  private <T extends ParseObject> void handleErrorEvent(JSONObject jsonObject)
  throws JSONException {
    int requestId = jsonObject.getInt("requestId");
    int code = jsonObject.getInt("code");
    String error = jsonObject.getString("error");
    boolean reconnect = jsonObject.getBoolean("reconnect");
    final Subscription<T> subscription = subscriptionForRequestId(requestId);
    LiveQueryException exc = new LiveQueryException.ServerReportedException(code, error, reconnect);

    if (subscription != null) {
      subscription.didEncounter(exc, subscription.getQuery());
    }

    dispatchServerError(exc);
  }

  private <T extends ParseObject> Subscription<T> subscriptionForRequestId(int requestId) {
    //noinspection unchecked
    return (Subscription<T>) subscriptions.get(requestId);
  }

  private <T extends ParseObject> void sendSubscription(final Subscription<T> subscription) {
    //    ParseUser.getCurrentSessionTokenAsync().onSuccess((Continuation<String, Void>) task -> {
    //      String sessionToken = task.getResult();
    ParseUser currentUser = Parse.getCurrentUser();
    String sessionToken = Parse.getSessionToken();

    SubscribeClientOperation<T> op =
      new SubscribeClientOperation<>(subscription.getRequestId(), subscription.getQueryState(),
        sessionToken);
    sendOperation(op);
  }

  private <T extends ParseObject> void endSubscription(Subscription<T> subscription) {
    sendOperation(new UnsubscribeClientOperation(subscription.getRequestId()));
  }

  @Override
  public void onOpen() {
    Reflect.announce("" + this);
    hasReceivedConnected = false;
    String sessionToken = Parse.getSessionToken();
    if (sessionToken == null) {
      return;
    }
    sendOperation(new ConnectClientOperation(mApplicationId, sessionToken));
  }

  @Override
  public void onMessage(String message) {
    Reflect.announce("" + this);
    StackTraceElement[] stackTrace = new Exception().getStackTrace();
    Task<?> run = Task.forVirtual(
      Void.class,
      this, "parseMessage", message, stackTrace);
    ThreadUtil.onExec(run);
  }

  @Override
  public void onClose() {
    Reflect.announce("" + this);

    hasReceivedConnected = false;
    dispatchDisconnected();
  }

  @Override
  public void onError(Throwable exception) {
    Reflect.announce("" + exception);
    exception.printStackTrace();
    hasReceivedConnected = false;
    dispatchSocketError(exception);
  }

  @Override
  public void stateChanged() {
    Reflect.announce();
  }

  class OnMessage implements RunOrCall<Void> {
    public final String mMsg;
    public final StackTraceElement[] mStackTraceElement;

    OnMessage(String msg, StackTraceElement[] stackTraceElement) {
      mMsg = msg;
      mStackTraceElement = null;
    }

    public void run() {
      try {
        parseMessage(mMsg, mStackTraceElement);
      } catch (LiveQueryException e) {
        Util.printStackTrace(e);
      }
    }
  }
}
