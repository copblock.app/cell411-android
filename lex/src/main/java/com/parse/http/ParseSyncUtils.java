package com.parse.http;

import static com.parse.http.ParseHttpHeader.HEADER_APPLICATION_ID;
import static com.parse.http.ParseHttpHeader.HEADER_APP_BUILD_VERSION;
import static com.parse.http.ParseHttpHeader.HEADER_APP_DISPLAY_VERSION;
import static com.parse.http.ParseHttpHeader.HEADER_CLIENT_KEY;
import static com.parse.http.ParseHttpHeader.HEADER_INSTALLATION_ID;
import static com.parse.http.ParseHttpHeader.HEADER_OS_VERSION;
import static com.parse.http.ParseHttpHeader.USER_AGENT;
import static com.parse.http.ParseHttpMethod.GET;
import static com.parse.model.ParseUser.KEY_SESSION_TOKEN;

import android.os.Build;
import android.os.Looper;
import android.os.NetworkOnMainThreadException;

import com.parse.Parse;
import com.parse.Parse.Configuration;
import com.parse.ParseClassName;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.callback.LogInCallback;
import com.parse.callback.ParseCallback1;
import com.parse.callback.ParseCallback2;
import com.parse.controller.ClassTool;
import com.parse.decoder.KnownParseObjectDecoder;
import com.parse.decoder.ParseDecoder;
import com.parse.encoder.AbstractParseEncoder;
import com.parse.encoder.ParseEncoder;
import com.parse.encoder.ParseObjectCoder;
import com.parse.encoder.PointerEncoder;
import com.parse.model.CurrentData;
import com.parse.model.ParseFile;
import com.parse.model.ParseInstallation;
import com.parse.model.ParseObject;
import com.parse.model.ParseRole;
import com.parse.model.ParseUser;
import com.parse.offline.InstallationId;
import com.parse.operation.ParseFieldOperation;
import com.parse.operation.ParseOperationSet;
import com.parse.operation.ParseSetOperation;
import com.parse.rest.JSONRequestBody;
import com.parse.rest.ParseRESTCloudCommand;
import com.parse.rest.ParseRESTCommand;
import com.parse.rest.ParseRESTObjectCommand;
import com.parse.rest.ParseRESTQueryCommand;
import com.parse.rest.ParseRESTUserCommand;
import com.parse.utils.ParseDateFormat;
import com.parse.utils.ParseIOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.config.ConfigDepot;
import cell411.json.JSONArray;
import cell411.json.JSONException;
import cell411.json.JSONObject;
import cell411.utils.Pair;
import cell411.utils.Util;
import cell411.utils.collect.LazyObject;
import cell411.utils.concurrent.ThreadName;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.IOUtil;
import cell411.utils.io.PrintString;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;
import kotlin.NotImplementedError;
import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

public class ParseSyncUtils
{
  private static final XTAG TAG = new XTAG();
  public static String sessionToken = null;
  public static final Map<String, ClassTool<? extends ParseObject>> smByName =
    new HashMap<>();

  private static final Object mutex = new Object();
  private static final LazyObject<OkHttpClient> smRestClient =
    LazyObject.fromSupplier(OkHttpClient.class, ParseSyncUtils::createRestClient);
  private static final LazyObject<OkHttpClient> smFileClient =
    LazyObject.fromSupplier(OkHttpClient.class, ParseSyncUtils::createFileClient);
  private static final ParseObjectCoder coder = ParseObjectCoder.get();
  private static final String dashed_line = new String(Util.mkDashedLine());
  private static final Object lock = new Object();
  public static ParseDateFormat smParseDateFormat = ParseDateFormat.get();

  /* package */ ScheduledExecutorService smService =
    Executors.newSingleThreadScheduledExecutor();

  public static String userAgent()
  {
    return "Parse Android SDK API Level " + Build.VERSION.SDK_INT;
  }

  public static boolean isSubclassValid(String className,
                                        Class<? extends ParseObject> clazz)
  {
    ClassTool<? extends ParseObject> classTool;

    synchronized (mutex) {
      if(Util.isNoE(smByName))
        throw new RuntimeException("Not Initialized");
      classTool = smByName.get(className);
      if (classTool == null)
        return clazz == ParseObject.class;
      else
        return classTool.getType().isAssignableFrom(clazz);

    }

  }

  /* package */
  public static ParseObject newInstance(String className)
  {
    ClassTool<? extends ParseObject> classTool;

    synchronized (mutex) {
      classTool = smByName.get(className);
    }

    try {
      if(classTool==null){
        System.out.println("Classname is: "+className);
        ParseObject po  = new ParseObject(className);
        System.out.println(po.toJSON());
        return po;
      } else {
        ParseObject po = classTool.newInstance();
        assert (po.getClassName()).equals(className);
        return classTool.getType().cast(po);
      }
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException("Failed to create instance of subclass.", e);
    }
  }

  public static String getTableForClass(Class<? extends ParseObject> aClass)
  {
    return getClassName(aClass);
  }

  public static String getClassName(Class<? extends ParseObject> clazz)
  {
    ParseClassName info = clazz.getAnnotation(ParseClassName.class);
    if (info == null) {
      throw new IllegalArgumentException(
        "No ParseClassName annotation provided on " + clazz);
    }
    return info.value();
  }

  public static Class<? extends ParseObject> getClassForTable(
    String targetClassName)
  {
    ClassTool<? extends ParseObject> tool = smByName.get(targetClassName);
    if (tool == null)
      return null;
    else
      return tool.getType();
  }

  static OkHttpClient createFileClient()
  {
    return ConfigDepot.getHttpClient();
  }

  static OkHttpClient createRestClient()
  {
    XLog.i(TAG, String.valueOf(OkHttpClient.Builder.class));
    XLog.i(TAG, String.valueOf(OkHttpClient.Builder.class));
    OkHttpClient.Builder clientBuilder = ConfigDepot.getHttpClientBuilder();
    clientBuilder.pingInterval(Duration.ofSeconds(600));

    return clientBuilder.build();
  }

  @Nonnull
  public static <T extends ParseObject> List<T> find(ParseQuery.State<T> state)
  throws ParseException
  {
    JSONObject jsonResponse = runQuery(state, false);
    JSONArray jsonResults = jsonResponse.getJSONArray("results");
    String resultClassName = jsonResponse.optString("className");
    if (resultClassName.isEmpty()) {
      resultClassName = state.className();
    }
    List<T> results = new ArrayList<>(jsonResults.length());
    for (int i = 0; i < jsonResults.length(); i++) {
      JSONObject jsonResult = jsonResults.getJSONObject(i);
      T result =
        ParseObject.fromJSON(jsonResult, resultClassName, ParseDecoder.get(),
          state.selectedKeys());
      results.add(result);
    }
    return results;
  }

  public static <T extends ParseObject> JSONObject runQuery(
    ParseQuery.State<T> state, boolean count)
  {
    ParseUser currentUser = CurrentData.getCurrentUser();
    if (currentUser == null) {
      throw new ParseException(ParseException.NOT_LOGGED_IN, "session missing");
    }

    String httpPath = String.format("classes/%s", state.className());
    Map<String, String> parameters = ParseRESTQueryCommand.encode(state, count);
    ParseRESTQueryCommand command =
      new ParseRESTQueryCommand(httpPath, GET, parameters, sessionToken);
    ParseHttpRequest request = command.newRequest();
    return executeRequest(request);
  }

  public static void logIn(String username, String password,
                           LogInCallback listener)
  {
      if (Looper.getMainLooper().isCurrentThread()) {
        ThreadUtil.onExec((() -> logIn(username, password, listener)));
        return;
      }

    try(ThreadName threadName = new ThreadName("Logging In")) {
      if (Util.isNoE(username)) {
        throw new IllegalArgumentException(
          "Must specify a username for the user to log in with");
      }
      if (Util.isNoE(password)) {
        throw new IllegalArgumentException(
          "Must specify a password for the user to log in with");
      }

      ParseRESTCommand command =
        ParseRESTUserCommand.logInUserCommand(username, password);
      ParseHttpRequest request = command.newRequest();
      if (Parse.getCurrentUser() != null) {
        Parse.setCurrentUser(null);
      }
      JSONObject jsonResponse = executeRequest(request);
      ParseUser.State.Builder builder = new ParseUser.State.Builder();

      ParseDecoder decoder = ParseDecoder.get();
      builder.isComplete(false);
      String token = jsonResponse.optString(KEY_SESSION_TOKEN, "");
      if (Util.isNoE(token)) {
        throw new ParseException(ParseException.SESSION_MISSING,
          "No token in server data", true, null);
      }
      jsonResponse.remove(KEY_SESSION_TOKEN);
      sessionToken = token;
      ParseObjectCoder objectCoder = ParseObjectCoder.get();
      ParseUser.State state =
        objectCoder.decode(builder, jsonResponse, decoder).isComplete(true)
          .build();

      ParseUser currentUser = ParseUser.from(state);
      CurrentData.setCurrentUser(currentUser);
      if (listener != null) {
        ThreadUtil.onMain(listener.wrap(currentUser, null));
      }
    } catch (Throwable ex) {
      Util.printStackTrace(ex);
      throw ex;
    } finally {
      Reflect.announce();
    }
  }

  public static void customizeRequest(@Nonnull ParseHttpRequest.Builder builder)
  {
    Configuration cfg = Parse.getConfiguration();
    builder.addHeader(HEADER_APPLICATION_ID, cfg.applicationId)
      .addHeader(HEADER_APP_BUILD_VERSION, "" + Parse.getVersionCode())
      .addHeader(HEADER_APP_DISPLAY_VERSION, Parse.getVersionName())
      .addHeader(HEADER_OS_VERSION, Build.VERSION.RELEASE)
      .addHeader(USER_AGENT, userAgent());

    InstallationId installationId = CurrentData.getInstallationId();
    builder.addHeader(HEADER_INSTALLATION_ID, installationId.get());
    // client key can be null with self-hosted Parse Server
    builder.addHeader(HEADER_CLIENT_KEY, cfg.clientKey);
  }

  public static void save(ParseObject object)
  {
    if (ThreadUtil.isMainThread()) {
      throw new ParseException(ParseException.OTHER_CAUSE, "On Main Thread",
        new NetworkOnMainThreadException());
    }

    HashSet<ParseObject> dirtyChildren = new HashSet<>();
    HashSet<ParseFile> dirtyFiles = new HashSet<>();
    ParseObject.collectDirtyChildren(object, dirtyChildren, dirtyFiles);
    ParseOperationSet operations;
    for (ParseFile pf : dirtyFiles) {
      pf.save();
    }
    for (ParseObject po : dirtyChildren) {
      po.updateBeforeSave();
      po.validateSave();
      operations = po.startSave();
      ParseObject.State state = po.getState();
      JSONObject objectJSON =
        coder.encode(state, operations, PointerEncoder.get());

      ParseRESTObjectCommand command =
        ParseRESTObjectCommand.saveObjectCommand(state, objectJSON,
          sessionToken);
      ParseHttpRequest request = command.newRequest();
      JSONObject result = executeRequest(request);
      final Map<String, ParseObject> fetched = po.collectFetchedObjects();
      ParseDecoder decoder = new KnownParseObjectDecoder(fetched);
      ParseObject.State.Init<?> builder = po.getState().newBuilder();
      builder.apply(operations);
      builder.isComplete(false);
      builder =
        ParseObjectCoder.get().decode(builder, new JSONObject(), decoder);
      ParseOperationSet operationSet = new ParseOperationSet();
      for (String key : result.keySet()) {
        Object val = decoder.decode(result.opt(key));
        ParseFieldOperation operation;
        if (val instanceof ParseFieldOperation) {
          operation = (ParseFieldOperation) val;
        } else {
          operation = new ParseSetOperation(val);
        }
        operationSet.put(key, operation);
      }
      builder.apply(operationSet);
      ListIterator<ParseOperationSet> it = po.operationSetQueue.listIterator();
      it.next();
      it.remove();
      builder.isComplete(true);
      ParseObject.State newState = builder.build();
      po.setState(newState);
      po.afterSave();
    }
  }
//  static {
//    String json = "{\n  \"$or\": [\n    {\n      \"owner\": {\n        " +
//      "\"__type\": \"Pointer\",\n        \"className\": \"_User\",\n        "+
//      "\"objectId\": \"dev1\"\n      }\n    },\n    {\n      \"audience\": {\n        \"__type\": \"Pointer\",\n        \"className\": \"_User\",\n        \"objectId\": \"dev1\"\n      }\n    },\n    {\n      \"audience\": \"\\\"dev1\\\"\"\n    },\n    {\n      \"audience\": \"dev1\"\n    },\n    {\n      \"owner\": \"dev1\"\n    }\n  ]\n}";
//    JSONObject object = new JSONObject(json);
//    System.out.println(object);
//    System.out.println(dashed_line);
//  }
  public static JSONObject executeRequest(ParseHttpRequest request)
  {
//    PrintString ps = new PrintString();
    try {
// //      noinspection ConstantValue
//      if(request.mStack!=null) {
//        for (StackTraceElement ste : request.mStack) {
//          ps.println(ste);
//        }
//      }
      Request okRequest = request.getOkRequest();
//      dumpCopy(okRequest, ps);
      Call okhttp3Call = getRestClient().newCall(okRequest);
      Response okResponse = okhttp3Call.execute();
      ParseHttpResponse result = ParseHttpResponse.getResponse(okResponse);
      JSONObject jsonResponse = getJSON(result);
//      dumpCopy(okResponse, ps);
      int statusCode = result.getStatusCode();
//      ps.println(jsonResponse.toString(2));
//      ps.println(dashed_line);
//      Pair<File, OutputStream> logFile
//        = ConfigDepot.getNextLogFile("query", false);
//      assert logFile!=null;
//      IOUtil.stringToStream(logFile.mVal2,String.valueOf(ps));
      switch (statusCode / 100) {
        case 4:
        case 5: {
          throwEx(jsonResponse);
        }
        case 2:
          return jsonResponse;
        default:
          throw new ParseException(ParseException.OTHER_CAUSE,
            "Unexpected http code");
      }
    } catch (ParseException pe) {
      Reflect.announce("Exception: ", pe);
      throw pe;
    } catch (IOException ioe) {
      Util.printStackTrace(ioe);
      throw ParseException.wrap(ioe);
    }
  }

  private static void dumpCopy(Response okResponse, PrintString ps)
  {
    ps.println(dashed_line);
    ps.println("Response");
    ps.println(dashed_line);
    ps.println("Code:    "+okResponse.code());
    ps.println("Message: "+okResponse.message());
    ps.println(dashed_line);
    dumpHeaders("Headers: ", okResponse.headers(), ps);
    ps.println(dashed_line);
  }

  public static JSONObject getJSON(ParseHttpResponse response)
  {
    InputStream responseStream = null;
    try {
      responseStream = response.getContent();
      byte[] bytes = ParseIOUtils.toByteArray(responseStream);
      String content = new String(bytes).trim();
      if (contentIsHtml(content)) {
        if (content.contains("<title>502 Bad Gateway")) {
          throw new ParseException(ParseException.CONNECTION_FAILED,
            "502 Bad Gateway");
        } else {
          throw new ParseException(ParseException.OTHER_CAUSE, content);
        }
      } else {
        return new JSONObject(content);
      }
    } catch (JSONException e) {
      throw new RuntimeException("Parsing query response", e);
    } finally {
      ParseIOUtils.closeQuietly(responseStream);
    }
  }

  public static OkHttpClient getRestClient()
  {
    return smRestClient.get();
  }

  private static boolean contentIsHtml(String content)
  {
    int p = 0;
    while (Character.isSpaceChar(content.charAt(p))) {
      p++;
    }
    return content.charAt(p) == '<';
  }

  public static String stringValue(RequestBody body)
  {
    if (body == null) {
      return "body is null";
    }
    try (Buffer buffer = new Buffer()) {
      body.writeTo(buffer);
      return buffer.toString();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static ParseEncoder minParseEncoder()
  {
    return new AbstractParseEncoder()
    {
      @Override
      public JSONObject encodeRelatedObject(final ParseObject po)
      {
        JSONObject object = new JSONObject();
        object.put("data", po.getClassName() + "." + po.getObjectId());
        return object;
      }
    };
  }

  public static <T extends ParseObject> int count(ParseQuery.State<T> state)
  {
    JSONObject result = runQuery(state, true);
    return result.optInt("count", 0);
  }

  public static <T> T run(@Nonnull String name, @Nullable Map<String, ?> params)
  {
    if(params==null)
      params = new HashMap<>();
    ParseRESTCommand command =
      ParseRESTCloudCommand.callFunctionCommand(name, params, sessionToken);
    ParseHttpRequest request = command.newRequest();
    JSONObject jsonResponse = executeRequest(request);
    Object result = jsonResponse.opt("result");
    //noinspection unchecked
    return (T) ParseSyncUtils.decodeObject(result);
  }

  public static Object decodeObject(Object o)
  {
    return ParseDecoder.get().decode(o);
  }

  @SuppressWarnings("unused")
  public static <X extends ParseObject> JSONObject encodeQuery(
    ParseQuery<X> query)
  {
    PointerEncoder encoder = PointerEncoder.get();
    return query.getBuilder().build().toJSON(encoder);
  }

  private static void dumpCopy(Request request, PrintString ps)
  {
    ps.println(dashed_line);
    ps.println("Request");
    ps.println(dashed_line);
    ps.println("Method:   " + request.method());
    ps.println("Url:      " + request.url());
    dumpHeaders("Headers:  ", request.headers(), ps);
    ps.println(dashed_line);
    RequestBody parseBody = request.body();
    if (parseBody != null) {
      assert parseBody instanceof JSONRequestBody;
      try {
        ps.println("CLength:  " + parseBody.contentLength());
      } catch (IOException ex) {
        Util.printStackTrace(ex);
      }
      ps.println("CType:    " + parseBody.contentType());
      String body = parseBody.toString();
      if(body.isEmpty()) {
        ps.printf("Body:     %s\n", "* Empty *");
      } else {
        String[] lines = body.split("\n");
        for (String line : lines) {
          ps.printf("Body:     %s\n", line);
        }
      }
    } else {
      ps.println("No Body");
    }
    ps.println(dashed_line);
  }

  public static OkHttpClient getFileClient()
  {
    return smFileClient.get();
  }

  public static ParseUser signUp(ParseUser user)
  {
    HashMap<String, Object> parameters = new HashMap<>(user.getEstimatedData());
    HashMap<String, Object> result = ParseCloud.run("signUp", parameters);
    if (Boolean.TRUE.equals(result.get("success"))) {
      user = (ParseUser) result.get("user");
      CurrentData.setCurrentUser(user);
      return CurrentData.getCurrentUser();
    } else {
      String message = (String) result.get("message");
      if (message == null) {
        message = "No Message";
      }
      throw new ParseException(ParseException.OTHER_CAUSE, message);
    }
  }

  @Nonnull
  private static String throwEx(final JSONObject jsonResponse)
  {
    String message = jsonResponse.optString("error", null);
    // Internal error server side, or something, we'll
    // flag that it might be worth trying again.
    if (message == null) {
      message = jsonResponse.optString("message", null);
    }
    if (message == null) {
      message = "No Message Supplied";
    }
    String temp = "Server-Side Error: "+message;
    int code = jsonResponse.optInt("code");
    throw new ParseException(code, temp, code < 500, null);
  }

  private static void dumpHeaders(String s, final Headers headers, final PrintString ps)
  {
    Map<String, List<String>> map = headers.toMultimap();
    String keyFmt = makeKeyFmt(map.keySet());
    String fmt = s+keyFmt+": %s\n";

    for (String key : map.keySet()) {
      List<String> values = map.get(key);
      if (values == null) {
        ps.printf(fmt, key,null);
      } else if (values.isEmpty()) {
        ps.printf(fmt, key,"*** NO VALUES ***");
      } else if (values.size()==1) {
        ps.printf(fmt, key,values.get(0));
      } else {
        ps.println(key+": ");
        for (String val : values) {
          ps.println("    " + val);
        }
      }
    }
  }

  private static String makeKeyFmt(Set<String> strings)
  {
    int keyLen=0;
    for (String key : strings) {
      keyLen=Math.max(keyLen,key.length());
    }
    keyLen+=4;
    return "%-"+keyLen+"s";
  }

  public static <T extends ParseObject> T fetch(T object)
  {
    assert !ThreadUtil.isMainThread();
    ParseQuery<T> query = ParseQuery.getQuery(object.getClassName());
    return query.get(object.getObjectId());
  }

  public static void post(Runnable runnable, ParseCallback1 callback)
  {
    if (callback == null) {
      ThreadUtil.onExec(runnable);
    } else {
      ThreadUtil.onExec(PostedOp.create(runnable, callback));
    }
  }

  public static int identityHashCode(ParseObject object)
  {
    return System.identityHashCode(object);
  }

  public static void saveFile(final ParseFile parseFile)
  {
//    assert !ThreadUtil.isMainThread();
//    ParseFile.State state = parseFile.getState();
//    ParseUser user = CurrentData.getCurrentUser();
//    byte[] fileData = parseFile.data;
//    File fileFile = parseFile.file;
//
//    ParseRESTCommand command;
//    if (fileData != null) {
//      command =
//        new ParseRESTFileCommand.Builder().fileName(state.name()).data(fileData)
//          .contentType(state.mimeType()).sessionToken(sessionToken).build();
//    } else if (fileFile != null) {
//      command =
//        new ParseRESTFileCommand.Builder().fileName(state.name()).file(fileFile)
//          .contentType(state.mimeType()).sessionToken(sessionToken).build();
//    } else {
//      throw new IllegalStateException("Either file or data must be non-null");
//    }
//    JSONObject result = executeRequest(command.newRequest());
//    ParseFile.State newState =
//      new ParseFile.State.Builder(state).name(result.getString("name"))
//        .url(result.getString("url")).build();
//    try {
//      ParseFileUtils.writeByteArrayToFile(getCacheFile(newState), fileData);
//    } catch (IOException e) {
//      System.out.println("Error saving ParseFile: " + e);
//    }
//    parseFile.state = newState;
  }

  public static void requestPasswordReset(String email)
  {
    ParseRESTCommand command =
      ParseRESTUserCommand.resetPasswordResetCommand(email);
    JSONObject result = executeRequest(command.newRequest());
    XLog.i(TAG, result.toString(2));
  }

  public static void delete(ParseObject object)
  {
    ParseObject.State state = object.getState();
    ParseRESTObjectCommand command =
      ParseRESTObjectCommand.deleteObjectCommand(state, sessionToken);
    executeRequest(command.newRequest());
  }

  public static byte[] getFileData(@Nonnull final ParseFile parseFile)
  {
    return IOUtil.fileToBytes(getFileFile(parseFile));
  }

  public static File getFileFile(final ParseFile parseFile)
  {
    throw new NotImplementedError();
  }

  public static InputStream getFileDataStream(
    @Nonnull final ParseFile parseFile)
  {
    return IOUtil.fileToStream(getFileFile(parseFile));
  }

  public static void dumpObjects(ParseObject oldObject, ParseObject newObject)
  {
    try (PrintString ps = new PrintString()) {
      if (oldObject == null) {
        ps.println("New Object: " + newObject);
        ps.println("Content: ");
        ps.println(newObject.toJSON().toString(200));
      } else if (newObject == null) {
        ps.println("Old Object: " + oldObject);
      } else {
        HashSet<String> keys = new HashSet<>(oldObject.availableKeys());
        keys.addAll(newObject.availableKeys());
        for (String key : keys) {
          boolean oldHas = oldObject.has(key);
          boolean newHas = newObject.has(key);
          if (oldHas && newHas) {
            Object oldValue = oldObject.get(key);
            Object newValue = newObject.get(key);
            if (oldValue == null && newValue == null)
              continue;
            if (oldValue != null && oldValue.equals(newValue))
              continue;
            ps.println("Key: " + key);
            ps.println("  O: " + oldValue);
            ps.println("  N: " + newValue);
          }
        }
      }
    }
  }

  /**
   * Fetches all the objects in the provided list.
   *
   * @param objects The list of objects to fetch.
   * @throws ParseException Throws an exception if the server returns an
   *                        error or is inaccessible.
   */
  public static <X extends ParseObject, C extends Collection<X>> void fetchAll(
    C objects)
  throws ParseException
  {
    HashMap<String, HashSet<String>> mSets = new HashMap<>();
    for (ParseObject t : objects) {
      if (t == null) {
        continue;
      }
      HashSet<String> set =
        mSets.computeIfAbsent(t.getClassName(), (className) -> new HashSet<>());
      set.add(t.getObjectId());
    }
    List<ParseObject> needed = new ArrayList<>();
    for (String className : mSets.keySet()) {
      HashSet<String> ids = mSets.get(className);
      ParseQuery<X> query = ParseQuery.getQuery(className);
      query.whereContainedIn("objectId", ids);
      List<X> batch = query.find();
    }
  }

  /**
   * Registers the Parse-provided {@code ParseObject} subclasses. Do this
   * here in a real method
   * rather than
   * as part of a static initializer because doing this in a static
   * initializer can lead to
   * deadlocks
   */

  public static void registerParseSubclasses()
  {
    registerSubclass(ParseUser.class);
    registerSubclass(ParseRole.class);
    registerSubclass(ParseInstallation.class);

  }

  public static <X extends ParseObject> void registerSubclass(
    Class<X> parseUserClass)
  {
    registerSubclass(new ClassTool<>(parseUserClass));
  }

  public static <T extends ParseObject> void registerSubclass(
    ClassTool<T> classTool)
  {
    Class<T> type = classTool.getType();
    Supplier<T> supplier = classTool.getSupplier();

    if (!ParseObject.class.isAssignableFrom(type)) {
      throw new IllegalArgumentException(
        "Cannot register a type that is not a subclass of ParseObject");
    }

    String className = getClassName(type);
    ClassTool<? extends ParseObject> prevClassTool;
    synchronized (mutex) {
      prevClassTool = smByName.get(className);
      if (prevClassTool != null) {
        Class<? extends ParseObject> prevType = prevClassTool.getType();
        if (type.isAssignableFrom(prevType)) {
          return;
        }
      }
      smByName.put(className, classTool);
    }

    if (prevClassTool != null) {
      // TODO: This is super tightly coupled. Let's remove it when automatic
      //  registration is
      // in.
      // NOTE: Perform this outside of the mutex, to prevent any potential
      // deadlocks.
      if (className.equals(getClassName(ParseUser.class))) {
        CurrentData.saveAndLoad();
      } else if (className.equals(getClassName(ParseInstallation.class))) {
        CurrentData.saveAndLoad();
      }
    }
  }

  public static void unregisterParseSubclasses()
  {
    unregisterSubclass(ParseUser.class);
    unregisterSubclass(ParseRole.class);
    unregisterSubclass(ParseInstallation.class);
  }

  public static void unregisterSubclass(Class<? extends ParseObject> clazz)
  {
    String className = getClassName(clazz);

    synchronized (mutex) {
      smByName.remove(className);
    }
  }

  static public String format(Date date)
  {
    if (date == null) {
      return "null";
    } else {
      return smParseDateFormat.format(date);
    }
  }


  public static <T extends ParseObject> T fromJSON(JSONObject json,
                                                   ParseQuery.State<T> state,
                                                   ParseDecoder decoder)
  {
    if (decoder == null) {
      decoder = ParseDecoder.get();
    }
    String className = null;
    Set<String> keys = null;
    if (state != null) {
      keys = state.selectedKeys();
      className = state.className();
    }
    T po = ParseObject.fromJSON(json, className, decoder, keys);
    if(Util.theGovernmentIsHonest())
      System.out.println(po);
//    Pair<File,OutputStream> p = ConfigDepot.getNextLogFile("psu",true);
//    IOUtil.stringToStream(p.mVal2, po.toString());
    return po;
  }

  public static void checkLogin()
  {
    run("checkLogin",null);
  }

  public static class PostedOp
    implements Runnable
  {
    Runnable mRunnable;
    Runnable mCallback;
    ParseException mException;
    RuntimeException mCreated = new RuntimeException("created here");
    int mState = 0;

    public PostedOp()
    {
      Reflect.announce("building PostedOp");
    }

    static <V> PostedOp create(@Nonnull Callable<V> callable,
                               @Nullable ParseCallback2<V> callback2)
    {
      PostedOp op = new PostedOp();
      ArrayList<V> result = new ArrayList<>();
      result.add(null);
      Runnable runnable = () ->
      {
        try {
          result.set(0, callable.call());
        } catch (Throwable t) {
          op.mException = ParseException.wrap(t);
        }
      };
      Runnable callback = null;
      if (callback2 != null) {
        callback = () -> callback2.done(result.get(0), op.mException);
      }

      PostedOp op1 = new PostedOp();
      op1.mRunnable = runnable;
      op1.mCallback = callback;
      return op1;
    }

    public static <X> PostedOp create(@Nonnull Callable<X> callable,
                                      @Nullable ParseCallback1 callback)
    {
      PostedOp op = new PostedOp();
      op.mRunnable = () ->
      {
        try {
          callable.call();
        } catch (Throwable t) {
          op.mException = ParseException.wrap(t);
        }
      };
      op.mCallback =
        callback == null ? null : () -> callback.done(op.mException);
      return op;
    }

    public static PostedOp create(@Nonnull Runnable runnable,
                                  @Nullable ParseCallback1 callback1)
    {
      PostedOp op = new PostedOp();
      op.mRunnable = () ->
      {
        try {
          runnable.run();
        } catch (Throwable t) {
          op.mException = ParseException.wrap(t);
        }
      };
      if (callback1 != null) {
        op.mCallback = () -> callback1.done(op.mException);
      }
      return op;
    }

    @Override
    public void run()
    {
      switch (mState++) {
        case 0:
          assert !ThreadUtil.isMainThread();
          mRunnable.run();
          if (mCallback == null) {
            return;
          }
          Throwable exception = mException;
          Throwable created = mCreated;

          Util.pushCause(exception, created);
          ThreadUtil.onMain(this);
          break;
        case 1:
          assert ThreadUtil.isMainThread();
          mCallback.run();
          break;
        default:
          throw new IllegalStateException(
            "PostedOp called " + mState + " times");
      }
    }
  }
}
