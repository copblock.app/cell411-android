/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.http;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * The http request we send to com.parse server. Instances of this class are not immutable. The
 * request body may be consumed only once. The other fields are immutable.
 */
@SuppressWarnings("unused")
public final class ParseHttpRequest {


  private final URL url;
  private final ParseHttpMethod method;
  private final Map<String, String> headers;
  private final RequestBody body;
  public final StackTraceElement[] mStack=new Throwable().getStackTrace();

  ParseHttpRequest(Builder builder)
  {
    this.url = builder.mURL;
    this.method = builder.mMethod;
    this.headers = Collections.unmodifiableMap(new HashMap<>(builder.mHeaders));
    this.body = builder.mBody;
  }

  @Nonnull
  public Request getOkRequest()
  {
    Request.Builder builder = new Request.Builder();
    ParseHttpMethod method = getMethod();
    // Set method
    switch (method) {
      case GET:
        builder.get();
        break;
      case DELETE:
      case POST:
      case PUT:
        // Since we need to set body and method at the same time for DELETE,
        // POST, PUT, we will
        // do it in
        // the following.
        break;
      default:
        // This case will never be reached since we have already handled this
        // case in
        // ParseRequest.newRequest().
        throw new IllegalStateException("Unsupported http method " + method);
    }
    // Set url
    builder.url(this.getUrl());
    builder.headers(getHeaders());

    // Set Body
    RequestBody parseBody = this.getBody();
    switch (method) {
      case PUT:
        assert parseBody != null;
        builder.put(parseBody);
        break;
      case POST:
        assert parseBody != null;
        builder.post(parseBody);
        break;
      case DELETE:
        builder.delete(parseBody);
    }
    return builder.build();
  }

  @Nonnull
  private Headers getHeaders()
  {
    // Set Header
    Headers.Builder builder = new Headers.Builder();
    for (Map.Entry<String, String> entry : this.getAllHeaders().entrySet()) {
      builder.add(entry.getKey(), entry.getValue());
    }
    return builder.build();
  }

  //    String.valueOf(ManifestInfo.getVersionCode()))
  //      ManifestInfo.getVersionName())

  /**
   * Gets the url of this {@code ParseHttpRequest}.
   *
   * @return The url of this {@code ParseHttpRequest}.
   */
  public URL getUrl() {
    return url;
  }

  /**
   * Gets the {@code Method} of this {@code ParseHttpRequest}.
   *
   * @return The {@code Method} of this {@code ParseHttpRequest}.
   */
  public ParseHttpMethod getMethod() {
    return method;
  }

  /**
   * Gets all headers from this {@code ParseHttpRequest}.
   *
   * @return The headers of this {@code ParseHttpRequest}.
   */
  public Map<String, String> getAllHeaders() {
    return headers;
  }

  /**
   * Retrieves the header value from this {@code ParseHttpRequest} by the given header name.
   *
   * @param name The name of the header.
   * @return The value of the header.
   */
  public String getHeader(String name) {
    return headers.get(name);
  }

  /**
   * Gets http body of this {@code ParseHttpRequest}.
   *
   * @return The http body of this {@code ParseHttpRequest}.
   */
  public RequestBody getBody() {
    return body;
  }

  /**
   * Builder of {@code ParseHttpRequest}.
   */
  public static final class Builder {

    private URL mURL;
    private ParseHttpMethod mMethod;
    private Map<String, String> mHeaders = new HashMap<>();
    private RequestBody mBody;

    /**
     * Creates an empty {@code Builder}.
     */
    public Builder() {
      this.mHeaders = new HashMap<>();
    }

    /**
     * Creates a new {@code Builder} based on the given {@code ParseHttpRequest}.
     *
     * @param request The {@code ParseHttpRequest} where the {@code Builder}'s values come from.
     */
    public Builder(ParseHttpRequest request) {
      this.mHeaders = new HashMap<>(request.headers);
      this.mURL = request.url;
      this.mMethod = request.method;
      this.mBody = request.body;
    }

    public Builder(ParseHttpMethod mMethod, URL url)
    {
      this.mMethod = mMethod;
      this.mURL =url;
    }

    /**
     * Sets the mURL of this {@code Builder}.
     *
     * @param url The mURL of this {@code Builder}.
     * @return This {@code Builder}.
     */
    public Builder setmURL(URL url) {
      mURL = url;
      return this;
    }

    /**
     * Sets the {@link ParseHttpMethod} of this {@code Builder}.
     *
     * @param method The {@link ParseHttpMethod} of this {@code Builder}.
     * @return This {@code Builder}.
     */
    public Builder setmMethod(ParseHttpMethod method) {
      mMethod = method;
      return this;
    }

    /**
     * Sets the {@link RequestBody} of this {@code Builder}.
     *
     * @param body The {@link RequestBody} of this {@code Builder}.
     * @return This {@code Builder}.
     */
    public Builder setBody(RequestBody body) {
      this.mBody = body;
      return this;
    }

    /**
     * Adds a header to this {@code Builder}.
     *
     * @param name  The name of the header.
     * @param value The value of the header.
     * @return This {@code Builder}.
     */
    public Builder addHeader(String name, String value) {
      mHeaders.put(name, value);
      return this;
    }

    /**
     * Adds headers to this {@code Builder}.
     *
     * @param headers The headers that need to be added.
     * @return This {@code Builder}.
     */
    public Builder addHeaders(Map<String, String> headers) {
      this.mHeaders.putAll(headers);
      return this;
    }

//    /**
//     * Sets headers of this {@code Builder}. All existing headers will be cleared.
//     *
//     * @param headers The headers of this {@code Builder}.
//     * @return This {@code Builder}.
//     */
//    public Builder setHeaders(Map<String, String> headers) {
//      this.headers = new HashMap<>(headers);
//      return this;
//    }

    /**
     * Builds a {@link ParseHttpRequest} based on this {@code Builder}.
     *
     * @return A {@link ParseHttpRequest} built on this {@code Builder}.
     */
    public ParseHttpRequest build() {
      return new ParseHttpRequest(this);
    }
  }
}

