package com.parse.utils;

import com.parse.decoder.ParseDecoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cell411.json.JSONArray;
import cell411.json.JSONObject;

public class ParseJSON
{

  public static List<Object> convertJSONArrayToList(JSONArray array)
  {
    ParseDecoder d = ParseDecoder.get();
    List<Object> list = new ArrayList<>();
    for (int i = 0; i < array.length(); ++i) {
      list.add(d.decode(array.opt(i)));
    }
    return list;
  }

  /* package */
  public static Map<String, Object> convertJSONObjectToMap(JSONObject object)
  {
    ParseDecoder d = ParseDecoder.get();
    Map<String, Object> outputMap = new HashMap<>();
    Iterator<String> it = object.keys();
    while (it.hasNext()) {
      String key = it.next();
      Object value = object.opt(key);
      outputMap.put(key, d.decode(value));
    }
    return outputMap;
  }
}
