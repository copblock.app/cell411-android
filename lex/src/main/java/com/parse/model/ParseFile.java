/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.model;

import com.parse.ParseException;
import com.parse.callback.SaveCallback;
import com.parse.decoder.ParseDecoder;
import com.parse.http.ParseSyncUtils;

import java.io.File;
import java.io.InputStream;

import cell411.json.JSONException;
import cell411.json.JSONObject;
import cell411.config.ConfigDepot;
import cell411.utils.reflect.Reflect;

/**
 * {@code ParseFile} is a local representation of a file that is saved to the Parse cloud.
 * <p/>
 * The workflow is to construct a {@code ParseFile} with data and optionally a filename. Then save
 * it and set it as a field on a {@link ParseObject}.
 * <p/>
 * Example:
 * <pre>
 * ParseFile file = new ParseFile("hello".getBytes());
 * file.save();
 *
 * ParseObject object = new ParseObject("TestObject");
 * object.put("file", file);
 * object.save();
 * </pre>
 */
public class ParseFile {

  //  public final static Creator<ParseFile> CREATOR = new Creator<ParseFile>() {
  //    @Override
  //    public ParseFile createFromParcel(Parcel source) {
  //      return new ParseFile(source);
  //    }
  //
  //    @Override
  //    public ParseFile[] newArray(int size) {
  //      return new ParseFile[size];
  //    }
  //  };
  //  /* package for tests */ final TaskQueue taskQueue = new TaskQueue();
  //  private final Set<TaskCompletionSource<?>> currentTasks = Collections.synchronizedSet(
  //    new HashSet<>());
  /**
   * Staging of {@code ParseFile}'s data is stored in memory until the {@code ParseFile} has been
   * successfully synced with the server.
   */
  /* package for tests */ public byte[] data;
  /* package for tests */ public File file;
  public State state;
  File cachePath;

  /**
   * Creates a new file from a file pointer.
   *
   * @param file The file.
   */
  public ParseFile(File file) {
    this(file, null);
  }

  /**
   * Creates a new file from a file pointer, and content type. Content type will be used instead of
   * auto-detection by file extension.
   *
   * @param file        The file.
   * @param contentType The file's content type.
   */
  public ParseFile(File file, String contentType) {
    this(new State.Builder().name(file.getName()).mimeType(contentType).build());
    this.file = file;
  }

  /**
   * Creates a new file from a byte array, file name, and content type. Content type will be used
   * instead of auto-detection by file extension.
   *
   * @param name        The file's name, ideally with extension. The file name must begin with an
   *                    alphanumeric
   *                    character, and consist of alphanumeric characters, periods, spaces,
   *                    underscores, or
   *                    dashes.
   * @param data        The file's data.
   * @param contentType The file's content type.
   */
  public ParseFile(String name, byte[] data, String contentType) {
    this(new State.Builder().name(name).mimeType(contentType).build());
    this.data = data;
  }

  /**
   * Creates a new file from a byte array.
   *
   * @param data The file's data.
   */
  public ParseFile(byte[] data) {
    this(null, data, null);
  }

  /**
   * Creates a new file from a byte array and a name. Giving a name with a proper file extension
   * (e.g. ".png") is ideal because it allows Parse to deduce the content type of the file and set
   * appropriate HTTP headers when it is fetched.
   *
   * @param name The file's name, ideally with extension. The file name must begin with an
   *             alphanumeric
   *             character, and consist of alphanumeric characters, periods, spaces, underscores, or
   *             dashes.
   * @param data The file's data.
   */
  public ParseFile(String name, byte[] data) {
    this(name, data, null);
  }

  /**
   * Creates a new file from a byte array, and content type. Content type will be used instead of
   * auto-detection by file extension.
   *
   * @param data        The file's data.
   * @param contentType The file's content type.
   */
  public ParseFile(byte[] data, String contentType) {
    this(null, data, contentType);
  }

  public ParseFile(State state) {
    this.state = state;
  }

  /*
   * Encode/Decode
   */
  @SuppressWarnings("unused")
  public ParseFile(JSONObject json, ParseDecoder decoder) {
    this(new State.Builder().name(json.optString("name")).url(json.optString("url")).build());
  }

  public File getTempFile() {
    return getTempFile(state);
  }

  private File getTempFile(final State state) {
    if (cachePath == null) {
      cachePath = ConfigDepot.getTempDir();
     // "temp");
    }
    if (state.url() == null) {
      return null;
    }
    return new File(cachePath, state.name() + ".tmp");
  }

  /* package for tests */
  public State getState() {
    return state;
  }

  /**
   * The filename. Before save is called, this is just the filename given by the user (if any).
   * After save is called, that name gets prefixed with a unique identifier.
   *
   * @return The file's name.
   */
  public String getName() {
    return state.name();
  }

  /**
   * Whether the file still needs to be saved.
   *
   * @return Whether the file needs to be saved.
   */
  public boolean isDirty() {
    return state.url() == null;
  }

  /**
   * Whether the file has available data.
   */
  public boolean isDataAvailable() {
    return data != null; // || getFileController().isDataAvailable(state);
  }

  /**
   * This returns the url of the file. It's only available after you save or after you get the file
   * from a ParseObject.
   *
   * @return The url of the file.
   */
  public String getUrl() {
    return state.url();
  }

  /**
   * Saves the file to the Parse cloud synchronously.
   */
  public void save() throws ParseException {
    ParseSyncUtils.saveFile(this);
  }

  /**
   * Saves the file to the Parse cloud in a background thread.
   *
   * @param callback A SaveCallback that gets called when the save completes.
   */
  public void saveInBackground(SaveCallback callback) {
    ParseSyncUtils.post(new Saver(), callback);
  }

  /**
   * Synchronously gets the data from cache if available or fetches its content from the network.
   * You probably want to use {@link # getDataInBackground()} instead unless you're already in a
   * background thread.
   */
  public byte[] getData() throws ParseException {
    if (data == null) {
      data = ParseSyncUtils.getFileData(this);
      assert data != null;
    }
    return data;
  }

  /**
   * Synchronously gets the file pointer from cache if available or fetches its content from the
   * network. You probably want to use {@link # getFileInBackground()} instead unless you're already
   * in a background thread.
   * <strong>Note: </strong> The {@link File} location may change without notice and should not be
   * stored to be accessed later.
   */
  public File getFile() throws ParseException {
    return ParseSyncUtils.getFileFile(this);
  }

  /**
   * Synchronously gets the data stream from cached file if available or fetches its content from
   * the network, saves the content as cached file and returns the data stream of the cached file.
   */
  public InputStream getDataStream() throws ParseException {
    return ParseSyncUtils.getFileDataStream(this);
  }

  /* package */
  public JSONObject encode() throws JSONException {
    JSONObject json = new JSONObject();
    json.put("__type", "File");
    json.put("name", getName());

    String url = getUrl();
    if (url == null) {
      throw new IllegalStateException("Unable to encode an unsaved ParseFile.");
    }
    json.put("url", getUrl());

    return json;
  }

  public static class State {

    private final String name;
    private final String contentType;
    private final String url;

    private State(Builder builder) {
      name = builder.name != null ? builder.name : "file";
      contentType = builder.mimeType;
      url = builder.url;
    }

    public String name() {
      return name;
    }

    public String mimeType() {
      return contentType;
    }

    public String url() {
      return url;
    }

    /* package */ public static class Builder {

      private String name;
      private String mimeType;
      private String url;

      public Builder() {
        // do nothing
      }

      public Builder(State state) {
        name = state.name();
        mimeType = state.mimeType();
        url = state.url();
      }

      public Builder name(String name) {
        this.name = name;
        return this;
      }

      public Builder mimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
      }

      public Builder url(String url) {
        this.url = url;
        return this;
      }

      public State build() {
        return new State(this);
      }
    }
  }

  class Saver implements Runnable {
    Saver() {
    }

    public void run() {
      long startTime = System.currentTimeMillis();
      Reflect.announce("saving");
      save();
      Reflect.announce("saved: " + (System.currentTimeMillis() - startTime));
    }
  }
}

