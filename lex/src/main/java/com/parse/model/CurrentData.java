package com.parse.model;

import static cell411.config.ConfigDepot.getParseDir;

import com.parse.Parse;
import com.parse.encoder.ParseObjectCurrentCoder;
import com.parse.encoder.ParseUserCurrentCoder;
import com.parse.http.ParseSyncUtils;
import com.parse.offline.FileObjectStore;
import com.parse.offline.InstallationId;

import java.io.File;
import java.util.ArrayList;

import cell411.utils.collect.ValueObserver;

public class CurrentData
{
  public static final String FILENAME_CURRENT_USER = "currentUser";
  public static final String FILENAME_CURRENT_INSTALLATION =
    "currentInstallation";
  public static final String FILENAME_CURRENT_CONFIG = "currentConfig";
  public static final String INSTALLATION_ID_LOCATION = "installationId";
  private static final Data mData = new Data();
  private static final ObserverList mCurrentUserObservers = new ObserverList();

  public static InstallationId getInstallationId()
  {
    return mData.getInstallationId();
  }

  public static ParseInstallation getCurrentInstallation()
  {
    return mData.mCurrentInstallationStore.get();
  }

  public static void setCurrentInstallation(ParseInstallation installation)
  {
    mData.setCurrentInstallation(installation);
  }

  public static void removeCurrentUserObserver(
    ValueObserver<ParseUser> observer)
  {
    mData.removeCurrentUserObserver(observer);
  }

  public static void logOut()
  {
    mData.logOut();
  }

  public static <T extends ParseObject> boolean isCurrent(T t)
  {
    return mData.isCurrent(t);
  }

  public static ParseUser getCurrentUser()
  {
    return mData.getCurrentUserStore().get();
  }

  public static void setCurrentUser(ParseUser user)
  {
    Parse.checkInit();
    mData.getCurrentUserStore().set(user);
  }

  public static void saveAndLoad()
  {
    mData.getCurrentUserStore().save();
    saveCurrentInstallation();
    loadInitialData();
  }

  public static void saveCurrentInstallation()
  {
    mData.getCurrentInstallationStore().save();
  }

  public static void loadInitialData()
  {
    mData.loadInitialData();
  }

  public static void addCurrentUserObservers()
  {
    for (ValueObserver<? super ParseUser> observer : mCurrentUserObservers) {
      addCurrentUserObserver(observer);
    }
  }

  public static void addCurrentUserObserver(
    ValueObserver<? super ParseUser> observer)
  {
    mData.addCurrentUserObserver(observer);
  }

  static class ObserverList
    extends ArrayList<ValueObserver<? super ParseUser>>
  {

  }

  static class Data
  {
    private FileObjectStore<ParseUser> mCurrentUserStore;
    private FileObjectStore<ParseInstallation> mCurrentInstallationStore;
    private InstallationId installationId;

    public InstallationId getInstallationId()
    {
      if (installationId == null) {
        installationId =
          new InstallationId(new File(getParseDir(), INSTALLATION_ID_LOCATION));
      }
      return installationId;
    }

    public void setCurrentInstallation(ParseInstallation installation)
    {
      mCurrentInstallationStore.set(installation);
    }

    public void logOut()
    {
      // Anybody caching objects needs to be observing this value
      // and clearing their caches when it changes, unless they don't
      // mind the next user inheriting it.
      getCurrentUserStore().set(null);
      getCurrentInstallationStore().set(null);
    }

    public FileObjectStore<ParseUser> getCurrentUserStore()
    {
      if (mCurrentUserStore == null) {
        mCurrentUserStore = createCurrentUserStore();
      }
      return mCurrentUserStore;
    }

    private static FileObjectStore<ParseUser> createCurrentUserStore()
    {
      FileObjectStore<ParseUser> res;
      ParseUserCurrentCoder coder = ParseUserCurrentCoder.get();
      File file = new File(getParseDir(), FILENAME_CURRENT_USER);
      res = new FileObjectStore<>(ParseUser.class, file, coder);
      res.setAlwaysFire(true);
      return res;
    }

    public void loadInitialData()
    {
      getCurrentInstallationStore().load();
      getCurrentUserStore().load();
      if(getCurrentUser()!=null) {
        ParseSyncUtils.checkLogin();
      }
    }

    // We initialize this lazily, but we only try to read the disk when Parse
    // calls init as it gets initialized.  That way, you can put an observer on
    // the value before we are open for business, and find out as soon as we
    // open up.
    public FileObjectStore<ParseInstallation> getCurrentInstallationStore()
    {
      if (mCurrentInstallationStore == null) {
        mCurrentInstallationStore = createCurrentInstallationStore();
      }
      return mCurrentInstallationStore;
    }

    private FileObjectStore<ParseInstallation> createCurrentInstallationStore()
    {
      FileObjectStore<ParseInstallation> res;
      File file = new File(getParseDir(), FILENAME_CURRENT_INSTALLATION);
      ParseObjectCurrentCoder coder = ParseObjectCurrentCoder.get();
      res = new FileObjectStore<>(ParseInstallation.class, file, coder);
//      System.out.println("initialize from: " + file);
      return res;
    }

    public <T extends ParseObject> boolean isCurrent(T t)
    {
      if (t == null) {
        throw new NullPointerException("Don't give me null");
      } else {
        Class<? extends ParseObject> aClass = t.getClass();
        if (ParseUser.class.isAssignableFrom(aClass)) {
          return getCurrentUserStore().get().hasSameId(t);
        } else if (ParseInstallation.class.isAssignableFrom(aClass)) {
          return getCurrentInstallation().hasSameId(t);
        } else {
          throw new IllegalArgumentException(
            "I don't know what you're saying.  Who sent you?");
        }
      }
    }

    public void addCurrentUserObserver(
      ValueObserver<? super ParseUser> observer)
    {
      if(Parse.isInitialized()) {
        ValueObserver<? super ParseUser> obs;
        while(!mCurrentUserObservers.isEmpty()) {
          obs=mCurrentUserObservers.remove(0);
          mCurrentUserStore.addObserver(obs);
        }
        mCurrentUserStore.addObserver(observer);
      } else {
        mCurrentUserObservers.add(observer);
      }
    }

    public void removeCurrentUserObserver(ValueObserver<ParseUser> observer)
    {
      if(Parse.isInitialized())
        mCurrentUserStore.removeObserver(observer);
    }
  }

}
