/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.model;

import android.text.TextUtils;
import cell411.config.ConfigDepot;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.offline.InstallationId;

import java.util.*;

/**
 * The {@code ParseInstallation} is a local representation of installation data that can be saved
 * and retrieved from the Parse cloud.
 */
@ParseClassName("_Installation")
public class ParseInstallation extends ParseObject {
  /* package */ public static final String KEY_CHANNELS = "channels";
  private static final String TAG = "com.parse.model.ParseInstallation";
  private static final String KEY_OBJECT_ID = "objectId";
  private static final String KEY_INSTALLATION_ID = "installationId";
  private static final String KEY_DEVICE_TYPE = "deviceType";
  private static final String KEY_APP_NAME = "appName";
  private static final String KEY_APP_IDENTIFIER = "appIdentifier";
  private static final String KEY_PARSE_VERSION = "parseVersion";
  private static final String KEY_DEVICE_TOKEN = "deviceToken";
  private static final String KEY_PUSH_TYPE = "pushType";
  private static final String KEY_TIME_ZONE = "timeZone";
  private static final String KEY_LOCALE = "localeIdentifier";
  private static final String KEY_APP_VERSION = "appVersion";
  private static final List<String> READ_ONLY_FIELDS = Collections.unmodifiableList(
    Arrays.asList(KEY_DEVICE_TYPE, KEY_INSTALLATION_ID, KEY_DEVICE_TOKEN, KEY_PUSH_TYPE,
      KEY_TIME_ZONE, KEY_LOCALE, KEY_APP_VERSION, KEY_APP_NAME, KEY_PARSE_VERSION,
      KEY_APP_IDENTIFIER, KEY_OBJECT_ID));

  public ParseInstallation() {
    // do nothing
  }

  /**
   * Constructs a query for {@code ParseInstallation}.
   * <p/>
   * <strong>Note:</strong> We only allow the following types of queries for installations:
   * <pre>
   * query.get(objectId)
   * query.whereEqualTo("installationId", value)
   * query.whereMatchesKeyInQuery("installationId", keyInQuery, query)
   * </pre>
   * <p/>
   * You can add additional query clauses, but one of the above must appear as a top-level
   * {@code AND} clause in the query.
   *
   * @see ParseQuery#getQuery(Class)
   */
  public static ParseQuery<ParseInstallation> getQuery() {
    return ParseQuery.getQuery(ParseInstallation.class);
  }

  @Override
  public void setObjectId(String newObjectId) {
    throw new RuntimeException("Installation's objectId cannot be changed");
  }

  @Override
  public void updateBeforeSave() {
    super.updateBeforeSave();
    if (CurrentData.isCurrent(this)) {
      updateTimezone();
      updateVersionInfo();
      updateDeviceInfo(CurrentData.getInstallationId());
      updateLocaleIdentifier();
    }
  }

  @Override
  public boolean isKeyMutable(String key) {
    return !READ_ONLY_FIELDS.contains(key);
  }

  @Override
  public boolean needsDefaultACL() {
    return false;
  }

  @Override
  public void afterSave()
  {
    super.afterSave();
    if(CurrentData.isCurrent(this)){
      CurrentData.saveAndLoad();
    }
  }

  // Android documentation states that getID may return one of many forms: America/LosAngeles,
  // GMT-<offset>, or code. We only accept the first on the server, so for now we will not upload
  // time zones from devices reporting other formats.
  private void updateTimezone() {
    String zone = TimeZone.getDefault().getID();
    if ((zone.indexOf('/') > 0 || zone.equals("GMT")) && !zone.equals(get(KEY_TIME_ZONE))) {
      performPut(KEY_TIME_ZONE, zone);
    }
  }

  private void updateVersionInfo() {
    synchronized (mutex) {
        String appName = ConfigDepot.getAppName();
        String appVersion = ConfigDepot.getAppVersion();
        String versionCode = ""+ConfigDepot.getVersionCode();
        String versionName = ConfigDepot.getVersionName();

        String packageName =
          ConfigDepot.getPackageName();
        if (packageName != null && !packageName.equals(get(KEY_APP_IDENTIFIER))) {
          performPut(KEY_APP_IDENTIFIER, packageName);
        }
        if (!appName.equals(get(KEY_APP_NAME))) {
          performPut(KEY_APP_NAME, appName);
        }
        if (!appVersion.equals(get(KEY_APP_VERSION))) {
          performPut(KEY_APP_VERSION, appVersion);
        }
    }
  }

  /*
   * Save locale in the following format:
   *   [language code]-[country code]
   *
   * The language codes are two-letter lowercase ISO language codes (such as "en") as defined by
   * <a href="http://en.wikipedia.org/wiki/ISO_639-1">ISO 639-1</a>.
   * The country codes are two-letter uppercase ISO country codes (such as "US") as defined by
   * <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3">ISO 3166-1</a>.
   *
   * Note that Java uses several deprecated two-letter codes. The Hebrew ("he") language
   * code is rewritten as "iw", Indonesian ("id") as "in", and Yiddish ("yi") as "ji". This
   * rewriting happens even if you construct your own {@code Locale} object, not just for
   * instances returned by the various lookup methods.
   */
  private void updateLocaleIdentifier() {
    final Locale locale = Locale.getDefault();

    String language = locale.getLanguage();
    String country = locale.getCountry();

    if (TextUtils.isEmpty(language)) {
      return;
    }

    // rewrite depreciated two-letter codes
    if (language.equals("iw")) {
      language = "he"; // Hebrew
    }
    if (language.equals("in")) {
      language = "id"; // Indonesian
    }
    if (language.equals("ji")) {
      language = "yi"; // Yiddish
    }

    String localeString = language;

    if (!TextUtils.isEmpty(country)) {
      localeString = String.format(Locale.US, "%s-%s", language, country);
    }

    if (!localeString.equals(get(KEY_LOCALE))) {
      performPut(KEY_LOCALE, localeString);
    }
  }

  /* package */
  public void updateDeviceInfo(InstallationId installationId) {
    /*
     * If we don't have an installationId, use the one that comes from the installationId file on
     * disk. This should be impossible since we set the installationId in setDefaultValues.
     */
    if (!has(KEY_INSTALLATION_ID)) {
      performPut(KEY_INSTALLATION_ID, installationId.get());
    }
    String deviceType = "android";
    if (!deviceType.equals(get(KEY_DEVICE_TYPE))) {
      performPut(KEY_DEVICE_TYPE, deviceType);
    }
  }

}

