package com.parse.model;

import static com.parse.http.ParseSyncUtils.newInstance;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParsePolygon;
import com.parse.ParseQuery;
import com.parse.callback.DeleteCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.ParseMulticastDelegate;
import com.parse.controller.LocalIdManager;
import com.parse.controller.ParseCorePlugins;
import com.parse.decoder.ParseDecoder;
import com.parse.encoder.FullObjectCoder;
import com.parse.encoder.ParseEncoder;
import com.parse.encoder.ParseObjectCoder;
import com.parse.encoder.PointerEncoder;
import com.parse.encoder.PointerOrLocalIdEncoder;
import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseObject.State.Init;
import com.parse.operation.ParseAddOperation;
import com.parse.operation.ParseAddUniqueOperation;
import com.parse.operation.ParseDeleteOperation;
import com.parse.operation.ParseFieldOperation;
import com.parse.operation.ParseIncrementOperation;
import com.parse.operation.ParseOperationSet;
import com.parse.operation.ParseRemoveOperation;
import com.parse.operation.ParseSetOperation;
import com.parse.rest.ParseRESTObjectCommand;
import com.parse.utils.ParseDateFormat;
import com.parse.utils.ParseJSON;
import com.parse.utils.ParseTextUtils;
import com.parse.utils.ParseTraverser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.json.JSONArray;
import cell411.json.JSONException;
import cell411.json.JSONObject;
import cell411.logic.ObjectCache;
import cell411.utils.Util;
import cell411.utils.collect.Capture;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

@SuppressWarnings({"unused", "WeakerAccess"})
public class ParseObject {
  /*
  REST JSON Keys
  */
  public static final String KEY_OBJECT_ID = "objectId";
  public static final String KEY_CREATED_AT = "createdAt";
  public static final String KEY_UPDATED_AT = "updatedAt";
  public static final String KEY_IS_DELETING_EVENTUALLY = "__isDeletingEventually";
  private static final String AUTO_CLASS_NAME = "_Automatic";
  private static final XTAG TAG = new XTAG();
  private static final String KEY_CLASS_NAME = "className";
  private static final String ALT_CLASS_NAME = "__className";
  private static final String KEY_ACL = "ACL";
  /*
  Internal JSON Keys - Used to store internal data when persisting {@code ParseObject}s locally.
  */
  private static final String KEY_COMPLETE = "__complete";
  private static final String KEY_OPERATIONS = "__operations";
  // Array of keys selected when querying for the object. Helps to decode
  // nested {@code ParseObject}s correctly, and helps to construct the
  // {@code State.availableKeys()} set.

  private static final String KEY_SELECTED_KEYS = "__selectedKeys";
  // Because Grantland messed up naming this... We'll only try to read from this for backward
  // compat, but I think we can be safe to assume any deleteEventually from long ago are obsolete
  // and not check after a while
  private static final String KEY_IS_DELETING_EVENTUALLY_OLD = "isDeletingEventually";
  private static final ThreadLocal<String> isCreatingPointerForObjectId =
    ThreadLocal.withInitial(() -> null);
  /*
   * This is used only so that we can pass it to createWithoutData as the objectId to make it create
   * an un-fetched pointer that has no objectId. This is useful only in the context of the offline
   * store, where you can have an un-fetched pointer for an object that can later be fetched from
   * the store.
   */
  private static final String NEW_OFFLINE_OBJECT_ID_PLACEHOLDER = "*** Offline Object ***";
  public static final ObjectCache smCache = new ObjectCache();
  static Comparator<String> smLenCompare = Comparator.comparing(String::length);
  static Comparator<String> smNoCaseCompare = String::compareToIgnoreCase;
  static Comparator<String> smKeyCompare = smLenCompare.thenComparing(smNoCaseCompare);
  static Comparator<ParseObject> smObjCompare =
    Comparator.comparing(ParseObject::getObjectId, smKeyCompare);
  public final Object mutex = new Object();
  public final LinkedList<ParseOperationSet> operationSetQueue = createOperationSetQueue();
  // Cached State
  private final TreeMap<String, Object> estimatedData = createEstimatedData();
  private final ParseMulticastDelegate<ParseObject> saveEvent = new ParseMulticastDelegate<>();
  String localId;
  boolean isDeleted;
  boolean isDeleting;
  // Since delete ops are queued, we don't need a counter.
  //TODO (grantland): Derive this off the EventuallyPins as opposed to +/- count.
  int isDeletingEventually;

  private State state;

  /**
   * The base class constructor to call in subclasses. Uses the class name specified with the
   * {@link ParseClassName} annotation on the subclass.
   */
  protected ParseObject() {
    this(AUTO_CLASS_NAME);
  }

  private static LocalIdManager getLocalIdManager() {
    return ParseCorePlugins.get().getLocalIdManager();
  }

  //  /* *
//   * Creates a new {@code ParseObject} based upon a subclass type. Note that the object will be
//   * created based upon the {@link ParseClassName} of the given subclass type. For example, calling
//   * create(ParseUser.class) may create an instance of a custom subclass of {@code ParseUser}.
//   *
//   * @param subclass The class of object to create.
//   * @return A new {@code ParseObject} based upon the class name of the given subclass type.
//   */
//  public static <T extends ParseObject> T create(Class<T> subclass) {
//    return subclass.cast(create(ParseSyncUtils.getTableForClass(subclass)));
//  }

  public static ParseObject createWithoutData(String className, String objectId) {
    try {
      ParseObject object;
      // FIXME:  I think this ugly shit can be removed.
      if (objectId == null) {
        assert !Util.theGovernmentIsLying();
        isCreatingPointerForObjectId.set(NEW_OFFLINE_OBJECT_ID_PLACEHOLDER);
        object = newInstance(className);
        if (object.hasChanges()) {
          throw new IllegalStateException(
            "A ParseObject subclass default constructor must not make changes " +
              "to the object that cause it to be dirty.");
        }
      } else {
        isCreatingPointerForObjectId.set(objectId);
        synchronized (smCache) {
          object = smCache.computeIfAbsent(objectId,oid-> newInstance(className));
          if(object.hasChanges()){
            System.out.printf("object %s has %d changes.\n",object,
              object.changes());
          }
        }
      }
      assert object!=null;
      return object;
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException("Failed to create instance of subclass.", e);
    } finally {
      isCreatingPointerForObjectId.remove();
    }
  }

  public static <X extends ParseObject>
  X getIfExists(String id) {
    synchronized (smCache) {
      //noinspection unchecked
      return (X) smCache.get(id);
    }
  }
  public static <X extends ParseObject>
  X getOrStub(String objectId, String className) {
    X res = getIfExists(objectId);
    if(res!=null)
      return res;
    //noinspection unchecked
    return (X)createWithoutData(objectId,className);
  }

  public static void putObject(final ParseObject object)
  {
    String objectId = object.getObjectId();
    smCache.put(objectId,object);

  }
    public static JSONObject createPointer(String className,
                                         String objectId)
  {
    JSONObject json = new JSONObject();
    try {
      json.put("__type", "Pointer");
      json.put("className", className);
      assert objectId!=null;
      json.put("objectId", objectId);
    } catch (JSONException e) {
      // This should not happen
      throw new RuntimeException(e);
    }
    return json;
  }

  @Override
  protected void finalize()
  throws Throwable
  {
    Reflect.announce(""+this);
    super.finalize();
  }

  /**
   * Creates a reference to an existing {@code ParseObject} for use in creating associations between
   * {@code ParseObject}s. Calling {@link #isDataAvailable()} on this object will return
   * {@code false} until  {@link # fetchIfNeeded()} or {@link #fetch()} has been called. No network
   * request will be made.
   *
   * @param subclass The {@code ParseObject} subclass to create.
   * @param objectId The object id for the referenced object.
   * @return A {@code ParseObject} without data.
   */
  @SuppressWarnings({"unused", "unchecked"})
  public static <T extends ParseObject> T createWithoutData(Class<T> subclass, String objectId) {
    return (T) createWithoutData(ParseSyncUtils.getTableForClass(subclass), objectId);
  }

  /**
   * Converts a {@code ParseObject.State} to a {@code ParseObject}.
   *
   * @param state The {@code ParseObject.State} to convert from.
   * @return A {@code ParseObject} instance.
   */
  public static <T extends ParseObject> T from(ParseObject.State state) {
    @SuppressWarnings("unchecked") T object =
      (T) ParseObject.createWithoutData(state.className(), state.objectId());
    synchronized (object.mutex) {
      State newState;
      if (state.isComplete()) {
        newState = state;
      } else {
        newState = object.getState().newBuilder().apply(state).build();
      }
      object.setState(newState);
    }
    return object;
  }

  /**
   * Creates a new {@code ParseObject} based on data from the Parse server.
   *
   * @param json             The object's data.
   * @param defaultClassName The className of the object, if none is in the JSON.
   * @param decoder          Delegate for knowing how to decodeObject the values
   *                         in the JSON.
   * @param selectedKeys     Set of keys selected when querying for this object.
   *                         If none, the object is assumed to be complete, i.e.
   *                         this is all the data for the object on the server.
   */
  public static <T extends ParseObject> T fromJSON(JSONObject json, String defaultClassName,
                                                   ParseDecoder decoder, Set<String> selectedKeys) {
    if (selectedKeys != null && !selectedKeys.isEmpty()) {
      JSONArray keys = new JSONArray(selectedKeys);
      try {
        json.put(KEY_SELECTED_KEYS, keys);
      } catch (JSONException e) {
        throw new RuntimeException(e);
      }
    }
    String defaultClassName1 = defaultClassName;
    if (defaultClassName1 == null) {
      defaultClassName1 = "";
    }
    String className = json.optString(KEY_CLASS_NAME, defaultClassName1);
    if (Util.isNoE(className)){
      className=json.optString(ALT_CLASS_NAME,defaultClassName);
    }
    if (className == null || className.isEmpty()) {
      throw new IllegalArgumentException("No class name in json, and no default");
    }
    String objectId = json.optString(KEY_OBJECT_ID, "");
    if (objectId == null || objectId.isEmpty()) {
      objectId = null;
    }
    boolean isComplete = !json.has(KEY_SELECTED_KEYS);
    //noinspection unchecked
    T object = (T) ParseObject.createWithoutData(className, objectId);
    State newState = object.mergeFromServer(object.getState(), json, decoder, isComplete);
    object.setState(newState);
    return object;
  }

//  /* *
//   * Method used by parse-server webhooks implementation to convert
//   * a raw JSONObject to Parse Object.
//   * <p>
//   * Method is used by parse-server webhooks implementation to create a
//   * new {@code ParseObject} from the incoming json payload. The method is different from
//   * fromJSON(JSONObject, String, ParseDecoder, Set) () in that it calls
//   * build(JSONObject, ParseDecoder) which populates operation queue
//   * rather than the server data from the incoming JSON, as at external server the incoming
//   * JSON may not represent the actual server data. Also, it handles
//   * {@link ParseFieldOperations} separately.
//   *
//   * @param json    The object's data.
//   * @param decoder Delegate for knowing how to decodeObject the values in the JSON.
//   */
//  static <T extends ParseObject> T fromJSONPayload(JSONObject json, ParseDecoder decoder) {
//    String className = json.optString(KEY_CLASS_NAME);
//    if (className == null || ParseTextUtils.isEmpty(className)) {
//      return null;
//    }
//    String objectId = json.optString(KEY_OBJECT_ID, null);
//    @SuppressWarnings("unchecked") T object =
//      (T) ParseObject.createWithoutData(className, objectId);
//    object.build(json, decoder);
//    return object;
//  }

  private static void collectDirtyChildren(Object node, final Collection<ParseObject> dirtyChildren,
                                           final Collection<ParseFile> dirtyFiles,
                                           final Set<ParseObject> alreadySeen,
                                           final Set<ParseObject> alreadySeenNew) {

    new ParseTraverser() {
      @Override
      protected boolean visit(Object node) {

        if (node instanceof ParseFile) {
          if (dirtyFiles == null) {
            return true;
          }

          ParseFile file = (ParseFile) node;
          if (file.getUrl() == null) {
            dirtyFiles.add(file);
          }
          return true;
        }

        if (!(node instanceof ParseObject)) {
          return true;
        }

        if (dirtyChildren == null) {
          return true;
        }

        ParseObject object = (ParseObject) node;
        Set<ParseObject> seen = alreadySeen;
        Set<ParseObject> seenNew = alreadySeenNew;

        // impossible to save this collection of objects, so throw an exception.
        if (object.getObjectId() != null) {
          seenNew = new HashSet<>();
        } else {
          if (seenNew.contains(object)) {
            throw new RuntimeException("Found a circular dependency while saving.");
          }
          seenNew = new HashSet<>(seenNew);
          seenNew.add(object);
        }

        // Check for cycles of any object. If this occurs, then there's no
        // problem, but we shouldn't recurse any deeper, because it would be
        // an infinite recursion.
        if (seen.contains(object)) {
          return true;
        }
        seen = new HashSet<>(seen);
        seen.add(object);

        // Recurse into this object's children looking for dirty children.
        // We only need to look at the child object's current estimated data,
        // because that's the only data that might need to be saved now.
        collectDirtyChildren(object.estimatedData, dirtyChildren, dirtyFiles, seen, seenNew);

        if (object.isDirty(false)) {
          dirtyChildren.add(object);
        }

        return true;
      }
    }.setYieldRoot(true).traverse(node);
  }

  /**
   * Helper version of collectDirtyChildren so that callers don't have to add the internally used
   * parameters.
   */
  public static void collectDirtyChildren(Object node, Collection<ParseObject> dirtyChildren,
                                          Collection<ParseFile> dirtyFiles) {
    collectDirtyChildren(node, dirtyChildren, dirtyFiles, new HashSet<>(), new HashSet<>());
  }

  @Nonnull
  private LinkedList<ParseOperationSet> createOperationSetQueue() {
    final LinkedList<ParseOperationSet> operationSetQueue;
    operationSetQueue = new LinkedList<>();
    operationSetQueue.add(new ParseOperationSet());
    return operationSetQueue;
  }

  public JSONObject toJSON() {
    FullObjectCoder encoder = FullObjectCoder.get();
    return encoder.encode(this);
  }

  public TreeMap<String, Object> createEstimatedData() {
    return new TreeMap<>();
  }

  public State.Init<?> newStateBuilder(String className) {
    return new State.Builder(className);
  }

  public State getState() {
    synchronized (mutex) {
      return state;
    }
  }

  /**
   * Updates the current state of this object as well as updates our in memory cached state.
   *
   * @param newState The new state.
   */
  public void setState(State newState) {
    synchronized (mutex) {
      setState(newState, true);
    }
  }

  private void setState(State newState, boolean notifyIfObjectIdChanges) {
    if (state == newState) {
      return;
    }
    synchronized (mutex) {
      String oldObjectId = state.objectId();
      String newObjectId = newState.objectId();

      state = newState;

      if (notifyIfObjectIdChanges && !ParseTextUtils.equals(oldObjectId, newObjectId)) {
        notifyObjectIdChanged(oldObjectId, newObjectId);
      }

      rebuildEstimatedData();
    }
  }

  /**
   * Accessor to the class name.
   */
  public String getClassName() {
    synchronized (mutex) {
      if(state==null)
        return null;
      return state.className();
    }
  }

  /**
   * This reports time as the server sees it, so that if you make changes to a {@code ParseObject
   * }, then
   * wait a while, and then call {@link #save()}, the updated time will be the time of the
   * {@link #save()} call rather than the time the object was changed locally.
   *
   * @return The last time this object was updated on the server.
   */
  public Date getUpdatedAt() {
    long updatedAt = getState().updatedAt();
    return updatedAt > 0 ? new Date(updatedAt) : null;
  }

  /**
   * This reports time as the server sees it, so that if you create a {@code ParseObject}, then
   * wait a
   * while, and then call {@link #save()}, the creation time will be the time of the first
   * {@link #save()} call rather than the time the object was created locally.
   *
   * @return The first time this object was saved on the server.
   */
  public Date getCreatedAt() {
    long createdAt = getState().createdAt();
    return createdAt > 0 ? new Date(createdAt) : null;
  }

  /**
   * Returns a set view of the keys contained in this object. This does not include createdAt,
   * updatedAt, authData, or objectId. It does include things like username and ACL.
   */
  public Set<String> keySet() {
    synchronized (mutex) {
      return Collections.unmodifiableSet(estimatedData.keySet());
    }
  }

  /**
   * Constructs a new {@code ParseObject} with no data in it. A {@code
   * ParseObject} constructed in this way will not have an objectId and will
   * not persist to the database until {@link #save()} is called.
   * <p>
   * Class names must be alphanumerical plus underscore, and start with a
   * letter. It is recommended to name classes in
   * <code>PascalCaseLikeThis</code>.
   *
   * @param theClassName The className for this {@code ParseObject}.
   */
  public ParseObject(String theClassName)
  {
    // We use a ThreadLocal rather than passing a parameter so that
    // createWithoutData can do the
    // right thing with subclasses. It's ugly and terrible, but it does
    // provide the development
    // experience we generally want, so... yeah. Sorry to whomever has to
    // deal with this in the
    // future. I pinky-swear we won't make a habit of this -- you believe me,
    // don't you?
    String objectIdForPointer = isCreatingPointerForObjectId.get();
    if(!Parse.isInitializedOrInitializing())
    {
      throw new RuntimeException("Parse Not Initialized");
    }

    if (theClassName == null) {
      throw new IllegalArgumentException(
        "You must specify a Parse class name when creating a new ParseObject.");
    }
    if (smCache.containsKey(objectIdForPointer)) {
      throw new IllegalArgumentException("There can be only one! "+objectIdForPointer);
    }
    if (AUTO_CLASS_NAME.equals(theClassName)) {
      theClassName = ParseSyncUtils.getTableForClass(getClass());
//      Reflect.announce("ParseObject created(%s,%s,%s)",
//        getClass().getSimpleName(),
//        objectIdForPointer,
//        Parse.getObject(objectIdForPointer)
//        );
//    } else {
//      Reflect.announce("ParseObject created(%s,%s,%s)", theClassName,
//        Parse.getObject(objectIdForPointer), objectIdForPointer);
    }

    // If this is supposed to be created by a factory but wasn't, throw an
    // exception.
    if (!ParseSyncUtils.isSubclassValid(theClassName,
      getClass())) {
      throw new IllegalArgumentException(
        "You must create this type of ParseObject using ParseObject.create() or the proper " +
          "subclass.");
    }

    State.Init<?> builder = newStateBuilder(theClassName);
    // When called from new, assume hasData for the whole object is true.
    if (objectIdForPointer == null) {
      setDefaultValues();
      builder.isComplete(true);
    } else {
      if (!objectIdForPointer.equals(NEW_OFFLINE_OBJECT_ID_PLACEHOLDER)) {
        builder.objectId(objectIdForPointer);
      }
      builder.isComplete(false);
    }
    // This is a new untouched object, we don't need cache rebuilding, etc.
    state = builder.build();
  }

  public void mergeFromObject(ParseObject other) {
    synchronized (mutex) {
      // If they point to the same instance, we don't need to merge.
      if (this == other) {
        return;
      }

      State copy = other.getState().newBuilder().build();

      // We don't want to notify if an objectId changed here since we
      // utilize this method to merge an anonymous current user with a
      // new ParseUser instance that's calling signUp(). This doesn't make
      // any sense. We should probably remove that code in ParseUser.
      // Otherwise, there shouldn't be any objectId changes here since
      // this method is only otherwise used in fetchAll.
      setState(copy, false);
    }
  }

  /**
   * Deep traversal on this object to grab a copy of any object referenced by this object. These
   * instances may have already been fetched, and we don't want to lose their data when refreshing
   * or saving.
   *
   * @return the map mapping from objectId to {@code ParseObject} which has been fetched.
   */
  public Map<String, ParseObject> collectFetchedObjects() {
    final Map<String, ParseObject> fetchedObjects = new HashMap<>();
    ParseTraverser traverse = new ParseTraverser() {
      @Override
      protected boolean visit(Object object) {
        if (object instanceof ParseObject) {
          ParseObject parseObj = (ParseObject) object;
          State state = parseObj.getState();
          if (state.objectId() != null && state.isComplete()) {
            fetchedObjects.put(state.objectId(), parseObj);
          }
        }
        return true;
      }
    };
    traverse.traverse(estimatedData);
    return fetchedObjects;
  }

  /**
   * Merges from JSON in REST format.
   * Updates this object with data from the server.
   *
   * @see #toJSONObjectForSaving(State, ParseOperationSet, ParseEncoder)
   */
  public State mergeFromServer(State state, JSONObject json, ParseDecoder decoder,
                               boolean completeData) {
    try {
      // If server data is complete, consider this object to be fetched.
      State.Init<?> builder = state.newBuilder();
      if (completeData) {
        builder.clear();
      }
      builder.isComplete(state.isComplete() || completeData);

      Iterator<?> keys = json.keys();
      while (keys.hasNext()) {
        String key = (String) keys.next();
        /*
        __type:       Returned by queries and cloud functions to designate body is a ParseObject
        __className:  Used by fromJSON, should be stripped out by the time it gets here...
         */
        if (key.equals("__type") || key.equals(KEY_CLASS_NAME)) {
          continue;
        }
        if (key.equals(KEY_OBJECT_ID)) {
          String newObjectId = json.getString(key);
          builder.objectId(newObjectId);
          continue;
        }
        if (decodeDate(json, builder, key, KEY_CREATED_AT))
          continue;
        if (decodeDate(json, builder, key, KEY_UPDATED_AT))
          continue;
        if (key.equals(KEY_ACL)) {
          ParseACL acl = ParseACL.createACLFromJSONObject(json.getJSONObject(key), decoder);
          builder.put(KEY_ACL, acl);
          continue;
        }
        if (key.equals(KEY_SELECTED_KEYS)) {
          JSONArray safeKeys = json.getJSONArray(key);
          if (safeKeys.length() > 0) {
            Collection<String> set = new HashSet<>();
            for (int i = 0; i < safeKeys.length(); i++) {
              // Don't add nested keys.
              String safeKey = safeKeys.getString(i);
              if (safeKey.contains(".")) {
                safeKey = safeKey.split("\\.")[0];
              }
              set.add(safeKey);
            }
            builder.availableKeys(set);
          }
          continue;
        }

        Object value = json.get(key);
        if (value instanceof JSONObject && json.has(KEY_SELECTED_KEYS)) {
          // This might be a ParseObject. Pass selected keys to understand if it is complete.
          JSONArray selectedKeys = json.getJSONArray(KEY_SELECTED_KEYS);
          JSONArray nestedKeys = new JSONArray();
          for (int i = 0; i < selectedKeys.length(); i++) {
            String nestedKey = selectedKeys.getString(i);
            if (nestedKey.startsWith(key + ".")) {
              nestedKeys.put(nestedKey.substring(key.length() + 1));
            }
          }
          if (nestedKeys.length() > 0) {
            ((JSONObject) value).put(KEY_SELECTED_KEYS, nestedKeys);
          }
        }
        Object decodedObject = decoder.decode(value);
        builder.put(key, decodedObject);
      }

      return builder.build();
    } catch (JSONException e) {
      throw new RuntimeException(e);
    }
  }

  public boolean decodeDate(JSONObject json, Init<?> builder, String key,
                            String keyCreatedAt)
  {
    if (key.equals(keyCreatedAt)) {
      Object value = json.get(key);
      if (value instanceof String) {
        Date date = ParseDateFormat.get().parse((String) value);
        if(date!=null)
          builder.createdAt(date);
        return true;
      } else if (value instanceof JSONObject) {
        JSONObject object = (JSONObject) value;
        if (object.getString("__type").equals("Date")) {
          String dateString = object.getString("iso");
          Date date = ParseDateFormat.get().parse(dateString);
          builder.createdAt(date);
          return true;
        }
      }
      throw new RuntimeException("Bad date: " + value);
    }
    return false;
  }

  private boolean hasDirtyChildren() {
    synchronized (mutex) {
      // We only need to consider the currently estimated children here,
      // because they're the only ones that might need to be saved in a
      // subsequent call to save, which is the meaning of "dirtiness".
      List<ParseObject> unsavedChildren = new ArrayList<>();
      collectDirtyChildren(estimatedData, unsavedChildren, null);
      return !unsavedChildren.isEmpty();
    }
  }

  /**
   * Whether any key-value pair in this object (or its children) has been added/updated/removed and
   * not saved yet.
   *
   * @return Whether this object has been altered and not saved yet.
   */
  public boolean isDirty() {
    return this.isDirty(true);
  }

  boolean isDirty(boolean considerChildren) {
    synchronized (mutex) {
      return (isDeleted || getObjectId() == null || hasChanges()) ||
        (considerChildren && hasDirtyChildren());
    }
  }

  public boolean hasChanges() {
    synchronized (mutex) {
      return !currentOperations().isEmpty();
    }
  }
  public int changes() {
    return currentOperations().size();
  }

  /**
   * Returns {@code true} if this {@code ParseObject} has operations in operationSetQueue that
   * haven't been completed yet, {@code false} if there are no operations in the operationSetQueue.
   */
  public boolean hasOutstandingOperations() {
    synchronized (mutex) {
      // > 1 since 1 is for unsaved changes.
      return operationSetQueue.size() > 1;
    }
  }

  /**
   * Whether a value associated with a key has been added/updated/removed and not saved yet.
   *
   * @param key The key to check for
   * @return Whether this key has been altered and not saved yet.
   */
  public boolean isDirty(String key) {
    synchronized (mutex) {
      return currentOperations().containsKey(key);
    }
  }

  /**
   * Accessor to the object id. An object id is assigned as soon as an object is saved to the
   * server. The combination of a className and an objectId uniquely identifies an object in your
   * application.
   *
   * @return The object id.
   */
  public String getObjectId() {
    synchronized (mutex) {
      if(state==null)
        return null;
      return state.objectId();
    }
  }

  /**
   * Setter for the object id. In general, you do not need to use this.
   * However, in some cases this can be convenient. For example, if you are
   * serializing a {@code ParseObject} yourself and wish to recreate it, you
   * can use this to recreate the {@code ParseObject} exactly.
   */
  public void setObjectId(String newObjectId) {
    synchronized (mutex) {
      String oldObjectId = state.objectId();
      if (ParseTextUtils.equals(oldObjectId, newObjectId)) {
        return;
      }

      // We don't need to use setState since it doesn't affect our cached state.
      state = state.newBuilder().objectId(newObjectId).build();
      notifyObjectIdChanged(oldObjectId, newObjectId);
    }
  }

  private boolean idEq(Object intern, Object res) {
    return intern == res;
  }

  /**
   * Returns the localId, which is used internally for serializing relations to objects that don't
   * yet have an objectId.
   */
  public String getOrCreateLocalId() {
    synchronized (mutex) {
      if (localId == null) {
        if (state.objectId() != null) {
          throw new IllegalStateException(
            "Attempted to get a localId for an object with an objectId.");
        }
        localId = getLocalIdManager().createLocalId();
      }
      return localId;
    }
  }

  // Sets the objectId without marking dirty.
  private void notifyObjectIdChanged(String oldObjectId, String newObjectId) {
    synchronized (mutex) {
      if (localId != null) {
        getLocalIdManager().setObjectId(localId, newObjectId);
        localId = null;
      }
    }
  }

  private ParseRESTObjectCommand currentSaveEventuallyCommand(ParseOperationSet operations,
                                                              ParseEncoder objectEncoder,
                                                              String sessionToken) {
    State state = getState();

    /*
     * Get the JSON representation of the object, and use the information to construct the
     * command.
     */
    JSONObject objectJSON = toJSONObjectForSaving(state, operations, objectEncoder);

    return ParseRESTObjectCommand.saveObjectCommand(state, objectJSON, sessionToken);
  }

  /**
   * Converts a {@code ParseObject} to a JSON representation for saving to Parse.
   * <p>
   * <pre>
   * {
   *   data: { // objectId plus any ParseFieldOperations },
   *   classname: class name for the object
   * }
   * </pre>
   * <p>
   * updatedAt and createdAt are not included. only dirty keys are represented in the data.
   *
   */
  // Currently only used by saveEventually
  public <T extends State> JSONObject toJSONObjectForSaving(T state, ParseOperationSet operations,
                                                            ParseEncoder objectEncoder)
  {
    ParseObjectCoder coder = ParseObjectCoder.get();
    return coder.encode(state,operations,objectEncoder);
  }

  public ParseOperationSet startSave() {
    synchronized (mutex) {
      ParseOperationSet currentOperations = currentOperations();
      operationSetQueue.addLast(new ParseOperationSet());
      return currentOperations;
    }
  }

  public void validateSave() {
    // do nothing
  }

  /**
   * Saves this object to the server.
   * <p>
   * Don't use it from the main thread.
   *
   * @throws ParseException Throws an exception if the server is inaccessible.
   */
  public final void save() throws ParseException {
    ParseSyncUtils.save(this);
  }

  /**
   * Copies all the operations that have been performed on another object
   * since its last save onto this one.
   */
  protected void copyChangesFrom(ParseObject other) {
    synchronized (mutex) {
      ParseOperationSet operations = other.operationSetQueue.getFirst();
      for (String key : operations.keySet()) {
        ParseFieldOperation operation = operations.get(key);
        if (operation != null) {
          performOperation(key, operation);
        }
      }
    }
  }

  public void validateSaveEventually() throws ParseException {
  }

  public void updateBeforeSave() {
    // do nothing
  }

  // Validates the delete method
  public void validateDelete() {
    // do nothing
  }

  @Override
  public int hashCode() {
    return getHashCode(getObjectId());
  }

  private int getHashCode(String id) {
    if (id == null) {
      return 0;
    } else {
      return id.hashCode();
    }
  }

  /**
   * Deletes this object on the server. This does not delete or destroy the object locally.
   *
   * @throws ParseException Throws an error if the object does not exist or if the internet fails.
   */
  public final void delete() throws ParseException {
    ParseSyncUtils.delete(this);
    //    ParseTaskUtils.wait(deleteInBackground());
  }

  /**
   * Deletes this object on the server in a background thread. This is preferable to using
   * {@link #delete()}, unless your code is already running from a background thread.
   *
   * @param callback {@code callback.done(e)} is called when the save completes.
   */
  public final void deleteInBackground(DeleteCallback callback) {
    ParseSyncUtils.post(this::delete, callback);
  }

  /**
   * Returns {@code true} if this object can be serialized for saving.
   */
  private boolean canBeSerialized() {
    synchronized (mutex) {
      final Capture<Boolean> result = new Capture<>(true);

      // This method is only used for batching sets of objects for saveAll
      // and when saving children automatically. Since it's only used to
      // determine whether save should be called on them, it only
      // needs to examine their current values, so we use estimatedData.
      new ParseTraverser() {
        @Override
        protected boolean visit(Object value) {
          if (value instanceof ParseFile) {
            ParseFile file = (ParseFile) value;
            if (file.isDirty()) {
              result.set(false);
            }
          }

          if (value instanceof ParseObject) {
            ParseObject object = (ParseObject) value;
            if (object.getObjectId() == null) {
              result.set(false);
            }
          }

          // Continue to traverse only if it can still be serialized.
          return result.get();
        }
      }.setYieldRoot(false).setTraverseParseObjects(true).traverse(this);

      return result.get();
    }
  }

  /**
   * Return the operations that will be sent in the next call to save.
   */
  private ParseOperationSet currentOperations() {
    synchronized (mutex) {
      return operationSetQueue.getLast();
    }
  }

  /**
   * Updates the estimated values in the map based on the given set of ParseFieldOperations.
   */
  private void applyOperations(ParseOperationSet operations, Map<String, Object> map) {
    for (String key : operations.keySet()) {
      ParseFieldOperation operation = operations.get(key);
      if (operation == null) {
        continue;
      }
      Object oldValue = map.get(key);
      Object newValue = operation.apply(oldValue, key);
      if (newValue != null) {
        map.put(key, newValue);
      } else {
        map.remove(key);
      }
    }
  }

  /**
   * Regenerates the estimatedData map from the serverData and operations.
   */
  private void rebuildEstimatedData() {
    synchronized (mutex) {
      estimatedData.clear();
      for (String key : state.keySet()) {
        estimatedData.put(key, state.get(key));
      }
      for (ParseOperationSet operations : operationSetQueue) {
        applyOperations(operations, estimatedData);
      }
    }
  }

  /**
   * performOperation() is like {@link #put(String, Object)} but instead of just taking a new value,
   * it takes a ParseFieldOperation that modifies the value.
   */
  public void performOperation(String key, ParseFieldOperation operation) {
    //    key=process(key);
    synchronized (mutex) {
      Object oldValue = estimatedData.get(key);
      Object newValue = operation.apply(oldValue, key);
      if (newValue != null) {
        estimatedData.put(key, newValue);
      } else {
        estimatedData.remove(key);
      }

      ParseFieldOperation oldOperation = currentOperations().get(key);
      ParseFieldOperation newOperation = operation.mergeWithPrevious(oldOperation);
      currentOperations().put(key, newOperation);
    }
  }

  /**
   * Add a key-value pair to this object. It is recommended to name keys in
   * <code>camelCaseLikeThis</code>.
   *
   * @param key   Keys must be alphanumerical plus underscore, and start with a letter.
   * @param value Values may be numerical, {@link String}, {@link JSONObject}, {@link JSONArray},
   *              {@link JSONObject#NULL}, or other {@code ParseObject}s. value may not be {@code
   *              null}.
   */
  public void put(@Nonnull String key, @Nonnull Object value) {
    checkKeyIsMutable(key);
    performPut(key, value);
  }

  public void performPut(String key, Object value) {
    if (key == null) {
      throw new IllegalArgumentException("key may not be null.");
    }

    if (value == null) {
      throw new IllegalArgumentException("value may not be null.");
    }
    key = key.intern();
    ParseDecoder decoder = ParseDecoder.get();
    ParseEncoder encoder = PointerEncoder.get();
    if (value instanceof JSONObject) {
      value = ParseJSON.convertJSONObjectToMap((JSONObject) value);
    } else if (value instanceof JSONArray) {
      value = ParseJSON.convertJSONArrayToList((JSONArray) value);
    } else if (value instanceof String) {
      value = ((String) value).intern();
    }

    if (!ParseEncoder.isValidType(value)) {
      throw new IllegalArgumentException("invalid type for value: " + value.getClass());
    }

    performOperation(key, new ParseSetOperation(value));
  }

  /**
   * Whether this object has a particular key. Same as {@link #containsKey(String)}.
   *
   * @param key The key to check for
   * @return Whether this object contains the key
   */
  public boolean has(@Nonnull String key) {
    return containsKey(key);
  }

  /**
   * Atomically increments the given key by 1.
   *
   * @param key The key to increment.
   */
  public void increment(@Nonnull String key) {
    increment(key, 1);
  }

  /**
   * Atomically increments the given key by the given number.
   *
   * @param key    The key to increment.
   * @param amount The amount to increment by.
   */
  public void increment(@Nonnull String key, @Nonnull Number amount) {
    ParseIncrementOperation operation = new ParseIncrementOperation(amount);
    performOperation(key, operation);
  }

  /**
   * Atomically adds an object to the end of the array associated with a given key.
   *
   * @param key   The key.
   * @param value The object to add.
   */
  public void add(@Nonnull String key, Object value) {
    this.addAll(key, Collections.singletonList(value));
  }

  /**
   * Atomically adds the objects contained in a {@code Collection} to the end of the array
   * associated with a given key.
   *
   * @param key    The key.
   * @param values The objects to add.
   */
  public void addAll(@Nonnull String key, Collection<?> values) {
    ParseAddOperation operation = new ParseAddOperation(values);
    performOperation(key, operation);
  }

  /**
   * Atomically adds an object to the array associated with a given key, only if it is not already
   * present in the array. The position of the insert is not guaranteed.
   *
   * @param key   The key.
   * @param value The object to add.
   * @return true iff items were added
   */
  public boolean addUnique(@Nonnull String key, Object value) {
    return addAllUnique(key, Collections.singletonList(value));
  }

  /**
   * Atomically adds the objects contained in a {@code Collection} to the array associated with a
   * given key, only adding elements which are not already present in the array. The position of the
   * insert is not guaranteed.
   *
   * @param key    The key.
   * @param values The objects to add.
   */
  public boolean addAllUnique(@Nonnull String key, Collection<?> values) {
    List<?> list = getList(key);
    if (list != null) {
      HashSet<?> added = new HashSet<>(values);
      list.forEach(added::remove);
      if (added.isEmpty()) {
        return false;
      }
    }
    ParseAddUniqueOperation operation = new ParseAddUniqueOperation(values);
    performOperation(key, operation);
    return true;
  }

  /**
   * Removes a key from this object's data if it exists.
   *
   * @param key The key to remove.
   */
  public void remove(@Nonnull String key) {
    checkKeyIsMutable(key);
    performRemove(key);
  }

  protected void performRemove(String key) {
    synchronized (mutex) {
      Object object = get(key, false);

      if (object != null)
        performOperation(key, ParseDeleteOperation.getInstance());
    }
  }

  public Object get(@Nonnull String key) {
    return get(key, true);
  }

  /**
   * Atomically removes all instances of the objects contained in a {@code Collection} from the
   * array associated with a given key. To maintain consistency with the Java Collection API, there
   * is no method removing all instances of a single object. Instead, you can call
   * {@code parseObject.removeAll(key, Arrays.asList(value))}.
   *
   * @param key    The key.
   * @param values The objects to remove.
   */
  public void removeAll(@Nonnull String key, Collection<?> values) {
    checkKeyIsMutable(key);

    ParseRemoveOperation operation = new ParseRemoveOperation(values);
    performOperation(key, operation);
  }

  private void checkKeyIsMutable(String key) {
    if (!isKeyMutable(key)) {
      throw new IllegalArgumentException(
        "Cannot modify `" + key + "` property of an " + getClassName() + " object.");
    }
  }

  public boolean isKeyMutable(String key) {
    return true;
  }

  /**
   * Whether this object has a particular key. Same as {@link #has(String)}.
   *
   * @param key The key to check for
   * @return Whether this object contains the key
   */
  public boolean containsKey(@Nonnull String key) {
    synchronized (mutex) {
      return estimatedData.containsKey(key);
    }
  }

  /**
   * Access a {@link String} value.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@link String}.
   */
  @Nullable
  public String getString(@Nonnull String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof String)) {
        return null;
      }
      return ((String) value).intern();
    }
  }

  /**
   * Access a {@code byte[]} value.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@code byte[]}.
   */
  @Nullable
  public byte[] getBytes(String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof byte[])) {
        return null;
      }

      return (byte[]) value;
    }
  }

  /**
   * Access a {@link Number} value.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@link Number}.
   */
  @Nullable
  public Number getNumber(String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof Number)) {
        return null;
      }
      return (Number) value;
    }
  }

  /**
   * Access a {@link JSONArray} value.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@link JSONArray}.
   */
  @Nullable
  public JSONArray getJSONArray(String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);

      if (value instanceof List) {
        value = PointerOrLocalIdEncoder.get().encode(value);
      }

      if (!(value instanceof JSONArray)) {
        return null;
      }
      return (JSONArray) value;
    }
  }

  /**
   * Access a {@link List} value.
   *
   * @param key The key to access the value for
   * @return {@code null} if there is no such key or if the value can't be converted to a
   * {@link List}.
   */
  @Nullable
  public <T> List<T> getList(String key) {
    synchronized (mutex) {
      Object value = estimatedData.get(key);
      if (!(value instanceof List)) {
        return null;
      }
      @SuppressWarnings("unchecked") List<T> returnValue = (List<T>) value;
      return returnValue;
    }
  }

  /**
   * Access a {@link Map} value
   *
   * @param key The key to access the value for
   * @return {@code null} if there is no such key or if the value can't be converted to a
   * {@link Map}.
   */
  @Nullable
  public <V> Map<String, V> getMap(String key) {
    synchronized (mutex) {
      Object value = estimatedData.get(key);
      if (!(value instanceof Map)) {
        return null;
      }
      @SuppressWarnings("unchecked") Map<String, V> returnValue = (Map<String, V>) value;
      return returnValue;
    }
  }

  /**
   * Access a {@link JSONObject} value.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@link JSONObject}.
   */
  @Nullable
  public JSONObject getJSONObject(String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);

      if (value instanceof Map) {
        value = PointerOrLocalIdEncoder.get().encode(value);
      }

      if (!(value instanceof JSONObject)) {
        return null;
      }

      return (JSONObject) value;
    }
  }

  /**
   * Access an {@code int} value.
   *
   * @param key The key to access the value for.
   * @return {@code 0} if there is no such key or if it is not a {@code int}.
   */
  public int getInt(@Nonnull String key) {
    Number number = getNumber(key);
    if (number == null) {
      return 0;
    }
    return number.intValue();
  }

  /**
   * Access a {@code double} value.
   *
   * @param key The key to access the value for.
   * @return {@code 0} if there is no such key or if it is not a {@code double}.
   */
  public double getDouble(@Nonnull String key) {
    Number number = getNumber(key);
    if (number == null) {
      return 0;
    }
    return number.doubleValue();
  }

  /**
   * Access a {@code long} value.
   *
   * @param key The key to access the value for.
   * @return {@code 0} if there is no such key or if it is not a {@code long}.
   */
  public long getLong(@Nonnull String key) {
    Number number = getNumber(key);
    if (number == null) {
      return 0;
    }
    return number.longValue();
  }

  /**
   * Access a {@code boolean} value.
   *
   * @param key The key to access the value for.
   * @return {@code false} if there is no such key or if it is not a {@code boolean}.
   */
  public boolean getBoolean(@Nonnull String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof Boolean)) {
        return false;
      }
      return (Boolean) value;
    }
  }

  /**
   * Access a {@link Date} value.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@link Date}.
   */
  @Nullable
  public Date getDate(@Nonnull String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof Date)) {
        return null;
      }
      return (Date) value;
    }
  }

  /**
   * Access a {@code ParseObject} value. This function will not perform a network request. Unless
   * the
   * {@code ParseObject} has been downloaded (e.g. by a {@link ParseQuery#include(String)} or by
   * calling
   * {@link # fetchIfNeeded()} or {@link #fetch()}), {@link #isDataAvailable()} will return
   * {@code false}.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@code ParseObject}.
   */
  @Nullable
  public ParseObject getParseObject(@Nonnull String key) {
    Object value = get(key);
    if (!(value instanceof ParseObject)) {
      return null;
    }
    return (ParseObject) value;
  }

  /**
   * Access a {@link ParseUser} value. This function will not perform a network request. Unless the
   * {@code ParseObject} has been downloaded (e.g. by a {@link ParseQuery#include(String)} or by
   * calling
   * {@link # fetchIfNeeded()} or {@link #fetch()}), {@link #isDataAvailable()} will return
   * {@code false}.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if the value is not a {@link ParseUser}.
   */
  @Nullable
  public ParseUser getParseUser(@Nonnull String key) {
    Object value = get(key, false);
    if (!(value instanceof ParseUser)) {
      return null;
    }
    return (ParseUser) value;
  }

  /**
   * Access a {@link ParseFile} value. This function will not perform a network request. Unless the
   * {@link ParseFile} has been downloaded (e.g. by calling {@link ParseFile#getData()}),
   * {@link ParseFile#isDataAvailable()} will return {@code false}.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key or if it is not a {@link ParseFile}.
   */
  @Nullable
  public ParseFile getParseFile(@Nonnull String key) {
    Object value = get(key);
    if (!(value instanceof ParseFile)) {
      return null;
    }
    return (ParseFile) value;
  }

  /**
   * Access a {@link ParseGeoPoint} value.
   *
   * @param key The key to access the value for
   * @return {@code null} if there is no such key or if it is not a {@link ParseGeoPoint}.
   */
  @Nullable
  public ParseGeoPoint getParseGeoPoint(@Nonnull String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof ParseGeoPoint)) {
        return null;
      }
      return (ParseGeoPoint) value;
    }
  }

  /**
   * Access a {@link ParsePolygon} value.
   *
   * @param key The key to access the value for
   * @return {@code null} if there is no such key or if it is not a {@link ParsePolygon}.
   */
  @Nullable
  public ParsePolygon getParsePolygon(@Nonnull String key) {
    synchronized (mutex) {
      checkGetAccess(key);
      Object value = estimatedData.get(key);
      if (!(value instanceof ParsePolygon)) {
        return null;
      }
      return (ParsePolygon) value;
    }
  }

  /**
   * Access the {@link ParseACL} governing this object.
   */
  @Nullable
  public ParseACL getACL() {
    return getACL(Util.theGovernmentIsLying());
  }

  /**
   * Set the {@link ParseACL} governing this object.
   */
  public void setACL(ParseACL acl) {
    put(KEY_ACL, acl);
  }

  private ParseACL getACL(boolean mayCopy) {
    synchronized (mutex) {
      checkGetAccess(KEY_ACL);
      Object acl = estimatedData.get(KEY_ACL);
      if (acl == null) {
        return null;
      }
      if (!(acl instanceof ParseACL)) {
        throw new RuntimeException("only ACLs can be stored in the ACL key");
      }
      if (mayCopy && ((ParseACL) acl).isShared()) {
        ParseACL copy = new ParseACL((ParseACL) acl);
        estimatedData.put(KEY_ACL, copy);
        return copy;
      }
      return (ParseACL) acl;
    }
  }

  /**
   * Gets whether the {@code ParseObject} has been fetched.
   *
   * @return {@code true} if the {@code ParseObject} is new or has been fetched or refreshed.
   * {@code false}
   * otherwise.
   */
  public boolean isDataAvailable() {
    synchronized (mutex) {
      return state != null && state.isComplete();
    }
  }

  /**
   * Gets whether the {@code ParseObject} specified key has been fetched.
   * This means the property can be accessed safely.
   *
   * @return {@code true} if the {@code ParseObject} key is new or has been fetched or refreshed.
   * {@code false}
   * otherwise.
   */
  public boolean isDataAvailable(@Nonnull String key) {
    synchronized (mutex) {
      // Fallback to estimatedData to include dirty changes.
      return isDataAvailable() ||
        (state != null && state.availableKeys().contains(key)) ||
        (estimatedData != null && estimatedData.containsKey(key));
    }
  }

  /**
   * Access or create a {@link ParseRelation} value for a key
   *
   * @param key The key to access the relation for.
   * @return the ParseRelation object if the relation already exists for the key or can be created
   * for this key.
   */
  @Nonnull
  public <T extends ParseObject> ParseRelation<T> getRelation(@Nonnull String key) {
    synchronized (mutex) {
      // All the sanity checking is done when add or remove is called on the relation.
      Object value = estimatedData.get(key);
      if (value instanceof ParseRelation) {
        @SuppressWarnings("unchecked") ParseRelation<T> relation = (ParseRelation<T>) value;
        relation.ensureParentAndKey(this, key);
        return relation;
      } else {
        ParseRelation<T> relation = new ParseRelation<>(this, key);
        /*
         * We put the relation into the estimated data so that we'll
         * get the same instance later, which may have known objects
         * cached. If we rebuildEstimatedData, then this relation will
         * be lost, and we'll get a new one. That's okay, because any
         * cached objects it knows about must be replay-able from the
         * operations in the queue. If there were any objects in this
         * relation that weren't still in the queue, then they would be
         * in the copy of the ParseRelation that's in the serverData,
         * so we would have gotten that instance instead.
         */
        estimatedData.put(key, relation);
        return relation;
      }
    }
  }

  /**
   * Access a value. In most cases it is more convenient to use a helper function such as
   * {@link #getString(String)} or {@link #getInt(String)}.
   *
   * @param key The key to access the value for.
   * @return {@code null} if there is no such key.
   */
  @Nullable
  public Object get(@Nonnull String key, boolean orDie) {
    synchronized (mutex) {
      if (key.equals(KEY_ACL)) {
        return getACL();
      }

      if (orDie)
        checkGetAccess(key);
      else if (!isDataAvailable())
        return null;

      Object value = estimatedData.get(key);

      // A relation may be deserialized without a parent or key.
      // Either way, make sure it's consistent.
      if (value instanceof ParseRelation) {
        ((ParseRelation<?>) value).ensureParentAndKey(this, key);
      }

      return value;
    }
  }

  public void checkGetAccess(String key) {
    if (!isDataAvailable(key)) {
      throw new IllegalStateException(
        "ParseObject has no data for '" + key + "'. Call fetchIfNeeded() to get the data.");
    }
  }

  public boolean hasSameId(ParseObject other) {
    synchronized (mutex) {
      if (this == other) {
        return true;
      }
      if (other == null) {
        return false;
      }
      return this.getClassName() != null && this.getObjectId() != null &&
        this.getClassName().equals(other.getClassName()) &&
        this.getObjectId().equals(other.getObjectId());
    }
  }

  public void registerSaveListener(GetCallback<ParseObject> callback) {
    synchronized (mutex) {
      saveEvent.subscribe(callback);
    }
  }

  public void unregisterSaveListener(GetCallback<ParseObject> callback) {
    synchronized (mutex) {
      saveEvent.unsubscribe(callback);
    }
  }

  /**
   * Called when a non-pointer is being created to allow additional initialization to occur.
   */
  void setDefaultValues() {
    ParseACL def = null;
    if (needsDefaultACL())
      def = ParseACL.getDefaultACL();
    if (def != null)
      setACL(def);
  }

  /**
   * Determines whether this object should get a default ACL. Override in subclasses to turn off
   * default ACLs.
   */
  public boolean needsDefaultACL() {
    return true;
  }

  public boolean isComplete() {
    return getState().isComplete();
  }

  public String attrString() {
    if(state==null)
      return "null";
    else
      return "complete: " + getState().isComplete() + ", keys: " + getState().availableKeys().size();
  }

  @Nonnull
  final public String toString() {
    return getClassName() + "(" + getObjectId() + ", " + attrString() + ")";
  }

  public Map<String, Object> getEstimatedData() {
    return Collections.unmodifiableMap(estimatedData);
  }

  public boolean isClean() {
    return !isDirty();
  }

  public <T extends ParseObject> T fetchIfNeeded() throws ParseException {
    if (isComplete()) {
      //noinspection unchecked
      return (T) this;
    } else {
      return fetch();
    }
  }

  /**
   * Fetches this object with the data from the server. Call this whenever you want the state of the
   * object to reflect exactly what is on the server.
   *
   * @return The {@code ParseObject} that was fetched.
   * @throws ParseException Throws an exception if the server is inaccessible.
   */
  @SuppressWarnings("unchecked")
  public <T extends ParseObject> T fetch() throws ParseException {
    ArrayList<T> list = new ArrayList<>();
    list.add((T) this);
    ParseSyncUtils.fetchAll(list);
    return list.get(0);
  }

  public void afterSave() {
  }

  public boolean equals(Object rhs) {
    boolean res = false;
    if (rhs == this) {
      res = true;
    } else if (rhs instanceof ParseObject) {
      res = getObjectId().equals(((ParseObject) rhs).getObjectId());
    } else if (rhs instanceof String) {
      res = rhs.equals(getObjectId());
    }
    //    System.out.println(this + " .equals " + rhs + " => " + res);
    return res;
  }

//  public TreeMap<String, Class<? extends ParseObject>> getRelations() {
//    TreeMap<String,Class<? extends ParseObject>> res = new TreeMap<>();
//    for(String key : availableKeys()){
//      if(!has(key))
//        continue;
//      Object val = get(key);
//      if(val instanceof ParseRelation<?>){
//        ParseRelation<? extends ParseObject> rel =
//          (ParseRelation<? extends ParseObject>) val;
//        String targetClassName=rel.getTargetClass();
//        Class<? extends ParseObject> type=
//          ParseSyncUtils.getClassForTable(targetClassName);
//        res.put(key,type);
//      }
//    }
//    return res;
//  }

  public Set<String> availableKeys() {
    return getState().availableKeys();
  }
  private static int smStringsSaved = 0;
  private static <X>
  X intern(X key)
  {
    if(key instanceof String){
      String tmp= ((String) key).intern();
      if(tmp!=key) {
        smStringsSaved++;
        //noinspection unchecked
        return (X)tmp;
      }
    }
    return key;
  }

  public static class State {

    private final String className;
    private final String objectId;
    private final long createdAt;
    private final long updatedAt;
    private final SortedMap<String, Object> serverData;
    private final SortedSet<String> availableKeys;
    private final boolean isComplete;

    protected State(Init<?> builder) {
      className = builder.className;
      objectId = builder.objectId;
      createdAt = builder.createdAt;
      updatedAt = builder.updatedAt > 0 ? builder.updatedAt : createdAt;
      TreeMap<String, Object> map = new TreeMap<>(builder.serverData);
      for (String key : builder.serverData.keySet()) {
        key = intern(key);
        Object val = builder.serverData.get(key);
        val=intern(val);
        map.put(key, val);
      }
      serverData = Collections.unmodifiableSortedMap(map);

      isComplete = builder.isComplete;
      availableKeys = Collections.synchronizedSortedSet(new TreeSet<>(builder.availableKeys));
    }


    public static Init<?> newBuilder(String className) {
      if ("_User".equals(className)) {
        return new ParseUser.State.Builder();
      }
      return new Builder(className);
    }

    @SuppressWarnings("unchecked")
    public <T extends Init<?>> T newBuilder() {
      return (T) new Builder(this);
    }

    public String className() {
      return className;
    }

    public String objectId() {
      return objectId;
    }

    public long createdAt() {
      return createdAt;
    }

    public long updatedAt() {
      return updatedAt;
    }

    public boolean isComplete() {
      return isComplete && availableKeys.size() > 1;
    }

    public Object get(String key) {
      return serverData.get(key);
    }

    public Set<String> keySet() {
      return serverData.keySet();
    }

    // Available keys for this object. With respect to keySet(), this includes also keys that are
    // undefined in the server, but that should be accessed without throwing.
    // These extra keys come e.g. from ParseQuery.selectKeys(). Selected keys must be available to
    // get() methods even if undefined, for consistency with complete objects.
    // For a complete object, this set is equal to keySet().
    public Set<String> availableKeys() {
      return availableKeys;
    }

    public void remove(final String key) {
      serverData.remove(key);
    }

    public void put(final String key, final Object newValue) {
      serverData.put(key, newValue);
    }

    public static abstract class Init<T extends Init<?>> {
      private final String className;
      protected SortedMap<String, Object> serverData = new TreeMap<>();
      private String objectId;
      private long createdAt = -1;
      private long updatedAt = -1;
      private boolean isComplete;
      private SortedSet<String> availableKeys = new TreeSet<>();

      public Init(String className) {
        this.className = className;
      }

      protected Init(State state) {
        className = state.className();
        objectId = state.objectId();
        createdAt = state.createdAt();
        updatedAt = state.updatedAt();
        availableKeys = Collections.synchronizedSortedSet(new TreeSet<>(state.availableKeys()));
        for (String key : state.keySet()) {
          serverData.put(key, state.get(key));
          availableKeys.add(key);
        }
        isComplete = state.isComplete();
      }

      public abstract T self();

      public abstract State build();

      public T objectId(String objectId) {
        this.objectId = objectId;
        return self();
      }

      public T createdAt(Date createdAt) {
        this.createdAt = createdAt.getTime();
        return self();
      }

      public T createdAt(long createdAt) {
        this.createdAt = createdAt;
        return self();
      }

      private void createdAt(final String newValue) {
        if (newValue.indexOf('-') == -1) {
          createdAt(Long.parseLong(newValue));
        } else {
          createdAt(ParseSyncUtils.smParseDateFormat.parse(newValue));
        }
      }

      public void updatedAt(Date updatedAt) {
        this.updatedAt = updatedAt.getTime();
      }

      public void updatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
      }

      private void updatedAt(final String newValue) {
        if (newValue.indexOf('-') == -1) {
          updatedAt(Long.parseLong(newValue));
        } else {
          updatedAt(ParseSyncUtils.smParseDateFormat.parse(newValue));
        }
      }

      public T isComplete(boolean complete) {
        isComplete = complete;
        return self();
      }

      public T put(String key, Object value) {
        serverData.put(key, value);
        availableKeys.add(key);
        return self();
      }

      public T remove(String key) {
        serverData.remove(key);
        return self();
      }

      public void availableKeys(Collection<String> keys) {
        availableKeys.addAll(keys);
      }

      public T clear() {
        objectId = null;
        createdAt = -1;
        updatedAt = -1;
        isComplete = false;
        serverData.clear();
        availableKeys.clear();
        return self();
      }

      /**
       * Applies a {@code State} on top of this {@code Builder} instance.
       *
       * @param other The {@code State} to apply over this instance.
       * @return A new {@code Builder} instance.
       */
      public T apply(State other) {
        if (other.objectId() != null) {
          objectId(other.objectId());
        }
        if (other.createdAt() > 0) {
          createdAt(other.createdAt());
        }
        if (other.updatedAt() > 0) {
          updatedAt(other.updatedAt());
        }
        isComplete(isComplete || other.isComplete());
        for (String key : other.keySet()) {
          put(key, other.get(key));
        }
        availableKeys(other.availableKeys());
        return self();
      }

      public T apply(ParseOperationSet operations) {
        for (String key : operations.keySet()) {
          ParseFieldOperation operation = operations.get(key);
          if (operation == null) {
            continue;
          }
          Object oldValue = serverData.get(key);
          Object newValue = operation.apply(oldValue, key);
          if (newValue != null) {
            switch (key) {
              case "objectId":
                objectId((String) newValue);
                break;
              case "createdAt":
                if (newValue instanceof Long) {
                  createdAt((Long) newValue);
                } else if (newValue instanceof Date) {
                  createdAt((Date) newValue);
                } else if (newValue instanceof String) {
                  createdAt((String) newValue);
                } else {
                  throw new IllegalArgumentException("createdAt: " + newValue);
                }
                break;
              case "updatedAt":
                if (newValue instanceof Long) {
                  updatedAt((Long) newValue);
                } else if (newValue instanceof Date) {
                  updatedAt((Date) newValue);
                } else if (newValue instanceof String) {
                  updatedAt((String) newValue);
                } else {
                  throw new IllegalArgumentException("updatedAt: " + newValue);
                }
                break;
              default:
                put(key, newValue);
                break;
            }
          } else {
            remove(key);
          }
        }
        return self();
      }

    }

    public static class Builder extends Init<Builder> {

      public Builder(String className) {
        super(className);
      }

      public Builder(State state) {
        super(state);
      }

      @Override
      public Builder self() {
        return this;
      }

      @Override
      public State build() {
        return new State(this);
      }
    }
  }

}