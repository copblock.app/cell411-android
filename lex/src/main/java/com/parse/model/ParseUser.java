/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.model;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.http.ParseSyncUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import cell411.utils.collect.ValueObserver;

/**
 * The {@code ParseUser} is a local representation of user data that can be
 * saved and retrieved from
 * the Parse cloud.
 */
@SuppressWarnings("unused")
@ParseClassName("_User")
public class ParseUser
  extends ParseObject
{

  public static final String KEY_SESSION_TOKEN = "sessionToken";
  public static final String KEY_AUTH_DATA = "authData";
  public static final String KEY_USERNAME = "username";
  public static final String KEY_PASSWORD = "password";
  public static final String KEY_EMAIL = "email";

  private static final List<String> READ_ONLY_KEYS =
    Collections.unmodifiableList(
      Arrays.asList(KEY_SESSION_TOKEN, KEY_AUTH_DATA));

  private static final String PARCEL_KEY_IS_CURRENT_USER = "_isCurrentUser";

  /**
   * Constructs a new ParseUser with no data in it. A ParseUser constructed
   * in this way will not
   * have an objectId and will not persist to the database until
   * {@link #signUp} is called.
   */
  public ParseUser()
  {
  }

  /**
   * Constructs a query for {@code ParseUser}.
   *
   * @see ParseQuery#getQuery(Class)
   */
  public static ParseQuery<ParseUser> getQuery()
  {
    return ParseQuery.getQuery(ParseUser.class);
  }


  /* package for tests */

  /* package for tests */

  public static void logIn(String username, String password)
  {
    ParseSyncUtils.logIn(username, password, null);
  }

  /**
   * Logs the current user out. This will remove the session from disk, log out
   * of linked services, and future calls to getCurrentUser() * will
   * return {@code null}.
   * <p/>
   * Typically, you should use {@link # logOutInBackground()} instead of this,
   * unless you are managing your own threading.
   * <p/>
   * <strong>Note:</strong>: Any errors in the flow will be swallowed due to
   * backward-compatibility reasons. Please use {@link # logOutInBackground()}
   * if you'd wish to handle them.
   */
  public static void logOut()
  {
    CurrentData.logOut();
  }

  /**
   * Requests a password reset email to be sent to the specified email
   * address associated with the
   * user account. This email allows the user to securely reset their
   * password on the Parse site.
   * <p/>
   * Typically, you should use {@link # requestPasswordResetInBackground}
   * instead of this, unless
   * you
   * are managing your own threading.
   *
   * @param email The email address associated with the user that forgot
   *              their password.
   * @throws ParseException Throws an exception if the server is
   * inaccessible, or if an account
   *                        with that email
   *                        doesn't exist.
   */
  public static void requestPasswordReset(String email)
  throws ParseException
  {
    ParseSyncUtils.requestPasswordReset(email);
  }

  public static String getCurrentId()
  {
    ParseUser user = Parse.getCurrentUser();
    if (user == null) {
      return null;
    } else {
      return user.getObjectId();
    }
  }

  public static void addCurrentUserObserver(ValueObserver<ParseUser> observer)
  {
    CurrentData.addCurrentUserObserver(observer);
  }

  public static void removeCurrentUserObserver(
    ValueObserver<ParseUser> observer)
  {
    CurrentData.removeCurrentUserObserver(observer);
  }

  @Override
  public State.Builder newStateBuilder(String className)
  {
    return new State.Builder();
  }

  @Override
  public
    /* package */ State getState()
  {
    return (State) super.getState();
  }

  @Override
  public void validateSave()
  {
    synchronized (mutex) {
      if (getObjectId() == null) {
        throw new IllegalArgumentException(
          "Cannot save a ParseUser until it has been signed up. Call signUp " +
            "first.");
      }

      if (isAuthenticated() || !isDirty() || isCurrentUser()) {
        return;
      }
    }

    throw new IllegalArgumentException(
      "Cannot save a ParseUser that is not authenticated.");
  }

  @Override
  public void remove(@Nonnull String key)
  {
    if (KEY_USERNAME.equals(key)) {
      throw new IllegalArgumentException("Can't remove the username key.");
    }
    super.remove(key);
  }

  @Override
  public boolean isKeyMutable(String key)
  {
    return !READ_ONLY_KEYS.contains(key);
  }

  @Override
  public boolean needsDefaultACL()
  {
    return false;
  }

  @Override
  public void afterSave()
  {
    if (Parse.getCurrentUser() == this) {
      CurrentData.saveAndLoad();
    }
  }

  /**
   * Whether the ParseUser has been authenticated on this device. This will
   * be true if the ParseUser
   * was obtained via a logIn or signUp method. Only an authenticated
   * ParseUser can be saved (with
   * altered attributes) and deleted.
   */
  public boolean isAuthenticated()
  {
    if (ParseSyncUtils.sessionToken == null) {
      return false;
    }
    ParseUser current = Parse.getCurrentUser();
    if (current == null) {
      return false;
    }
    return current.hasSameId(this);
  }

  //  /**
  //   * @return the session token for a user, if they are logged in.
  //   */
  //  public String getSessionToken() {
  //    return getState().sessionToken();
  //  }

  /* package */
  public boolean isCurrentUser()
  {
    synchronized (mutex) {
      ParseUser user = Parse.getCurrentUser();
      return user != null && user.getObjectId().equals(getObjectId());
    }
  }

  /**
   * Retrieves the username.
   */
  public String getUsername()
  {
    return getString(KEY_USERNAME);
  }

  /**
   * Sets the username. Usernames cannot be null or blank.
   *
   * @param username The username to set.
   */
  public void setUsername(String username)
  {
    put(KEY_USERNAME, username);
  }

  /* package for tests */ String getPassword()
  {
    return getString(KEY_PASSWORD);
  }

  /**
   * Sets the password.
   *
   * @param password The password to set.
   */
  public void setPassword(String password)
  {
    put(KEY_PASSWORD, password);
  }

  /**
   * Retrieves the email address.
   */
  public String getEmail()
  {
    return getString(KEY_EMAIL);
  }

  /**
   * Sets the email address.
   *
   * @param email The email address to set.
   */
  public void setEmail(String email)
  {
    put(KEY_EMAIL, email);
  }

  /**
   * Indicates whether this {@code ParseUser} was created during this session
   * through a call to
   * {@link #signUp()} or by logging in with a linked service such as Facebook.
   */
  public boolean isNew()
  {
    return getState().isNew();
  }

  /**
   * Signs up a new user. You should call this instead of {@link #save} for
   * new ParseUsers. This
   * will create a new ParseUser on the server, and also persist the session
   * on disk so that you can
   * access the user using Parse.getCurrentUser.
   * <p/>
   * A username and password must be set before calling signUp.
   * <p/>
   * your own threading.
   *
   * @throws ParseException Throws an exception if the server is
   * inaccessible, or if the username
   *                        has already
   *                        been taken.
   */
  public ParseUser signUp()
  throws ParseException
  {

    return ParseSyncUtils.signUp(this);
  }

  public static class State
    extends ParseObject.State
  {

    private final boolean isNew;

    private State(Builder builder)
    {
      super(builder);
      isNew = builder.isNew;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Builder newBuilder()
    {
      return new Builder(this);
    }

    @SuppressWarnings("unchecked")
    public Map<String, Map<String, String>> authData()
    {
      Map<String, Map<String, String>> authData =
        (Map<String, Map<String, String>>) get(KEY_AUTH_DATA);
      if (authData == null) {
        authData = new HashMap<>();
      }
      return authData;
    }

    public boolean isNew()
    {
      return isNew;
    }

    public static class Builder
      extends Init<Builder>
    {

      private boolean isNew;

      public Builder()
      {
        super("_User");
      }

      /* package */ Builder(State state)
      {
        super(state);
        isNew = state.isNew();
      }

      @Override
      public Builder self()
      {
        return this;
      }

      @Override
      public State build()
      {
        return new State(this);
      }

      @Override
      public Builder apply(ParseObject.State other)
      {
        isNew(((State) other).isNew());
        return super.apply(other);
      }

      public Builder isNew(boolean isNew)
      {
        this.isNew = isNew;
        return this;
      }

    }
  }
}

