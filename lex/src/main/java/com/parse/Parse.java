/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse;

import android.content.Context;
import android.util.Log;

import com.parse.controller.ParseCorePlugins;
import com.parse.http.ParseSyncUtils;
import com.parse.model.CurrentData;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;
import com.parse.operation.ParseFieldOperations;
import com.parse.rest.ParseRESTCommand;
import com.parse.utils.ParseFileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.config.ConfigDepot;
import cell411.utils.Util;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.func.Func0;
import cell411.utils.io.XLog;
import cell411.utils.reflect.XTAG;
import okhttp3.OkHttpClient;

public class Parse
{
  private static final XTAG TAG = new XTAG();
  public static final int LOG_LEVEL_VERBOSE = Log.VERBOSE;
  public static final int LOG_LEVEL_DEBUG = Log.DEBUG;
  public static final int LOG_LEVEL_INFO = Log.INFO;
  public static final int LOG_LEVEL_WARNING = Log.WARN;
  public static final int LOG_LEVEL_ERROR = Log.ERROR;
  //region LDS
  public static final int LOG_LEVEL_NONE = Integer.MAX_VALUE;
  final static Object lock = new Object();
  // 0 is uninitialized.
  // 1 is initializing
  // 2 is initialized.
  static final AtomicInteger mInitialized = new AtomicInteger(0);
  private static final int DEFAULT_MAX_RETRIES =
    ParseRESTCommand.DEFAULT_MAX_RETRIES;
  private static final Object MUTEX = new Object();
  //region ParseCallbacks
  private static final Object MUTEX_CALLBACKS = new Object();
  static File parseDir;
  static File cacheDir;
  static File filesDir;
  static Configuration mConfiguration;
  //endregion
  //region Server URL
  private static Context applicationContext;
  private static boolean isLocalDatastoreEnabled;
  static ObservableValueRW<LoginState> smLogState;
  static {
    smLogState=new ObservableValueRW<>(LoginState.Initializing);
  }

  String mFlavor;

  // Suppress constructor to prevent subclassing
  private Parse()
  {
    throw new AssertionError();
  }

  public static synchronized void initialize(Configuration configuration)
  {
    synchronized (mInitialized) {
      if (isInitialized()) {
        XLog.w(TAG, "Parse is already initialized");
        new Exception("Parse is already initialized! (Not Thrown)").printStackTrace();
        return;
      }
      mConfiguration = configuration;
      if (configuration == null)
        throw new NullPointerException("Cannot initialize parse from nothing!");

      ParseSyncUtils.registerParseSubclasses();

      checkCacheApplicationId();
      ParseFieldOperations.registerDefaultDecoders();
      mInitialized.compareAndSet(0, 1);
      CurrentData.loadInitialData();
      mInitialized.compareAndSet(1, 2);
      CurrentData.addCurrentUserObservers();
      if(CurrentData.getCurrentUser()==null){
        smLogState.set(LoginState.NoUserLoggedIn);
      } else {
        smLogState.set(LoginState.UserLoggedIn);
      }
    }
  }

  public static void setCurrentUser(ParseUser user){
    CurrentData.setCurrentUser(user);
  }
  public static ParseUser getCurrentUser()
  {
    if(!Parse.isInitialized())
      return null;
    if (Parse.getConfiguration() == null)
      return null;
    return CurrentData.getCurrentUser();
  }
  public static ParseUser reqCurrentUser()
  {
    return Util.req(getCurrentUser());
  }

  public static ObservableValueRW<LoginState> getLoginState()
  {
    return smLogState;
  }

  public static Collection<String> getObjectIds()
  {
    return new ArrayList<>(ParseObject.smCache.keySet());
  }

  public static int countObjects() {
    Integer res= getObjectIds().size();
    XLog.i(TAG, "Parse.countObjectIds() => %d",res);
    return res;
  }

  static class UserWaiter implements Callable<ParseUser>,
                                     Func0<Boolean>
  {

    @Override
    public ParseUser call()
    throws Exception
    {
      ThreadUtil.waitUntil(this, this, 5000);
      return getCurrentUser();
    }

    @Override
    public Boolean apply()
    {
      return CurrentData.getCurrentUser()!=null;
    }
  }
  static UserWaiter mCurWaiter = new UserWaiter();
  public static Future<ParseUser> getUserWaiter() {
    return ThreadUtil.submit(mCurWaiter);
  }
  public static boolean isInitializedOrInitializing() {
    synchronized (mInitialized) {
      return mInitialized.get()!=0;
    }
  }
  public static boolean isInitialized()
  {
    synchronized (mInitialized) {
      return mInitialized.get()==2;
    }
  }

  /**
   * Verifies that the data stored on disk for Parse was generated using the
   * same application that
   * is running now.
   */
  static void checkCacheApplicationId()
  {
    synchronized (MUTEX) {
      String applicationId = Parse.applicationId();
      if (applicationId != null) {
        File dir = ConfigDepot.getParseDir();
        dir = ConfigDepot.mkdirs(dir);
        File applicationIdFile = new File(dir,"applicationId");
        if(applicationIdFile.isDirectory()) {
          try {
            ParseFileUtils.deleteDirectory(dir);
          } catch (IOException e) {
            System.out.println(""+e);
          }
        }
        if (applicationIdFile.exists()) {
          // Read the file
          boolean matches = false;
          try {
            RandomAccessFile f = new RandomAccessFile(applicationIdFile, "r");
            byte[] bytes = new byte[(int) f.length()];
            f.readFully(bytes);
            f.close();
            String diskApplicationId =
              new String(bytes, StandardCharsets.UTF_8);
            matches = diskApplicationId.equals(applicationId);
            if(matches)
              return;
          } catch (IOException e) {
            // Hmm, the applicationId file was malformed or something. Assume it
            // doesn't match.
          }

          // The application id has changed, so everything on disk is invalid.
          try {
            ParseFileUtils.deleteDirectory(dir);
          } catch (IOException e) {
            // We're unable to delete the directory...
          }
        }

        try {
          FileOutputStream out = new FileOutputStream(applicationIdFile);
          out.write(applicationId.getBytes(StandardCharsets.UTF_8));
          out.close();
        } catch (IOException e) {
          System.out.println("e="+e);
          // Nothing we can really do about it.
        }
      }
    }
  }

  public static String applicationId()
  {
    Configuration config = reqConfiguration();
    return config.applicationId;
  }

  @Nonnull
  public static Configuration reqConfiguration()
  {
    return Objects.requireNonNull(mConfiguration);
  }

  /**
   * Returns the current server URL.
   */
  public static @Nonnull URL getURL()
  {
    URL res = reqConfiguration().mServerURL;
    return Objects.requireNonNull(res);
  }

  /**
   * Validates the server URL.
   *
   * @param serverURL The server URL to validate.
   * @return The validated server URL.
   */
  private static @Nullable URL validateServerUrl(@Nullable URL serverURL)
  {
    if(serverURL==null)
      return null;
    String stringURL = serverURL.toString();
    // Add an extra trailing slash so that Parse REST commands include
    // the path as part of the server URL (i.e. http://api.myhost.com/parse)
    if (!stringURL.endsWith("/")) {
      stringURL = stringURL + "/";
    }
    try {
      return new URL(stringURL);
    } catch ( Exception ex ) {
      throw Util.rethrow(ex);
    }
  }

  /**
   * Destroys this client and erases its local data store.
   * Calling this after {@link Parse#initialize} allows you to re-initialize
   * this client with a
   * new configuration. Calling this while server requests are in progress
   * can cause undefined
   * behavior.
   */
  public static void destroy()
  {
    ParseSyncUtils.unregisterParseSubclasses();
    ParseCorePlugins.get().reset();
  }

  /**
   * Used by Parse LiveQuery
   */
  public static void checkInit()
  {
    if (Parse.applicationId() == null) {
      throw new RuntimeException(
        "applicationId is null. " + "You must call Parse.initialize(Context)" +
          " before using the Parse library.");
    }
  }

  /**
   * Returns the level of logging that will be displayed.
   */
  public static int getLogLevel()
  {
    return XLog.getLogLevel();
  }

  /**
   * Sets the level of logging to display, where each level includes all
   * those below it. The default
   * level is {@link #LOG_LEVEL_NONE}. Please ensure this is set to
   * {@link #LOG_LEVEL_ERROR}
   * or {@link #LOG_LEVEL_NONE} before deploying your app to ensure no
   * sensitive information is
   * logged. The levels are:
   * <ul>
   * <li>{@link #LOG_LEVEL_VERBOSE}</li>
   * <li>{@link #LOG_LEVEL_DEBUG}</li>
   * <li>{@link #LOG_LEVEL_INFO}</li>
   * <li>{@link #LOG_LEVEL_WARNING}</li>
   * <li>{@link #LOG_LEVEL_ERROR}</li>
   * <li>{@link #LOG_LEVEL_NONE}</li>
   * </ul>
   *
   * @param logLevel The level of logcat logging that Parse should do.
   */
  public static void setLogLevel(int logLevel)
  {
    XLog.setLogLevel(logLevel);
  }

  public static Configuration getConfiguration()
  {
    return mConfiguration;
  }

  public static String clientKey()
  {
    return reqConfiguration().clientKey;
  }

  public static int getVersionCode()
  {
    return ConfigDepot.getVersionCode();
  }

  public static String getVersionName()
  {
    return ConfigDepot.getVersionName();
  }

  public static void addCurrentUserObserver(
    ValueObserver<? super ParseUser> observer)
  {
    CurrentData.addCurrentUserObserver(observer);
  }

  public static String getSessionToken()
  {
    return ParseSyncUtils.sessionToken;
  }

  @Nullable
  public static <X extends ParseObject> X getObject(String objectId)
  {
    return ParseObject.getIfExists(objectId);
  }

  @Nonnull
  public static <X extends ParseObject> X getOrStub(String objectId,
                                                    Class<X> type)
  {
    return ParseObject.createWithoutData(type, objectId);
  }

  /**
   * Represents an opaque configuration for the {@code Parse} SDK configuration.
   */
  public static final class Configuration
  {
    public final @Nonnull String applicationId;
    public final @Nonnull String clientKey;
    public final @Nonnull URL mServerURL;
    public final int maxRetries;
    public final @Nonnull String versionCode;
    public final @Nonnull String versionName;

    private Configuration(Builder builder)
    {
      this.applicationId = Util.req(builder.mApplicationId);
      this.clientKey = Util.req(builder.mClientKey);
      this.mServerURL = Util.req(builder.mServerURL);
      this.maxRetries = Util.req(builder.mMaxRetries);
      this.versionCode = Util.req(builder.mVersionCode);
      this.versionName = Util.req(builder.mVersionName);
    }

    /**
     * Allows for simple constructing of a {@code Configuration} object.
     */
    public static final class Builder
    {
      public String mVersionCode;
      public String mVersionName;
      private String mApplicationId;
      private String mClientKey;
      private URL mServerURL;
      private boolean mLocatDataStoreEnabled;
      private OkHttpClient.Builder mClientBuilder;
      private int mMaxRetries = DEFAULT_MAX_RETRIES;

      /**
       * Set the application id to be used by Parse.
       *
       * @param applicationId The application id to set.
       * @return The same builder, for easy chaining.
       */
      public Builder applicationId(String applicationId)
      {
        this.mApplicationId = applicationId;
        return this;
      }

      /**
       * Set the client key to be used by Parse.
       *
       * @param clientKey The client key to set.
       * @return The same builder, for easy chaining.
       */
      public Builder clientKey(String clientKey)
      {
        this.mClientKey = clientKey;
        return this;
      }

      /**
       * Set the server URL to be used by Parse.
       *
       * @param server The server URL to set.
       * @return The same builder, for easy chaining.
       */
      public Builder server(URL server)
      {
        this.mServerURL = validateServerUrl(server);
        return this;
      }

      /**
       * Enable pinning in your application. This must be called before your
       * application can use
       * pinning.
       *
       * @return The same builder, for easy chaining.
       */
      public Builder enableLocalDataStore()
      {
        mLocatDataStoreEnabled = true;
        return this;
      }

      private Builder setLocalDatastoreEnabled(boolean enabled)
      {
        mLocatDataStoreEnabled = enabled;
        return this;
      }

      /**
       * Set the {@link okhttp3.OkHttpClient.Builder} to use when
       * communicating with the Parse
       * REST API
       * <p>
       *
       * @param builder The client builder, which will be modified for
       *                compatibility
       * @return The same builder, for easy chaining.
       */
      public Builder clientBuilder(OkHttpClient.Builder builder)
      {
        mClientBuilder = builder;
        return this;
      }

      /**
       * Set the max number of times to retry Parse operations before deeming
       * them a failure
       * <p>
       *
       * @param maxRetries The maximum number of times to retry. <=0 to never
       *                  retry commands
       * @return The same builder, for easy chaining.
       */
      public Builder maxRetries(int maxRetries)
      {
        this.mMaxRetries = maxRetries;
        return this;
      }

      /**
       * Construct this builder into a concrete {@code Configuration} instance.
       *
       * @return A constructed {@code Configuration} object.
       */
      public Configuration build()
      {
        return new Configuration(this);
      }
      public void versionName(String versionName) {
        mVersionName=Util.req(versionName);
      }
      public void versionCode(String s) {
        mVersionCode=Util.req(s);
      }
    }
  }
}

