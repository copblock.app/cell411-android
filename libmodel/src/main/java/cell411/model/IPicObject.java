package cell411.model;

import android.graphics.Bitmap;

import java.net.URL;

import javax.annotation.Nonnull;

import cell411.utils.collect.ValueObserver;

public interface IPicObject
  extends IObject
{
  URL getAvatarUrl();

  boolean hasProfileImage();

  void addAvatarObserver(ValueObserver<Bitmap> observer);

  void removeAvatarObserver(ValueObserver<? super Bitmap> observer);

  Bitmap getAvatarPic();

  void setAvatarPic(@Nonnull Bitmap bitmap);
}
