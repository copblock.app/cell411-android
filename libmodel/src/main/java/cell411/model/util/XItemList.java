package cell411.model.util;

import java.util.ArrayList;

import cell411.model.IObject;
import cell411.utils.ViewType;

public class XItemList
  extends ArrayList<XItem>
{
  public boolean contains(String id)
  {
    for (XItem item : this) {
      if (item.getObjectId().equals(id)) {
        return true;
      }
    }
    return false;
  }

  public XItemList withViewType(ViewType viewType)
  {
    XItemList res = new XItemList();
    for (XItem item : this) {
      if (item.getViewType() == viewType) {
        res.add(item);
      }
    }
    return res;
  }

  public boolean addItems(Iterable<IObject> objects)
  {
    boolean res = false;
    for (IObject object : objects) {
      res = add(new XItem(object)) || res;
    }
    return res;
  }

}
