package cell411.model;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.model.ParseObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import cell411.enums.RequestType;

@ParseClassName("Request")
public class XRequest
  extends XObject
{
  private static final Set<String> smStatusSet =
    new HashSet<>(Arrays.asList("PENDING", "RESENT"));

  public static ParseQuery<XRequest> qAll(XUser user)
  {
    ParseQuery<XRequest> or = ParseQuery.or(qOwned(user), qReceived(user));
    or.whereContainedIn("status", smStatusSet);
    return or;
  }

  public static ParseQuery<XRequest> qOwned(XUser user)
  {
    return XRequest.q().whereEqualTo("owner", user);
  }

  public static ParseQuery<XRequest> qReceived(XUser user)
  {
    return XRequest.q().whereEqualTo("owner", user);
  }

  public static ParseQuery<XRequest> q()
  {
    return ParseQuery.getQuery(XRequest.class);
  }

  static String getObjectId(ParseObject object)
  {
    if (object == null)
      return null;
    else
      return object.getObjectId();
  }

  public boolean isFriendRequest()
  {
    return getCell() == null;
  }

  public XPublicCell getCell()
  {
    return (XPublicCell) getParseObject("cell");
  }

  public void setCell(XPublicCell cell)
  {
    put("cell", cell);
  }

  public boolean isCellRequest()
  {
    return getCell() != null;
  }

  public String getStatus()
  {
    return getString("status");
  }

  public void setStatus(String status)
  {
    put("status", status);
  }

  public boolean isSelfAlert()
  {
    return getOwner().equals(Parse.getCurrentUser());
  }

  public XUser getSentTo()
  {
    return (XUser) getParseUser("sentTo");
  }

  public void setSentTo(XUser user)
  {
    put("sentTo", user);
  }

  public RequestType getType()
  {
    return RequestType.valueOf(getString("type"));
  }

  @Override
  public String attrString()
  {
    XUser owner = (XUser) get("owner", false);
    XUser sentTo = (XUser) get("sentTo", false);
    String oid = owner == null ? null : owner.getObjectId();
    String sid = sentTo == null ? null : sentTo.getObjectId();
    return super.attrString() + ", owner=" + oid + ", sentTo=" + sid;
  }
}

