package cell411.model;

import androidx.annotation.NonNull;

import com.parse.ParseApp;

import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;

import cell411.config.UtilApp;
import cell411.utils.Util;

public abstract class ModelApp extends ParseApp {
  @NonNull
  public static ModelApp req() {
    return Util.req(opt());
  }
  public static ModelApp opt() {
    return (ModelApp) UtilApp.opt();
  }

  abstract public Collection<String> getMemberIds(XBaseCell xBaseCell);

}
