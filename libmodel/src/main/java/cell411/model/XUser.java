package cell411.model;

import static java.lang.Character.isDigit;

import android.graphics.Bitmap;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;
import com.parse.model.ParseUser;

import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.UrlUtils;
import cell411.utils.Util;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

@SuppressWarnings("unused")
@ParseClassName("_User")
public class XUser
  extends ParseUser
  implements IPicObject
{
  public static final XTAG TAG = new XTAG();
  static Comparator<XUser> mComparator =
    Comparator.comparing(XUser::getFirstName).thenComparing(XUser::getLastName)
      .thenComparing(XUser::getObjectId);
  static Comparator<String> smComparator =
    //new StringComparatorWithNumbers();
    String.CASE_INSENSITIVE_ORDER;
  private final ObservableValueRW<Bitmap> mAvatarPic =
    new ObservableValueRW<Bitmap>()
    {
      @Override
      public boolean set(final Bitmap newValue)
      {
        return super.set(newValue);
      }
    };
  boolean mAvatarRequested = false;

  public XUser()
  {
    super();
  }

  @Nullable
  public static XUser getCurrentUser()
  {
    return (XUser) Parse.getCurrentUser();
  }

  @Nonnull
  public static XUser reqCurrentUser()
  {
    return (XUser) Parse.reqCurrentUser();
  }

  @Nonnull
  public static XUser from(State state)
  {
    XUser object = new XUser();
    synchronized (object.mutex) {
      XLog.i(TAG, "reconstituted from state.  I feel funny");
      XLog.i(TAG, "I seem to be an XUser, though!");
      ParseObject.State newState;
      if (state.isComplete()) {
        newState = state;
      } else {
        newState = object.getState().newBuilder().apply(state).build();
      }
      object.setState(newState);
    }
    return object;
  }

  public static int staticNameCompare(XUser lhs, XUser rhs)
  {
    if (lhs == rhs) {
      return 0;
    } else if (lhs == null) {
      return -1;
    } else if (rhs == null) {
      return 1;
    } else {
      return lhs.nameCompare(rhs);
    }
  }

  public int nameCompare(XUser xUser)
  {
    int cmp = compare(getFirstName(), xUser.getFirstName());
    if (cmp == 0) {
      cmp = compare(getLastName(), xUser.getLastName());
    }
    if (cmp == 0) {
      cmp = getObjectId().compareTo(xUser.getObjectId());
    }
    return cmp;
  }

  public static int compare(String lhs, String rhs)
  {
    return smComparator.compare(lhs, rhs);
  }

  public String getFirstName()
  {
    return getString("firstName");
  }

  public void setFirstName(String firstName)
  {
    if (Util.isNoE(firstName)) {
      return;
    }
    put("firstName", firstName);
  }

  public String getLastName()
  {
    return getString("lastName");
  }  @Override
  public void setAvatarPic(@Nonnull final Bitmap bitmap)
  {
    mAvatarPic.set(bitmap);
  }

  public void setLastName(String lastName)
  {
    if (Util.isNoE(lastName)) {
      return;
    }
    put("lastName", lastName);
  }

  public static void nop()
  {
  }

  @Override
  public State getState()
  {
    return super.getState();
  }

  @Override
  public void setState(ParseObject.State newState)
  {
    super.setState(newState);
  }

  @Override
  public String attrString()
  {
    String result = "";
    result += ", name=" + getName();
    result += ", isCurrentUser: " + isCurrentUser();
    result += ", " + super.attrString();
    return result.substring(2);
  }

  public final boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }
    if (o instanceof ParseObject) {
      ParseObject other = (ParseObject) o;
      return other.hasSameId(this);
    } else if (o instanceof String) {
      return getObjectId().equals(o);
    } else {
      return false;
    }
  }

  public String getName()
  {
    if (isDataAvailable("firstName")) {
      String f = getFirstName();
      if (isDataAvailable("lastName"))
        return f + " " + getLastName();
      else
        return f;
    } else if (isDataAvailable("lastName")) {
      return getLastName();
    } else {
      return "I have no name";
    }
  }

  @Override
  public String getEmail()
  {
    String res = getString("email");
    if (res == null || res.isEmpty()) {
      String un = getUsername();
      if (un == null) {
        return "";
      }
      if (un.contains("@")) {
        res = un;
      } else {
        res = "";
      }
    }
    return res;
  }



  @Override
  public URL getAvatarUrl()
  {
    return UrlUtils.toURL(getString("avatarURL"));
  }

  @Override
  public boolean hasProfileImage()
  {
    return has("avatarURL");
  }

  @Override
  protected void finalize()
  throws Throwable
  {
    Reflect.announce("" + this);
    super.finalize();
  }

  public String getMobileNumber()
  {
    return getString("mobileNumber");
  }

  public void setMobileNumber(String newNumber)
  {
    put("mobileNumber", newNumber);
  }

  public ParseGeoPoint getLocation()
  {
    return getParseGeoPoint("location");
  }

  public void setLocation(ParseGeoPoint point)
  {
    put("location", point);
  }

  public boolean getPatrolMode()
  {
    return getBoolean("patrolMode");
  }

  public void setPatrolMode(boolean patrolMode)
  {
    put("patrolMode", patrolMode);
  }

  public String getPrivilege()
  {
    return getString("privilege");
  }

  public void setPrivilege(String privilege)
  {
    put("privilege", privilege);
  }

  public boolean getNewPublicCellAlert()
  {
    return getInt("newPublicCellAlert") != 0;
  }

  public void setNewPublicCellAlert(boolean newPublicCellAlert)
  {
    put("newPublicCellAlert", newPublicCellAlert);
  }

  public String getBloodType()
  {
    return getString("bloodType");
  }

  public String getAllergies()
  {
    return getString("allergies");
  }

  public String getOtherMedicalConditions()
  {
    return getString("otherMedicalConditions");
  }

  public String getEmergencyContactNumber()
  {
    return getString("emergencyContactNumber");
  }

  public String getEmergencyContactName()
  {
    return getString("emergencyContactName");
  }

  public Boolean getConsented()
  {
    return getBoolean("consented");
  }

  public void setConsented(Boolean consented)
  {
    put("consented", consented);
  }

  public ParseQuery<XUser> querySpamUsers()
  {
    ParseQuery<XUser> flaggedMe = q();
    flaggedMe.whereEqualTo("spamUsers", this);
    ParseRelation<XUser> flaggedByR = getRelation("spamUsers");
    ParseQuery<XUser> flaggedBy = flaggedByR.getQuery();
    return ParseQuery.or(Arrays.asList(flaggedBy, flaggedMe));
  }

  @Nonnull
  public static ParseQuery<XUser> q()
  {
    return ParseQuery.getQuery(XUser.class);
  }

  public ParseQuery<XUser> queryFriends()
  {
    ParseRelation<XUser> relation = getRelation("friends");
    return relation.getQuery();
  }

  public void setEmailVerified(final boolean verified)
  {
    put("emailVerified", verified);
  }

  public void deleteInBackground()
  {
    deleteInBackground(null);
  }

  static class StringComparatorWithNumbers
    implements Comparator<String>
  {

    @Override
    public int compare(String lhs, String rhs)
    {
      int i = 0;
      while (i < lhs.length() && i < rhs.length() &&
        compare(lhs.charAt(i), rhs.charAt(i)) == 0)
        i++;

      if (i == lhs.length()) {
        if (i == rhs.length()) {
          return 0;
        } else {
          return -1;
        }
      } else if (i == rhs.length()) {
        return 1;
      }
      char lc = lhs.charAt(i);
      char rc = rhs.charAt(i);
      if (i > 0 && (isDigit(lc) || isDigit(rc))) {
        if (isDigit(lhs.charAt(i - 1))) {
          --i;
        }
      }
      lc = lhs.charAt(i);
      rc = rhs.charAt(i);
      if (isDigit(lc) && isDigit(rc)) {
        int b = i;
        while (b >= 1 && isDigit(lhs.charAt(b - 1))) {
          --b;
        }
        int le = i;
        while (le < lhs.length() && isDigit(lhs.charAt(le))) {
          le++;
        }
        int re = i;
        while (re < rhs.length() && isDigit(rhs.charAt(re))) {
          re++;
        }
        String lStr = lhs.substring(b, le);
        String rStr = rhs.substring(b, re);
        int lVal = Integer.parseInt(lStr);
        int rVal = Integer.parseInt(rStr);
        return Integer.compare(lVal, rVal);
      } else if (isDigit(lc)) {
        return -1;
      } else if (isDigit(rc)) {
        return 1;
      } else {
        return Integer.compare(lc, rc);
      }
    }

    private int compare(final char ch1, final char ch2)
    {
      return Character.toUpperCase(ch1) - Character.toUpperCase(ch2);
    }
  }



  @Override
  public void addAvatarObserver(final ValueObserver<Bitmap> bitmapValueObserver)
  {
    mAvatarPic.addObserver(true, bitmapValueObserver);
  }

  @Override
  public void removeAvatarObserver(
    final ValueObserver<? super Bitmap> bitmapValueObserver)
  {
    mAvatarPic.removeObserver(bitmapValueObserver);
  }

  @Override
  public Bitmap getAvatarPic()
  {
    return mAvatarPic.get();
  }

}

