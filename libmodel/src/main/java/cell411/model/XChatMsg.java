package cell411.model;

import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.model.ParseFile;
import com.parse.model.ParseGeoPoint;

import cell411.utils.Util;

@ParseClassName("ChatMsg")
public class XChatMsg
  extends XObject
{

  public XChatMsg()
  {
    System.out.println("Created new Chat Message");
  }

  public static ParseQuery<XChatMsg> q()
  {
    return ParseQuery.getQuery(XChatMsg.class);
  }

  public XChatRoom getChatRoom()
  {
    return (XChatRoom) getParseObject("chatRoom");
  }

  public void setChatRoom(XChatRoom chatRoom)
  {
    put("chatRoom", chatRoom);
  }

  public String getText()
  {
    return getString("text");
  }

  public void setText(String text)
  {
    if (Util.isNoE(text)) {
      remove("text");
    } else {
      put("text", text);
    }
  }

  public ParseGeoPoint getLocation()
  {
    return getParseGeoPoint("location");
  }

  public void setLocation(ParseGeoPoint location)
  {
    if (location == null) {
      remove("location");
    } else {
      put("location", location);
    }
  }

  public ParseFile getImage()
  {
    return getParseFile("image");
  }

  public void setImage(ParseFile parseFile)
  {
    put("image", parseFile);
  }
}
