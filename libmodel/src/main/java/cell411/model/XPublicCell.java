package cell411.model;

import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.model.ParseGeoPoint;

import java.util.Collection;

import cell411.enums.CellCategory;
import cell411.enums.EntityType;
import cell411.utils.Util;
import cell411.utils.reflect.XTAG;

@ParseClassName("PublicCell")
public class XPublicCell
  extends XBaseCell
{
  public static final XTAG TAG = new XTAG();

  public XPublicCell()
  {
    //    XLog.i(TAG, "constructor");
  }

  @Override
  public boolean isPublic()
  {
    return true;
  }



  public static ParseQuery<XPublicCell> q()
  {
    return ParseQuery.getQuery(XPublicCell.class);
  }

  public String getDescription()
  {
    return getString("description");
  }

  public void setDescription(String description)
  {
    put("description", description);
  }

  public ParseGeoPoint getLocation()
  {
    return getParseGeoPoint("location");
  }

  public void setLocation(ParseGeoPoint parseGeoPoint)
  {
    if (parseGeoPoint == null) {
      remove("location");
    } else {
      put("location", parseGeoPoint);
    }
  }

  public CellCategory getCategory()
  {
    String category = getString("category");
    if (Util.isNoE(category)) {
      setCategory(CellCategory.None);
      return getCategory();
    } else {
      try {
        return CellCategory.forString(category);
      } catch (IllegalArgumentException iae) {
        return CellCategory.None;
      }
    }
  }

  /** @noinspection unused*/
  Collection<String> getMembers() {
    return mApp.getMemberIds(this);
  }

  public void setCategory(CellCategory category)
  {
    put("category", category.toString());
  }

  public boolean isVerified()
  {
    return getVerificationStatus() > 0;
  }

  final public int getVerificationStatus()
  {
    return getInt("verificationStatus");
  }

  final public void setVerificationStatus(int status)
  {
    put("verificationStatus", status);
  }

  @Override
  public EntityType getType()
  {
    return EntityType.PUBLIC_CELL;
  }

  @Override
  public String getEntityName()
  {
    return "PublicCell: " + getName();
  }
}

