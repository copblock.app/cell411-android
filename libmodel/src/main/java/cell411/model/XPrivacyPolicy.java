package cell411.model;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.concurrent.Future;

import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.concurrent.Task;
import cell411.utils.reflect.XTAG;

@ParseClassName("PrivacyPolicy")
public class XPrivacyPolicy
  extends XObject
{
  public static final XTAG TAG = new XTAG();

  public XPrivacyPolicy()
  {
    XLog.i(TAG, "constructor");
  }

  static XPrivacyPolicy getLatestCall()
  throws ParseException
  {
    assert !ThreadUtil.isMainThread();
    ParseQuery<XPrivacyPolicy> query =
      ParseQuery.getQuery(XPrivacyPolicy.class);
    query.orderByDescending("createdAt");
    query.setLimit(1);
    return query.getFirst();
  }

  static Future<XPrivacyPolicy> getLatestFuture()
  {
    Task<XPrivacyPolicy> callable =
      Task.forStatic(XPrivacyPolicy.class,
        XPrivacyPolicy.class,
        "getLatestCall");
    
    return ThreadUtil.submit(callable);
  }

  public String getTOSUrl()
  {
    return getString("tosUrl");
  }

  public String getPPUrl()
  {
    return getString("ppUrl");
  }
}
