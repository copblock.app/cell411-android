package cell411.json;

import java.util.Iterator;

public class TypedIterator<Type> implements Iterator<Type> {
  final Iterator<?> mIterator;
  final Class<Type> mType;

  public TypedIterator(Class<Type> type, Iterator<?> iterator) {
    mIterator = iterator;
    mType = type;
  }

  @Override
  public boolean hasNext() {
    return mIterator.hasNext();
  }

  public Object nextRaw() {
    return mIterator.next();
  }

  public Type next() {
    return mType.cast(nextRaw());
  }

  public TypedIterator<String> strings() {
    return new TypedIterator<String>(String.class, mIterator);
  }

  public TypedIterator<Integer> integers() {
    return new TypedIterator<>(Integer.class, mIterator);
  }

  public TypedIterator<Double> doubles() {
    return new TypedIterator<>(Double.class, mIterator);
  }

  public TypedIterator<JSONArray> arrays() {
    return new TypedIterator<>(JSONArray.class, mIterator);
  }

  public TypedIterator<JSONObject> objects() {
    return new TypedIterator<>(JSONObject.class, mIterator);
  }

  public String nextString() {
    return JSON.toString(nextRaw());
  }

  public Boolean nextBoolean() {
    return JSON.toBoolean(nextRaw());
  }

  public Double nextDouble() {
    return JSON.toDouble(nextRaw());
  }

  public Integer nextInteger() {
    return JSON.toInteger(nextRaw());
  }

  public Long nextLong() {
    return JSON.toLong(nextRaw());
  }

  public JSONArray nextArray() {
    return JSON.toJSONArray(nextRaw());
  }

  public JSONObject nextObject() {
    return JSON.toJSONObject(nextRaw());
  }
}
