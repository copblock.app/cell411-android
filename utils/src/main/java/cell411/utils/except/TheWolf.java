package cell411.utils.except;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import javax.annotation.Nonnull;

import cell411.ui.base.RealMessageForwarder;
import cell411.utils.OnCompletionListener;
import cell411.utils.concurrent.ThreadUtil;

public class TheWolf {
  static final private ArrayList<Runnable> mPending = new ArrayList<>();

  private static RealExceptionHandler smRealExceptionHandler;
  private static RealMessageForwarder smRealMessageForwarder;

  public static class PendingPoster extends HashSet<Runnable> implements Runnable {
    final private ArrayList<Runnable> mPayLoad;
    final private Iterator<Runnable> mIterator;
    int mIndex=0;
    {
      synchronized (mPending) {
        mPayLoad = new ArrayList<>(mPending);
        mPending.clear();
        mIterator=mPayLoad.iterator();
      }
    }
    PendingPoster() {

    }
    @Override
    public void run()
    {
      while(mIterator.hasNext()) {
        Runnable next = mIterator.next();
        if(next == null)
          continue;
        next.run();
      }
    }
  }
  public static void setRealExceptionHandler(RealExceptionHandler handler){
    smRealExceptionHandler=handler;
    ThreadUtil.onExec(new PendingPoster());
  }

  static {
    Thread.setDefaultUncaughtExceptionHandler(new Vincent());
  }

  public static void showAlertDialog(String title, String message, OnCompletionListener listener) {
    RealExceptionHandler app = smRealExceptionHandler;
    if (app != null) {
      app.realShowAlertDialog(title, message, listener);
      return;
    }
    new Jules(null, "showAlertDialog", message,
      null, listener).walkTheEarth();
  }

  public static void showToast(String message) {
    RealMessageForwarder app = smRealMessageForwarder;
    if (app != null) {
      app.realShowToast(message);
      return;
    }
    new Jules(null,"showToast", message,
      null, null).walkTheEarth();
  }

  public static void handleException(String message, Throwable throwable,
                                     OnCompletionListener listener, final boolean dialog) {
    RealMessageForwarder app = smRealMessageForwarder;
    if (app != null) {
      app.realHandleException(message, throwable, listener, dialog);
      return;
    }
    new Jules(null, "handleException",
      message, throwable, listener).walkTheEarth();
  }

  public static void showYesNoDialog(String title, String text,
                                     OnCompletionListener listener)
  {
    RealMessageForwarder app = smRealMessageForwarder;
    if (app == null) {
      new Jules(title, "showYesNoDialog", text, null, listener).walkTheEarth();
    } else {
      app.realShowYesNoDialog(title, text, listener);
    }
  }

  static class Vincent
    implements Thread.UncaughtExceptionHandler
  {
    public void uncaughtException(@Nonnull Thread t, @Nonnull Throwable e)
    {
      handleException("Uncaught exception in thread: " + t, e, null,
        false);
    }
  }

  private static class Jules implements Runnable
  {
    private final String mMethod;
    private final String mTitle;
    private final String mMessage;
    private final OnCompletionListener mListener;
    private final Throwable mThrowable;

    public Jules(
      String title,
      String method, String message,
      Throwable throwable,
      OnCompletionListener listener) {
      mTitle=title;
      mMethod = method;
      mMessage = message;
      mListener = listener;
      mThrowable = throwable;
    }

    @Override
    public void run() {
      if( smRealExceptionHandler ==null ) {
        walkTheEarth();
        return;
      }
      switch (mMethod) {
        default:
        case "showAlertDialog":
          showAlertDialog(mTitle, mMessage, mListener);
          break;
        case "showYesNoDialog":
          showYesNoDialog(mTitle, mMessage, mListener);
          break;
        case "showToast":
          assert mTitle == null;
          assert mListener == null;
          showToast(mMessage);
          break;
        case "handleException":
          assert mTitle==null;
          handleException(mMessage, mThrowable, mListener, false);
          break;
      }
    }

    public void walkTheEarth() {
      mPending.add(this);
    }
  }
}