package cell411.utils.except;

public class WrapperException extends RuntimeException {
  public WrapperException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
