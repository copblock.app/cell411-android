package cell411.utils.except;

import cell411.utils.OnCompletionListener;

public interface RealExceptionHandler
{
  void realShowAlertDialog(String title, String message, OnCompletionListener listener);

}
