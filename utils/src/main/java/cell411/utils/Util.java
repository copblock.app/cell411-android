package cell411.utils;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Looper;
import android.text.Editable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import cell411.config.ConfigDepot;
import cell411.utils.collect.LazyObject;
import cell411.utils.collect.Transformer;
import cell411.utils.except.WrapperException;
import cell411.utils.func.Func1;
import cell411.utils.io.IOUtil;
import cell411.utils.io.PrintString;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

@SuppressWarnings("unused")
public class Util {
  public static final XTAG TAG = new XTAG();
  public static final DialogInterface.OnClickListener mNullClickListener = Util::onClick;
  private final static boolean smDebugging = true;

  public static <T> boolean anyMatch(@Nonnull Iterable<T> iterable,
                                     @Nonnull Predicate<T> predicate) {
    for (T t : iterable) {
      if (predicate.test(t)) {
        return true;
      }
    }
    return false;
  }

  public static long unboxLong(@Nullable Object val, long def) {
    if (val == null) {
      return def;
    }
    if (val instanceof Long) {
      return (long) val;
    }
    if (val instanceof Number) {
      return longValue((Number) val);
    }
    // We are, at this point, expecting a class cast exception here.
    return (long) val;
  }

  public static long longValue(@Nullable Number number) {
    if(number==null)
      return 0;
    return number.longValue();
  }

  public static Object convert(@Nonnull Class<?> type,
                               @Nullable Object object) {
    if (object == null) {
      return null;
    }
    final Class<?> objType = object.getClass();
    if (type.isAssignableFrom(objType)) {
      return object;
    }
    if (type == String.class) {
      return String.valueOf(object);
    }
    if (type == Boolean.class) {
      return booleanValue(object, type);
    }
    if (Number.class.isAssignableFrom(type)) {
      return numberValue(object, type);
    }
    if (type.isEnum()) {
      return enumValue(object, type);
    }
    if (type.isArray()) {
      return arrayValue(type, object, objType);
    }
    return null;
  }

  private static Object[] arrayValue(@Nonnull Class<?> type,
                                     @Nullable Object object,
                                     @Nonnull Class<?> objType)
  {
    Class<?> cType = type.getComponentType();
    if (cType == null || object==null) {
      return null;
    }

    Object[] result;
    if (objType.isArray()) {
      Object[] arrayObj = (Object[]) object;
      Class<?> eleType = objType.getComponentType();
      assert eleType!=null;
      result = new Object[arrayObj.length];
      for (int i = 0; i < arrayObj.length; i++) {
        result[i] = convert(eleType, arrayObj[i]);
      }
    }
    if (Collection.class.isAssignableFrom(objType)) {
      Collection<?> collObj = (Collection<?>) object;
      result = new Object[collObj.size()];
      Iterator<?> iterator = collObj.iterator();
      for (int i = 0; i < result.length; i++) {
        if (!iterator.hasNext()) {
          break;
        }
        result[i] = iterator.next();
      }
      return result;
    }
    return null;
  }

  private static Enum<? extends Enum<?>> enumValue(Object object, Class<?> objType) {
    Enum<?>[] values = getEnumValues(objType);
    if (values == null) {
      return null;
    }
    if (Number.class.isAssignableFrom(objType)) {
      Number numObj = (Number) object;
      int intObj = numObj.intValue();
      for (Enum<?> value : values) {
        if (value != null && value.ordinal() == intObj) {
          return value;
        }
      }
    } else {
      String objString = String.valueOf(object);
      for (Enum<?> value : values) {
        if (value != null && value.name().equals(objString)) {
          return value;
        }
      }
      try {
        double objDouble = Double.parseDouble(objString);
        return enumValue(objDouble, Double.class);
      } catch (Exception ignored) {
      }
    }
    return null;
  }

  private static double numberValue(Object object, Class<?> objType) {
    if (objType == Boolean.class) {
      Boolean boolObj = (Boolean) object;
      return boolObj ? 0 : 1;
    } else if (objType.isEnum()) {
      Enum<?> enumObj = (Enum<?>) object;
      return enumObj.ordinal();
    } else {
      return Double.parseDouble(String.valueOf(object));
    }
  }

  @SuppressWarnings("rawtypes")
  private static Enum<?>[] getEnumValues(Class<?> objType) {
    try {
      Method getValues = objType.getDeclaredMethod("values");
      Object rawValues = getValues.invoke(null);
      if (rawValues != null) {
        Class valType = rawValues.getClass();
        Class eleType = valType.getComponentType();
        if (eleType != null && eleType.isEnum()) {
          return (Enum[]) rawValues;
        }
      }
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
    return null;
  }

  @Nullable
  public static <X> X unbox(@Nullable Object val, @Nullable X def) {
    if (val == null) {
      return def;
    } else {
      //noinspection unchecked
      return (X) val;
    }
  }

  private static boolean booleanValue(@Nullable Object val) {
    if (val == null) {
      return false;
    } else {
      return booleanValue(val, val.getClass());
    }
  }

  private static boolean booleanValue(Object object, Class<?> objType) {
    if (Number.class.isAssignableFrom(objType)) {
      Number numObj = (Number) object;
      return compare(numObj, 0) != 0;
    } else {
      return Boolean.parseBoolean(object.toString());
    }
  }

  public static int compare(Number lhs, Number rhs) {
    return Double.compare(lhs.doubleValue(), rhs.doubleValue());
  }

  public static <I, O> void transform(@Nonnull Iterator<I> input,
                                      @Nonnull Collection<O> output,
                                      @Nonnull Function<I, O> func) {
    addAll(output, transformer(input, func));
  }

  private static <E> void addAll(@Nonnull Collection<E> output, @Nonnull Iterable<E> input) {
    input.forEach(output::add);
  }

  public static <I, O> Transformer<I, O> transformer(@Nonnull Iterator<I> input,
                                                     @Nonnull Function<I, O> func) {
    return new Transformer<I, O>(input) {
      @Override
      public O transform(I next) {
        return func.apply(next);
      }
    };
  }

  public static <I> Iterable<I> combine(@Nonnull Iterable<I> it1, @Nonnull Iterable<I> it2) {
    return () -> combine(it1.iterator(), it2.iterator());
  }

  public static <I> Iterator<I> combine(@Nonnull Iterator<I> itr1, @Nonnull Iterator<I> itr2) {
    return new Iterator<I>() {
      @Override
      public boolean hasNext() {
        return itr1.hasNext() || itr2.hasNext();
      }

      @Override
      public I next() {
        return itr1.hasNext() ? itr1.next() : itr2.next();
      }
    };
  }

  public static String getClassName(@Nullable Object o) {
    if (o == null) {
      return Void.class.getName();
    } else {
      return o.getClass().getName();
    }
  }

  public static boolean isNoE(@Nullable Editable text) {
    return text == null || isNoE(text.toString());
  }

  public static boolean isNoE(@Nullable String str) {
    return str == null || str.trim().isEmpty();
  }

  @SafeVarargs
  public static <Type> Type nvl(Type... args) {
    for (Type arg : args) {
      if (arg != null) {
        return arg;
      }
    }
    return null;
  }

  public static boolean theGovernmentIsHonest() {
    return !theGovernmentIsLying();
  }

  public static boolean theGovernmentIsLying() {
    return System.currentTimeMillis() != 0x1;
  }

  public static <K, V> String stringify(@Nullable HashMap<K, V> map) {
    if (map == null) {
      return "null";
    }
    ArrayList<String> sb = new ArrayList<>();
    boolean first = true;
    sb.add("{\n");
    for (K k : map.keySet()) {
      if (first) {
        first = false;
      } else {
        sb.add(",\n");
      }
      String v = String.valueOf(map.get(k));
      sb.add(format("{ key: %s, val: %s }", k, v));
    }
    sb.add("\n{\n");
    return String.join("", sb);
  }

  // This is trivial, but avoids the warning about using the default locale.
  // What other locale would we use?
  public static String format(@Nullable String format, Object... args) {
    if (format == null) {
      return "(null)";
    }
    int count=escapes(format);
    if(count!=args.length) {
      XLog.w(TAG,"Warning:  format '%s' with %d args",
        format, args.length);
    }
    if (args.length == 0) {
      return format;
    }
    return String.format(format, args);
  }

  private static int escapes(@Nonnull String format)
  {
    int c=0;
    for(int i=0;i<format.length()-1;i++){
      if(format.charAt(i)=='%') {
        i++;
        if(format.charAt(i)!='%')
          c++;
      }
    }
    return c;
  }

  public static String repeat(String s, int n) {
    byte[] bs = s.getBytes();
    byte[] res = new byte[bs.length * n];
    for (int i = 0; i < res.length; i++) {
      res[i] = bs[i % bs.length];
    }
    return new String(res);
  }

  public static String reverseLines(String text) {
    Pattern pattern = Pattern.compile("\n");
    String[] parts = pattern.split(text);
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < parts.length; i++) {
      builder.append(parts[parts.length - i - 1]).append("\n");
    }
    return builder.toString();
  }

  public static <T> T[] reverse(T[] values) {
    T[] res = values.clone();
    for (int i = 0; i < res.length; i++) {
      T temp = res[i];
      res[i] = res[res.length - 1 - i];
      res[res.length - 1 - i] = temp;
    }
    return res;
  }

  @Nonnull
  public static String formatDate(Date date, boolean today) {
    return formatDate(date.getTime(), today);
  }

  public static void formatMap(PrintStream ps, Map<String, String> data) {
    int maxKeyLen = 0;
    for (Map.Entry<String, String> entry : data.entrySet()) {
      maxKeyLen = Math.max(maxKeyLen, entry.getKey()
        .length());
    }
    char[] padding = new char[maxKeyLen + 2];
    Arrays.fill(padding, ' ');
    for (Map.Entry<String, String> entry : data.entrySet()) {
      String key = entry.getKey();
      String val = entry.getValue();
      char[] keyBuf = new char[padding.length];
      key.getChars(0, key.length(), keyBuf, 0);
      int i = 0;
      while (i < key.length()) {
        keyBuf[i] = key.charAt(i);
        ++i;
      }
      while (i < padding.length) {
        keyBuf[i] = padding[i];
        ++i;
      }
      ps.print(keyBuf);
      ps.print(" => ");
      ps.println(val);
    }
  }

  @Nonnull
  public static String formatDate(long millis, boolean today) {
    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(millis);
    return formatDate(cal, today);
  }

  @Nonnull
  public static String formatDate(@Nonnull GregorianCalendar cal, boolean today) {
    GregorianCalendar dayStart = new GregorianCalendar();
    dayStart.setTimeInMillis(System.currentTimeMillis());
    dayStart.set(Calendar.HOUR, 0);
    dayStart.set(Calendar.MINUTE, 0);
    dayStart.set(Calendar.SECOND, 0);
    dayStart.set(Calendar.MILLISECOND, 0);
    String dayText;
    if (cal.getTimeInMillis() < dayStart.getTimeInMillis()) {
      int year = cal.get(Calendar.YEAR);
      int month = cal.get(Calendar.MONTH) + 1;
      int day = cal.get(Calendar.DAY_OF_MONTH);
      String at = "at";
      dayText = format("%04d-%02d-%02d %s", year, month, day, at);
    } else {
      dayText = "today at";
    }
    return dayText;
  }

  @Nonnull
  public static String formatDateTime(@Nullable Date createdAt) {
    if (createdAt == null) {
      return "Unknown Time";
    }
    return formatDateTime(createdAt.getTime());
  }

  @Nonnull
  public static String formatDateTime(long time) {
    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(time);
    return formatDateTime(cal);
  }

  @Nonnull
  public static String formatDateTime(@Nonnull GregorianCalendar cal) {
    String dayText = formatDate(cal, true);
    String timeText = formatTime(cal);
    return dayText + " " + timeText;
  }

  @Nonnull
  public static String formatTime(@Nonnull GregorianCalendar cal) {
    int hour = cal.get(Calendar.HOUR);
    int minute = cal.get(Calendar.MINUTE);
    int amPm = cal.get(Calendar.AM_PM);
    String ap = (amPm == 0) ? "am" : "pm";
    if (hour == 0) {
      hour = 12;
    }
    return format("%02d:%02d %s", hour, minute, ap);
  }

  public static String format(int resId, Object... args) {
    return format(getString(resId), args);
  }

  public static String getString(int resId) {
    return ConfigDepot.getString(resId);
  }

  public static String formatTime(Date date) {
    if (date == null) {
      return "Never";
    }
    return formatTime(date.getTime());
  }

  public static String formatTime(long millis) {
    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(millis);
    return formatTime(cal);
  }

//  @Nonnull
//  public static String getString(@StringRes int resId, Object... args) {
//    String string = getString(resId);
//    if (args == null || args.length == 0) return string;
//    return Util.format(string, args);
//  }

  public static boolean isCurrentThread(Looper looper) {
    return looper.getThread() == Thread.currentThread();
  }

  public static String getBaseName(URL url) {
    return getBaseName(url.getPath());
  }

  public static String getBaseName(String path) {
    int index = path.lastIndexOf('/');
    if (index == -1) {
      return path;
    }
    return path.substring(index + 1);
  }

  public static String getBaseName(URI uri) {
    return getBaseName(uri.getPath());
  }

  public static String join(String del, String part1, String ... parts) {
    return del+del+join(del,parts);
  }

  public static Integer nullCompare(Object o1, Object o2)
  {
    if(o1==null || o2==null)
      return (o1==null?-1:0)+(o2==null?1:0);
    else
      return null;
  }

  public static Integer lenCompare(String o1, String o2)
  {
    Integer res = nullCompare(o1,o2);
    if(res!=null)
      return res;
    res = compare(o1.length(),o2.length());
    if(res!=0)
      return res;
    return o1.compareTo(o2);
  }

  public static <T>
  int iteratorCompare(Iterator<T> i1,
                      Iterator<T> i2,
                      Comparator<T> c)
  {
    int res=0;
    while(res == 0 && i1.hasNext() && i2.hasNext()) {
      res=c.compare(i1.next(),i2.next());
    }
    if(res==0)
      res=boolCompare(i1.hasNext(), i2.hasNext());
    return res;
  }

  private static int boolCompare(boolean b1, boolean b2)
  {
    return (b1?1:0)+(b2?-1:0);
  }

  //  private static Context getContext() {
//    return BaseApp.get();
//  }

  static class ArrayIterator<X>
    implements Iterator<X>, Iterable<X>
  {
    X[] mArray;
    int mIdx=0;
    <Y extends X> ArrayIterator(Y[] array) {
      mArray=array;
    }
    @Nonnull
    @Override
    public Iterator<X> iterator()
    {
      return this;
    }

    @Override
    public boolean hasNext()
    {
      return mIdx < mArray.length;
    }

    @Override
    public X next()
    {
      return mArray[mIdx++];
    }
  }

  public static String join(String s, String[] letters) {
    return join(s, new ArrayIterator<>(letters));
  }

  public static String join(String s, Iterable<String> letters) {
    return String.join(s, letters);
  }

  @Nonnull
  public static String cleanFileName(@Nonnull String folderName) {
    char[] chars = new char[folderName.length()];
    folderName.getChars(0, chars.length, chars, 0);
    for (int i = 0; i < chars.length; i++) {
      char ch = chars[i];
      if (Character.isSpaceChar(ch) || Character.isISOControl(ch)) {
        ch = '_';
      }
      switch (ch) {
        case '/':
        case ':':
        case '\\':
          ch = '_';
        default:
          break;
      }
      chars[i] = ch;
    }
    folderName = new String(chars);
    return folderName;
  }

  public static void printStackTrace(Throwable e) {
    PrintString trace = new PrintString();
    e.printStackTrace(trace);
    // make sure it ends with a newline probably does, but ... the code
    // below will discard the last line of it is not terminated.
    trace.pl();
    Pair<File, OutputStream> p = ConfigDepot.getNextLogFile("ext",true);
    IOUtil.stringToStream(p.mVal2, String.valueOf(trace));
    Throwable t = e;
    System.err.print(trace);
    while(t.getCause()!=null){
      XLog.e(TAG, ""+t);
      t=t.getCause();
    }
    XLog.e(TAG, ""+t);
  }

  public static void onClick(DialogInterface dialog, int which) {
  }

  public static DialogInterface.OnClickListener nullClickListener() {
    return mNullClickListener;
  }

  public static <T, R> R castAndCall(Object object, Class<T> type, Function<T, R> function) {
    if (object != null && type.isAssignableFrom(object.getClass())) {
      return function.apply(type.cast(object));
    } else {
      return null;
    }
  }

  @Nonnull
  public static String stringify(Throwable t) {
    return t.getClass() + ": " + t.getMessage();
  }

  public static String sortableDate() {
    return sortableDate(new Date());
  }

  public static String sortableDate(Date date) {
    return sortableDate(date, true);
  }

  public static String sortableDate(Date date, boolean withTime) {
    return sortableDate(date.getTime(), withTime);
  }

  public static String sortableDate(long date, boolean withTime) {
    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(date);
    return sortableDate(cal, withTime);
  }

  private static String sortableDate(GregorianCalendar cal, boolean withTime) {
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH) + 1;
    int day = cal.get(Calendar.DAY_OF_MONTH);
    String at = "at";
    String dayText = format("%04d%02d%02d", year, month, day);
    if (withTime) {
      int hour = cal.get(Calendar.HOUR_OF_DAY);
      int min = cal.get(Calendar.MINUTE);
      int sec = cal.get(Calendar.SECOND);
      String timeText = format("-%02d%02d%02d", hour, min, sec);
      return dayText + timeText;
    }
    return dayText;
  }

  public static String isoDate() {
    return isoDate(Instant.now());
  }

  public static String isoDate(Instant instant) {
    String result = instant.toString();
    XLog.i(TAG, "Date: " + result);
    return result;
  }

  public static String isoDate(Date date) {
    return isoDate(date.toInstant());
  }

  public static Iterable<String> filter(Predicate<String> predicate, Iterable<String> asList) {
    Iterator<String> iterator = asList.iterator();
    final String[] next = new String[1];
    final Boolean[] hasNext = new Boolean[1];
    hasNext[0] = iterator.hasNext();
    if (hasNext[0]) {
      next[0] = iterator.next();
    }
    Iterator<String> filtered = new Iterator<String>() {
      public boolean hasNext() {
        return hasNext[0];
      }

      public String next() {
        if (!hasNext[0]) {
          throw new ArrayIndexOutOfBoundsException("Iterator over-extended");
        }
        String res = next[0];
        while (hasNext[0]) {
          hasNext[0] = iterator.hasNext();
          next[0] = iterator.next();
          if (predicate.test(next[0])) {
            break;
          }
        }
        return res;
      }
    };
    return () -> filtered;
  }

  public static String formatDataSize(int units, int each) {
    return formatDataSize(units * each);
  }

  public static String formatDataSize(int bytes) {
    String[] marker = {"b", "k", "M", "G"};
    int pos = 0;
    if (bytes > 1024) {
      pos++;
      bytes /= 1024;
    }
    if (bytes > 1024) {
      pos++;
      bytes /= 1024;
    }
    if (bytes > 1024) {
      pos++;
      bytes /= 1024;
    }
    return bytes + marker[pos];
  }

  public static String makeWords(String mc) {
    return makeWords(mc, new HashSet<>());
  }

  public static String makeWords(String mc, Set<String> exclude) {
    int start = 0;
    List<String> words = new ArrayList<>();
    String lc = mc.toLowerCase(Locale.ROOT);
    for (int i = 1; i < mc.length(); i++) {
      if (mc.charAt(i) != lc.charAt(i)) {
        words.add(mc.substring(start, i));
        start = i;
      }
    }
    if (words.isEmpty()) {
      return mc;
    }
    words.add(mc.substring(start));
    words.removeIf(exclude::contains);
    assert !words.isEmpty();
    return String.join(" ", words);
  }

  public static int constrain(int min, int val, int max) {
    return Math.max(min, Math.min(val, max));
  }

  public static List<Class<?>> loadClass(List<String> names) {
    return transform(names, Util::loadClass);
  }

  public static <I, O> ArrayList<O> transform(Iterable<I> input, Function<I, O> func) {
    ArrayList<O> output;
    if (input instanceof Collection) {
      // we can pre-size the array.
      output = new ArrayList<>(((Collection<?>) input).size());
    } else {
      output = new ArrayList<>();
    }
    transform(input, output, func);
    return output;
  }

  public static Class<?> loadClass(String name) {
    try {
      return Class.forName(name, true, null);
    } catch (ClassNotFoundException e) {
      Reflect.announce("Exception: %s", e);
      return null;
    }
  }

  public static <I, O> void transform(Iterable<I> input, Collection<O> output, Function<I, O> func) {
    addAll(output, transformer(input, func));
  }

  public static <I, O> Transformer<I, O> transformer(Iterable<I> input, Function<I, O> func) {
    return transformer(input.iterator(), func);
  }

  public static void nop() {
  }

  public static int max(int... is) {
    int res = 0;
    if (is.length > 0) {
      res = is[0];
      for (final int val : is) {
        if (res < val) {
          res = val;
        }
      }
    }
    return res;
  }

  static <N extends Number> Func1<N, Double> converter(Class<N> type) {
    try {
      Constructor<N> constructor = type.getConstructor(classArray(type));
      return (val) -> {
        try {
          return constructor.newInstance(val);
        } catch (IllegalAccessException | InstantiationException |
                 InvocationTargetException e) {
          throw Util.rethrow(e);
        }
      };
    } catch (NoSuchMethodException e) {
      throw Util.rethrow(e);
    }
  }

  static Class<?>[] classArray(Class<?>... cs) {
    return cs;
  }

  public static WrapperException rethrow(Throwable e) {
    if(smDebugging) {
      Util.printStackTrace(e);
    }
    return rethrow("", e);
  }

  public static WrapperException rethrow(String text, Throwable throwable) {
    if (throwable instanceof Error) {
      throw ((Error) throwable);
    } else if (throwable instanceof RuntimeException) {
      throw ((RuntimeException) throwable);
    } else {
      throw new WrapperException(text, throwable);
    }
  }

  public static boolean isNoE(final Uri uri) {
    if (uri == null) {
      return true;
    }
    return uri.toString().isEmpty();
  }

  @Nonnull
  static public String getTimeStr() {
    return "" + Instant.now().truncatedTo(ChronoUnit.SECONDS);
  }

  public static String getBaseName(final File file) {
    return file.getName();
  }

  public static void throwThreadDeath(final Throwable e) {
    if (!(e instanceof ThreadDeath)) {
      return;
    }
    throw (ThreadDeath) e;
  }


  public static boolean isNoE(Collection<?> factories) {
    return factories == null || factories.isEmpty();
  }

  public static String lc(String text) {
    if (text == null) {
      return null;
    } else {
      return text.toLowerCase(Locale.ROOT);
    }
  }

  public static String trim(String text) {
    if (text == null) {
      return null;
    } else {
      return text.trim();
    }
  }

  public static long millis(int i, TimeUnit unit) {
    return unit.toMillis(i);
  }

  public static String uc(String op) {
    if (op == null) {
      return null;
    } else {
      return op.toUpperCase(Locale.ROOT);
    }
  }


  public static <O, I> Iterator<O> transform(Iterator<I> iterator,
                                      Function<I, O> valueOf)
  {
    return new TransformIterator<>(iterator, valueOf);
  }

  public static <X>
  X req(X ref) {
    if(ref==null)
      throw new NullPointerException("Missing Required Object");
    return ref;
  }
  public static <X>
  X req(X ref, String msg) {
    if(ref==null)
      throw new NullPointerException(msg);
    return ref;
  }

  private static <X>
  X def(X obj, X msg) {
    return obj == null ? msg : obj;
  }


  public static <X>
  X getRef(WeakReference<X> ref, boolean req) {
    if(req) {
      return req(getRef(req(ref)));
    } else {
      return getRef(ref);
    }
  }

  public static <X>
  X getRef(AtomicReference<X> ref, boolean req) {
    if(req) {
      return req(getRef(req(ref,"Ref is null")),"Ref is empty");
    } else {
      return getRef(ref);
    }
  }

  public static <X>
  X getRef(LazyObject<X> ref, boolean req) {
    if(req) {
      return req(getRef(req(ref,"Ref is null")),"Ref is empty");
    } else {
      return getRef(ref);
    }
  }

  public static <X>
  X getRef(AtomicReference<X> ref) {
    return ref == null ? null : ref.get();
  }
  public static <X>
  X getRef(WeakReference<X> ref) {
    return ref == null ? null : ref.get();
  }
  public static <X>
  X getRef(Future<X> ref) {
    try {
      return ref.get();
    } catch (ExecutionException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
  public static <X>
  X getRef(X ref) {
    return ref;
  }
  public static <X>
  X getRef(LazyObject<X> ref) {
    return ref == null ? null : ref.get();
  }

  public static boolean isNoE(URL url) {
    return url == null || isNoE(url.toString());
  }

  static byte[] dashed_line;
  public static byte[] mkDashedLine()
  {
    if(dashed_line==null) {
      dashed_line = new byte[70];
      Arrays.fill(dashed_line, (byte) '-');
    }
    return dashed_line;
  }

  public static void pushCause(Throwable exception, Throwable created)
  {
    if (exception != null) {
      Throwable cause = exception.getCause();
      while (cause != null) {
        exception = cause;
        cause = exception.getCause();
      }
      exception.initCause(created);
    }
  }

  public static boolean isNoE(final Map<?,?> map)
  {
    return map == null || map.isEmpty();
  }

  /**
   * Handy function to get a loggable stack trace from a Throwable
   *
   * @param tr An exception to log
   */
  public static String getStackTraceString(Throwable tr)
  {
    if (tr == null) {
      return "";
    }

    // This is to reduce the amount of log spew that apps do in the non-error
    // condition of the network being unavailable.
    Throwable t = tr;
    while (t != null) {
      if (t instanceof UnknownHostException) {
        return "";
      }
      t = t.getCause();
    }

    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    tr.printStackTrace(pw);
    pw.flush();
    return sw.toString();
  }

  private static class TransformIterator<O,I>
    implements Iterator<O>
  {
    private final Iterator<I> mIterator;
    private final Function<I, O> mValueOf;

    public TransformIterator(Iterator<I> iterator, Function<I, O> valueOf)
    {
      mIterator = iterator;
      mValueOf = valueOf;
    }

    @Override
    public boolean hasNext()
    {
      return mIterator.hasNext();
    }

    @Override
    public O next()
    {
      return mValueOf.apply(mIterator.next());
    }
  }
}
