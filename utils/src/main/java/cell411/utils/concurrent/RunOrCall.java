package cell411.utils.concurrent;

import java.util.concurrent.Callable;

import cell411.utils.Util;

public interface RunOrCall<X>
  extends Runnable,
          Callable<X>
{
  @Override
  default void run() {
    try {
      X obj = call();
    } catch ( Exception ex ) {
      Util.printStackTrace(ex);
    }
  };
  @Override
  default X call() throws Exception {
    run();
    return null;
  };

  default Runnable asRunnable() {
    return this;
  }
  default Callable<X> asCallable() {
    return this;
  };
}
