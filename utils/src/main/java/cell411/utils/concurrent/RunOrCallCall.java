package cell411.utils.concurrent;

import java.util.concurrent.Callable;

import cell411.utils.except.WrapperException;

public class RunOrCallCall<X>
  implements RunOrCall<X>
{
  final Callable<X> mCallable;
  X mResult;
  private Throwable mThrowable;

  public RunOrCallCall(Callable<X> callable)
  {
    mCallable = callable;
  }

  @Override
  public void run()
  {
    try {
      call();
    } catch (Throwable throwable) {
      mThrowable = throwable;
    }
  }

  @Override
  public X call()
  throws Exception
  {
    mResult = mCallable.call();
    return mResult;
  }

  private Exception rethrow(Throwable throwable)
  {
    if (throwable instanceof RuntimeException) {
      throw (RuntimeException) throwable;
    }
    if (throwable instanceof Exception) {
      return (Exception) throwable;
    }
    if (throwable instanceof Error) {
      throw (Error) throwable;
    }
    throw new WrapperException(
      "What kind of throwable is not an exception, or an Error?", throwable);
  }

  @Override
  public Runnable asRunnable()
  {
    return this;
  }

  @Override
  public Callable<X> asCallable()
  {
    return this;
  }
}
