package cell411.utils.concurrent;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import javax.annotation.Nonnull;

import cell411.utils.Util;
import cell411.utils.func.Func0;
import cell411.utils.reflect.Reflect;

public class Caller<X>
  implements Func0<X>
{
  final Class<X> mResType;
  final Object mTarget;
  final Method mMethod;
  final Object[] mArgs;

  public Caller(Class<X> resType, Class<?> type, Object target, Method method,
                Object[] args)
  {
    mResType = resType;
    mTarget = target;
    mMethod = method;
    mArgs = args;
    assert ((target == null) == (isStatic(method)));
    assert Reflect.convertible(resType, method.getReturnType());
  }

  public static <X>
  Func0<X> forVirtual(@Nonnull Class<X> resType,
                      @Nonnull Object target,
                      @Nonnull String name,
                      Object ... args)
  {
    if(args!=null && args.length!=0) {
      throw new RuntimeException("Only Func0 implemented");
    }
    Class<?> type = target.getClass();
    Method method = Reflect.getMethod(type, target, name, Reflect.smNoArgs);
    return new Caller<>(resType,type,target,method, Reflect.smNoArgs);
  }

  public static <X> Caller<X> forStatic(
    @Nonnull Class<X> resType,
    @Nonnull Class<?> type,
    @Nonnull String name,
    Object... args)
  {
    Method method = Reflect.getMethod(type, null, name, args);
    assert Reflect.convertible(resType,method.getReturnType());
    if (args.length == 0) {
      return new Caller<>(resType, type, null, method, args);
    }
    throw new RuntimeException("Only Func0 implemented");
  }

  private boolean isStatic(Method method)
  {
    return Modifier.isStatic(method.getModifiers());
  }

  @Override
  public X apply()
  {
    try {
      Object res = mMethod.invoke(mTarget, mArgs);
      return mResType.cast(res);
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  @Nonnull
  public String toString()
  {
    String target = String.valueOf(mTarget);
    String name = mMethod.getName();
    String args = argsToString();
    return Util.format("%s.%s(%s)", target, name, args);
  }

  private String argsToString()
  {
    return Reflect.argsToString(mArgs);
  }
}
