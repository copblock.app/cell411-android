package cell411.utils.concurrent;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

import javax.annotation.Nonnull;

import cell411.utils.Util;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

final public class Task<X>
  implements RunOrCall<X>
{

  private static final XTAG TAG = new XTAG();
  final RunOrCall<X> mRunOrCall;

  private Task(RunOrCall<X> runOrCall)
  {
    mRunOrCall = runOrCall;
  }

  public static <X> Task<X> forVirtual(@Nonnull Class<X> resType,
                                   @Nonnull Object target,
                                   @Nonnull String name,
                                   Object... args)
  {
    Class<?> type = target.getClass();
    Method method = Reflect.getMethod(type, target, name, args);
    if (Reflect.isVoid(method.getReturnType())) {
      return new Task<>(new RunnableMethod<>(target, method, args));
    } else {
      return new Task<>(new CallableMethod<>(target,method,args));
    }
  }

  public static <X> Task<X> forRunnable(Runnable r, X o)
  {
    return new Task<>(new RunOrCallRun<>(r, o));
  }

  public static <X> Task<X> forCallable(Callable<X> c)
  {
    return new Task<>(new RunOrCallCall<>(c));
  }

  public static <X>
  Task<X> forStatic(
    @Nonnull Class<X> resType,
    @Nonnull Class<?> type,
    @Nonnull String name, Object... args)
  {
    Method method = Reflect.getMethod(type, null, name, args);
    if (Reflect.isVoid(method.getReturnType())){
      return new Task<>(new RunnableMethod<>(null, method, args));
    } else {
      return new Task<>(new CallableMethod<>(null, method, args));
    }
  }

  public static <X> Task<X> forRunOrCall(RunOrCall<X> roc)
  {
    return new Task<>(roc);
  }

  @Override
  public void run()
  {
    try (ThreadName threadName = new ThreadName("" + this)) {
      mRunOrCall.run();
    } catch ( Throwable t ) {
      throw Util.rethrow(t);
    }
  }

  @Override
  public X call()
  throws Exception
  {
    return mRunOrCall.call();
  }
}
