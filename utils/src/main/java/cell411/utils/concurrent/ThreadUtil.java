package cell411.utils.concurrent;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cell411.utils.Util;
import cell411.utils.func.Func0;
import cell411.utils.reflect.Reflect;

public class ThreadUtil
{
  static final private ScheduledThreadPoolExecutor mExec;
  static final private Thread mMainThread;
  static final private Handler mMain;

  static {
    mExec = new ScheduledThreadPoolExecutor(10);
    mMainThread = Looper.getMainLooper().getThread();
    mMain = new Handler(Looper.getMainLooper());
  }
  public static void onExec(RunOrCall<?> t)
  {
    onExec(Task.forRunOrCall(t));
  }
  public static void onExec(Task<?> t, long d)
  {
    mExec.schedule(t.asCallable(), d, TimeUnit.MILLISECONDS);
  }
  public static <X> Future<X> submit(Task<X> task) {
    return mExec.submit(task.asCallable());
  }
  public static <X> Future<X> submit(RunOrCall<X> roc)
  {
    if(roc instanceof Task<?>) {
      return submit((Task<X>)roc);
    } else {
      return submit(Task.forRunOrCall(roc));
    }
  }
  public static
  ScheduledFuture<?> scheduleAtFixedRate(Task<?> t,
                                         long delay,
                                         long gap)
  {
    return
      mExec.scheduleAtFixedRate(t.asRunnable(), delay, gap,
      TimeUnit.MILLISECONDS);
  }
  public static ScheduledFuture<?> scheduleAtFixedRate(Runnable r,
                                                       long delay,
                                                       long gap)
  {
    return scheduleAtFixedRate(Task.<Void>forRunnable(r,null), delay, gap);
  }
  public static ScheduledFuture<?> scheduleAtFixedRate(Callable<?> c,
                                                           long delay,
                                                           long gap)
  {
    return scheduleAtFixedRate(Task.forCallable(c), delay, gap);
  }

  public static boolean isMainThread()
  {
    return Thread.currentThread() == mMainThread;
  }

  public static Future<?> submit(Runnable r)
  {
    return submit(Task.<Void>forRunnable(r,null));
  }
  public static <X> Future<X> submit(Callable<X> c)
  {
    return submit(Task.forCallable(c));
  }
  public static void onExec(Callable<?> c)
  {
    onExec(c, 0);
  }
  public static void onExec(Runnable r)
  {
    onExec(r, 0);
  }
  public static void onExec(Task<?> t)
  {
    onExec(t,0);
  }
  public static void onExec(Callable<?> c, long delay)
  {
    onExec(Task.forCallable(c),delay);
  }

  public static void onExec(Runnable r, long delay)
  {
    onExec(Task.forRunnable(r,null), delay);
  }


  // This is the one true onMain.  All other onMains call this
  // one.
  public static void onMain(Task<?> t, long d)
  {
    mMain.postDelayed(t.asRunnable(), d);
  }

  public static void onMain(Task<?> t) {
    onMain(t, 0);
  }

  public static void onMain(Runnable r, long d)
  {
    if(r instanceof Task<?>) {
      onMain((Task<?>) r,d);
    } else {
      onMain(Task.forRunnable(r, null), d);
    }
  }

  public static void onMain(Callable<?> c, long d)
  {
    if(c instanceof Task<?>) {
      onMain((Task<?>) c, d);
    } else {
      onMain(Task.forCallable(c),d);
    }
  }

  public static void onMain(Callable<?> c)
  {
    onMain(c, 0);
  }

  public static void onMain(Runnable r)
  {
    onMain(r,0);
  }

  public static void waitUntil(Object o, Func0<Boolean> cond,
                               long interval)
  {
    Reflect.announce(Thread.currentThread()+":  sync on "+o);
    //noinspection SynchronizationOnLocalVariableOrMethodParameter
    synchronized (o) {
      while (!cond.apply()) {
//        Reflect.announce("Waiting");
        wait(o, interval);
      }
    }
  }
  public static void waitUntil(Object o, Func0<Boolean> cond,
                               long interval,
                               long timeout)
  {
    long deadline = absTime(timeout);
    Reflect.announce(Thread.currentThread()+":  sync on "+o);
    //noinspection SynchronizationOnLocalVariableOrMethodParameter
    synchronized (o) {
      while (!cond.apply()) {
        wait(o, interval);
        if(time()>deadline)
          break;
      }
    }
  }

  private static long time()
  {
    return System.currentTimeMillis();
  }

  private static long absTime(long timeout)
  {
    if(timeout<=0)
      return Long.MAX_VALUE;
    else
      return System.currentTimeMillis()+timeout;
  }

  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
  public static boolean wait(Object o, long timeout)
  {
    Reflect.announce(Thread.currentThread()+":  sync on "+o);
    long endTime = System.currentTimeMillis() + timeout;
    synchronized (o) {
      try {
        if (timeout < 0) {
          o.wait();
        } else {
          o.wait(timeout);
        }
        return true;
      } catch (InterruptedException e) {
        Util.printStackTrace(e);
        return false;
      }
    }
  }

  public static void sleep(long i)
  {
    if (i > 0)
      sleepUntil(System.currentTimeMillis() + i);
  }

  public static void sleepUntil(final long endTime)
  {
    long millis = System.currentTimeMillis();
    while (millis < endTime) {
      try {
        Thread.sleep(endTime - System.currentTimeMillis());
      } catch (InterruptedException ignored) {
      }
      millis = System.currentTimeMillis();
    }
  }

  public static void join(Thread thread)
  {
    join(thread, 0);
  }

  public static void join(Thread thread, long time)
  {
    long startTime = System.currentTimeMillis();
    try {
      thread.join(time);
    } catch (InterruptedException e) {
      Util.printStackTrace(e);
    }
  }

  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
  public static void notify(final Object o, boolean all)
  {
    Reflect.announce(Thread.currentThread()+":  sync on "+o);
    synchronized (o) {
      if (all) {
        o.notifyAll();
      } else {
        o.notify();
      }
    }
  }

  public static boolean wait(Object o)
  {
    return wait(o, -1);
  }

}