package cell411.utils.concurrent;

import java.lang.reflect.Method;

public class CallableMethod<X>
  implements RunOrCall<X>
{
  final Object mTarget;
  final Method mMethod;
  final Object[] mArgs;
  X mRes;
  private Throwable mThrowable;

  public CallableMethod(Object target, Method method, Object[] args)
  {
    mTarget = target;
    mMethod = method;
    mArgs = args;
  }

  @Override
  public void run()
  {
    try {
      mRes=call();
    } catch (Throwable t) {
      mThrowable=t;
    }
  }

  @Override
  public X call()
  throws Exception
  {
    Object res = mMethod.invoke(mTarget, mArgs);
    //noinspection unchecked
    Class<X> type = (Class<X>) mMethod.getReturnType();
    return type.cast(res);
  }
}
