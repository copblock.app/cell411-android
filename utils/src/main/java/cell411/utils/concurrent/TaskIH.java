package cell411.utils.concurrent;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

import cell411.utils.reflect.Reflect;

final public class TaskIH
  implements InvocationHandler
{
  static private final Class<?> mIHClass = Task.class;
  static private final ClassLoader mLoader = mIHClass.getClassLoader();
  static private final Class<?>[] mTypes =
    new Class<?>[]{Runnable.class, Callable.class};

  public Object invoke(Object proxy, Method method, Object[] args)
  {
    String name = method.getName();

    switch (name) {
      case "equals":
        return args[0] == this;
      case "hashCode":
        return 42;
      case "toString":
        return proxy.toString();
      default:
        break;
    }
    try {
      return method.invoke(proxy, method, args);
    } catch (RuntimeException re) {
      Reflect.announce("RuntimeException: " + re);
      throw re;
    } catch (Exception e) {
      Reflect.announce("Exception: " + e);
      throw new RuntimeException("exception in invokeed method", e);
    } catch (Error e) {
      Reflect.announce("Error: " + e);
      throw e;
    } catch (Throwable t) {
      Reflect.announce("Other Throwable?" + t);
      throw t;
    }
  }
}
