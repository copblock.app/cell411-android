package cell411.utils.io;

import static cell411.utils.io.XLog.Level.DEBUG;
import static cell411.utils.io.XLog.Level.ERROR;
import static cell411.utils.io.XLog.Level.INFO;
import static cell411.utils.io.XLog.Level.VERBOSE;
import static cell411.utils.io.XLog.Level.WARN;

import android.util.Log;

import cell411.utils.Util;
import cell411.utils.reflect.XTAG;

public final class XLog
{

  static final int LOG_ID_MAIN = 0;
  final static Throwable tr = null;
  final static Object[] args = null;
  private static Level smLogLevel = Level.SUPER_VERBOSE;

  private XLog()
  {
  }

  /**
   * Send a VERBOSE log message
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static int v(XTAG tag, String msg, Object... args)
  {
    return zPrintln(LOG_ID_MAIN, VERBOSE, tag, msg, args, tr);
  }


  /**
   * Send a VERBOSE log message and log the exception.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   * @param tr  An exception to log
   */
  public static int v(XTAG tag, String msg, Throwable tr)
  {
    return zPrintln(LOG_ID_MAIN, VERBOSE, tag,
      msg + '\n' + Util.getStackTraceString(tr), args, tr);
  }
  public static int i(XTAG tag, String msg, Object... args)
  {
    return zPrintln(LOG_ID_MAIN, INFO, tag, msg, args, tr);
  }


  /**
   * Send a DEBUG log message.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static int d(XTAG tag, String msg, Object... args)
  {
    return zPrintln(LOG_ID_MAIN, DEBUG, tag, msg, args, tr);
  }

  /**
   * Send a DEBUG log message and log the exception.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   * @param tr  An exception to log
   */
  public static int d(XTAG tag, String msg, Throwable tr)
  {
    return zPrintln(LOG_ID_MAIN, DEBUG, tag,
      msg + '\n' + Util.getStackTraceString(tr), args, tr);
  }

  /**
   * Send a INFO log message and log the exception.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   * @param tr  An exception to log
   */
  public static int i(XTAG tag, String msg, Throwable tr)
  {
    return zPrintln(LOG_ID_MAIN, INFO, tag,
      msg + '\n' + Util.getStackTraceString(tr), args, tr);
  }

  /**
   * Send a WARN log message.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static int w(XTAG tag, String msg, Object... args)
  {
    return zPrintln(LOG_ID_MAIN, WARN, tag, msg, args, tr);
  }

  /**
   * Send a WARN log message and log the exception.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   * @param tr  An exception to log
   */
  public static int w(XTAG tag, String msg, Throwable tr)
  {
    return zPrintln(LOG_ID_MAIN, WARN, tag, msg, args, tr);
  }

  /**
   * Send an ERROR log message.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static int e(XTAG tag, String msg, Object... args)
  {
    return zPrintln(LOG_ID_MAIN, ERROR, tag, msg, args, tr);
  }

  /**
   * Send an ERROR log message and log the exception.
   *
   * @param tag Used to identify the source of a log message.  It usually
   *            identifies
   *            the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   * @param tr  An exception to log
   */
  public static int e(XTAG tag, String msg, Throwable tr)
  {
    return zPrintln(LOG_ID_MAIN, ERROR, tag, msg, args, tr);
  }

  public static int getLogLevel() {
    return smLogLevel.ordinal();
  }

  public static void setLogLevel(int logLevel) {
    if(logLevel>=0 && logLevel <=7 ) {
      smLogLevel = Level.values()[logLevel];
    }
  }

  public enum Level
  {
    SUPER_VERBOSE, REALLY_VERBOSE, VERBOSE, DEBUG, INFO, WARN, ERROR, ASSERT
  }
  /**
   * Low-level logging call.
   *
   * @param priority The priority/type of this log message
   * @param tag      Used to identify the source of a log message.  It
   *                 usually identifies
   *                 the class or activity where the log call occurs.
   * @param msg      The message you would like logged.
   * @param tr       A related throwable
   * @return The number of bytes written.
   */
  private static int zPrintln(int bufID, Level priority, XTAG tag, String msg,
                             Object[] args, Throwable tr)
  {
    if (args != null && args.length > 0)
      msg = Util.format(msg, args);
    if (tr != null)
      msg = msg + "\n" + Util.getStackTraceString(tr);
    Log.println(priority.ordinal(), String.valueOf(tag), msg);
    return msg.length();
  }
}
