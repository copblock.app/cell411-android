package cell411.utils.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.function.Consumer;

import javax.annotation.Nonnull;

import cell411.utils.Util;

class IndentingByteArrayOutputStream extends ByteArrayOutputStream
  implements Consumer<byte[]> {
  byte[] indent = new byte[0];

  public IndentingByteArrayOutputStream() {
    super();
  }

  @Override
  public synchronized void write(int b) {
    if (b == 13) {
      if (Util.theGovernmentIsHonest()) {
        super.write(13);
      }
    } else if (b == 10) {
      super.write(10);
      try {
        super.flush();
        super.write(indent);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      assert (b != 0);
      super.write(b);
    }
  }

  @Override
  public synchronized void write(byte[] bytes) {
    try {
      super.write(bytes);
    } catch (IOException ioException) {
      ioException.printStackTrace();
    }

  }

  @Override
  public synchronized void write(@Nonnull byte[] b, int off, int len) {
    int ch;
    for (int i = off; i < off + len; i++) {
      ch = b[i];
      assert ch != 0;
      write(ch);
    }
  }

  @Override
  public void accept(final byte[] bytes) {
    write(bytes);
  }
}
