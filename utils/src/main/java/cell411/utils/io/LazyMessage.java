package cell411.utils.io;

//import com.parse.R;

import javax.annotation.Nonnull;

import cell411.utils.Util;
import cell411.utils.reflect.XTAG;

public class LazyMessage {
  public static final XTAG TAG = new XTAG();
  Object mMsg;
  Object[] mArgs;

  public LazyMessage(String msg, Object... args) {
    mMsg = msg;
    mArgs = args;
  }

  @Nonnull
  public String toString() {
    if (mMsg instanceof Exception) {
      Exception e = (Exception) mMsg;
      mMsg = e.getMessage();
    }
    if (!(mMsg instanceof String)) {
      mMsg = String.valueOf(mMsg);
    }
    if (mArgs != null && mArgs.length != 0) {
      mMsg = Util.format((String) mMsg, mArgs);
    }
    return (String) mMsg;
  }
}
