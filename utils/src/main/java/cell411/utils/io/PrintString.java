package cell411.utils.io;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import javax.annotation.Nonnull;

public class PrintString
  extends SimplePrintStream
  implements PrintHelper
{
  ArrayList<byte[]> mStack = new ArrayList<>();
  final ByteArrayOutputStream mBuf;

  public PrintString()
  {
    super(new ByteArrayOutputStream());
    mBuf = (ByteArrayOutputStream) out;
  }
  @Override
  public void write(int b)
  {
    super.write(b);
  }

  @Override
  public void write(byte[] buf)
  {
    super.write(buf);
  }

  @Override
  public void write(byte[] buf, int off, int len)
  {
    super.write(buf, off, len);
  }

  @Override
  public void write(String str)
  {
    super.write(str);
  }

  @Override
  public void write(int ch, boolean nl)
  {
    super.write(ch, nl);
  }

  @Override
  public void write(byte[] bs, boolean nl)
  {
    super.write(bs, nl);
  }

  @Override
  public void write(byte[] bs, int off, int len, boolean nl)
  {
    super.write(bs, off, len, nl);
  }

  public int size() {
    return mBuf.size();
  }

  @Nonnull
  public byte[] toByteArray() {
    return mBuf.toByteArray();
  }

  @Nonnull
  public String toString() {
    flush();
    return mBuf.toString();
  }

  @Nonnull
  public String reset() {
    String ret = out.toString();
    mBuf.reset();
    return ret;
  }

}
