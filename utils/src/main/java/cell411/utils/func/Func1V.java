package cell411.utils.func;

public interface Func1V<A1> {
  void apply(A1 a1);

  default <B1 extends A1> Func0V getClosure(B1 b1) {
    return () -> apply(b1);
  }
}
