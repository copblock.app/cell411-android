package cell411.utils.func;

import java.util.function.Function;

public interface Func1<R, A1> extends Function<A1, R> {
  @Override
  R apply(A1 t);

  default <B1 extends A1> Func0<R> getClosure(B1 b1) {
    return () -> apply(b1);
  }
}
