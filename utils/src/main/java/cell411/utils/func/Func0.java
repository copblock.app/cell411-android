package cell411.utils.func;

import java.util.function.Supplier;

public interface Func0<R>
  extends Supplier<R>
{

  class Impl<R> implements Func0<R> {
    Func0<R> mFunc0;
    Impl(Func0V func, R res){
      mFunc0=()->{func.apply();return res;};
    }
    Impl(Func0<R> func){
      mFunc0=func;
    }
    @Override
    public R apply()
    {
      return mFunc0.apply();
    }
  }
  static <R> Func0<R> create(Func0<R> func0)
  {
    return new Impl<>(func0);
  }
  static <R> Func0<R> create(Func0V func0V, R r)
  {
    return new Impl<>(func0V,r);
  }

  R apply();

  @Override
  default R get() {
    return apply();
  }
}
