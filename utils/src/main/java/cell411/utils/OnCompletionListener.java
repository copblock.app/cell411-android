package cell411.utils;

import java.util.concurrent.Callable;

import cell411.utils.func.Func1V;

public interface OnCompletionListener
  extends Func1V<Boolean>
{
  static boolean callDone(OnCompletionListener listener, final boolean b)
  {
    if(listener!=null)
      listener.done(b);
    return b;
  }

  void done(boolean success);

  default Runnable getRunnable(boolean success) {
    return getClosure(success);
  }

  @Override
  default void apply(Boolean aBoolean) {
    done(aBoolean);
  }
}
