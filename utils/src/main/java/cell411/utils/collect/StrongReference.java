package cell411.utils.collect;

import java.lang.ref.WeakReference;

public class StrongReference<X> extends WeakReference<X> {
  X mStrong;

  public StrongReference(X referant) {
    super(referant);
    mStrong = referant;
  }
}
