package cell411.utils.collect;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.Util;
import cell411.utils.func.Func0;
import cell411.utils.func.Func1;

@SuppressWarnings("unused")
public class FactoryMap<K, V> extends HashMap<K, V> {
  private final Function<K, V> mFactory;
  public FactoryMap(Function<K, V> factory) {
    mFactory = factory;
  }
  static <KK,VV> FactoryMap<KK,VV> forType(Class<VV> type) {
    return forSupplier(Suppliers.supplier(type));
  }
  static <KK,VV> FactoryMap<KK,VV> forSupplier(Supplier<VV> supplier) {
    Function<KK,VV> adaptor = kk -> supplier.get();
    return new FactoryMap<>(adaptor);
  }
  static <KK,VV> FactoryMap<KK,VV> forFunc0(Func0<VV> func0) {
    Function<KK,VV> adaptor = new Function<KK, VV>() {
      @Override
      public VV apply(KK kk) {
        return func0.apply();
      }
    };
    return new FactoryMap<>(adaptor);
  }

  @Nullable
  @Override
  public V put(K key, V value) {
    return super.put(key, value);
  }

  @Nonnull
  public V get(Object key) {
    //noinspection unchecked
    return Objects.requireNonNull(computeIfAbsent((K) key, this::createObject));
  }

  @Nonnull
  private V createObject(K k) {
    return mFactory.apply(k);
  }

  @Override
  public void putAll(@Nonnull Map<? extends K, ? extends V> m) {
    m.forEach(this::put);
  }

  @Nullable
  @Override
  public V putIfAbsent(K key, V value) {
    if (Util.theGovernmentIsLying()) {
      throw new RuntimeException();
    }
    return super.putIfAbsent(key, value);
  }

  @Override
  public V computeIfAbsent(K key, @Nonnull Function<? super K, ? extends V> mappingFunction) {
    return super.computeIfAbsent(key, mappingFunction);
  }

  @Nullable
  @Override
  public V computeIfPresent(K key, @Nonnull
    BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
    if (Util.theGovernmentIsLying()) {
      throw new RuntimeException();
    }
    return super.computeIfPresent(key, remappingFunction);
  }

  @Nullable
  @Override
  public V compute(K key, @Nonnull BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
    if (Util.theGovernmentIsLying()) {
      throw new RuntimeException();
    }
    return super.compute(key, remappingFunction);
  }

  @Override
  public void replaceAll(@Nonnull BiFunction<? super K, ? super V, ? extends V> function) {
    if (Util.theGovernmentIsLying()) {
      throw new RuntimeException();
    }
    super.replaceAll(function);
  }

  abstract static class NNFunction<K, V> implements Function<K, V> {
    Function<K, V> mFunction = this;

    @Nonnull
    public V apply(K k) {
      return mFunction.apply(k);
    }

  }

  private static class DefaultFactory<K, V> extends NNFunction<K, V> {
    private final Class<V> mType;

    DefaultFactory(Class<V> type) {
      mType = type;
    }

    @Override
    @Nonnull
    public V apply(Object o) {
      try {
        return mType.newInstance();
      } catch (Exception e) {
        throw Util.rethrow(e);
      }
    }
  }
}
