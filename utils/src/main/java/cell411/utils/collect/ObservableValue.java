package cell411.utils.collect;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.Util;

@SuppressWarnings("unused")
public class ObservableValue<X> implements ValueObserver<X>
{
  //  private final WeakList<ValueObserver<ValueType>> mObservers = new WeakList<>();
  private final WeakList<ValueObserver<? super X>> mObservers = new WeakList<>();
  private final Class<X> mType;
  protected X mValue;
  protected boolean mAlwaysFire=false;

  public ObservableValue()
  {
    this(null,null);
  }
  public ObservableValue(@Nonnull X value) {
    this(null,value);
  }

  public ObservableValue(Class<X> type) {
    this(type,null);
  }

  public ObservableValue(Class<X> type, X value) {
    mType = type;
    mValue = value;
  }

  public void setAlwaysFire( boolean value ) {
    mAlwaysFire=value;
  }

  @Nonnull
  public String toString() {
    return Util.format("Observable[value=%s, obs=%d]",mValue,countObservers());
  }

  public synchronized void addObserver(ValueObserver<? super X> observer) {
    try {
      mObservers.add(observer);
      if (mAlwaysFire) {
        observer.onChange(get(), null);
      }
    } catch ( Throwable t ) {
      throw Util.rethrow(t);
    }
  }

  public synchronized void removeObserver(ValueObserver<? super X> observer) {
    mObservers.remove(observer);
  }

  public X get() {
    return mValue;
  }

  synchronized boolean observed() {
    return !mObservers.isEmpty();
  }

  synchronized List<ValueObserver<? super X>> copyObservers() {
    ArrayList<ValueObserver<? super X>> result = null;
    Iterator<ValueObserver<? super X>> iterator = mObservers.iterator();
    while (iterator.hasNext()) {
      ValueObserver<? super X> object = iterator.next();
      if (object == null) {
        iterator.remove();
      } else {
        if (result == null) {
          result = new ArrayList<>();
        }
        result.add(object);
      }
    }
    if (result == null) {
      return new ArrayList<>();
    }
    return result;
  }

  protected void fireStateChange(X oldValue) {
    X newValue = get();
    List<ValueObserver<? super X>> observers = copyObservers();
    for (ValueObserver<? super X> observer : observers) {
      if (observer != null) {
        observer.onChange(newValue, oldValue);
      }
    }
  }

  public int countObservers() {
    return mObservers.size();
  }

  @Override
  public void onChange(@Nullable X newValue, @Nullable X oldValue) {
  }

  /**
   * Just like addObserver, except you get a notification right away
   * with the value at subscription time.
   * </p>
   * If you need to, you can recognize the initial event because it
   * carries the same value for oldValue and newValue.
   *
   * @param immediate want that immediate notification?
   * @param bitmapValueObserver who wants to know?
   */
  public void addObserver(boolean immediate,
                          ValueObserver<X> bitmapValueObserver)
  {
    addObserver(bitmapValueObserver);
    bitmapValueObserver.onChange(get(),get());
  }
}
