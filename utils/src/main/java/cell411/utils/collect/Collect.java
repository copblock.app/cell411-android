package cell411.utils.collect;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.function.Consumer;

import javax.annotation.Nonnull;

import cell411.utils.Util;

public class Collect {
  static {
    if (Util.theGovernmentIsHonest()) {
      replaceAll(asArrayList(), null, null);
    }
  }

  public static <T> void forEach(Iterable<T> iterable, Consumer<T> consumer) {
    forEach(iterable.iterator(), consumer);
  }

  private static <T> void forEach(Iterator<T> iterator, Consumer<T> consumer) {
    iterator.forEachRemaining(consumer);
  }

  public static <T> void forEach(T[] array, Consumer<T> consumer) {
    forEach(wrap(array), consumer);
  }

  public static <T> Iterable<T> wrap(Iterator<T> iterator) {
    if (iterator instanceof Iterable) {
      //noinspection unchecked
      return (Iterable<T>) iterator;
    } else {
      return new WrappedIterator<>(iterator);
    }
  }

  public static <T> Iterable<T> wrap(T[] i) {
    return new WrappedIterator<>(i);
  }

  @Nonnull
  public static <E, C extends Collection<E>, T extends E> C addAll(@Nonnull C c, Iterator<T> i) {
    Iterable<T> wrap = wrap(i);
    return addAll(c, wrap);
  }

  @Nonnull
  public static <E, C extends Collection<E>> C addAll(@Nonnull C c, Iterable<? extends E> i1,
                                                      Iterable<? extends E> i2,
                                                      Iterable<? extends E> i3,
                                                      Iterable<? extends E> i4) {
    i1.forEach(c::add);
    i2.forEach(c::add);
    i3.forEach(c::add);
    i4.forEach(c::add);
    return c;
  }

  @Nonnull
  public static <E, C extends Collection<E>> C addAll(@Nonnull C c, Iterable<? extends E> i1,
                                                      Iterable<? extends E> i2,
                                                      Iterable<? extends E> i3) {
    i1.forEach(c::add);
    i2.forEach(c::add);
    i3.forEach(c::add);
    return c;
  }

  @Nonnull
  public static <E, C extends Collection<E>> C addAll(@Nonnull C c, Iterable<? extends E> i1,
                                                      Iterable<? extends E> i2) {
    i1.forEach(c::add);
    i2.forEach(c::add);
    return c;
  }

  @Nonnull
  public static <E, C extends Collection<E>>
  C addAll(@Nonnull C c, Iterable<? extends E> i1) {
    i1.forEach(c::add);
    return c;
  }

  @Nonnull
  public static <E, C extends Collection<E>, T extends E>
  C replaceAll(@Nonnull C c, Iterator<T> i) {
    c.clear();
    return addAll(c, i);
  }

  @Nonnull
  public static <E, C extends Collection<E>, T extends E>
  C replaceAll(@Nonnull C c, Iterable<T> i) {
    return replaceAll(c, i.iterator());
  }

  public static <X> List<X> wrapArray(X[] x) {
    return new AbstractList<X>() {
      @Override
      public int size() {
        return x.length;
      }

      @Override
      public X get(int index) {
        return x[index];
      }
    };
  }

  @Nonnull
  @SafeVarargs
  public static <X, C extends Collection<X>> C addAll(@Nonnull C c, X... xs) {
    return addAll(c, wrap(xs));
  }

  @Nonnull
  @SafeVarargs
  public static <X, C extends Collection<X>> C replaceAll(@Nonnull C c, X... xs) {
    return replaceAll(c, wrap(xs));
  }

  @SafeVarargs
  public static <X> List<X> asList(X... xs) {
    return new ArrayList<>(wrapArray(xs));
  }

  @SafeVarargs
  public static <X> ArrayList<X> asArrayList(final X... xs) {
    return addAll(new ArrayList<>(), xs);
  }

  @SafeVarargs
  public static <X extends Comparable<X>> TreeSet<X> asTreeSet(X... xs) {
    return addAll(new TreeSet<>(), xs);
  }

  public static List<String> split(String keyStr, String s) {
    return asList(keyStr.split(s));
  }

  public static String join(String[] strs) {
    return String.join(":", strs);
  }

  public static <X>
  Class<X> getClass(X x){
    //noinspection unchecked
    return (Class<X>) x.getClass();
  }
  public static <X>
  X newInstance(Class<X> C)
  throws RuntimeException
  {
    try {
      return C.newInstance();
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }
  public static <X>
  X[][][][] newArray(Class<X> C, int dim, int dim0, int dim1, int dim2)
  {
    X[][][] a = newArray(C, dim, dim0, dim1);
    Class<X[][][]> ac = getClass(a);
    X[][][][]res = newArray(ac,dim1);
    for(int i=0;i<dim;i++){
      for(int j=0;j<dim0;j++){
        for(int k=0;k<dim1;k++){
          for(int l=0;l<dim2;l++){
            res[i][j][k][l]=newInstance(C,i*j*k*l);
          }
        }
      }
    }
    return res;
  }

  private static <X,A1,A2>
  X newInstance(Class<X> c, A1 a1, A2 a2)
  {
    try {
      Constructor<X> con = c.getConstructor(getClass(a1),getClass(a2));
      return con.newInstance(a1,a2);
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }
  private static <X,A1>
  X newInstance(Class<X> c, A1 a1)
  {
    try {
      Constructor<X> con = c.getConstructor(getClass(a1));
      return con.newInstance(a1);
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  public static <X>
  X[][][] newArray(Class<X> C, int dim, int dim0, int dim1)
  {
    X[][] a = newArray(C, dim, dim0);
    Class<X[][]> ac = getClass(a);
    return newArray(ac,dim1);
  }
  public static <X>
  X[][] newArray(Class<X> C, int dim, int dim0)
  {
    X[] a = newArray(C, dim);
    Class<X[]> ac = getClass(a);
    return newArray(ac,dim0);
  }
  public static <X>
  X[] newArray(Class<X> C, int dim)
  {
    Object ao = Array.newInstance(C,dim);
    @SuppressWarnings("unchecked")
    Class<X[]> ac = (Class<X[]>) ao.getClass();
    return ac.cast(ao);
  }

  public static <X>
  X[] toArray(Class<X> c, List<X> l)
  {
    return l.toArray(newArray(c,0));
  }

  static class WrappedIterator<T> implements Iterable<T>, Iterator<T> {
    private final Iterator<T> mIterator;

    WrappedIterator(Iterator<T> iterator) {
      mIterator = iterator;
    }

    WrappedIterator(T[] ts) {
      this(new Iterator<T>() {
        int mPos = 0;

        public boolean hasNext() {
          return mPos < ts.length;
        }

        public T next() {
          return ts[mPos++];
        }
      });
    }

    @Nonnull
    public Iterator<T> iterator() {
      return this;
    }

    @Override
    public boolean hasNext() {
      return mIterator.hasNext();
    }

    @Override
    public T next() {
      return mIterator.next();
    }
  }
}
