package cell411.utils.collect;

import java.util.function.Supplier;

import cell411.utils.func.Func0;
import cell411.utils.func.Func1;

public class LazyObject<X>
  implements Func0<X>
{
  final Supplier<X> mCreator;
  private final Class<X> mType;
  X mValue;

  private LazyObject(Class<X> type, Supplier<X> creator) {
    mType=type;
    mCreator=creator;
    mValue=null;
  }

  public LazyObject(Class<X> type, X value)
  {
    mType=type;
    mValue=value;
    mCreator=null;
  }

  public LazyObject(Class<X> type)
  {
    this(type, Suppliers.supplier(type));
  }
  public static <R>
  LazyObject<R> fromValue(Class<R> type, Supplier<R> sup)
  {
    return new LazyObject<>(type,sup);
  }
  // Note:  This will work for Func0, which extends Supplier.
  public static <R>
  LazyObject<R> fromSupplier(Class<R> type, Supplier<R> sup)
  {
    return new LazyObject<>(type,sup);
  }
  public static <R,A1>
  LazyObject<R> fromSupplier(Class<R> type, Func1<R,A1> sup, A1 arg)
  {
    return new LazyObject<>(type,sup.getClosure(arg));
  }
  public static <R>
  LazyObject<R> fromClass(Class<R> type)
  {
    return new LazyObject<R>(type);
  }

  public synchronized X apply() {
    if(mValue==null && mCreator !=null) {
      mValue=mCreator.get();
    }
    return mValue;
  }
}
