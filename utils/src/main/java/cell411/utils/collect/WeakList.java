package cell411.utils.collect;


import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.Nonnull;

import cell411.utils.Util;

// This is a simple list of WeakReferences to objects.  It wraps an ArrayList
// of weak references.  When an object is added, it is wrapped in a weak ref,
// and when it is queried, it dereferences the weak ref.
//
// It uses two functions, ref and deref, to do the transformations.
//
// It removes nulls frequently.  Items may be moved when this happens.  This
// is why it does not allow access by index.  Also, it uses identify, not equals,
// to compare for removal.  And when you remove an object, it removes every copy
// of that object from the list.

public class WeakList<Type> extends AbstractList<Type>
{
  ArrayList<WeakReference<Type>> mData = new ArrayList<>();

  @Override
  public Type get(int index)
  {
    return Util.getRef(mData.get(index));
  }

  @Nonnull
  @Override
  public Iterator<Type> iterator()
  {
    Iterator<WeakReference<Type>> itr = mData.iterator();
    return new Iterator<Type>()
    {
      @Override
      public boolean hasNext()
      {
        return itr.hasNext();
      }

      @Override
      public Type next()
      {
        WeakReference<Type> ref = itr.next();
        return ref.get();
      }

      @Override
      public void remove()
      {
        itr.remove();
      }
    };
  }

  @Override
  public void add(int index, Type element)
  {
    mData.add(index,new WeakReference<>(element));
  }

  @Override
  public Type remove(int index) {
    return Util.getRef(mData.get(index));
  }

  @Override
  public Type set(int index, Type obj) {
    return Util.getRef(mData.set(index, new WeakReference<>(obj)));
  }

  @Override
  public int size()
  {
    return mData.size();
  }
  //  final ArrayList<WeakReference<Type>> mData = new ArrayList<>();
//
//  @SuppressWarnings("unused")
//  public WeakList(Class<Type> type, Iterable<Type> input) {
//    addAll(input);
//  }
//
//  public synchronized void addAll(Iterable<Type> input) {
//    mData.addAll(Util.transform(input, WeakReference::new));
//  }
//
//  public int size() {
//    return mData.size();
//  }
//
//  public synchronized boolean deadRef(int pos) {
//    WeakReference<Type> ref = mData.get(pos);
//    return ref == null || ref.get() == null;
//  }
//
//  public synchronized void add(Type val) {
//    WeakReference<Type> newRef = ref(val);
//    for (int i = 0; i < mData.size() - 1; i++) {
//      if (deadRef(i)) {
//        if (newRef == null) {
//          newRef = mData.remove(mData.size() - 1);
//        }
//
//        mData.set(i, newRef);
//        newRef = null;
//      }
//    }
//    if (newRef != null) {
//      mData.add(newRef);
//    }
//  }
//
//  public synchronized Type remove(Type object) {
//    mData.removeIf((ref) ->
//    {
//      Type temp = deref(ref);
//      return temp == object || temp == null;
//    });
//    return object;
//  }
//
//  private WeakReference<Type> ref(Type val) {
//    return new WeakReference<>(val);
//  }
//
//  private Type deref(WeakReference<Type> ref) {
//    return ref == null ? null : ref.get();
//  }
//
//  public synchronized boolean isEmpty() {
//    remove(null);
//    return mData.isEmpty();
//  }
//
//  @Nonnull
//  @Override
//  public Iterator<Type> iterator() {
//    return new WeakListItr();
//  }
//
//  private class WeakListItr implements Iterator<Type> {
//    @Nonnull
//    final Iterator<WeakReference<Type>> mItr;
//    Type mNext;
//
//    WeakListItr() {
//      mItr = mData.iterator();
//      mNext = findNext();
//    }
//
//    @Override
//    public boolean hasNext() {
//      return mNext != null;
//    }
//
//    @Nonnull
//    public Type next() {
//      Type next = mNext;
//      mNext = findNext();
//      return next;
//    }
//
//    @Nullable
//    private Type findNext() {
//      Type result = null;
//      while (result == null && mItr.hasNext()) {
//        WeakReference<Type> ref = mItr.next();
//        if (ref != null) {
//          result = ref.get();
//        }
//      }
//      return result;
//    }
//  }
}
