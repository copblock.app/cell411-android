package cell411.utils.reflect;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

public class FileTree
{
  final Map<String,File> mFiles;
  final File mRoot;
  FileTree(@Nonnull File root) {
    mRoot=root;
    mFiles =new HashMap<>();
    fill(mRoot,"");
  }

  private void fill(@Nonnull final File file, String path)
  {
    String[] list = file.list();
    if(list==null)
      return;
    while(path.endsWith("/"))
      path.substring(0,path.length()-1);

    path=path+"/";
    for(String name : list ) {
      final File value = new File(file, name);
      final String key = path + name;
      mFiles.put(key, value);
      if(value.isDirectory()) {
        fill(value,key);
      }
    }
  }
}
