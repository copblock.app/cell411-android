package cell411.utils;

public interface ResultListener<R> {
  void result(R result, Throwable throwable);
}
