package cell411.utils;

import android.net.Uri;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

import okhttp3.HttpUrl;

@SuppressWarnings("unused")
public class UrlUtils {

  public static HttpUrl toHttpUrl(HttpUrl url) {
    return url;
  }

  public static HttpUrl toHttpUrl(String str) {
    return HttpUrl.get(str);
  }

  public static HttpUrl toHttpUrl(Uri uri) {
    return HttpUrl.get(uri.toString());
  }

  public static HttpUrl toHttpUrl(URI uri) {
    return HttpUrl.get(uri);
  }

  public static HttpUrl toHttpUrl(URL uri) {
    return HttpUrl.get(uri);
  }

  public static Uri toUri(HttpUrl url) {
    return url == null ? null : toUri(url.toString());
  }

  public static Uri toUri(String url) {
    return url == null ? null : Uri.parse(url);
  }

  public static Uri toUri(Uri uri) {
    return uri;
  }

  public static Uri toUri(URI uri) {
    return uri == null ? null : toUri(uri.toString());
  }

  public static Uri toUri(URL uri) {
    return uri == null ? null : toUri(uri.toString());
  }

  public static URI toURI(HttpUrl url) {
    if (url == null) {
      return null;
    }
    return toURI(url.toString());
  }

  public static URI toURI(String str) {
    try {
      return str == null ? null : new URI(str);
    } catch (URISyntaxException ex) {
      throw new RuntimeException("Converting '" + str + "'");
    }
  }

  public static URI toURI(Uri uri) {
    return toURI(uri.toString());
  }

  public static URI toURI(URI uri) {
    return uri;
  }

  public static URI toURI(URL url) {
    return url == null ? null : toURI(url.toString());
  }

  public static URL toURL(HttpUrl url) {
    return url == null ? null : url.url();
  }

  public static URL toURL(String str) {
    try {
      return str == null ? null : new URL(str);
    } catch (Exception e) {
      throw new RuntimeException("Bad URL: " + str + "\n" + e);
    }
  }

  public static URL toURL(Uri uri) {
    return uri == null ? null : toURL(uri.toString());
  }

  public static URL toURL(URI uri) {
    try {
      return uri == null ? null : uri.toURL();
    } catch (MalformedURLException e) {
      e.printStackTrace();
      throw new RuntimeException("Bad URL: " + uri + "\n" + e);
    }
  }

  public static URL toURL(URL url) {
    return url;
  }

  public static String urlEncode(String objectId) {
    try {
      return URLEncoder.encode(objectId, "UTF-8");
    } catch (UnsupportedEncodingException uee) {
      throw new RuntimeException("Error encoding url", uee);
    }
  }

  public static String urlEncode(Map<String, String> args) {
    ArrayList<String> parts = new ArrayList<>();
    for (String string : args.keySet()) {
      parts.add(string + "=" + urlEncode(args.get(string)));
    }
    return String.join("&", parts);
  }

  public static String getFileName(String url)
  {
    byte[] bytes = url.getBytes();
    int end=bytes.length-1;
    while(end>0 && bytes[end]=='/'){
      --end;
    }
    int beg=end;
    while(beg>0 && bytes[beg-1]!='/')
      --beg;
    return new String(bytes,beg,end-beg+1);
  }

  public static String getFileName(URL url)
  {
    return url==null?null:getFileName(url.getPath());
  }

  public static URL toURL(File path)
  {
    return path==null?null:toURL(path.toURI());
  }
}