package cell411;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.IconCompat;

import cell411.android.RingtoneData;
import cell411.config.UtilApp;
import cell411.model.ModelApp;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;

public abstract class LogicApp extends ModelApp {


  private RingtoneData mRingtoneData;

  @NonNull
  public static LogicApp req() {
    return Util.req(opt());
  }
  public static LogicApp opt() {
    return (LogicApp) ModelApp.opt();
  }

  /**
   * This is where wee set the referencee for UtilApp in the
   * very beginning.  So we should create any objects that
   * are going to want to call opt or req at this point, just
   * after theey have a way to reach us.
   *
   * @param base The new base context for this wrapper.
   */
  @Override
  protected void attachBaseContext(Context base) {
    ThreadUtil.onExec(()->{
//      System.out.println("Waiting for ready.");
      ThreadUtil.waitUntil(this, UtilApp::ready, 1000);
//      System.out.println("Waited for ready.");
      mRingtoneData = new RingtoneData();
    });
    super.attachBaseContext(base);
  }

  public abstract IconCompat getSmallIcon();

  public abstract Activity getCurrentActivity();

  public abstract SharedPreferences getAppPrefs();

  public abstract Class<? extends Activity> getActivityClass();

  public abstract int getNotificationWidth();

  public abstract int getLargeIcon();

  public abstract SharedPreferences getPrefs();

  public abstract RingtoneData getRingtoneData();

  public abstract Drawable getUserPlaceholder();

  public abstract int getPrimaryColor();

  public abstract int getNotificationHeight();


}
