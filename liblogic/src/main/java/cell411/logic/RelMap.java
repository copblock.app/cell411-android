package cell411.logic;

import java.util.TreeMap;
import java.util.function.Supplier;

import cell411.logic.rel.Key;
import cell411.logic.rel.Rel;

public class RelMap
  extends TreeMap<Key, Rel>
{

  public Rel get(Key key) {
    return computeIfAbsent(key, Rel::createInRelMap);
  }
}
