package cell411.logic;

import com.parse.Parse;
import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import cell411.logic.rel.AggRel;
import cell411.logic.rel.Rel;
import cell411.model.XRequest;
import cell411.model.XUser;

public class RequestWatcher
  extends Watcher<XRequest> implements MyObserver {
  private static final Set<String> smStatusSet = Collections.unmodifiableSet(
    new HashSet<>(Arrays.asList("PENDING", "RESENT")));
  private final Rel mOwned;
  private final Rel mReceived;
  private final AggRel mCounterParties;

  public RequestWatcher(RelationWatcher relationWatcher)
  {
    super(relationWatcher, "Request", XRequest.class);
    mOwned = relationWatcher.getOwnedRequests();
    mReceived = relationWatcher.getSentToRequests();
    mCounterParties = relationWatcher.getCounterParties();
    mIds.addRels(mOwned,mReceived);
    mIds.addObserver(this);
  }

  @Override
  public ParseQuery<XRequest> directQuery()
  {
    getOwned();
    getReceived();
    ParseQuery<XRequest> q1 = XRequest.q();
    XUser currentUser = XUser.reqCurrentUser();
    q1.whereEqualTo("owner", currentUser);

    ParseQuery<XRequest> q2 = XRequest.q();
    q2.whereEqualTo("sentTo", currentUser);
    ParseQuery<XRequest> q3 = ParseQuery.or(q1, q2);
    return q3.whereContainedIn("status", smStatusSet);
  }

  @Override
  public synchronized void onEvents(ObjectEvent<XRequest> event)
  {
    getOwned();
    getReceived();
    super.onEvents(event);
  }

  @Override
  synchronized void greetObject(XRequest po)
  {
    getOwned();
    getReceived();
    super.greetObject(po);
    XUser user = XUser.getCurrentUser();
    XUser cp;
    if(po.getOwner()==user) {
      mOwned.add(po);
      mCounterParties.add(po.getSentTo());
    }
    if(po.getSentTo()==user) {
      mReceived.add(po);
      mCounterParties.add(po.getOwner());
    }

  }

  public Rel getOwned()
  {
    assert getRelationWatcher().getOwnedRequests()==mOwned;
    assert getRelationWatcher()==LiveQueryService.req().getRelationWatcher();
    return mOwned;
  }

  public Rel getReceived()
  {
    assert getRelationWatcher().getSentToRequests()==mReceived;
    assert getRelationWatcher()==LiveQueryService.req().getRelationWatcher();
    return mReceived;
  }

  @Override
  public void update(MyObservable o, Object arg) {
    TreeSet<String> ids = new TreeSet<>(mIds.getRelatedIds());
    TreeSet<String> counter = new TreeSet<>();
    for(String id : ids) {
      XRequest req = Parse.getObject(id);
      assert req!=null;
    }
  }
}
