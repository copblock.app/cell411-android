package cell411.logic;

import com.parse.decoder.ParseDecoder;
import com.parse.model.ParseObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import cell411.json.JSONArray;
import cell411.json.JSONObject;
import cell411.logic.rel.BaseRel;
import cell411.logic.rel.Key;
import cell411.logic.rel.Rel;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

class Cracker {
  static final XTAG TAG = new XTAG();

  final ArrayList<String> counterParties;
  final Map<String, Date> dates;
  final Map<String, ParseObject> objs;
  final TreeMap<Key, Rel> rels;
  final ArrayList<String> removed;
  final RelationWatcher mWatcher;

  public Cracker(RelationWatcher watcher, Map<String, ?> data) {
    mWatcher = watcher;
    counterParties = getCounterParties(data);
    dates = getDates(data);
    objs = getObjs(data);
    TreeMap<String, List<String>> rawRels = getRels(data);
    rels = new TreeMap<>();
    for (String rawKey : rawRels.keySet()) {
      List<String> ids = rawRels.get(rawKey);
      Key key = Key.create(rawKey);
      Rel rel = mWatcher.getRel(key);
      rels.put(key, rel);
      rel.setRelatedIds(ids);
    }
    removed = getRemoved(data);
  }

  public static Map<String, Date> dateMap(Map<String, ?> dates) {
    Map<String, Date> res = new HashMap<>();
    for (String key : dates.keySet()) {
      Date val = objToDate(dates.get(key));
      if (val != null) res.put(key, val);
    }
    return res;
  }


  private static Date objToDate(Object obj) {
    if (obj == null) return null;
    if (obj instanceof Date) {
      return (Date) obj;
    }
    if (obj instanceof Number) {
      long val = ((Number) obj).longValue();
      return new Date(val);
    }
    return null;
  }

  public static TreeMap<String, List<String>> listMap(Map<String, ?> data) {
    if (data == null) {
      throw new NullPointerException("No data");
    }
    TreeMap<String, List<String>> res = new TreeMap<>();
    for (String key : new TreeSet<>(data.keySet())) {
      Object raw = data.get(key);
      List<?> objectList = objectList(raw);
      List<String> val = stringList(objectList);
      res.put(key, val);
    }
    return res;
  }

  private static List<?> objectList(Object o) {
    if (o == null) return null;
    if (o instanceof JSONArray) {
      o = ((JSONArray) o).asList();
    }
    if (o instanceof List) {
      return (List<?>) o;
    }
    return null;
  }

  private static List<String> stringList(Object o) {
    List<?> temp;
    if (o instanceof List) {
      temp = (List<?>) o;
    } else if (o instanceof JSONArray) {
      temp = ((JSONArray) o).asList();
    } else {
      throw new IllegalArgumentException("Bad List: " + o);
    }
    ArrayList<String> res = new ArrayList<>();
    for (Object obj : temp) {
      res.add(String.valueOf(obj));
    }
    return res;
  }

  public static Map<String, ?> objectMap(Map<String, ?> data, String key) {
    Map<String, ?> res = objectMap(data.remove(key));
//    XLog.i(TAG, "res=%s", res);
    return res;
  }

  private static ParseObject objToParse(Object o) {
    if (o instanceof ParseObject) {
      return (ParseObject) o;
    } else {
      return (ParseObject) ParseDecoder.get().decode(o);
    }
  }


  public static Map<String, ParseObject> parseMap(Map<String, ?> objs) {
    Map<String, ParseObject> res = new HashMap<>();
    for (String key : objs.keySet()) {
      ParseObject obj = objToParse(objs.get(key));

      if (obj != null) res.put(key, obj);
    }
    return res;
  }


  private static Map<String, ?> objectMap(Object obj) {
    if (obj == null) return null;
    Map<String, ?> map;
    if (obj instanceof JSONObject) {
      map = new HashMap<>(((JSONObject) obj).asMap());
    } else if (obj instanceof Map) {
      //noinspection unchecked
      map = (Map<String, ?>) obj;
    } else {
      String type = Reflect.getClass(obj).getName();
      throw new IllegalArgumentException("bad object map: type=" + type);
    }
    return map;
  }

  public static Map<String, Date> getDates(Map<String, ?> data) {
    return dateMap(objectMap(data, "dates"));
  }

  public static Map<String, ParseObject> getObjs(Map<String, ?> data) {
    return parseMap(objectMap(data, "objs"));
  }

  /**
   * @noinspection unchecked
   */
  public static ArrayList<String> getCounterParties(Map<String, ?> data) {
    return (ArrayList<String>) data.get("counterParties");
  }

  /**
   * @noinspection unchecked
   */
  public static ArrayList<String> getRemoved(Map<String, ?> data) {
    return (ArrayList<String>) data.remove("removed");
  }

  public static TreeMap<String, List<String>> getRels(Map<String, ?> data) {
    return listMap(objectMap(data, "rels"));
  }

  public BaseRel getRel(Key str) {
    Rel res = rels.get(str);
    assert res != null;
    return res;
  }
}
