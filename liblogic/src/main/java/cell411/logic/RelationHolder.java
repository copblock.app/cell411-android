// package cell411.logic;
//
// import java.util.Objects;
// import java.util.TreeMap;
// import java.util.function.Function;
//
// import cell411.logic.rel.AggregateRel;
// import cell411.logic.rel.BaseRel;
// import cell411.logic.rel.Key;
// import cell411.logic.rel.Rel;
// import cell411.model.XBaseCell;
// import cell411.utils.Util;
// import cell411.utils.io.XLog;
// import cell411.utils.reflect.XTAG;
//
// public class RelationHolder
// {
//
//
//   public RelationHolder()
//   {
//     friendKey = Key.create("_User:friends:_User:objectId:*");
//   }
//
//   public AggregateRel getAggRel(String str)
//   {
//     Key key = Key.create(str);
//     BaseRel rel = mRelMap.computeIfAbsent(key, this::mappingFunction);
//     if (rel instanceof AggregateRel)
//       return (AggregateRel) rel;
//     else
//       return null;
//   }
//
//   private BaseRel mappingFunction(Key s)
//   {
//     return BaseRel.create(s);
//   }
//
//   public AggregateRel getAggRel(Key key)
//   {
//     assert key.isAggregate();
//     BaseRel rel = mRelMap.computeIfAbsent(key, AggregateRel::new);
//     if (rel instanceof AggregateRel)
//       return (AggregateRel) rel;
//     else
//       return null;
//   }
//
//   public void clear()
//   {
//   }
//
//   public BaseRel getFriends()
//   {
//     return getRel(friendKey);
//   }
//
//   public BaseRel getRel(Key key)
//   {
//     BaseRel rel = mRelMap.computeIfAbsent(key, this::mappingFunction);
//     if(Util.theGovernmentIsLying() && key==rel.getKey()) {
//       XLog.e(TAG, "%s",key);
//       assert key == rel.getKey();
//     }
//     return rel;
//   }
//
//   public BaseRel getCellMembers(XBaseCell cell)
//   {
//     Key key = Key.create(cell.getClassName(), "members", "_User", "objectId",
//       cell.getObjectId());
//     BaseRel rel = getRel(key);
//     assert key.equals(rel.getKey());
//     assert key == rel.getKey();
//     return rel;
//   }
//
//   public Rel getOwnedPrivateCells()
//   {
//     return (Rel) BaseRel.create(smOwnedPrivateKey);
//   }
//
//   public Rel getOwnedPublicCells()
//   {
//     return (Rel) BaseRel.create(smOwnedPublicKey);
//   }
//
//   public Rel getJoinedPublicCells()
//   {
//     return (Rel) BaseRel.create(smJoinedPublicKey);
//   }
//
//   public Rel getJoinedPrivateCells()
//   {
//     return (Rel) BaseRel.create(smJoinedPrivateKey);
//   }
//
//   public Rel getBlocksMe()
//   {
//     return null;
//   }
//
//   public Rel getMyBlocks()
//   {
//     return null;
//   }
//
//   public Rel getCurrentUserRel()
//   {
//     return null;
//   }
//
//   public Rel getOwnedALerts()
//   {
//     return null;
//   }
//
//   public Rel getAudienceAlerts()
//   {
//     return null;
//   }
//
//   public static Key getSmOwnedPrivateKey() {
//     return smOwnedPrivateKey;
//   }
//
//   public Rel getOwnedRequests()
//   {
//     return null;
//   }
//
//   public Rel getReceivedRequests()
//   {
//     return null;
//   }
//
//   public Rel getCounterParties()
//   {
//     return mAggMap.getCounterParties();
//   }
//
//   public Rel getAllUsers()
//   {
//     return null;
//   }
//
// }
//