package cell411.logic;

import cell411.model.XAlert;
import cell411.model.XUser;
import cell411.utils.reflect.XTAG;
import com.parse.ParseQuery;

import javax.annotation.Nonnull;

public class AlertWatcher
  extends Watcher<XAlert>
{
  private static final XTAG TAG = new XTAG();

  public AlertWatcher(@Nonnull RelationWatcher relationWatcher)
  {
    super(relationWatcher, "Alerts", XAlert.class);
  }

  @Override

  public ParseQuery<XAlert> directQuery()
  {
    ParseQuery<XAlert> query1 = ParseQuery.getQuery(mType);
    XUser user = XUser.reqCurrentUser();
    query1.whereEqualTo("owner", user);
    ParseQuery<XAlert> query2 = ParseQuery.getQuery(mType);
    query2.whereEqualTo("audience", user);
    ParseQuery<XAlert> query3 = ParseQuery.getQuery(mType);
    query3.whereEqualTo("owner", user.getObjectId());
    //    ParseQuery<XAlert> query3 = ParseQuery.getQuery(mType);
    //    query3.whereEqualTo("audience", user.getObjectId());
    return ParseQuery.or(query1, query2, query3);
  }

}
