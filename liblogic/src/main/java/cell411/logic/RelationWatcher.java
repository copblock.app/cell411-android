package cell411.logic;

import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseQuery;
import com.parse.decoder.ParseDecoder;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;

import cell411.config.ConfigDepot;
import cell411.json.JSONObject;
import cell411.logic.rel.AggRel;
import cell411.logic.rel.BaseRel;
import cell411.logic.rel.Key;
import cell411.logic.rel.Rel;
import cell411.model.XChatRoom;
import cell411.model.XEntity;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.model.XRequest;
import cell411.model.XUpdate;
import cell411.model.XUser;
import cell411.utils.collect.FactoryMap;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.IOUtil;
import cell411.utils.io.TimeLog;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

// The data is pretty minimal, but if we come up without
// net, we need it.
public class RelationWatcher {
  protected static final AggFactory mAggFactory = new AggFactory();
  private static final XTAG TAG = new XTAG();
  private static final File mCacheFile = ConfigDepot.getJsonCacheFile("RelationWatcher");
  private static final File mBackupFile = new File(mCacheFile.getAbsolutePath() + ".bak");
  private static final TreeSet<String> smEmptyIdSet = new TreeSet<>();
  private static final Key smBlocksMe;
  private static final Key smBlockMy;
  private static final Key smCounterPartyKey;
  private static final Key smEntityKey;
  private static final Key smFriendsKey;
  private static final Key smPriJoinedKey;
  private static final Key smPriOwnedKey;
  private static final Key smPubJoinedKey;
  private static final Key smPubOwnedKey;
  private static final Key smReqOwnedKey;
  private static final Key smReqSentToKey;
  private static final Key smPrivateMemberBaseKey;
  private static final Key smPublicMemberBaseKey;
  private static final Key smToSaveKey;
  private static final Key smCurrentUser;
  private static final Key smAudienceAlertKey;
  private static final Key smOwnedAlertKey;

  private static final Key smChatRoomKey;
  private final static Key smAllCellMembersKey;
  private static final Key smAllCellsKey;

  static {
    smToSaveKey = Key.create("ToSave");
    smPublicMemberBaseKey = Key.create("PublicCell:members:_User:objectId:*");
    smPrivateMemberBaseKey = Key.create("PrivateCell:members:_User:objectId:*");
    smEntityKey = Key.create("Entities");

    smAudienceAlertKey = Key.create("_User:audienceOf:Alert:objectId:*");
    smOwnedAlertKey = Key.create("_User:ownerOf:Alert:objectId:*");

    smReqSentToKey = Key.create("_User:sentToOf:Request:objectId:*");
    smReqOwnedKey = Key.create("_User:ownerOf:Request:objectId:*");

    smPubOwnedKey = Key.create("PublicCell:ownerOf:_User:objectId:*");
    smPubJoinedKey = Key.create("PrivateCell:memberOf:_User:objectId:*");
    smPriOwnedKey = Key.create("PrivateCell:ownerOf:_User:objectId:*");
    smPriJoinedKey = Key.create("PrivateCell:memberOf:_User:objectId:*");

    smCurrentUser = Key.create("CurrentUser");
    smCounterPartyKey = Key.create("CounterParties");
    smFriendsKey = Key.create("_User:friends:_User:objectId:*");
    smBlockMy = Key.create("_User:objectId:_User:spamUsers:*");
    smBlocksMe = Key.create("_User:spamUsers:_User:objectId:*");
    smChatRoomKey = Key.create("_User:connectedTo:ChatRoom:objectId:*");
    smAllCellMembersKey = Key.create("AllCellMembers");
    smAllCellsKey = Key.create("AllCells");
  }

  private final TreeMap<String, Date> mDates = new TreeMap<>();
  private final AggRel mToSave = AggRel.create(smToSaveKey);
  private final boolean smShortCircuitCache = false;
  private final AggMap mAggMap = AggRel.getMap();
  private final RelMap mRelMap = Rel.getMap();

  public RelationWatcher() {
    Reflect.announce();
    long time = System.currentTimeMillis();
    BaseRel.hangFire(true);
    getAllCellMembers();
    getCounterParties();
    getEntities();
    getBlocksMe();
    getMyBlocks();
    getFriends();

    getJoinedPrivateCells();
    getOwnedPrivateCells();

    getJoinedPublicCells();
    getOwnedPublicCells();

    getOwnedRequests();
    getSentToRequests();
    getChatRooms();

    getToSave();

    assert mToSave.getKey().equals(smToSaveKey);
    mToSave.addRels(getAudienceAlerts());
    mToSave.addRels(getBlocksMe());
    mToSave.addRels(getChatRooms());
    mToSave.addRels(getEntities());
    mToSave.addRels(getFriends());
    mToSave.addRels(getJoinedPrivateCells());
    mToSave.addRels(getJoinedPublicCells());
    mToSave.addRels(getMyBlocks());
    mToSave.addRels(getOwnedAlerts());
    mToSave.addRels(getOwnedAlerts());
    mToSave.addRels(getOwnedPrivateCells());
    mToSave.addRels(getOwnedPrivateCells());
    mToSave.addRels(getOwnedRequests());
    mToSave.addRels(getSentToRequests());

    Rel userRel = getRel(smCurrentUser);
    userRel.add(XUser.reqCurrentUser());
    AggRel all = AggRel.create(Key.create("all"));
    BaseRel.hangFire(true);
    for (Key key : mAggMap.keySet()) {
      AggRel rel = mAggMap.get(key);
//      System.out.println("rel: " + rel);
      all.addRels(rel);
    }
    for (Key key : mRelMap.keySet()) {
      Rel rel = mRelMap.get(key);
//      System.out.println("rel: " + rel);
      all.addRels(rel);
    }
    BaseRel.hangFire(false);

  }

  private AggRel getChatRooms() {
    return getAggRel(smChatRoomKey);
  }

  private AggRel getToSave() {
    return getAggRel(smToSaveKey);
  }

  public AggRel getEntities() {
    return getAggRel(smEntityKey);
  }

  public Rel getRel(String str) {
    Key key = Key.create(str);
    Rel rel = getRel(key);
    XLog.i(TAG, "rel: " + rel);
    return rel;
  }

  public AggRel getAggRel(Key className) {
    return mAggMap.get(className);
  }

  public Rel getRel(Key key) {
    return mRelMap.get(key);
  }

  public void loadFromCache(final TimeLog log) {
    if (Parse.getCurrentUser() == null) return;
    if (smShortCircuitCache && getCacheFile().delete())
      Reflect.announce("Killed Cache File");
    if (!getCacheFile().exists()) return;
    final File cache = getCacheFile();
    if (cache != null) {
      try {
        String text = IOUtil.fileToString(cache);
        if (text != null) {
          text = text.trim();
          JSONObject json = new JSONObject(text);
          Object dec = ParseDecoder.get().decode(json);
          //noinspection unchecked
          TreeMap<String, ?> map = new TreeMap<>((Map<String, ?>) dec);
          Cracker cracker = new Cracker(this, map);
          //System.out.println(map);
          acceptData(cracker, log);
        }
      } catch (Exception e) {
        System.out.println("" + e);
        File fail = new File(cache.getAbsolutePath() + ".fail");
        if (!cache.renameTo(fail)) XLog.e(TAG, "Rename failed");
      }
    }
  }

  public void loadFromNet(TimeLog log) {
    if (Parse.getCurrentUser() == null) return;
//    log.println(Instant.now());
    TreeMap<String, Object> params = new TreeMap<>();
    params.put("dates", gatherDates(log));
    HashMap<String, ?> result = ParseCloud.run("relations", params);
//    log.println(result);
//    report(log);
    acceptData(new Cracker(this, result), log);
//    report(log);
  }

  public HashMap<String, Long> gatherDates(TimeLog log) {
    HashMap<String, Long> dates = new HashMap<>();
    for (String id : Parse.getObjectIds()) {
      ParseObject obj = Parse.getObject(id);
      if (obj == null) continue;
      fetchUnlessComplete(obj, log);
      stashDate(obj, dates);
      if (obj instanceof XRequest) {
        XRequest req = (XRequest) obj;
        XUser owner = req.getOwner();
        XUser sentTo = req.getSentTo();
        fetchUnlessComplete(owner, log);
        stashDate(owner, dates);
        fetchUnlessComplete(sentTo, log);
        stashDate(sentTo, dates);
      } else if (obj instanceof XEntity) {
        XEntity entity = (XEntity) obj;
        XChatRoom room = entity.getChatRoom();
        if (room != null) {
          fetchUnlessComplete(room, log);
          stashDate(room, dates);
        }
      }
    }
    return dates;
  }

  private void stashDate(ParseObject obj, HashMap<String, Long> dates) {
    String id = obj.getObjectId();
    Date date = obj.getUpdatedAt();
    if (date == null) {
      System.out.println(obj.toJSON().toString(2));
      date = obj.getUpdatedAt();
    }
    assert date != null;
    Long time = date.getTime();
    dates.put(id, time);
  }

  public void fetchUnlessComplete(ParseObject obj, TimeLog log) {
    if (checkComplete(obj, log))
      return;
    obj.fetch();
  }

  public boolean checkComplete(ParseObject obj, TimeLog log) {
    if (obj == null)
      return true;
    return obj.isComplete() && obj.getUpdatedAt() != null;
  }

  public void report(TimeLog ps) {
    String line = "----------------------------------------------";

    ps.pl(line);
    ps.pl("Relations To Save");
    ps.pl(line);
    for (Iterator<BaseRel> it = mToSave.relIterator(); it.hasNext(); ) {
      BaseRel rel = it.next();
      ps.printf("%15s: %s\n", rel.getKey(), rel);
      for (String id : rel.getRelatedIds()) {
        ParseObject obj = Parse.getObject(id);
        ps.printf("    %12s: %s\n", id, obj);
      }
    }
    ps.pl(line);
  }

  TreeMap<String, ParseObject> makeMap(String ignored) {
    return new TreeMap<>();
  }

  void acceptData(Cracker cracker, TimeLog log) {
    FactoryMap<String, TreeMap<String, ParseObject>> map =
      new FactoryMap<>(this::makeMap);
    synchronized (this) {
      for (Key key : cracker.rels.keySet()) {
        BaseRel rel = cracker.getRel(key);
        mToSave.addRels(rel);
        BaseRel myRel = getRel(key);
        for (String id : rel.getRelatedIds()) {
          ParseObject po = Parse.getObject(id);
          if (po == null)
            continue;
          if (checkComplete(po, log))
            continue;
          map.get(po.getClassName()).put(id, po);
        }
        if (rel == myRel)
          continue;
        myRel.setRelatedIds(rel.getRelatedIds());
      }
    }
    for (String type : map.keySet()) {
      TreeMap<String, ParseObject> inner = map.get(type);
      TreeSet<String> keys = new TreeSet<>(inner.keySet());
      ParseQuery<ParseObject> query = ParseQuery.getQuery(type);
      query.whereContainedIn("objectId", keys);
      List<ParseObject> list = ParseQuery.findFully(query);
      for (ParseObject obj : list) {
        assert checkComplete(obj, log);
      }
    }
  }

  public void timeRun(Runnable runnable) {
    long time = System.currentTimeMillis();
    BaseRel.hangFire(true);
    runnable.run();
    BaseRel.hangFire(false);
    String res = "Elapsed: " + (System.currentTimeMillis() - time);
    System.out.println(res);
  }

  public File getCacheFile() {
    return mCacheFile;
  }

  /**
   * @noinspection unused
   */
  public ParseQuery<XUpdate> createQuery(boolean dirty) {
    ParseQuery<XUpdate> q1 = XUpdate.q();
    if (dirty) {
      return q1.whereContainedIn("objectId", smEmptyIdSet);
    }
    XUser currentUser = XUser.reqCurrentUser();
    ParseQuery<XUpdate> q2 = XUpdate.q();
    q1.whereEqualTo("owningId", currentUser.getObjectId());
    q2.whereEqualTo("relatedId", currentUser.getObjectId());
    return ParseQuery.or(q1, q2);
  }

  public void saveToCache() {
    TreeMap<String, Long> dates = new TreeMap<>();
    TreeMap<String, ParseObject> objs = new TreeMap<>();
    TreeMap<String, TreeSet<String>> rels = new TreeMap<>();
    for (Iterator<BaseRel> it = mToSave.relIterator(); it.hasNext(); ) {
      BaseRel rel = it.next();
      Key key = rel.getKey();
      String keyStr = key.toString();
      assert rel != null;
      TreeSet<String> ids = new TreeSet<>(rel.getRelatedIds());
      rels.put(keyStr, ids);
      for (String id : ids) {
        ParseObject obj = Parse.getObject(id);
        if (obj == null) {
          XLog.w(TAG, "Warning:  id=%s obj=null", id);
          continue;
        }
        objs.put(id, obj);
        Date date = obj.getUpdatedAt();
        if (date == null)
          continue;
        Long time = date.getTime();
        dates.put(id, time);
      }
    }
    JSONObject data = new JSONObject();
    data.put("dates", new JSONObject(dates));
    JSONObject enc = new JSONObject();
    for (String key : objs.keySet()) {
      ParseObject po = objs.get(key);
      if (po == null) continue;
      JSONObject jo = po.toJSON();
      enc.put(key, jo);
    }
    data.put("objs", enc);
    data.put("rels", new JSONObject(rels));
    String text = data.toString(2);
    if (getCacheFile().exists()) {
      IOUtil.renameTo(getCacheFile(), getBackupFile());
    }
    IOUtil.stringToFile(getCacheFile(), text);
  }

  private File getBackupFile() {
    return mBackupFile;
  }

  public void clear() {
    mRelMap.clear();
    mAggMap.clear();
  }

  public Date getDate(String objectId) {
    return mDates.get(objectId);
  }

  public void setDate(String objectId, Date date) {
    mDates.put(objectId, date);
  }

  public Rel getFriends() {
    return mRelMap.get(smFriendsKey);
  }

  public void removeCellMember(XPublicCell cell, XUser user) {
    ThreadUtil.onExec(() -> {
      ParseRelation<XUser> members = cell.getRelation("members");
      members.remove(user);
    });
  }

  /**
   * @noinspection unused
   */
  public Collection<String> getMembers(XPublicCell cell) {
    Rel rel = getMemberRel(cell);
    return rel.getRelatedIds();
  }

  /**
   * @noinspection unused
   */
  public boolean setMembers(XPublicCell cell, Collection<String> ids) {
    Rel rel = getMemberRel(cell);
    return rel.setRelatedIds(ids);
  }

  public Rel getMemberRel(XPrivateCell cell) {
    return getRel(smPrivateMemberBaseKey + cell.getObjectId());
  }

  public Rel getMemberRel(XPublicCell cell) {
    return getRel(smPublicMemberBaseKey + cell.getObjectId());
  }

  public Rel getJoinedPrivateCells() {
    return getRel(smPriJoinedKey);
  }

  public Rel getOwnedPrivateCells() {
    return getRel(smPriJoinedKey);
  }

  public Rel getOwnedPublicCells() {
    return getRel(smPubOwnedKey);
  }

  public Rel getBlocksMe() {
    return getRel(smBlocksMe);
  }

  public Rel getMyBlocks() {
    return getRel(smBlockMy);
  }

  public AggRel getCounterParties() {
    return getAggRel(smCounterPartyKey);
  }

  public AggRel getAggRel(String entity) {
    return getAggRel(Key.create(entity, true));
  }

  public Rel getOwnedRequests() {
    return getRel(smReqOwnedKey);
  }

  public Rel getSentToRequests() {
    return getRel(smReqSentToKey);
  }

  public Rel getJoinedPublicCells() {
    return getRel(smPubJoinedKey);
  }

  public Rel getOwnedAlerts() {
    return getRel(smOwnedAlertKey);
  }

  public Rel getAudienceAlerts() {
    return getRel(smAudienceAlertKey);
  }

  public AggRel getAllCellMembers() {
    return getAggRel(smAllCellMembersKey);
  }

  private static class AggFactory implements Function<Key, AggRel> {
    @Override
    public AggRel apply(Key key) {
      return AggRel.create(key);
    }
  }

//  private class AllCellsObserver implements MyObserver {
//    AggRel mAllCellsRel = getAllCells();
//    AllCellsObserver() {
//
//    }
//
//    @Override
//    public void update(MyObservable o, Object arg) {
//      Set<String> ids = mAllCellsRel.getRelatedIds();
//      for(String id : ids) {
//        XBaseCell cell = Parse.getObject(id);
//        Key key=null;
//        if(cell instanceof XPrivateCell) {
//          key = Key.create( "PrivateCell:members:_User:objectId:"+id );
//        } else {
//          key = Key.create("PublicCell:members:_User:objectId:"+id);
//        }
//        AggRel members = getAggRel(key);
//        System.out.println(members);
//      }
//    }
//  }

//  public AggRel getAllCells() {
//    return getAggRel(smAllCellsKey);
//  }
}

