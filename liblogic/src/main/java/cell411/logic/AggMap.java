package cell411.logic;

import java.util.TreeMap;

import cell411.logic.rel.AggRel;
import cell411.logic.rel.Key;

public class AggMap
  extends TreeMap<Key, AggRel>
{
  public AggRel create(Key key) {
    return AggRel.create(key);
  }

  public AggRel get(Key key) {
    return computeIfAbsent(key, RelationWatcher.mAggFactory);
  }
}
