package cell411.logic.rel;

import com.parse.model.ParseObject;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nonnull;

import cell411.logic.MyObservable;
import cell411.utils.Util;
import cell411.utils.collect.Collect;
import cell411.utils.reflect.Reflect;

public abstract class BaseRel
  extends MyObservable
  implements Comparable<BaseRel>
{
  private static boolean smHangfire;
  private final TreeSet<String> mRelatedIds = new TreeSet<>();
  private final Key mKey;

  protected BaseRel(Key key)
  {
    mKey = key;
  }

  protected BaseRel(String str)
  {
    this(Key.create(str));
  }

  final static HashSet<BaseRel> smPending = new HashSet<>();
  public static void hangFire(boolean on) {
    if(smHangfire==on)
      return;
    smHangfire=!smHangfire;
    if(smHangfire)
      return;
    HashSet<BaseRel> pending = new HashSet<>(smPending);
    smPending.clear();
    for(BaseRel rel : pending)
      rel.fire();
    assert smPending.isEmpty();
  }

  public Iterator<String> idIterator() {
    return mRelatedIds.iterator();
  }
  public Iterator<BaseRel> relIterator() {
    return new NullIterator<>();
  }
  @Override
  public int hashCode()
  {
    return toString().hashCode();
  }

  @Nonnull
  @Override
  public String toString()
  {
    String key = mKey.toString();
    return Util.format("%s => %s", key, mRelatedIds);
  }

  public void dump(PrintStream out)
  {
    int lineWidth = 80;
    out.println(this);
    if (mRelatedIds.size() == 0) {
      out.println("  []");
      return;
    }
    // we sort first by length, so the last key is the longest
    String lastId = mRelatedIds.last();
    int maxWidth = lastId.length();
    int fieldWidth = maxWidth + 6;
    int columns = lineWidth / fieldWidth + 1;
    ArrayList<String[]> groups = new ArrayList<>();
    String fmt = "%-" + (1 + maxWidth) + "s";
    {
      String[] group = new String[columns];
      int gridx = 0;
      for (String rel : getRelatedIds()) {
        rel = Util.format(fmt, rel + ",");
        if (gridx == group.length) {
          groups.add(group);
          group = new String[columns];
          gridx = 0;
        }
        group[gridx++] = rel;
      }
      while (gridx < group.length) {
        group[gridx++] = Util.format(fmt, "");
      }
      groups.add(group);
    }
    {
      for (String[] group : groups) {
        String line = String.join(" ", group);

        out.println("  [" + line + "]");
      }
    }
  }

  /**
   * Get your very own copy of the set of related objects.
   *
   * @return A copy of the set of related Objects
   */
  public Set<String> getRelatedIds()
  {
    return Collections.unmodifiableSet(mRelatedIds);
  }

  public boolean add(String id)
  {
    return addAll(Collect.asList(id));
  }

  public boolean addAll(Collection<String> ids)
  {
    Set<String> newSet = new HashSet<>(mRelatedIds);
    newSet.addAll(ids);
    return setRelatedIds(newSet);
  }

  /**
   * Set the set of related objects to a new set.  This
   * actually updates the database, if it can.  For now,
   * that means if this object exactly mirrors an actual
   * relation in the physical database.  You can change
   * your friends list ... you cannot change the list of
   * people that have listed you as a friend.  It's not
   * a security thing, I just have not had time to write
   * it.
   *
   * @param set The new set of objectIds.
   */
  public synchronized boolean setRelatedIds(Collection<String> set)
  {
    Rel temp = Rel.smCounts.get(getKey());
    assert temp==null || temp==this;

    TreeSet<String> newSet = new TreeSet<>(set);
    if(newSet.equals(mRelatedIds))
      return false;
    boolean changed;
    if (newSet.isEmpty()) {
      changed = !mRelatedIds.isEmpty();
      mRelatedIds.clear();
    } else {
      boolean removed = mRelatedIds.retainAll(newSet);
      boolean added = mRelatedIds.addAll(newSet);
      changed = added || removed;
    }
    if (changed) {
      setChanged();
    }
    fire();
    return changed;
  }

  public void fire()
  {
    if(smHangfire) {
      smPending.add(this);
    } else {
      notifyObservers();
    }
  }

  public boolean remove(String id)
  {
    return removeAll(Collect.asList(id));
  }

  public boolean removeAll(Collection<String> ids)
  {
    Set<String> newSet = new HashSet<>(mRelatedIds);
    newSet.removeAll(ids);
    return setRelatedIds(newSet);
  }

  public boolean contains(String id)
  {
    return mRelatedIds.contains(id);
  }

  public void clear()
  {
    setRelatedIds(new HashSet<>());
  }

  public int compareTo(BaseRel o)
  {
    return getKey().compareTo(o.getKey());
  }

  public int getSize()
  {
    return mRelatedIds.size();
  }

//  public JSONArray toJSON()
//  {
//    Key key = getKey();
//    if (key == null) {
//      System.out.println("Its null");
//      return null;
//    }
//    JSONArray res = key.toJSON();
//    TreeSet<String> ids = new TreeSet<>(getRelatedIds());
//    res.put(ids.size());
//    res.putAll(ids);
//    return res;
//  }

  public Key getKey()
  {
    return mKey;
  }

  public void add(ParseObject po)
  {
    add(po.getObjectId());
  }

  private static class NullIterator<X> implements Iterator<X> {
    @Override
    public boolean hasNext() {
      return false;
    }

    @Override
    public X next() {
      throw new ArrayIndexOutOfBoundsException("Iterated NullIterator");
    }
  }
}
