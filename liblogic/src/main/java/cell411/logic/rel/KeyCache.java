package cell411.logic.rel;

class KeyCache
{
//  private static final Func2<Key,String,Boolean>  mMapper =
//    new Key.Mapper();
//  private static final class EntryList extends AbstractList<Entry>
//  {
//    ArrayList<Entry> mData = new ArrayList<>();
//
//    @Override
//    public Entry get(int index)
//    {
//      return mData.get(index);
//    }
//    public Entry set(int index, Entry value) {
//      return mData.set(index,value);
//    }
//    @Override
//    public int size()
//    {
//      return mData.size();
//    }
//
//    public int find(Entry entry)
//    {
//      return find(entry.mKey);
//    }
//
//    public int find(String key)
//    {
//      int res;
//      int i=0;
//      while(i<size()) {
//        Entry e = get(i);
//        res = compare(key,e.mKey);
//        if(res==0)
//          return i;
//        else if (res<0)
//          return i;
//        i++;
//      }
//      return i;
//    }
//    public void add(int index, Entry value)
//    {
//      mData.add(index, value);
//      Entry e;
//      int cmp;
//      {
//        e = mData.get(index);
//        cmp = compare(value, e.mKey);
//        System.out.printf("we placed %d (%s).  compare to %d (%s) == %d\n",
//          index, value.mKey, index, e.mVal, cmp);
//        // we just put it there.
//        assert (cmp == 0);
//      }
//      if (index > 0) {
//        e = mData.get(index - 1);
//        cmp = compare(value, e.mKey);
//        System.out.printf("we placed %d (%s).  compare to %d (%s) == %d\n",
//          index, value.mKey, index - 1, e.mKey, cmp);
//        assert (cmp > 0);
//      }
//      if (index + 1 < mData.size()) {
//        e = mData.get(index + 1);
//        cmp = compare(value, e.mKey);
//        System.out.printf("we placed %d (%s).  compare to %d (%s) == %d\n",
//          index, value.mKey, index + 1, e.mKey, cmp);
//        assert (cmp < 0);
//      }
//      System.out.printf("\n\n");
//    }
//  }
//  static {
//    EntryList list = new EntryList();
//    list.add(0,new Entry("e",true));
//
//    int idx = list.find("e");
//    assert idx==0;
//
//    idx=list.find("a");
//    assert idx==0;
//    list.add(idx,new Entry("a",true));
//
//    idx=list.find("b");
//    assert idx==1;
//    list.add(idx,new Entry("b",true));
//
//    idx=list.find("f");
//    assert idx==list.size();
//    list.add(idx, new Entry("f", true));
//
//    idx=list.find("x");
//    assert idx==list.size();
//    list.add(idx, new Entry("x", true));
//
//    idx=list.find("z");
//    assert idx==list.size();
//    list.add(idx, new Entry("z", true));
//
//    idx=list.find("y");
//    assert idx==list.size()-1;
//    list.add(idx, new Entry("y", true));
//
//  }
//  final EntryList mEntries = new EntryList();
//
//  static final KeyCache mCache = new KeyCache();
//
//  public boolean isEmpty()
//  {
//    return mEntries.isEmpty();
//  }
//
//  public Key get(String str)
//  {
//    return get(str,str.contains(":"));
//  }
//
//  static class Compare
//    implements Comparator<Entry>
//  {
//    public int compare(Entry o1, Entry o2) {
//      return KeyCache.compare(o1,o2);
//    }
//  }
//  public boolean containsKey(@Nonnull String key)
//  {
//    Iterator<Entry> iterator = mEntries.iterator();
//    int index = mEntries.find(key);
//    if(index==mEntries.size())
//      return false;
//    Entry entry = mEntries.get(index);
//    return compare(entry,key)==0;
//  }
//
//  public Key get(String key, boolean plain)
//  {
//    int idx = mEntries.find(key);
//    if(idx<mEntries.size()) {
//      Entry entry = mEntries.get(idx);
//      int cmp = compare(entry, key);
//      assert cmp <= 0;
//      if (cmp == 0) {
//        return entry.mVal;
//      }
//    }
//    mEntries.add(idx, new Entry(key, plain));
//    return mEntries.get(idx).mVal;
//  }
//
//  @Nullable
//  public Key remove(@Nullable Key key)
//  {
//    return remove(key.toString());
//  }
//  @Nullable
//  public Key remove(@Nullable String key)
//  {
//    int idx = mEntries.find(key);
//    Entry entry = mEntries.get(idx);
//    if(compare(entry,key)==0)
//      mEntries.remove(idx);
//    return entry.mVal;
//  }
//
//
//  public static class Entry
//    implements Comparable<Entry>
//  {
//    String mKey;
//    Key mVal;
//
//    public Entry(String key, boolean plain)
//    {
//      mKey=key;
//      mVal=mMapper.apply(key,plain);
//    }
//
//    public int compareTo(String key) {
//      return compare(this,key);
//    }
//    @Override
//    public int compareTo(Entry o)
//    {
//      return compare(this,o);
//    }
//  }
//  static int compare(KeyCache kc1, KeyCache kc2) {
//    Integer res = Util.nullCompare(kc1,kc2);
//    if(res!=null)
//      return res;
//    res=Util.compare(kc1.size(),kc2.size());
//    if(res!=0)
//      return res;
//    Iterator<Entry> i1 = kc1.mEntries.iterator();
//    Iterator<Entry> i2 = kc2.mEntries.iterator();
//    return Util.iteratorCompare(i1,i2,KeyCache::compare);
//  }
//  static int compare(Entry o1, String o2) {
//    if(o1==null)
//      return -1;
//    return compare(o1.mKey, o2);
//  }
//  static int compare(Entry o1, Entry o2) {
//    Integer res = Util.nullCompare(o1,o2);
//    if(res!=null)
//      return res;
//    return compare(o1.mKey,o2.mKey);
//  }
//  private static int compare(String o1, String o2)
//  {
//    Integer res = Util.nullCompare(o1,o2);
//    if(res!=null)
//      return res;
//    return Util.lenCompare(o1,o2);
//  }
//
//  static boolean equal(KeyCache o1, KeyCache o2) {
//    if(o1==null || o2==null)
//      return o1==o2;
//    Iterator<Entry> i1 = o1.mEntries.iterator();
//    Iterator<Entry> i2 = o2.mEntries.iterator();
//    int res=0;
//    while(res==0 && i1.hasNext() && i2.hasNext()) {
//      res=compare(i1.next(), i2.next());
//    }
//    return res==0;
//  }
//
//  public boolean equals(@Nullable Object o)
//  {
//    if(o instanceof KeyCache)
//      return equal(this,(KeyCache)o);
//    else
//      return false;
//  }
//
//  public int hashCode()
//  {
//    return mEntries.hashCode();
//  }
//
//
//  @Nonnull
//  public String toString()
//  {
//    return mEntries.toString();
//  }
//
//
//  public int size()
//  {
//    return mEntries.size();
//  }
//
//
//  public void clear()
//  {
//    mEntries.clear();
//  }

}
