package cell411.logic;

import static android.app.NotificationManager.IMPORTANCE_LOW;
import static java.lang.Boolean.FALSE;
import static cell411.utils.Util.getRef;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.drawable.IconCompat;

import com.parse.Parse;
import com.parse.ParseQuery;
import com.parse.livequery.LiveQueryException;
import com.parse.livequery.ParseLiveQueryClient;
import com.parse.livequery.ParseLiveQueryClientCallbacks;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import cell411.LogicApp;
import cell411.config.ConfigDepot;
import cell411.config.UtilApp;
import cell411.logic.rel.AggRel;
import cell411.logic.rel.Rel;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.utils.Util;
import cell411.utils.collect.LazyObject;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.Suppliers;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadName;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.io.TimeLog;
import cell411.utils.reflect.Reflect;

public class LiveQueryService
  extends Service
  implements ParseLiveQueryClientCallbacks,
             ExceptionHandler
{
  private static final int ID_FOREGROUND = 17751984;
  private static WeakReference<LiveQueryService> smInstance;
  LocalBinder binder = new LocalBinder();
  private final File mLogDir;
  private final LogicApp mApp = LogicApp.req();
  AtomicReference<Data> mData = new AtomicReference<>();
  public File getLogDir() {
    return mLogDir;
  };
  public LiveQueryService()
  {
    smInstance = new WeakReference<>(this);
    if(!mData.compareAndSet(null,new Data(new LocalBinder()))){
      throw new RuntimeException("There can be only one!");
    }
    mLogDir = ConfigDepot.getJsonCacheFile("Holder");
  }

  public Rel getRel(String key) {
    MultiCacheObject mco =  getCache(false);
    assert mco!=null;
    RelationWatcher rw = mco.getRelationWatcher();
    assert rw!=null;
    Rel rel = rw.getRel(key);
    assert rel!=null;
    return rel;
  }

  static class FileComp implements Comparator<File>
  {

    @Override
    public int compare(File o1, File o2)
    {
      String p1 = o1.getPath();
      String p2 = o2.getPath();
      return String.CASE_INSENSITIVE_ORDER.compare(p1,p2);
    }
  }
  public static LiveQueryService convert(IBinder iBinder)
  {
    return ((LocalBinder) iBinder).getService();
  }

  public static LiveQueryService req()
  {
    return Objects.requireNonNull(opt());
  }

  public static LiveQueryService opt()
  {
    return getRef(smInstance);
  }

  public static Set<String> getPermRequests()
  {
    HashSet<String> perms = new HashSet<>();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
      perms.add(Manifest.permission.FOREGROUND_SERVICE);
    }
    return perms;
  }

  public AggRel getCounterParties()
  {
    return getRelationWatcher().getCounterParties();
  }

  public RelationWatcher getRelationWatcher()
  {
    return getCache(true).getRelationWatcher();
  }

  public synchronized MultiCacheObject getCache(boolean create)
  {
    if (create && getData().mMultiCacheObject == null) {
      getData().mMultiCacheObject = new MultiCacheObject(this);
    }
    return getData().mMultiCacheObject;
  }

  public Data getData()
  {
    return getRef(mData);
  }

  public void addReadyObserver(ValueObserver<? super Boolean> observer)
  {
    getData().mReady.addObserver(observer);
  }

  public boolean isReady()
  {
    return getData().mReady.get();
  }

  public void setReady(boolean b)
  {
    getData().mReady.set(b);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    super.onStartCommand(intent, flags, startId);
    Future<UtilApp> fut = UtilApp.fut();
    Reflect.announce("Got the Future");
    try {
      fut.get();
    } catch (ExecutionException | InterruptedException e) {
      throw new RuntimeException(e);
    }
    Reflect.announce("Ready to GOOOOO!");
    showNotification();
    ThreadUtil.onExec(mStarter);
    return START_STICKY;
  }
  final static Starter mStarter = new Starter();
  // run
  public void showNotification()
  {
    NotificationManagerCompat nm = NotificationManagerCompat.from(this);
    NotificationChannel mChannel = getData().mChannel;
    if (mChannel == null) {
      mChannel =
        new NotificationChannel(getData().FOREGROUND, getData().FOREGROUND,
          IMPORTANCE_LOW);
      mChannel.setShowBadge(false);
      nm.createNotificationChannel(mChannel);
    }
    Class<?> type = getClass();

    Intent notificationIntent = new Intent(this, type);
    PendingIntent pendingIntent =
      PendingIntent.getActivity(this, 0, notificationIntent,
        PendingIntent.FLAG_IMMUTABLE);
    NotificationCompat.Builder notificationBuilder =
      new NotificationCompat.Builder(this, getData().FOREGROUND);
    IconCompat drawable = mApp.getSmallIcon();
    if(drawable!=null) {
      notificationBuilder.setSmallIcon( drawable );
    }
    notificationBuilder.setOngoing(true);
    notificationBuilder.setPriority(NotificationCompat.PRIORITY_MIN);
    notificationBuilder.setShowWhen(false);
    notificationBuilder.setWhen(0);
    notificationBuilder.setContentTitle(getClass().getSimpleName());
    notificationBuilder.setChannelId(getData().FOREGROUND);

    String message = "The Service Is Running";
    notificationBuilder.setContentText(message);

    notificationBuilder.setStyle(
      new NotificationCompat.BigTextStyle().bigText(message));
    if (pendingIntent != null) {
      notificationBuilder.setContentIntent(pendingIntent);
    }

    startForeground(ID_FOREGROUND, notificationBuilder.build());
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent)
  {
    return getData().mBinder;
  }

  public <T extends ParseObject> ParseObjectSubscription<T> subscribe(
    ParseQuery<T> query, ObjectEventsCallback<T> callback)
  {
    getData().mParseQueries.add(query);
    return new ParseObjectSubscription<>(this, query, callback);
  }

  public ParseLiveQueryClient getClient()
  {
    if (getData().mClient == null) {
      getData().mClient = new ParseLiveQueryClient();
      getData().mClient.registerListener(this);
    }
    return getData().mClient;
  }

  public ChatRoomWatcher getChatRoomWatcher()
  {
    return getCache(true).getChatRoomWatcher();
  }

  public void clear()
  {
    final List<ParseQuery<?>> mParseQueries = getData().mParseQueries;
    for (ParseQuery<?> query : mParseQueries) {
      unsubscribe(query);
    }
    mParseQueries.clear();
    clearCache();
  }

  private void clearCache()
  {
    Data data = getData();
    if (data.mMultiCacheObject != null) {
      data.mMultiCacheObject.clear();
      data.mMultiCacheObject=null;
    }
  }

  public <T extends ParseObject> void unsubscribe(ParseQuery<T> query)
  {
    getClient().unsubscribe(query);
  }

  public AlertWatcher getAlertWatcher()
  {
    return getCache(true).getAlertWatcher();
  }

  public RequestWatcher getRequestWatcher()
  {
    return getCache(true).getRequestWatcher();
  }

  public CellWatcher<XPublicCell> getPublicCellWatcher()
  {
    return getCache(true).getPublicCellWatcher();
  }

  public UserWatcher getUserWatcher()
  {
    return getCache(true).getUserWatcher();
  }

  public CellWatcher<XPrivateCell> getPrivateCellWatcher()
  {
    return getCache(true).getPrivateCellWatcher();
  }

  @Override
  public void onLiveQueryClientConnected(final ParseLiveQueryClient client)
  {
    Reflect.announce("" + client);
  }

  @Override
  public void onLiveQueryClientDisconnected(final ParseLiveQueryClient client,
                                            final boolean userInitiated)
  {
    ThreadUtil.onExec(this::maybeReconnect, 15000);
  }

  public void maybeReconnect()
  {
    getData().mClient.connectIfNeeded();
  }

  @Override
  public void onLiveQueryError(final ParseLiveQueryClient client,
                               final LiveQueryException reason)
  {
    Reflect.announce("" + client);
  }

  @Override
  public void onSocketError(final ParseLiveQueryClient client,
                            final Throwable reason)
  {
    Reflect.announce(client + " " + reason);
  }

  public String report()
  {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    TimeLog log = new TimeLog(stream);
    report(log);
    return stream.toString();
  }

  public void report(TimeLog timeLog)
  {
    MultiCacheObject multiCacheObject = getCache(true);
    timeLog.pf("multiCacheObject=%s", multiCacheObject);
    if (multiCacheObject != null) {
      multiCacheObject.report(timeLog);
    }
    timeLog.pf("multiCacheObject=%s", multiCacheObject);
    System.out.println("report complete");
  }

  public MultiCacheObject get()
  {
    return getData().mCache.get();
  }

  static class Data
  {
    static int smReloadCount = 0;
    public final String FOREGROUND = "ForeGround";
    private final List<ParseQuery<?>> mParseQueries = new ArrayList<>();
    private final LocalBinder mBinder;
    private final ObservableValueRW<Boolean> mReady;
    private final Supplier<MultiCacheObject> mSupplier;
    private final LazyObject<MultiCacheObject> mCache;
    private MultiCacheObject mMultiCacheObject;
    private NotificationChannel mChannel;
    private ParseLiveQueryClient mClient;
    private ByteArrayOutputStream mLogText;

    {
      mSupplier = Suppliers.supplier(MultiCacheObject.class);
      mReady = new ObservableValueRW<>(Boolean.class, FALSE);
      mCache = LazyObject.fromSupplier(MultiCacheObject.class, mSupplier);
    }

    Data(LocalBinder binder)
    {
      mBinder = binder;
    }
  }

  class LocalBinder
    extends Binder
  {
    public LocalBinder()
    {
      Reflect.announce();
    }

    LiveQueryService getService()
    {
      return LiveQueryService.this;
    }
  }

  static class Starter
    implements Runnable
  {
    int mRunCount=0;
    private LiveQueryService mLiveQueryService;

    @Override
    public void run()
    {
      mLiveQueryService=opt();
      try (ThreadName threadName =
             new ThreadName("Starter Thread: " + mRunCount++)) {
        ThreadUtil.waitUntil(this, Parse::isInitialized, 2000);
        Future<ParseUser> wait = Parse.getUserWaiter();
        try {
          ParseUser user = wait.get();
          assert user!=null;
          assert user==Parse.getCurrentUser();
        } catch (Throwable e) {
          Util.printStackTrace(e);
          ThreadUtil.onExec(this,30000);
        }
        if (Parse.getCurrentUser() == null)
          return;
        Data data = mLiveQueryService.getData();
        try {
          MultiCacheObject mco = mLiveQueryService.getCache(true);
          mco.run();
          mLiveQueryService.showAlertDialog("alert", this+" is done");
//          ThreadUtil.wait(this);
        } catch ( Throwable throwable ) {
          Util.printStackTrace(throwable);
          throw Util.rethrow(throwable);
        }
      }
    }
  }



}
