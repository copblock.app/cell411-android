package cell411.logic;

import androidx.annotation.CallSuper;

import com.parse.Parse;
import com.parse.ParseQuery;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;
import com.parse.model.ParseUser;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Nonnull;

import cell411.enums.CellStatus;
import cell411.logic.rel.BaseRel;
import cell411.logic.rel.Key;
import cell411.model.XBaseCell;
import cell411.model.XUser;
import cell411.utils.Util;
import cell411.utils.collect.Collect;

public class CellWatcher<X extends XBaseCell>
  extends Watcher<X>
{
  public CellWatcher(@Nonnull RelationWatcher relationWatcher, Class<X> type)
  {
    super(relationWatcher, "CellWatcher(" + type.getSimpleName() + ")", type);
  }

  public void doAllRels(final RelationWatcher relationWatcher,
                        final ParseUser user)
  {
    String userId = user.getObjectId();
    //    addRel(relationWatcher.getRel(mClassName + ".tempCells", mType));
  }

  @Override
  public ParseQuery<X> directQuery()
  {
    XUser user = XUser.reqCurrentUser();
    ParseQuery<X> query1 =
      ParseQuery.getQuery(mType).whereEqualTo("owner", user.getObjectId());
    ParseQuery<X> query2 =
      ParseQuery.getQuery(mType).whereEqualTo("owner", user);
    ParseQuery<X> query3 =
      ParseQuery.getQuery(mType).whereEqualTo("members", user);
    ParseQuery<X> query4 =
      ParseQuery.getQuery(mType).whereEqualTo("members", user.getObjectId());

    return ParseQuery.or(query1, query2, query3, query4);
  }


    @CallSuper
    @Override
    void greetObject(X po)
    {
      super.greetObject(po);
      XUser user = po.getOwner();
      XUser currentUser = XUser.getCurrentUser();

      Collection<String> members = po.getMemberIds();
      if (members == null)
      {
        ParseRelation<XUser> relation = po.getRelation("members");
        ParseQuery<XUser> query = relation.getQuery();
        List<XUser> newMembers = query.find();
        List<String> parts =
            Collect.asList(po.getClassName(), "members", "_User",
            "objectId", po.getObjectId());
        Key key = Key.create(parts);
        BaseRel rel = mRelationWatcher.getRel(key);
        HashSet<String> ids = new HashSet<>(Util.transform(newMembers,
        ParseObject::getObjectId));
        rel.setRelatedIds(ids);
        members = po.getMemberIds();
        assert members != null;
      }
      if (user == currentUser || user.hasSameId(currentUser))
      {
        po.setStatus(CellStatus.OWNER);
      } else if (members.contains(currentUser.getObjectId())) {
        po.setStatus(CellStatus.JOINED);
      } else {
        po.setStatus(CellStatus.NOT_JOINED);
      }
    }
}
