package cell411.logic;

import com.parse.ParseQuery;
import com.parse.model.ParseObject;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import cell411.logic.rel.AggRel;
import cell411.model.XChatRoom;
import cell411.model.XEntity;

public class ChatRoomWatcher
  extends Watcher<XChatRoom>
  implements MyObserver
{
  private final AggRel mEntityRel;

  public ChatRoomWatcher(@Nonnull RelationWatcher relationWatcher)
  {
    super(relationWatcher, "ChatRooms", XChatRoom.class);
//    System.out.println(mRelationWatcher);
    mEntityRel = mRelationWatcher.getAggRel("Entity");
    mEntityRel.addObserver(this);
  }
  @Override
  public void update(MyObservable o, Object arg)
  {
    if (o != mEntityRel)
      return;
    Map<String, XEntity> wholeSet;
    for (Iterator<String> it = mEntityRel.idIterator(); it.hasNext(); ) {
      String id = it.next();
      ParseObject object = ParseObject.getIfExists(id);
      if (!(object instanceof XEntity))
        continue;
      XEntity xo = (XEntity) object;
      if (object.has("chatRoom")) {
        XChatRoom chatRoom = xo.getChatRoom();
      }
    }
  }



  @Override
  public synchronized ParseQuery<XChatRoom> createQuery(Set<String> dirty)
  {
    return directQuery();
  }

  @Override
  public ParseQuery<XChatRoom> directQuery()
  {
    ParseQuery<XChatRoom> query = XChatRoom.q();
    query.whereContainedIn("entity", mEntityRel.getRelatedIds());
    return query;
  }

  @Override
  protected synchronized Set<String> getIds(boolean dirty)
  {
    Set<String> set = super.getIds(dirty);
    set.add("x");
    return set;
  }
}
