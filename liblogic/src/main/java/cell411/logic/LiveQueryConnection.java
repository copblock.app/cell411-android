package cell411.logic;

import android.content.Context;
import android.os.IBinder;

import androidx.annotation.Nullable;

import cell411.utils.collect.ValueObserver;
import cell411.utils.func.Func0;
import cell411.utils.func.Func1;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class LiveQueryConnection
  extends CallableConnection<LiveQueryService>
  implements ValueObserver<Boolean>
{
  final ConditionIsReady mConditionIsReady = new ConditionIsReady();
  private final XTAG TAG = new XTAG();
  private LiveQueryService mService;

  public LiveQueryConnection(Context app, Class<LiveQueryService> type,
                             Func1<LiveQueryService, IBinder> convert)
  {
    super(app, type, convert);
  }

  boolean isReady()
  {
    LiveQueryService service = LiveQueryService.opt();
    if (service == null)
      return false;
    return service.isReady();
  }

  @Override
  public void onChange(@Nullable Boolean newValue, @Nullable Boolean oldValue)
  {
    Reflect.announceStr("newValue="+newValue+" oldValue="+oldValue);
    notifyAll();
  }

  class ConditionIsReady
    implements Func0<Boolean>
  {
    @Override
    public Boolean apply()
    {
      LiveQueryService service = LiveQueryService.opt();
      if (service == null)
        return false;
      if (service != mService)
        return false;
      return service.isReady();
    }
  }
}
