package cell411.logic;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import java.util.concurrent.Callable;

import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.func.Func1;

public class CallableConnection<X extends Service>
  implements ServiceConnection,
             Callable<X>
{
  protected final Intent mIntent;
  private final Class<X> mType;
  private final Func1<X, IBinder> mConvert;
  X mService;
  private Context mApp;
  private Context mContext;

  public CallableConnection(Context app, Class<X> type,
                            Func1<X, IBinder> convert)
  {
    mType = type;
    mApp = app;
    mIntent = new Intent(app, type);
    mConvert = convert;
  }

  @Override
  public void onServiceConnected(ComponentName name, IBinder service)
  {
    mService = mConvert.apply(service);
    ThreadUtil.notify(this, true);
  }

  @Override
  public void onServiceDisconnected(ComponentName name)
  {
    mService = null;
  }

  @Override
  public X call()
  throws Exception
  {
    setContext(mApp);
    assert !ThreadUtil.isMainThread();
    getApp().bindService(mIntent, this, Context.BIND_ABOVE_CLIENT);
    ComponentName name = getApp().startService(mIntent);
    ThreadUtil.waitUntil(this, () -> mService != null, 1000);
    return mService;
  }

  public void setContext(Context context)
  {
    mApp = context;
  }

  public Context getApp()
  {
    return mApp;
  }
}
