MAKEFLAGS=-rR
assembleDebug:
SHELL:=bash

assembleDebug $(MAKECMDGOALS):
	set -o pipefail; ./gradlew $@ 2>&1 | tee grad.log

.PHONY: assembleDebug $(MAKECMDGOALS)
