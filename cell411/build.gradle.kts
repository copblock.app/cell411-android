@file:Suppress("UnstableApiUsage")

plugins {
    id("com.android.application")
}
android {
    compileSdk = 33

    defaultConfig {
        applicationId = "com.safearx.cell411"
        minSdk = 27
        targetSdk = 33
        versionCode = 2010020
        versionName = "20.100.20"
    }
    namespace = "com.safearx.cell411"
    dependenciesInfo {
        includeInApk = false
        includeInBundle = false
    }
    signingConfigs {
        create("release") {
            storeFile = file("cell411_keystore.jks")
            storePassword = "cell411"
            keyAlias = "Cell 411"
            keyPassword = "cell411"
        }
    }
    buildTypes {
        getByName("debug") {
            isDebuggable = true
            isRenderscriptDebuggable = true
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    lint {
        baseline = file("baseline.xml")
    }
    buildFeatures {
        aidl = true
    }
}
dependencies {
    implementation(project(mapOf("path" to ":libmodel")))
    implementation(project(mapOf("path" to ":liblogic")))
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    implementation("androidx.annotation:annotation:1.6.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.exifinterface:exifinterface:1.3.6")
    implementation("androidx.gridlayout:gridlayout:1.0.0")
    implementation("com.google.android.material:material:1.9.0")
    implementation("com.google.code.findbugs:jsr305:3.0.2")
    implementation("com.squareup.okhttp3:okhttp:4.11.0")
    implementation("org.codehaus.mojo:animal-sniffer-annotations:1.23")
    implementation(project(mapOf("path" to ":lex")))
    implementation(project(mapOf("path" to ":utils")))
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.junit.jupiter:junit-jupiter:5.10.0")
    configurations.implementation {
        exclude(group = "org.jetbrains.kotlin", module= "kotlin-stdlib-jdk8")
    }

}
