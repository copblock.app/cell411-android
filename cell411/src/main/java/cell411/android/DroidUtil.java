package cell411.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewParent;

import androidx.annotation.BoolRes;
import androidx.annotation.IntegerRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Nonnull;

import cell411.config.UtilApp;
import cell411.utils.reflect.XTAG;

public class DroidUtil
{
  static XTAG TAG = new XTAG();
  public static final NullHandler NULL_HANDLER = new NullHandler();


  public static <B, A1 extends B, A2 extends B> B getBoolean(@BoolRes int resId,
                                                             A1 tv, A2 fv)
  {
    return getBoolean(resId) ? tv : fv;
  }

  private static boolean getBoolean(@BoolRes int resId)
  {
    return UtilApp.req().getResources().getBoolean(resId);
  }

  public static int getInteger(@IntegerRes int resId)
  {
    return UtilApp.req().getResources().getInteger(resId);
  }

  public static String getString(int res, Object... args)
  {
    String str = "No Strings Available";
    Context context = UtilApp.opt();
    if (context != null)
      str = context.getString(res, args);
    return str;
  }

  public static String getFuzzyTimeOrDay(long millis)
  {
    Calendar calCurrentTime = Calendar.getInstance();
    calCurrentTime.setTimeInMillis(System.currentTimeMillis());
    calCurrentTime.set(Calendar.HOUR, 0);
    calCurrentTime.set(Calendar.MINUTE, 0);
    calCurrentTime.set(Calendar.SECOND, 0);
    calCurrentTime.set(Calendar.MILLISECOND, 0);
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(millis);
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH) + 1;
    int day = cal.get(Calendar.DAY_OF_MONTH);
    int hour = cal.get(Calendar.HOUR);
    int minute = cal.get(Calendar.MINUTE);
    int ampm = cal.get(Calendar.AM_PM);
    String ap;
    if (ampm == 0)
      ap = "AM";
    else
      ap = "PM";
    String hourMinute = "" + hour;
    if (minute > 0 && minute < 10)
      hourMinute += ":0" + minute;
    else if (minute > 9)
      hourMinute += ":" + minute;
    String time;
    if (cal.getTimeInMillis() <
      calCurrentTime.getTimeInMillis()) // Check if the alert was issued
      // yesterday
      time = month + "/" + day + "/" + year;
    else
      time = hourMinute + " " + ap;
    return time;
  }

  public static ArrayList<View> ancestors(View view)
  {
    ArrayList<View> res;
    ViewParent parent = view.getParent();
    if (parent instanceof View) {
      res = ancestors((View) parent);
    } else {
      res = new ArrayList<>();
    }
    res.add(view);
    return res;
  }

  @Nonnull
  public static String cleanFileName(@Nonnull String folderName)
  {
    char[] chars = new char[folderName.length()];
    folderName.getChars(0, chars.length, chars, 0);
    for (int i = 0; i < chars.length; i++) {
      char ch = chars[i];
      if (Character.isSpaceChar(ch) || Character.isISOControl(ch)) {
        ch = '_';
      }
      switch (ch) {
        case '/':
        case ':':
        case '\\':
          ch = '_';
        default:
          break;
      }
      chars[i] = ch;
    }
    folderName = new String(chars);
    return folderName;
  }

  public static Resources getResources()
  {
    return app().getResources();
  }

  @Nonnull
  private static UtilApp app()
  {
    return UtilApp.req();
  }

  public static List<String> missingPerms(Activity activity, List<String> perms)
  {
    ArrayList<String> res = new ArrayList<>();
    for (String perm : perms) {
      if (!hasPerm(activity, perm))
        res.add(perm);
    }
    return res;
  }

  public static boolean hasPerm(@Nonnull Activity activity,
                                String accessFineLocation)
  {
    assert activity != null;
    int res = ActivityCompat.checkSelfPermission(activity, accessFineLocation);
    return res == PackageManager.PERMISSION_GRANTED;
  }

  public static int getColor(Activity activity, int color)
  {
    Resources resources = activity.getResources();
    Resources.Theme theme = activity.getTheme();
    return ResourcesCompat.getColor(resources, color, theme);
  }

  public static int getColor(Resources resources, int color,
                             Resources.Theme theme)
  {
    return ResourcesCompat.getColor(resources, color, theme);
  }

  static class NullHandler
    implements DialogInterface.OnClickListener,
               DialogInterface.OnCancelListener
  {
    @Override
    public void onClick(DialogInterface dialog, int which)
    {
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {
    }
  }

}
