package cell411.imgstore;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Looper;
import android.util.Size;

import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.Nonnull;

import cell411.json.JSONObject;
import cell411.model.IPicObject;
import cell411.model.XUser;
import cell411.ui.base.BaseApp;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.IOUtil;
import cell411.utils.ImageUtils;
import cell411.utils.io.PrintString;
import cell411.utils.UrlUtils;
import cell411.utils.Util;

public class ImageData
  implements CachedResource,
             ImageListener,
             Runnable
{
  static final ArrayList<ImageData> smAllImageData = new ArrayList<>();
  private static final OkImageDownloader smOkImageDownloader =
    new OkImageDownloader(BaseApp.req().getHttpClient());
  private final String mKey;
  private final IPicObject mPicObject;
  private final ArrayList<Notifier> mNotifiers = new ArrayList<>();
  private final SharedCropper mSharedCropper = SharedCropper.get();
  private final URL mURL;
  private Instant mSince = Instant.now();
  private Bitmap mBitmap;
  private Instant mFailed;
  private CachedStatus mStatus = CachedStatus.NEW_REQUEST;
  private File mFile;
  private long mSize = 0;
  private String mSource = null;
  private Size mOrigDim;
  private int mUses = 0;

  {
    synchronized (smAllImageData) {
      smAllImageData.add(this);
    }
  }

  public ImageData(@Nonnull String key, IPicObject picObject, URL url,
                   BitmapDrawable imageView)
  {
    mKey = key;
    mPicObject = picObject;
    if (Util.isNoE(url)) {
      if (picObject instanceof ParseUser) {
        XUser user = (XUser) picObject;
        mURL = user.getAvatarUrl();
      } else {
        throw new RuntimeException("Only users implemented");
      }
    } else {
      mURL = url;
    }
    if (imageView != null)
      merge(imageView);
  }

  public synchronized void merge(@Nonnull BitmapDrawable imageView)
  {
    ImageStore store = ImageStore.opt();
    if (mBitmap != null && Looper.getMainLooper().isCurrentThread()) {
      updateListeners(imageView);
    } else {
      Notifier notifier = new Notifier(imageView);
      Bitmap bitmap;
      if (mBitmap == null) {
        bitmap = ImageUtils.getPlaceHolder();
      } else {
        bitmap = mBitmap;
      }
      if (VERSION.SDK_INT >= VERSION_CODES.S) {
        ThreadUtil.onMain(() -> imageView.setBitmap(bitmap));
      }
      mNotifiers.add(notifier);
    }
    if (mNotifiers.size() != 0)
      store.requestLoad(this);
  }

  public void updateListeners(BitmapDrawable imageView)
  {
    if (mPicObject instanceof XUser) {
      XUser user = (XUser) mPicObject;
      user.setAvatarPic(mBitmap);
    }
    if (imageView != null) {
      if (VERSION.SDK_INT >= VERSION_CODES.S) {
        imageView.setBitmap(mBitmap);
      }
    }
  }

  public static ImageData fromJSON(JSONObject jsonData)
  {
    try {
      ImageStore imageStore = ImageStore.opt();
      String key = jsonData.getString("key");
      String file = jsonData.optString("file");
      String urlStr = jsonData.optString("url");
      String objStr = jsonData.optString("object");
      final IPicObject user = ParseObject.getOrStub("_User", objStr);
      assert (user.getClass() == XUser.class);
      long failed = jsonData.optLong("failed");
      long since = jsonData.optLong("since");
      ImageData data = new ImageData(key, user, UrlUtils.toURL(urlStr), null);
      if (since != 0)
        data.mSince = Instant.ofEpochMilli(since);
      if (failed != 0)
        data.mFailed = Instant.ofEpochMilli(failed);
      if (!Util.isNoE(file))
        data.mFile = new File(file);
      if (data.mFile != null) {
        data.loadFromDisk();
      }
      if (data.mBitmap != null) {
        user.setAvatarPic(data.mBitmap);
      }
      return data;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void loadFromDisk()
  {
    assert !ThreadUtil.isMainThread();
    if (mFile == null || !mFile.exists()) {
      return;
    }
    try {
      decodeFromStream(IOUtil.fileToStream(mFile));
      if (mSource == null && mBitmap != null)
        mSource = "Disk";
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void decodeFromStream(InputStream stream)
  throws IOException
  {
    try {
      byte[] bytes = IOUtil.streamToBytes(stream);
      mSize = bytes.length;
      mBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
      mOrigDim = new Size(mBitmap.getWidth(), mBitmap.getHeight());
      if (mBitmap == null) {
        CachedStatus why = new CachedStatus(FailType.DECODING_ERROR, null);
        onLoadingFailed(String.valueOf(mURL), why);
        return;
      }
      cropBitmap();
    } finally {
      stream.close();
    }
  }

  @Override
  public void onLoadingFailed(String imageUri, CachedStatus failReason)
  {
    mFailed = Instant.now();
  }

  public void cropBitmap()
  {
    try {
      // FIXME:  This is opaque as shit.
      if (mURL == null) {
        mBitmap = mSharedCropper.getCroppedBitmapFromGravatar(mBitmap);
      } else {
        mBitmap = mSharedCropper.getCroppedBitmap4Map(mBitmap);
      }
    } catch (Exception ex) {
      mStatus = new CachedStatus(FailType.DECODING_ERROR,
        new RuntimeException("Failed to crop image", ex));
    }
  }

  @Override
  public void onLoadingComplete(URL imageUri, Bitmap loadedImage)
  {
    assert loadedImage == mBitmap;
    try (PrintString ps = new PrintString()) {
      ps.pl("-----------------------------------------------------------");
      ps.pl("complete: " + getKey());
      ps.pl("  thread: " + Thread.currentThread());
      ps.pl("    user: " + mPicObject);
      ps.pl("  source: " + mSource);
      ps.pl("    mURL: " + mURL);
      ps.pl("   mFile: " + mFile);
      ps.pl("    size: " + mSize);
      ps.pl(" mBitmap: " + mBitmap);
      ps.pl("    oDim: " + mOrigDim);
      ps.pl("     Dim: " + new Size(mBitmap.getWidth(), mBitmap.getHeight()));
      ps.pl("-----------------------------------------------------------");
      ps.pl("");
      ps.toString();
    }
    fire();
  }

  public synchronized void dump(PrintString ps)
  {
    ps.pl(toJSON().toString(2));
  }

  JSONObject toJSON()
  {
    JSONObject res = new JSONObject();
    res.put("key", mKey);
    res.put("object", getObjectId());
    res.put("file", mFile == null ? null : mFile.getAbsolutePath());
    res.put("url", String.valueOf(mURL));
    if (mFailed != null)
      res.put("failed", mFailed.toEpochMilli());
    res.put("origDim", mOrigDim);
    res.put("size", mSize);
    res.put("status", mStatus);
    if (mSince != null)
      res.put("since", mSince.toEpochMilli());
    return res;
  }

  private String getObjectId()
  {
    if (mPicObject == null)
      return null;
    else
      return mPicObject.getObjectId();
  }

  public URL getURL()
  {
    return mURL;
  }

  public synchronized void run()
  {
    if (ThreadUtil.isMainThread()) {
      ThreadUtil.onExec(this, 0);
      return;
    }
    try {
      assert !ThreadUtil.isMainThread();
      if (mBitmap == null && mFile != null) {
        loadFromDisk();
      }
      if (mBitmap == null && mURL != null) {
        loadFromNet();
      }
      assert ((mBitmap != null) == (mSource != null));
      if (mBitmap != null) {
        mStatus = CachedStatus.SUCCESS;
        onLoadingComplete(mURL, mBitmap);
      } else if (mStatus == null) {
        mStatus = new CachedStatus(FailType.UNKNOWN);
      }
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  private void loadFromNet()
  {
    assert !ThreadUtil.isMainThread();
    try {
      if (mURL == null)
        return;
      InputStream stream = smOkImageDownloader.getStream(mURL, null);
      if (stream != null)
        decodeFromStream(stream);
      if (mSource == null && mBitmap != null)
        mSource = "Net";
      mFile = ImageUtils.storeBitmap(getKey(), mBitmap);
      ImageStore.opt().saveImageCache();
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  public synchronized void fire()
  {
    Iterator<Notifier> iterator = mNotifiers.iterator();
    while (iterator.hasNext()) {
      Runnable loadLocationRunnable = iterator.next();
      ThreadUtil.onExec(loadLocationRunnable);
      mUses++;
      iterator.remove();
    }
  }

  public String getKey()
  {
    return mKey;
  }

  @Override
  public CachedStatus getStatus()
  {
    return mStatus;
  }

  public File getFile()
  {
    return mFile;
  }

  public IPicObject getObject()
  {
    return mPicObject;
  }

  public Bitmap getBitmap()
  {
    return mBitmap;
  }

  //  public void updateListeners(Marker marker, ImageView imgView,
  //  ImageListener listener) {
  //    if (!DroidUtil.isMainThread()) {
  //      DroidUtil.onMain(() ->
  //        updateListeners(marker, imgView, listener));
  //    } else {
  //      if (listener != null) {
  //        listener.onLoadingComplete(mURL, mBitmap);
  //        mUses++;
  //      }
  //      if (marker != null) {
  //        BitmapDescriptor bd =
  //          BitmapDescriptorFactory.fromBitmap(mBitmap);
  //        marker.setIcon(bd);
  //        mUses++;
  //      }
  //      if (imgView != null) {
  //        Object tag = imgView.getTag(R.id.actionDownUp);
  //        if (tag == this) {
  //          imgView.setImageBitmap(mBitmap);
  //        }
  //        mUses++;
  //      }
  //    }
  //  }

  class Notifier
    implements Runnable
  {
    boolean mFired = false;
    BitmapDrawable mImageView;

    public Notifier(final BitmapDrawable view)
    {
      assert view != null;
      mImageView = view;
    }

    @Override
    public synchronized void run()
    {
      if (needsUiRun() && !ThreadUtil.isMainThread()) {
        ThreadUtil.onMain(this);
      } else {
        ImageStore.opt().imageLoaded(ImageData.this);
        updateListeners(mImageView);
      }
    }

    private boolean needsUiRun()
    {
      return mImageView != null;
    }
  }
}
