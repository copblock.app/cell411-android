package cell411.imgstore;

import java.util.HashMap;
import java.util.Iterator;

import cell411.json.JSONException;
import cell411.json.JSONObject;

class ImageCache
{
  final HashMap<String, ImageData> mMap = new HashMap<>();

  public ImageCache(JSONObject jsonObject)
  {
    Iterator<String> it = jsonObject.keys();
    while (it.hasNext()) {
      String key = it.next();
      JSONObject jsonData = jsonObject.optJSONObject(key);
      if (jsonData != null) {
        ImageData data = ImageData.fromJSON(jsonData);
        put(data);
      }
    }
  }

  public synchronized void put(ImageData imageData)
  {
    mMap.put(imageData.getKey(), imageData);
  }

  public synchronized JSONObject toJSON()
  {
    JSONObject object = new JSONObject();
    for (String key : mMap.keySet()) {
      ImageData imageData = mMap.get(key);
      if (imageData != null) {
        try {
          object.put(key, imageData.toJSON());
        } catch (JSONException e) {
        }
      }
    }
    return object;
  }

  public ImageData get(String key)
  {
    return mMap.get(key);
  }
}
