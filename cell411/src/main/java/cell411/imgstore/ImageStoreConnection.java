package cell411.imgstore;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.CallableConnection;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadName;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class ImageStoreConnection
  extends CallableConnection<ImageStore>
  implements ValueObserver<Boolean>
{
  private final Intent mIntent;
  private final Context mContext;
  private final XTAG TAG = new XTAG();
  private ImageStore mService;

  public ImageStoreConnection(@Nonnull Context context)
  {
    super(logIt(context), ImageStore.class, ImageStore::getRef);
    assert context != null;
    mContext = context;
    mIntent = new Intent(context, ImageStore.class);
    ImageStore.addReadyObserver(this);
  }

  static Context logIt(Context context)
  {
    Reflect.announce("Constructing");
    return context;
  }

  @Override
  public void onServiceConnected(ComponentName name, IBinder service)
  {
    super.onServiceConnected(name, service);
  }

  public synchronized ImageStore call()
  throws Exception
  {
    try (ThreadName name = new ThreadName("ImageStoreConnection")) {
      ImageStore result = super.call();
      ThreadUtil.waitUntil(this, result::isReady, 5000);
      return result;
    }
  }

  @Override
  public void onChange(@Nullable Boolean newValue, @Nullable Boolean oldValue)
  {
    if (newValue)
      ThreadUtil.notify(this,true);
  }
}
