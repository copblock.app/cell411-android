package cell411.ui.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

//class WaitForMeRunnable implements RunOrCall<Void> {
//  @Override
//  public synchronized void run()
//  {
//    notify();
//  }
//}

public class FragmentFrame
  extends FrameLayout
  implements BaseContext, FragmentStackListener
{
  private static final XTAG TAG = new XTAG();
  static {
    XLog.v(TAG, "Loading Class");
  }
  final Stack<FragmentFactory> mFragmentStack = new Stack<>();
  FragmentManager mFragMan;

  public FragmentFrame(@Nonnull Context context)
  {
    super(context);
  }

  public FragmentFrame(@Nonnull Context context, @Nullable AttributeSet attrs)
  {
    super(context, attrs);
  }

  public FragmentFrame(@Nonnull Context context, @Nullable AttributeSet attrs,
                       int defStyleAttr)
  {
    super(context, attrs, defStyleAttr);
  }

  void setFragMan(FragmentManager fragMan)
  {
    assert mFragMan == null;
    mFragMan = fragMan;
  }

  @Override
  protected void attachViewToParent(View child, int index,
                                    ViewGroup.LayoutParams params)
  {
    assert(mFragMan!=null);
    super.attachViewToParent(child, index, params);
  }

  public int getDepth()
  {
    return mFragmentStack.size();
  }

  public boolean onBackPressed()
  {
    if (getDepth() < 2)
      return false;
    pop();
    return true;
  }

  public boolean pop()
  {
    push(null, true, false);
    return true;
  }

  public void push(FragmentFactory factory, boolean pop, boolean all)
  {
    final FragPusher pusher = new FragPusher(factory,pop,all);
    pusher.run();
//    final FragPusher pusher = new FragPusher(factory, pop, all);
//    if (ThreadUtil.isMainThread()) {
//      Reflect.announce("On Main");
//      pusher.run();
//    } else {
//      synchronized(pusher) {
//        Reflect.announce("On Main");
//        ThreadUtil.onMain(pusher);
//        ThreadUtil.sleep(1000);
//        ThreadUtil.wait(pusher);
//      }
//    }

  }

  public void push(final FragmentFactory newFactory)
  {
    push(newFactory, false, false);
  }

  public void popAll()
  {
    push(null, true, true);
  }

  public FragmentFactory getTopFactory()
  {
    if (isEmpty()) {
      return null;
    } else {
      return mFragmentStack.peek();
    }
  }

  public boolean isEmpty()
  {
    return mFragmentStack.isEmpty();
  }

  public BaseFragment peek()
  {
    if(mFragmentStack.isEmpty())
      return null;
    else
      return mFragmentStack.peek().get(false);
  }

  @Override
  public void onFragmentPushed(FragmentFrame fragmentFrame, FragmentFactory factory)
  {

  }

  @Override
  public void onFragmentPopped(FragmentFrame fragmentFrame, FragmentFactory factory)
  {

  }

  private class FragPusher
    implements Runnable
  {
    private final boolean mAll;
    private final boolean mPop;
    private final FragmentFactory mNewFactory;

    public FragPusher(FragmentFactory factory, boolean pop, boolean all)
    {
      mNewFactory = factory;
      mPop = pop;
      mAll = all;
    }

    @Override
    public synchronized void run() {
      if (ThreadUtil.isMainThread()) {
        try {
          FragmentTransaction ft = mFragMan.beginTransaction();
          ArrayList<FragmentFactory> removed = new ArrayList<>();
          if (mAll) {
            for (int i = 0; i < mFragmentStack.size(); i++) {
              FragmentFactory factory = mFragmentStack.remove(i--);
              BaseFragment fragment = factory.get(false);
              if (fragment != null && fragment.isAdded()) {
                ft.remove(fragment);
                removed.add(factory);
              }
            }
          } else if (mPop && !mFragmentStack.isEmpty()) {
            FragmentFactory factory = mFragmentStack.pop();
            BaseFragment fragment = factory.get(false);
            if (fragment != null && fragment.isAdded()) {
              ft.remove(fragment);
              removed.add(factory);
            }
          } else if (!mFragmentStack.isEmpty()) {
            for (FragmentFactory factory : mFragmentStack) {
              BaseFragment fragment = factory.get(false);
              if (fragment != null && fragment.isAdded()) {
                ft.hide(fragment);
              }
            }
          }
          if (mNewFactory != null) {
            mFragmentStack.push(mNewFactory);
          }
          FragmentFactory top = null;
          if (!mFragmentStack.isEmpty()) {
            top = mFragmentStack.peek();
            Fragment newFragment = top.get(true);
            if (!newFragment.isAdded()) {
              ft.add(getId(), newFragment);
            }
            ft.show(newFragment);
          }
          ft.commitNowAllowingStateLoss();
          fragmentStackChanged(removed, mNewFactory, top);
        } finally{
          ThreadUtil.notify(this,true);
        }
      } else {
        ThreadUtil.onMain(this);
        ThreadUtil.wait(this);
      }
    }
  }
  public void addFragmentStackListener(FragmentStackListener listener) {
    synchronized (mFragmentStackListeners) {
      mFragmentStackListeners.add(listener);
    }
  }
  public void removeFragmentStackListener(FragmentStackListener listener) {
    synchronized (mFragmentStackListeners) {
      mFragmentStackListeners.remove(listener);
    }
  }
  final HashSet<FragmentStackListener> mFragmentStackListeners = new HashSet<>();
  private void fragmentStackChanged(ArrayList<FragmentFactory> removed,
                                   FragmentFactory added, FragmentFactory top)
  {
    if(mFragmentStackListeners.isEmpty())
      return;

    HashSet<FragmentStackListener> listeners;
    synchronized(mFragmentStackListeners) {
      listeners = new HashSet<>(mFragmentStackListeners);
    }
    for (FragmentStackListener listener : listeners) {
      try {
        for (FragmentFactory factory : removed) {
          listener.onFragmentPopped(FragmentFrame.this, factory);
        }
        listener.onFragmentPushed(FragmentFrame.this, added);
      } catch ( Throwable t ) {
        Util.printStackTrace(t);
      }
    }
  }

}

