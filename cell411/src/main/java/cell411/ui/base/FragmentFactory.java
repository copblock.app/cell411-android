package cell411.ui.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;

import java.util.List;
import java.util.TreeSet;

import javax.annotation.Nonnull;

import cell411.enums.ProblemType;
import cell411.ui.welcome.GalleryFragment;
import cell411.utils.Util;
import cell411.utils.collect.Collect;
import cell411.utils.concurrent.ThreadUtil;

public abstract class FragmentFactory
  implements Cloneable
{
  private static final TreeSet<String> smExcludes =
    new TreeSet<>(Collect.asList("Activity", "Fragment"));
  BaseFragment mFragment;
  Bundle mArguments;
  public static List<FragmentFactory> fromLayout(
    List<Class<? extends BaseFragment>> types)
  {
    return Util.transform(types, FragmentFactory::fromClass);
  }

  public static <X extends BaseFragment> ClassFactory<X> fromClass(
    Class<X> type)
  {
    return fromClass(type, Util.makeWords(type.getSimpleName(), smExcludes));
  }

  public static <X extends BaseFragment> ClassFactory<X> fromClass(
    Class<X> type, String title)
  {
    return new ClassFactory<>(type, title);
  }

  public static FragmentFactory fromLayout(String title, int layout)
  {
    return new LayoutFactory(LayoutFragment.class, title, layout);
  }

  public void set(GalleryFragment fragment)
  {
    mFragment = fragment;
  }

  public void setProblemType(ProblemType type)
  {
    mArguments = getArguments();
    mArguments.putString("problemType", type.toString());
  }

  private Bundle getArguments()
  {
    if (mArguments == null) {
      mArguments = new Bundle();
    }
    return mArguments;
  }

  public void setObjectId(String objectId)
  {
    mArguments = getArguments();
    mArguments.putString("objectId", objectId);
  }

  public BaseFragment get(boolean create)
  {
    if (mFragment == null && create) {
      create();
    }
    return mFragment;
  }

  public final void create() {
    mFragment = doCreate();
    if (hasArguments()) {
      mFragment.setArguments(getArguments());
    }
    ThreadUtil.onExec(()->ThreadUtil.notify(this,true));
//    ThreadUtil.notify(this,true);
  }

  public abstract BaseFragment doCreate();

  public boolean hasArguments()
  {
    return mArguments != null;
  }

  public abstract String getTitle();

  @Nonnull
  public Object clone()
  {
    try {
      return super.clone();
    } catch (CloneNotSupportedException ex) {
      throw new RuntimeException("clone FragmentFactory", ex);
    }
  }

  public boolean isForClass(Class<? extends BaseFragment> type)
  {
    return false;
  }

  public Boolean isAdded()
  {
    BaseFragment fragment = get(false);
    if(fragment==null)
      return false;
    return fragment.isAdded();
  }

  public Boolean notAdded()
  {
    return !isAdded();
  }

  static class LayoutFactory
    extends TypedFactory<LayoutFragment>
  {
    @LayoutRes
    final int mLayout;

    public LayoutFactory(Class<LayoutFragment> type, String title, int layout)
    {
      super(type, title);
      mLayout = layout;
    }

    @Override
    public String getTitle()
    {
      return "Layout";
    }

    @Override
    public LayoutFragment doCreate()
    {
      return new LayoutFragment(mLayout);
    }
  }

  public static abstract class TypedFactory<X extends BaseFragment>
    extends FragmentFactory
  {
    protected final Class<X> mType;
    protected final String mTitle;

    public TypedFactory(Class<X> type, String title)
    {
      super();
      mType = type;
      if (title == null) {
        title = Util.makeWords(type.getSimpleName());
      }
      mTitle = title;
    }

    abstract public X doCreate();

    public X get(boolean create)
    {
      return mType.cast(super.get(create));
    }
  }

  public static class ClassFactory<X extends BaseFragment>
    extends TypedFactory<X>
  {
    public ClassFactory(Class<X> type, String title)
    {
      super(type, title);
    }

    @Nonnull
    public String toString()
    {
      return getClass().getSimpleName() + "[" + mType.getSimpleName() + "]";
    }

    @Override
    public X doCreate()
    {
      try {
        return mType.getDeclaredConstructor().newInstance();
      } catch (Exception e) {
        throw Util.rethrow("Creating fragment: " + mType, e);
      }
    }

    public String getTitle()
    {
      return mTitle;
    }

    @Override
    public boolean isForClass(Class<? extends BaseFragment> type)
    {
      return type == mType;
    }
  }
}
