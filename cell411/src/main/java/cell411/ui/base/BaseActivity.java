package cell411.ui.base;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.methods.Dialogs;
import cell411.model.XEntity;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public abstract class BaseActivity
  extends AppCompatActivity
  implements BaseContext
{
  public static final XTAG TAG = new XTAG();
  static public final int PERMISSION_GRANTED =
    PackageManager.PERMISSION_GRANTED;

  public BaseActivity()
  {
    this(0);
  }

  public BaseActivity(@LayoutRes int layout)
  {
    super(layout);
  }

  @CallSuper
  @Override
  protected void onSaveInstanceState(@Nonnull Bundle outState)
  {
    super.onSaveInstanceState(outState);
  }

  @Override
  public void onBackPressed()
  {
    super.onBackPressed();
  }

  @SuppressWarnings("deprecation")
  @Override
  public void startActivityForResult(@Nonnull Intent intent, int requestCode,
                                     @Nullable Bundle options)
  {
    try {
      Reflect.announce();
      if (Dialogs.isDialogShowing()) {
        XLog.i(TAG, "delaying startActivity For result");
        ThreadUtil.onMain(
          () -> startActivityForResult(intent, requestCode, options),
          500);
      } else {
        XLog.i(TAG, "calling startActivity For result");
        super.startActivityForResult(intent, requestCode, options);
      }
    } finally {
      Reflect.announce();
    }
  }

  @Override
  protected void onRestoreInstanceState(@Nonnull Bundle savedInstanceState)
  {
    super.onRestoreInstanceState(savedInstanceState);
  }

  @Override
  public boolean onOptionsItemSelected(@Nonnull MenuItem item)
  {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return false;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void startActivity(Intent intent, @Nullable Bundle options)
  {
    try {
      Reflect.announce();
      if (Dialogs.isDialogShowing()) {
        for (Dialog dialog : Dialogs.getShowing()) {
          dialog.dismiss();
        }
        ThreadUtil.onMain(() -> startActivity(intent, options), 500);
      } else {
        super.startActivity(intent, options);
      }
    } catch (Throwable t) {
      t.printStackTrace();
    } finally {
      Reflect.announce();
    }
  }

  public void finish()
  {
    if (Dialogs.isDialogShowing()) {
      ThreadUtil.onMain(this::finish, 500);
    } else {
      super.finish();
    }
  }

  @CallSuper
  @Override
  protected void onActivityResult(int requestCode, int resultCode,
                                  @Nullable Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    hideSoftKeyboard();
    setDisplayUpAsHome();
  }

  @Override
  protected void onPause()
  {
    super.onPause();
  }

  protected void onResume()
  {
    super.onResume();
    hideSoftKeyboard();
  }

  public void hideSoftKeyboard()
  {
    if (getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(
        getCurrentFocus().getWindowToken(), 0);
    }
  }

  public BaseActivity activity()
  {
    return this;
  }

  public void showSoftKeyboard()
  {
    if (getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.showSoftInput(getCurrentFocus(), 0);
    }
  }

  @Override
  public void openChat(final XEntity publicCell)
  {
  }

  public void setDisplayUpAsHome()
  {
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
      actionBar.setDisplayShowHomeEnabled(true);
    }
  }

  public boolean hasPerm(String thePerm)
  {
    return checkSelfPermission(thePerm) == PERMISSION_GRANTED;
  }

}