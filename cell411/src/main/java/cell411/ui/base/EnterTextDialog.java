package cell411.ui.base;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.safearx.cell411.R;

import javax.annotation.Nonnull;

import cell411.utils.Util;

public class EnterTextDialog
  extends AlertDialog
{
  final @Nonnull Context mContext;
  String mAnswer = null;
  TextView mInstructions;
  EditText mEditText;

  public EnterTextDialog()
  {
    this(BaseApp.req().getCurrentActivity());
  }

  public EnterTextDialog(@Nonnull ContextThemeWrapper context)
  {
    super(context);
    mContext = context;
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(
      Service.LAYOUT_INFLATER_SERVICE);
    @SuppressLint("InflateParams") View view =
      inflater.inflate(R.layout.layout_enter_text_dialog, null);
    setView(view);
    mInstructions = view.findViewById(R.id.instructions);
    mEditText = view.findViewById(R.id.edit_text);
    setPositiveButton(getString(R.string.dialog_btn_ok));
    setNegativeButton(getString(R.string.dialog_btn_cancel));
  }

  public void setPositiveButton(String okText)
  {
    setButton(DialogInterface.BUTTON_POSITIVE, okText, this::onButtonClick);
  }

  public String getString(@StringRes int res)
  {
    return mContext.getString(res);
  }

  public void setNegativeButton(String cancelText)
  {
    setButton(DialogInterface.BUTTON_NEGATIVE, cancelText, this::onButtonClick);
  }

  public void onButtonClick(DialogInterface dialog, int which)
  {
    assert dialog == this;
    if (which == BUTTON_POSITIVE) {
      mAnswer = String.valueOf(mEditText.getText());
    }
  }

  public String getAnswer()
  {
    return mAnswer;
  }

  public void setHint(String hint)
  {
    mEditText.setHint(hint);
  }

  public void setInitVal(String initVal)
  {
    mEditText.setText(initVal);
  }

  public void setInstructions(String instructions)
  {
    if (Util.isNoE(instructions)) {
      mInstructions.setVisibility(View.GONE);
    } else {
      mInstructions.setVisibility(View.VISIBLE);
      mInstructions.setText(instructions);
    }
  }
}
