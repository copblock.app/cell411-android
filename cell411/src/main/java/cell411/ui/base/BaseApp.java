package cell411.ui.base;

import static android.os.Environment.DIRECTORY_DOCUMENTS;
import static android.os.Environment.DIRECTORY_PICTURES;

import android.Manifest;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.widget.Toast;

import androidx.annotation.CallSuper;
import androidx.annotation.StringRes;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseGeoPoint;
import com.safearx.cell411.R;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.SocketException;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.LogicApp;
import cell411.android.ConnectivityNotifier;
import cell411.android.LifeCycleIH;
import cell411.android.ManifestInfo;
import cell411.android.UncaughtHandler;
import cell411.config.Config;
import cell411.config.ConfigDepot;
import cell411.methods.Dialogs;
import cell411.model.XAlert;
import cell411.model.XChatMsg;
import cell411.model.XChatRoom;
import cell411.model.XPrivacyPolicy;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.model.XRequest;
import cell411.model.XResponse;
import cell411.model.XUpdate;
import cell411.model.XUser;
import cell411.services.DataService;
import cell411.services.LocationService;
import cell411.utils.ImageUtils;
import cell411.utils.LocationUtil;
import cell411.utils.OnCompletionListener;
import cell411.utils.UrlUtils;
import cell411.utils.Util;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.except.RealExceptionHandler;
import cell411.utils.except.TheWolf;
import cell411.utils.io.SimplePrintStream;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

public abstract class BaseApp
  extends LogicApp
  implements BaseContext,
             ExceptionHandler,
             RealMessageForwarder,
             RealExceptionHandler
{
  private final static XTAG TAG = new XTAG();
  static {
    Reflect.announce("Loading Class");
    System.setOut(new SimplePrintStream(System.out));
  }
  private static final boolean smUseInterceptor = false;

  static {
    UncaughtHandler.startCatcher();
  }

  final AtomicReference<Config> smConfig;
  final DataService mDataService = DataService.init(this);
  final ObservableValueRW<Boolean> smConnected =
    new ObservableValueRW<>(Boolean.class, false);
  final ConnectListener mConnectListener = new ConnectListener();
  private final Stack<BaseActivity> mActivityStack = new Stack<>();
  private final ScreenOffReceiver mScreenOffReceiver = createAndRegister();
  private final ObservableValueRW<MainActivity> mMainActivity =
    new ObservableValueRW<>(MainActivity.class, null);
  private OkHttpClient smClient;
  private OkHttpClient.Builder mBuilder;

  {
    smConfig = new AtomicReference<>();
  }

  public BaseApp()
  {
    super();
    Reflect.announce(1, "Building instance");
    TheWolf.setRealExceptionHandler(this);
  }

  public static DataService getDataServer()
  {
    return req().mDataService;
  }

  @Nonnull
  public static BaseApp req()
  {
    return Objects.requireNonNull(opt());
  }

  @Nullable
  public static BaseApp opt()
  {
    return (BaseApp) LogicApp.opt();
  }

  ScreenOffReceiver createAndRegister()
  {
    return new ScreenOffReceiver();
  }

  abstract public LocationService getLocationService();

  @Override
  public synchronized void initParse()
  {
    if (Parse.isInitialized()) {
      return;
    }
    Reflect.announce("Starting Parse");
    Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
    ParseSyncUtils.registerSubclass(XUser.class);
    ParseSyncUtils.registerSubclass(XPublicCell.class);
    ParseSyncUtils.registerSubclass(XAlert.class);
    ParseSyncUtils.registerSubclass(XPrivateCell.class);
    ParseSyncUtils.registerSubclass(XPrivacyPolicy.class);
    ParseSyncUtils.registerSubclass(XResponse.class);
    ParseSyncUtils.registerSubclass(XRequest.class);
    ParseSyncUtils.registerSubclass(XChatRoom.class);
    ParseSyncUtils.registerSubclass(XChatMsg.class);
    ParseSyncUtils.registerSubclass(XUpdate.class);

    ParseSyncUtils.registerParseSubclasses();
    Parse.Configuration.Builder builder = new Parse.Configuration.Builder();

    String app = ConfigDepot.getAppId();
      URL url = ConfigDepot.getParseUrl();
    String key = ConfigDepot.getClientKey();
    String flavor = ConfigDepot.getFlavor();

    url = cleanUrl(url, flavor);
    builder.versionCode(""+ConfigDepot.getVersionCode());
    builder.versionName(ConfigDepot.getVersionName());
    builder.applicationId(app);
    builder.clientKey(key);
    builder.server(url);
    builder.clientBuilder(getClientBuilder());
    Parse.initialize(builder.build());
    Reflect.announce("Got Parse");
  }

  @androidx.annotation.Nullable
  private static URL cleanUrl(URL url, String flavor) {
    if(url !=null) {
      String str = url.toString();
      while (str.endsWith("/")) {
        str = str.substring(0, str.length() - 1);
      }
      str = str + "/" + flavor + "/";
      url = UrlUtils.toURL(str);
    }
    return url;
  }

  public synchronized OkHttpClient.Builder getClientBuilder()
  {
    if (mBuilder == null) {
      mBuilder = new OkHttpClient.Builder();
    }
    return mBuilder;
  }

  @Override
  protected void attachBaseContext(Context base)
  {
    super.attachBaseContext(base);
  }

  public synchronized OkHttpClient getHttpClient()
  {
    if (smClient == null) {
      smClient = getClientBuilder().build();
    }
    return smClient;
  }

  public ApplicationInfo getApplicationInfo()
  {
    if (getBaseContext() == null)
      return null;
    return super.getApplicationInfo();
  }

  @CallSuper
  @Override
  public void onCreate()
  {
    ConfigDepot.init(new ConfigImpl());
    registerActivityLifecycleCallbacks(LifeCycleIH.create());
    Reflect.announce("BaseApp");
    super.onCreate();
    setupConnectivityWatcher();
    addConnectionListener(
      (newValue, oldValue) -> System.out.println(mConnectListener));
    getLocationService().addObserver(new LocationObserver());
  }

  private void setupConnectivityWatcher()
  {
    ConnectivityNotifier notifier = ConnectivityNotifier.getNotifier(this);
    notifier.addListener(mConnectListener);
    smConnected.set(null);
    mConnectListener.networkConnectivityStatusChanged(this, null);
  }

  public void addConnectionListener(ValueObserver<Boolean> listener)
  {
    smConnected.addObserver(listener);
  }

  @Override
  public void realShowAlertDialog(String title, final String message,
                                  final OnCompletionListener listener)
  {
    req();
    if (ThreadUtil.isMainThread()) {
      Dialogs.realShowAlertDialog(message, listener);
    } else {
      req();
      ThreadUtil.onMain(() -> realShowAlertDialog(title, message, listener));
    }
  }

  @Override
  public void realShowYesNoDialog(String title, String text,
                                  OnCompletionListener listener)
  {
    req();
    Dialogs.showYesNoDialog(title, text, listener);
  }

  @Override
  public void realShowToast(String msg)
  {
    req();
    BaseActivity curr = getCurrentActivity();
    if (curr != null && ThreadUtil.isMainThread()) {
      Toast.makeText(curr, msg, Toast.LENGTH_LONG).show();
    } else {
      req();
      ThreadUtil.onMain(() -> realShowToast(msg), (long) 250);
    }
  }

  @Override
  public void realHandleException(@Nonnull String whileDoing,
                                  @Nonnull Throwable pe,
                                  @Nullable OnCompletionListener listener,
                                  final boolean dialog)
  {
    req();
    if (pe instanceof ThreadDeath) {
      System.out.println("" + pe);
      throw (ThreadDeath) pe;
    }
    pe.printStackTrace();
    if (isBadSessionException(pe)) {
      handleBadSessionException();
    } else if (isNetworkException(pe) && !smConnected.get()) {
      Reflect.announce(1, "Ignoring network exception while disconnected");
    } else if (isBadGatewayException(pe)) {
      XLog.i(TAG, "Ignoring bad gateway exception");
    } else if (dialog) {
      Dialogs.showExceptionDialog(pe, whileDoing, listener);
    }
  }

  public static boolean isBadSessionException(Throwable e)
  {
    if (e instanceof ParseException) {
      ParseException pe = (ParseException) e;
      switch (pe.getCode()) {
        case ParseException.SESSION_MISSING:
        case ParseException.INVALID_LINKED_SESSION:
        case ParseException.INVALID_SESSION_TOKEN:
          return true;
      }
    }
    return false;
  }

  private void handleBadSessionException()
  {
    showSessionExpiredDialog();
  }

  private boolean isNetworkException(final Throwable t)
  {
    if (t instanceof ParseException) {
      ParseException pe = (ParseException) t;
      Throwable cause = pe.getCause();
      if (pe.getCode() == ParseException.CONNECTION_FAILED) {
        if (cause instanceof SocketException) {
          SocketException se = (SocketException) cause;
          String message = se.getMessage();
          if (message == null) {
            return false;
          } else {
            return message.equals("Software caused connection abort");
          }
        }
      }
    }
    return false;
  }

  public boolean isBadGatewayException(Throwable e)
  {
    if (!(e instanceof ParseException)) {
      return false;
    }
    ParseException pe = (ParseException) e;
    String message = pe.getMessage();
    return "502 Bad Gateway".equals(message);
  }

  private void showSessionExpiredDialog()
  {
    XUser.logOut();
    Dialogs.showSessionExpiredAlertDialog(success ->
    {
      try {
        XUser.logOut();
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        XUser.logOut();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Intent intent = new Intent(BaseApp.this, getMainActivityClass());
      intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
      BaseActivity activity = getCurrentActivity();
      if (activity == null) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
      } else {
        activity.startActivity(intent);
      }
    });
  }

  protected abstract Class<?> getMainActivityClass();

  public BaseActivity getCurrentActivity()
  {
    if (hasCurrentActivity()) {
      return mActivityStack.peek();
    } else {
      return null;
    }
  }

  public boolean hasCurrentActivity()
  {
    return !mActivityStack.isEmpty();
  }

  public boolean isConnected()
  {
    return smConnected.get() == Boolean.TRUE;
  }

  @Override
  protected void finalize()
  throws Throwable
  {
    removeConnectionListener(null);
    System.out.println(getScreenOffReceiver());
    super.finalize();
  }

  public void removeConnectionListener(ValueObserver<Boolean> changed)
  {
    if(smClient!=null)
      smConnected.removeObserver(changed);
  }

  public ScreenOffReceiver getScreenOffReceiver()
  {
    return mScreenOffReceiver;
  }

  public void onBootComplete()
  {
    XLog.i(TAG, "onBootComplete called");
  }

  public Set<String> getMissingPermissions()
  {
    BaseActivity activity = req().getCurrentActivity();
    if (activity == null) {
      return getAllPermissions();
    }
    return getMissingPermissions(activity);
  }

  public BaseActivity activity()
  {
    return getCurrentActivity();
  }

  public abstract Set<String> getAllPermissions();

  public Set<String> getMissingPermissions(BaseActivity activity)
  {
    HashSet<String> res = new HashSet<>();
    for (String perm : getAllPermissions()) {
      if (!activity.hasPerm(perm)) {
        res.add(perm);
      }
    }
    return res;
  }

  public SharedPreferences getAppPrefs()
  {
    return getSharedPreferences("AppPrefs", MODE_PRIVATE);
  }

  public File getJsonCacheFile(String name)
  {
    return new File(ConfigDepot.getFlavorDocDir(), name + ".json");
  }

  protected abstract void sendLocationNotification(ParseGeoPoint geoPoint);

  ObservableValueRW<String> mTopAct = new ObservableValueRW<>();
  {
    mTopAct.addObserver(
      (newValue, oldValue) -> System.out.println("New Top Act: "+newValue));
  }
  public void pushActivity(BaseActivity baseActivity)
  {
    mActivityStack.push(baseActivity);
    if (baseActivity instanceof MainActivity) {
      setMainActivity((MainActivity) baseActivity);
    }
    if(mActivityStack.isEmpty()) {
      mTopAct.set("NONE");
    } else {
      Object obj = mActivityStack.peek();
      if(obj==null)
        mTopAct.set("Null");
      else
        mTopAct.set(obj.getClass().getSimpleName());
    }
  }

  @CallSuper
  protected void setMainActivity(MainActivity baseActivity)
  {
    mMainActivity.set(baseActivity);
  }

  public void popActivity(BaseActivity baseActivity)
  {
    assert mActivityStack.peek() == baseActivity;
    mActivityStack.pop();
    if(mActivityStack.isEmpty()) {
      mTopAct.set("NONE");
    } else {
      Object obj = mActivityStack.peek();
      if(obj==null)
        mTopAct.set("Null");
      else
        mTopAct.set(obj.getClass().getSimpleName());
    }
  }

  public MainActivity getMainActivity()
  {
    return mMainActivity.get();
  }

  public Boolean hasPermissions()
  {
    Reflect.announce();
    BaseActivity activity = getCurrentActivity();
    if (activity == null) {
      Reflect.announce("No Activity");
      return false;
    }
    Set<String> missing = new HashSet<>(getAllPermissions());
    missing.removeIf(activity::hasPerm);
    if(missing.isEmpty()) {
      Reflect.announce("All Present!");
      return true;
    } else {
      Reflect.announce(Util.join(",", missing));
      return false;
    }
  }

  abstract public int getPrimaryColor();

  public abstract boolean isDone();

  public abstract int getNotificationWidth();

  public abstract int getNotificationHeight();

  public void sendToast(Throwable e)
  {
    showToast(String.valueOf(e));
  }

  public void sendToast(String format, Object[] args)
  {
    showToast(format, args);
  }

  public void sendToast(int format, Object[] args)
  {
    showToast(format, args);
  }

  static class ScreenOffReceiver
    extends BroadcastReceiver
  {
    boolean mScreenOn = true;
    String mText = "[ ScreenOffReceiver: Not yet set ]";

    public ScreenOffReceiver()
    {
      Manifest manifest;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
      mText = Util.format("ScreenOffReceiver received:  %s", mScreenOn);
      mScreenOn = Objects.equals(intent.getAction(), Intent.ACTION_SCREEN_ON);
    }

    @Nonnull
    public String toString()
    {
      return mText;
    }
  }

  class LocationObserver
    implements ValueObserver<Location>,
               Runnable
  {
    Location mNewValue;
    Location mOldValue;
    long mLastUpdate = 0;
    long mLastSave = 0;

    @Override
    public void onChange(@Nullable final Location newValue,
                         @Nullable final Location oldValue)
    {
      mNewValue = newValue;
      mOldValue = oldValue;
      mLastUpdate = System.currentTimeMillis();
      run();
    }

    @Override
    public void run()
    {
      if (mLastSave >= mLastUpdate) {
        return;
      }
      if (!Parse.isInitialized() || Parse.getCurrentUser() == null) {
        return;
      }
      ParseGeoPoint geoPoint = LocationUtil.getGeoPoint(mNewValue);
      if (geoPoint == null) {
        return;
      }
      XUser user = XUser.reqCurrentUser();
      ParseGeoPoint lastLoc = user.getLocation();
      if (lastLoc == null || geoPoint.distanceInKilometersTo(lastLoc) > 0.1) {
        sendLocationNotification(geoPoint);
        user.setLocation(geoPoint);
        user.save();
      }
    }

  }

  class ConnectListener
    implements ConnectivityNotifier.ConnectivityListener
  {
    @Override
    public void networkConnectivityStatusChanged(Context context, Intent intent)
    {
      smConnected.set(ConnectivityNotifier.isConnected(context));
    }

    @Nonnull
    public String toString()
    {
      return Util.format("[ConnectListener; Net is %s ]", smConnected);
    }
  }

  class ConfigImpl
    implements Config
  {
    private final ApplicationInfo mAppInfo;
    private final Builder mHttpClientBuilder;
    private final String mPackageName;
    private final PackageInfo mPkgInfo;
    private final String mAppVersion;
    private final String mAppName;
    private final File mParseDir;
    private final ObservableValueRW<Boolean> mConnected =
      new ObservableValueRW<>(false);
    private final ObservableValueRW<URL> mParseUrl
      = new ObservableValueRW<>();

    public ConfigImpl()
    {
      try {
        PackageManager pm = getPackageManager();
        mPackageName = BaseApp.this.getPackageName();
        mParseDir = BaseApp.this.getDir("Parse", MODE_PRIVATE);
        mPkgInfo = pm.getPackageInfo(mPackageName, 0);
        mAppVersion = String.valueOf(mPkgInfo.versionCode);
        mAppInfo = pm.getApplicationInfo(mPackageName, 0);
        mAppName = pm.getApplicationLabel(mAppInfo).toString();
        String urlStr= getString(R.string.parse_api_url);
        mParseUrl.set(UrlUtils.toURL(urlStr));
        mHttpClientBuilder = new Builder();
        addConnectionListener(mConnected);
      } catch (Exception ex) {
        throw Util.rethrow(ex);
      }
    }

    @Override
    public int getVersionCode()
    {
      return ManifestInfo.req().getVersionCode();
    }

    @Override
    public Builder getHttpClientBuilder()
    {
      return mHttpClientBuilder;
    }

    @Override
    public String getAppId()
    {
      return getString(R.string.parse_application_id);
    }

    @Override
    public String getAppName()
    {
      return mAppName;
    }

    public File getDocDir()
    {
      return getExtDir(DIRECTORY_DOCUMENTS);
    }
    public File getPicDir()
    {
      return getExtDir(DIRECTORY_PICTURES);
    }

    @Override
    public File getTempDir() {
      return getExtDir("temp");
    }

    @Override
    public File getCacheDir(String keyVal) {
      return getExtDir(keyVal);
    }

    @Override
    public String getAppVersion()
    {
      return mAppVersion;
    }

    @Override
    public String getClientKey()
    {
      return getString(R.string.parse_client_key);
    }

    @Override
    public String getFlavor()
    {
      return getString(R.string.parse_api_flavor);
    }

    @Override
    public String getPackageName()
    {
      return mPackageName;
    }

    @Override
    public URL getParseUrl()
    {
      return mParseUrl.get();
    }

    @Override
    public String getString(@StringRes int resId)
    {
      return req().getString(resId);
    }

    @Override
    public String getVersionName()
    {
      return ManifestInfo.req().getVersionName();
    }

    @Override
    public File getExtDir(String sub)
    {
      return getDir(sub, Context.MODE_PRIVATE);
    }

    public Bitmap getPlaceHolder()
    {
      return ImageUtils.getPlaceHolder();
    }

    public Application getApp()
    {
      return BaseApp.this;
    }

    public File getCacheDir()
    {
      return req().getCacheDir();
    }

    public File getParseDir()
    {
      return mParseDir;
    }

    @Override
    public boolean isConnected()
    {
      return mConnected.get() != null && mConnected.get();
    }

    @Override
    public File getAvatarDir()
    {
      return getPictureDir();
    }

    @Override
    public File getPictureDir()
    {
      return new File(BaseApp.req().getFilesDir(), "avatars");
    }

  }
}
