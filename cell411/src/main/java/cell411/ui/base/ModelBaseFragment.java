package cell411.ui.base;

import cell411.logic.LiveQueryService;

public class ModelBaseFragment
  extends BaseFragment
{

  public ModelBaseFragment(int layout)
  {
    super(layout);
        assert LiveQueryService.opt().isReady();
  }

}
