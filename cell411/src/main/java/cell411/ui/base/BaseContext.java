package cell411.ui.base;

import android.content.SharedPreferences;

import cell411.model.XEntity;
import cell411.utils.except.ExceptionHandler;

public interface BaseContext
  extends ExceptionHandler
{

  default void hideSoftKeyboard()
  {
    BaseApp app = BaseApp.opt();
    if(app==null)
      return;
    BaseActivity activity = app.getCurrentActivity();
    if (activity == null)
      return;
    activity.hideSoftKeyboard();
  }

  default void showSoftKeyboard()
  {
    BaseApp app = BaseApp.opt();
    if(app==null)
      return;
    BaseActivity activity = app.getCurrentActivity();
    if (activity == null)
      return;
    activity.hideSoftKeyboard();
  }

  default SharedPreferences getAppPrefs()
  {
    return BaseApp.req().getAppPrefs();
  }

  default void openChat(XEntity publicCell)
  {
    BaseApp.req().openChat(publicCell);
  }
}
