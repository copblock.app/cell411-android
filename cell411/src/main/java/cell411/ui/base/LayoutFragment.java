package cell411.ui.base;

import androidx.annotation.LayoutRes;

import com.safearx.cell411.R;

public class LayoutFragment
  extends ModelBaseFragment
{
  public LayoutFragment()
  {
    super(R.layout.empty_layout);
  }

  public LayoutFragment(@LayoutRes int layout)
  {
    super(layout);
  }
}
