package cell411.ui.welcome;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.MainThread;



import com.safearx.cell411.R;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.BaseFragment;
import cell411.ui.base.XSelectFragment;
import cell411.ui.widget.GalleryPageFragment;
import cell411.utils.collect.Collect;
import cell411.utils.reflect.Reflect;

public class GalleryFragment
  extends XSelectFragment
{
  private Button mBtnSignIn;
  private Button mBtnSignUp;
  private AccountFragment mAccountFragment;

  public GalleryFragment()
  {
    super(R.layout.fragment_account);
  }

  @Override
  public List<Class<? extends BaseFragment>> getTypes()
  {
    return Collect.asList(GalleryPageFragment.class, GalleryPageFragment.class,
      GalleryPageFragment.class);
  }

  @Override
  public int getHeaderVisibility()
  {
    return View.VISIBLE;
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    assert isAdded();
    mAccountFragment = (AccountFragment) getParentFragment();
    mBtnSignIn = view.findViewById(R.id.btn_signin);
    mBtnSignUp = view.findViewById(R.id.btn_signup);
    mBtnSignIn.setOnClickListener(this::onButtonPressed);
    mBtnSignUp.setOnClickListener(this::onButtonPressed);
  }

  private void onButtonPressed(View view)
  {
    if (view == mBtnSignIn) {
      mAccountFragment.selectFragment(LoginFragment.class);
    } else if (view == mBtnSignUp) {
      mAccountFragment.selectFragment(RegisterFragment.class);
    } else {
      showToast("Unexpected view: " + view);
    }
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @MainThread
  @Override
  public void onSaveInstanceState(@Nonnull Bundle outState)
  {
    Reflect.announce("onSaveInstanceState");
    super.onSaveInstanceState(outState);
  }

}
