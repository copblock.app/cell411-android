package cell411.ui.welcome;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.safearx.cell411.R;

import java.util.Collection;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseFragment;

public class PermissionFragment
  extends BaseFragment
{
  ActivityResultContracts.RequestMultiplePermissions mContract =
    new ActivityResultContracts.RequestMultiplePermissions();
  ActivityResultLauncher<String[]> mLauncher =
    registerForActivityResult(mContract, this::callback);

  public PermissionFragment()
  {
    super(R.layout.fragment_permission);
  }

  private void callback(final Map<String, Boolean> results)
  {
    int missing = 0;
    for (String key : results.keySet()) {
      Boolean val = results.get(key);
      System.out.println(key + ":  " + val);
      if (val == null || !val) {
        missing++;
      }
    }
    if (missing == 0) {
      pop();
    }
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

    TextView txtPermissionTitle = view.findViewById(R.id.name);
    String title =
      getString(R.string.permission_title, getString(R.string.app_name));
    txtPermissionTitle.setText(title);
    TextView txtBtnGrantAccess = view.findViewById(R.id.txt_btn_grant_access);
    txtBtnGrantAccess.setOnClickListener(this::onGrantClicked);
  }
  int mLastCount = -1;
  private void onGrantClicked(View view)
  {
    BaseApp app = BaseApp.req();
      Collection<String> missingPerms = app.getMissingPermissions(BaseApp.req().getCurrentActivity());
    if(missingPerms.size()==mLastCount){
      pop();
    } else {
      mLastCount=missingPerms.size();
      String[] missingPermArray = missingPerms.toArray(new String[0]);
      mLauncher.launch(missingPermArray);
    }
  }

}

