package cell411.ui.welcome;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.model.ParseUser;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.config.UtilApp;
import cell411.methods.Dialogs;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseFragment;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.collect.ObservableValueRW;

public class LoginFragment
  extends BaseFragment
{
  private static final String TAG = "LoginActivity";
  final ObservableValueRW<Boolean> mLoggingIn = new ObservableValueRW<>();
  private final ArrayList<View> mToggleButtons = new ArrayList<>();
  ButtonHandler mButtonHandler = new ButtonHandler();
  private EditText etEmail;
  private EditText etPassword;
  private TextView txtBtnLogin;
  private AccountFragment mAccountFragment;
  private TextView mTxtBtnForgotPassword;
  private TextView mTxtBtnSignUp;

  {
    mLoggingIn.addObserver(new LoggingInObserver());
    mLoggingIn.set(false);
  }

  public LoginFragment()
  {
    super(R.layout.fragment_login);
  }

  @Nonnull
  private String getEmail()
  {
    return etEmail.getText().toString().trim().toLowerCase(Locale.US);
  }

  @Nonnull
  private String getPassword()
  {
    return etPassword.getText().toString().trim();
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    assert isAdded();
    mAccountFragment = (AccountFragment) getParentFragment();

    etEmail = view.findViewById(R.id.et_email);
    etPassword = view.findViewById(R.id.et_password);
    txtBtnLogin = view.findViewById(R.id.txt_btn_login);
    mTxtBtnForgotPassword = view.findViewById(R.id.txt_btn_forgot_password);
    mTxtBtnSignUp = view.findViewById(R.id.txt_btn_sign_up);

    mToggleButtons.add(txtBtnLogin);

    txtBtnLogin.setOnClickListener(mButtonHandler);
    mTxtBtnForgotPassword.setOnClickListener(mButtonHandler);
    mTxtBtnSignUp.setOnClickListener(mButtonHandler);
    ViewGroup group = view.findViewById(R.id.button_grid);
    BaseActivity currentActivity = BaseApp.req().getCurrentActivity();
    Activity activity = getActivity();
    for (int i = 0; i < 8; i++) {
      Button button = new Button(activity);
      mToggleButtons.add(button);
      String text = "Dev" + (i + 1);
      button.setText(text);
      button.setOnClickListener(mButtonHandler);
      group.addView(button);
    }

    view.findViewById(R.id.rl_separator).setVisibility(View.GONE);
    view.findViewById(R.id.txt_copblock_plug).setVisibility(View.VISIBLE);
  }

  class ButtonHandler
    implements Runnable,
               OnClickListener
  {
    @Override
    public void onClick(final View view)
    {
      if (view == mTxtBtnForgotPassword) {
        Dialogs.showForgotPasswordDialog(BaseApp.req().getCurrentActivity(), success ->
        {
          if (success) {
            showAlertDialog("alert", "Request sent");
          } else {
            showAlertDialog("alert", "Request not sent");
          }
        });
      } else if (view == mTxtBtnSignUp) {
        mAccountFragment.selectFragment(RegisterFragment.class);
      } else {
        if (view instanceof Button) {
          Button button = (Button) view;
          String email =
            button.getText().toString().toLowerCase() + "@copblock.app";
          etEmail.setText(email);
          etPassword.setText(R.string.bullshit_password);
        }
        if (ThreadUtil.isMainThread()) {
          ThreadUtil.onExec(this);
        } else {
          run();
        }
      }
    }

    public void run()
    {
      if (mLoggingIn.get()) {
        UtilApp.req().showToast("Please Wail", new Object[]{});
      } else {
        String email = getEmail();
        String password = getPassword();
        if (email.isEmpty()) {
          BaseApp.req().showToast(R.string.validation_email);
        } else if (password.isEmpty()) {
          BaseApp.req().showToast(R.string.validation_password);
        } else {
          hideSoftKeyboard();
          if (!email.contains("@")) {
            email = email + "@copblock.app";
          }
          try {
            mLoggingIn.set(true);
            ParseUser.logIn(email, password);
          } catch (Throwable pe) {
            pe.printStackTrace();
            handleException("logging in", pe);
          } finally {
            mLoggingIn.set(false);
          }
        }
      }
    }

    private void forgotPasswordClicked(View view)
    {
    }
  }

  private class LoggingInObserver
    implements ValueObserver<Boolean>,
               Runnable
  {

    private Integer mVis = View.INVISIBLE;

    @Override
    public void onChange(@Nullable Boolean newValue, @Nullable Boolean oldValue)
    {
      mVis = (newValue == Boolean.FALSE) ? View.VISIBLE : View.INVISIBLE;
      ThreadUtil.onMain(this);
    }

    @Override
    public void run()
    {
      for (View button : mToggleButtons) {
        button.setVisibility(mVis);
      }
    }
  }
}

