package cell411.ui.welcome;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import cell411.model.XUser;
import cell411.ui.Cell411GuiUtils;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.OnCompletionListener;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;
import com.parse.ParseException;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.function.Function;

public class UserConsentFragment
  extends ModelBaseFragment
{
  private final XUser mUser = XUser.reqCurrentUser();
  ConsentUpdater mSaveCallback = new ConsentUpdater();
  private ArrayList<CheckBox> mCBList;
  private TextView mTxtBtnOk;
  private TextView mTxtTitle;
  private String mTextTitle;
  private String mTextDescription;
  private TextView mTxtDescription;

  public UserConsentFragment()
  {
    super(R.layout.fragment_user_consent);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mTxtTitle = view.findViewById(R.id.page_title);
    mTextTitle = getString(R.string.title_privacy_policy);
    mTxtDescription = view.findViewById(R.id.description);
    mTextDescription = getString(R.string.description_privacy_policy);
    mCBList = new ArrayList<>();
    mCBList.add(view.findViewById(R.id.cb_we_dont_sell_or_give_data));
    mCBList.add(view.findViewById(R.id.cb_can_delete_account));
    mCBList.add(view.findViewById(R.id.cb_you_agree_to_process_data));
    mCBList.add(view.findViewById(R.id.cb_read_privacy_policy));
    mTxtBtnOk = view.findViewById(R.id.txt_btn_ok);
    Drawable drawable = mCBList.get(0).getButtonDrawable();
    Function<URLSpan, ClickableSpan> pSpan = PolicySpan::new;
    Cell411GuiUtils.setTextViewHTML(mTxtDescription, mTextDescription, pSpan);
    Cell411GuiUtils.setTextViewHTML(mTxtTitle, mTextTitle, pSpan);

    CompoundButton.OnCheckedChangeListener onCheckedChangeListener =
      (compoundButton, isChecked) ->
      {
        mTxtBtnOk.setEnabled(allChecked(mCBList));
      };
    for (CheckBox box : mCBList) {
      box.setOnCheckedChangeListener(onCheckedChangeListener);
    }
    mTxtBtnOk.setOnClickListener(new ConsentUpdater());
  }

  private boolean allChecked(ArrayList<CheckBox> cbList)
  {
    for (CheckBox box : cbList) {
      if (!box.isChecked()) {
        return false;
      }
    }
    return true;
  }

  class ConsentUpdater
    implements OnClickListener,
               OnCompletionListener
  {
    // This thing handles a whole flow.  onClick gets called when the user
    // clicks consent.

    // It saves the user on another thread, and gets a callback when done.

    // If the save was ok, it shuts pops itself, but if not it asks the
    // user if they want to try again.  If so, we return to the start.


    @Override
    public void done(boolean success)
    {
      if (success) {
        onClick(null);
      } else {
        pop();
      }
    }

    public void onClick(View v)
    {
      if (!allChecked(mCBList)) {
        showToast("Still More Items!");
        return;
      }
      mUser.setConsented(true);
      ThreadUtil.onExec(() ->
      {
        try {
          mUser.save();
          pop();
        } catch ( Exception e ) {
          showYesNoDialog("Error", "Failed to save User", this);
        }
      });
    }
    public void done(ParseException e)
    {
    }
  }

  class PolicySpan
    extends ClickableSpan
  {
    private final URLSpan span;

    PolicySpan(URLSpan span)
    {
      this.span = span;
    }

    public void onClick(View view)
    {
      String url;
      String spanUrl = span.getURL();
      if (spanUrl.equalsIgnoreCase("terms")) {
        url = getString(R.string.terms_and_conditions_url);
      } else if (spanUrl.equalsIgnoreCase("privacy_policy")) {
        url = getString(R.string.privacy_policy_url);
      } else {
        Cell411.req().showAlertDialog("alert", "Unexpected link url: " + spanUrl);
        return;
      }
      Intent intentWeb = new Intent(Intent.ACTION_VIEW);
      if (!url.isEmpty()) {
        intentWeb.setData(Uri.parse(url));
      }
      startActivity(intentWeb);
    }
  }
}

