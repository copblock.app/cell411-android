package cell411.ui.cell;

import static cell411.enums.RequestType.CellJoinRequest;
import static cell411.utils.ViewType.vtNull;
import static cell411.utils.ViewType.vtPublicCell;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cell411.ui.base.BaseApp;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.model.ParseGeoPoint;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.Rel;
import cell411.model.XPublicCell;
import cell411.model.XRequest;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.services.DataService;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.LocationUtil;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class PublicCellSearchFragment
  extends ModelBaseFragment
{
  private static final XTAG TAG = new XTAG();
  private final List<XItem> mAllCells = new ArrayList<>();
  private final Rel mOwnedRequests;
  private final Rel mJoinedCells;
  private final Rel mOwnerOfCells;
  private final RelationWatcher watcher;
  TreeSet<String> mRemoved = new TreeSet<>();
  FragmentFactory mPublicCellMembersFactory =
    FragmentFactory.fromClass(PublicCellMembersFragment.class);
  LiveQueryService lqs = LiveQueryService.opt();
  private SearchView mSearchView;
  private SeekBar sbRadius;
  private RadioButton rbNearBy;
  private TextView mTextRadius;
  private CellListAdapter mAdapter;
  //  private QueryRunner<XPublicCell> mRunner;
  private ParseGeoPoint mLocation;

  {
    XUser user = XUser.reqCurrentUser();
    assert lqs != null;
    watcher = lqs.getRelationWatcher();
    mOwnedRequests = watcher.getOwnedRequests();
    mJoinedCells = watcher.getJoinedPublicCells();
    mOwnerOfCells = watcher.getOwnedPublicCells();
  }

  public PublicCellSearchFragment()
  {
    super(R.layout.explore_cells_fragment);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mAdapter = createAdapter();

    RecyclerView cellView = view.findViewById(R.id.lvCells);
    cellView.setLayoutManager(new LinearLayoutManager(getContext()));
    cellView.setAdapter(mAdapter);
    mTextRadius = view.findViewById(R.id.txt_radius);
    TextView count = view.findViewById(R.id.count);
    mSearchView = view.findViewById(R.id.searchView);
    rbNearBy = view.findViewById(R.id.rb_nearby);
    sbRadius = view.findViewById(R.id.sb_radius);
    mSearchView.setIconifiedByDefault(false);
    mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
    {
      @Override
      public boolean onQueryTextSubmit(String query)
      {
        return true;
      }

      @Override
      public boolean onQueryTextChange(String query)
      {
        return true;
      }
    });
    sbRadius.setProgress(
      Cell411.req().getAppPrefs().getInt("SearchRadius", 100));
    sbRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
    {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress,
                                    boolean fromUser)
      {
        saveRadius();
        mTextRadius.setText(LocationUtil.formatDistance(progress));
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar)
      {
      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar)
      {
      }
    });
    rbNearBy.setOnCheckedChangeListener(this::onCheckedChanged);
    mSearchView.setEnabled(true);
    sbRadius.setEnabled(true);
    rbNearBy.setEnabled(true);
    showSoftKeyboard();
  }

  private CellListAdapter createAdapter()
  {
    CellListAdapter res = new CellListAdapter();//getContext(), R.layout
    mOwnerOfCells.addObserver(res);
    mJoinedCells.addObserver(res);
    mOwnedRequests.addObserver(res);
    return res;
  }

  public void saveRadius()
  {
    SharedPreferences.Editor ed = Cell411.req().getAppPrefs().edit();
    ed.putInt("SearchRadius", sbRadius.getProgress());
    ed.apply();
  }

  private void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
  {
  }

  @Override
  public void onResume()
  {
    super.onResume();
    showSoftKeyboard();
    mSearchView.setEnabled(true);
    sbRadius.setEnabled(true);
    rbNearBy.setEnabled(true);
    mLocation = BaseApp.req().getLocationService().getParseGeoPoint();
    if(mLocation!=null) {
      DataService.get().requestAddress(mLocation, address -> XLog.i(TAG, address.mAddress));
    } else {
      showToast("Unable to geocode address ... no location avail");
    }
  }

  @Override
  public void onPause()
  {
    super.onPause();
    //    mRunner = null;
  }

  @CallSuper
  public void loadData2()
  {
    throw new Error("Not Implemented");
    //    ParseQuery<XPublicCell> query = constructQuery();
    //    if (query != null) {
    //      hideSoftKeyboard();
    //      mRunner = new QueryRunner<>(query, this::getRunner, evt ->
    //      {
    //        QueryRunner<XPublicCell> runner = (QueryRunner<XPublicCell>)
    //        evt.getSource();
    //
    //        synchronized (mAllCells) {
    //          mAllCells.clear();
    //          mAllCells.addAll(Util.transform(runner.getList(), XItem::new));
    //        }
    //        mAdapter.run();
    //      });
    //    }
    //
    //    if (mRunner != null) {
    //      mRunner.run();
    //    }
  }

  @CallSuper
  public void loadData3()
  {
  }

  public void updateData()
  {
    mAdapter.run();
  }

  @Nullable
  private ParseQuery<XPublicCell> constructQuery()
  {
    ParseGeoPoint location = null;
    if (rbNearBy != null && rbNearBy.isChecked()) {
      location = mLocation;
    }
    String searchRegex = constructRegex();
    if (location == null && searchRegex == null) {
      return null;
    }
    ParseQuery<XPublicCell> query = XPublicCell.q();
    if (location != null) {
      query.whereWithinMiles("location", mLocation, sbRadius.getProgress());
    }
    query = ParseQuery.or(query, XPublicCell.q().whereDoesNotExist("location"));
    if (searchRegex != null) {
      query.whereMatches("name", searchRegex, "i");
    }
    return query.whereNotEqualTo("owner", Parse.getCurrentUser());
  }

  @Nullable
  private String constructRegex()
  {
    String query = mSearchView.getQuery().toString();
    if (Util.isNoE(query)) {
      return null;
    }
    String[] queryStringArr = query.trim().split(" ");
    StringBuilder searchRegex = new StringBuilder(queryStringArr[0]);
    for (int i = 1; i < queryStringArr.length; i++) {
      searchRegex.append("|").append(queryStringArr[i]);
    }
    return searchRegex.toString();
  }

  private void onJoinButtonClicked(View view)
  {
    XItem item = (XItem) view.getTag();
    if (item.getViewType() != ViewType.vtPublicCell) {
      showAlertDialog("alert", "That button is not connected to a cell");
    } else {
      BaseApp.getDataServer().handleRequest(CellJoinRequest, item.getParseObject(),
        this::onJoinComplete, null);
    }
  }

  private void onJoinComplete(ParseException e)
  {
    if (e != null)
      Reflect.announce("ParseException: " + e);
  }

  private void onCellClicked(View view)
  {
    XItem item = (XItem) view.getTag();
    if (item.getViewType() != ViewType.vtPublicCell) {
      return;
    }
    mPublicCellMembersFactory.setObjectId(item.getObjectId());
    push(mPublicCellMembersFactory);
  }

  private static class ViewHolder
    extends RecyclerView.ViewHolder
  {
    public ImageView mImgVerified;
    public TextView mTxtCellName;
    public TextView mTxtDistance;
    public ImageView mImgChat;
    public TextView mBtnAction;
    public View mCellView;

    public ViewHolder(View v)
    {
      super(v);
      mCellView = v;
      mBtnAction = v.findViewById(R.id.txt_btn_action);
      mImgChat = v.findViewById(R.id.img_chat);
      mTxtCellName = v.findViewById(R.id.txt_cell_name);
      mTxtDistance = v.findViewById(R.id.txt_distance);
      mImgVerified = v.findViewById(R.id.img_verified);
      mBtnAction.setEnabled(true);
      mBtnAction.setText(R.string.join);
      mImgChat.setVisibility(View.GONE);
    }
  }

  class CellListAdapter
    extends RecyclerView.Adapter<ViewHolder>
    implements MyObserver,
               Runnable
  {
    final ArrayList<XItem> mItems = new ArrayList<>();

    CellListAdapter()
    {

    }

    @Nonnull
    @Override
    public ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int vType)
    {
      ViewType viewType = ViewType.valueOf(vType);
      View v;
      if (viewType == vtPublicCell) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_public_cell, parent, false);
      } else if (viewType == vtNull) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_public_cell, parent, false);
      } else {
        throw new RuntimeException("Bad ViewType: " + viewType);
      }
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@Nonnull ViewHolder vh, int position)
    {
      final XItem item = mItems.get(position);
      ViewType viewType = item.getViewType();
      if (viewType == vtPublicCell) {
        vh.mBtnAction.setOnClickListener(
          PublicCellSearchFragment.this::onJoinButtonClicked);
        vh.mCellView.setOnClickListener(
          PublicCellSearchFragment.this::onCellClicked);
        vh.mBtnAction.setTag(item);
        vh.mCellView.setTag(item);
        vh.mImgChat.setTag(item);
        vh.mTxtCellName.setTag(item);
        vh.mTxtCellName.setText(item.getText());
        vh.mImgChat.setVisibility(View.GONE);
        vh.mTxtDistance.setTag(item);
        if (mLocation == null) {
          String our_location_is_unknown = "Our location is unknown";
          vh.mTxtDistance.setText(our_location_is_unknown);
        } else {
          ParseGeoPoint cellLocation = item.getPublicCell().getLocation();
          if (cellLocation == null) {
            String cell_location_unknown = "Cell location unknown";
            vh.mTxtDistance.setText(cell_location_unknown);
          } else {
            vh.mTxtDistance.setText(LocationUtil.getFormattedDistance(mLocation,
              item.getPublicCell().getLocation()));
          }
        }
        if (item.getPublicCell().isVerified()) {
          vh.mImgVerified.setVisibility(View.VISIBLE);
        } else {
          vh.mImgVerified.setVisibility(View.INVISIBLE);
        }
      } else {
        throw new IllegalArgumentException("Unexpected ViewType: " + viewType);
      }
    }

    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void run()
    {
      if (!ThreadUtil.isMainThread()) {
        ThreadUtil.onMain(this);
        return;
      }
      mRemoved.clear();
      for (String id : mOwnedRequests.getRelatedIds()) {
        XRequest request = Parse.getObject(id);
        if (request == null) {
          continue;
        }
        XPublicCell cell = request.getCell();
        if (cell == null) {
          continue;
        }
        mRemoved.add(cell.getObjectId());
      }
      mRemoved.addAll(mOwnerOfCells.getRelatedIds());
      mRemoved.addAll(mJoinedCells.getRelatedIds());
      Comparator<XItem> comp =
        Comparator.comparing(XItem::getText).thenComparing(XItem::getObjectId);

      mAllCells.sort(comp);
      ArrayList<XItem> xItems = new ArrayList<>(mAllCells);
      for (int i = 0; i < xItems.size(); i++) {
        if (mRemoved.contains(xItems.get(i).getObjectId())) {
          xItems.remove(i--);
        }
      }
      int acIndex = 0;
      int itIndex = 0;
      while (acIndex < xItems.size() && itIndex < mItems.size()) {
        XItem acItem = xItems.get(acIndex);
        String acText = acItem.getText();
        XItem itItem = mItems.get(itIndex);
        String itText = itItem.getText();
        System.out.println(acText);
        System.out.println(itText);
        int cmp = comp.compare(acItem, itItem);
        System.out.println(cmp);
        if (cmp < 0) {
          mItems.add(itIndex, acItem);
          notifyItemInserted(itIndex);
        } else if (cmp > 0) {
          mItems.remove(itIndex);
          notifyItemRemoved(itIndex);
        } else {
          acIndex++;
          itIndex++;
        }
      }
      if (acIndex < xItems.size()) {
        while (acIndex < xItems.size()) {
          mItems.add(xItems.get(acIndex++));
          notifyItemInserted(mItems.size() - 1);
        }
      } else {
        while (itIndex < mItems.size()) {
          mItems.remove(itIndex);
          notifyItemRemoved(itIndex);
        }
      }
    }

    @Override
    public void update(final MyObservable o, final Object arg)
    {
      updateData();
    }

  }
}
