package cell411.ui.cell;

import static cell411.enums.CellCategory.Activism;
import static cell411.enums.CellCategory.None;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cell411.ui.base.BaseApp;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.model.ParseGeoPoint;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.enums.CellCategory;
import cell411.methods.Dialogs;
import cell411.model.XPublicCell;
import cell411.model.XUser;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.DialogShower;
import cell411.utils.LocationUtil;
import cell411.utils.OnCompletionListener;
import cell411.utils.Util;
import cell411.utils.collect.Collect;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 19-04-2016.
 */
public class PublicCellCreateOrEditFragment
  extends BaseFragment
{
  private final ObservableValueRW<ParseGeoPoint> mLocation;
  private final OnClickListener mClickListener = new ControlButtonListener();
  private EditText etCellName;
  private EditText etCellDescription;
  private android.widget.Spinner spCellCategory;
  private XPublicCell mPublicCell;
  private TextView txtCity;
  private ImageView mBtnGPS;
  private CellCategoryListAdapter mAdapterCategory;
  private Menu mMenu;
  private DialogShower mShower;
  private Button mDoneButton;
  private Button mSaveButton;

  {
    mLocation = new ObservableValueRW<>(ParseGeoPoint.class, null);
  }

  public PublicCellCreateOrEditFragment()
  {
    super(R.layout.fragment_create_public_cell);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);

  }

  @Override
  public void onViewCreated(@Nonnull final View view,
                            @Nullable final Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mDoneButton = findViewById(R.id.done);
    mSaveButton = findViewById(R.id.save);
    mDoneButton.setOnClickListener(mClickListener);
    mSaveButton.setOnClickListener(mClickListener);
    txtCity = findViewById(R.id.txt_city);
    etCellName = findViewById(R.id.et_cell_name);
    etCellDescription = findViewById(R.id.et_cell_description);
    spCellCategory = findViewById(R.id.sp_cell_category);
    mAdapterCategory = new CellCategoryListAdapter(BaseApp.req().getCurrentActivity(),
      R.layout.cell_public_cell_category);
    mAdapterCategory.setDropDownViewResource(
      android.R.layout.simple_spinner_dropdown_item);
    spCellCategory.setAdapter(mAdapterCategory);
    spCellCategory.setSelection(0);
    mBtnGPS = findViewById(R.id.btn_gps);

    mBtnGPS.setOnClickListener(this::selectLocation);
    txtCity.setOnClickListener(this::selectLocation);
    mLocation.addObserver(this::onLocationChanged);
  }

  private void selectLocation(View view)
  {
    if (view == mBtnGPS) {
      getLocationService().addObserver(this::onLocationChanged);
      // FIXME.  New location should not be null.
      onLocationChanged(null, mLocation.get());
    } else {
      OnCompletionListener listener = success ->
      {
        if (success) {
            BaseApp.getDataServer().requestCity(mShower.getText(), address -> mLocation.set(
            LocationUtil.getGeoPoint(address.mLocation)));
        } else {
          showToast("Cancelled Location Selection");
        }
      };
      mShower = Dialogs.createEnterTextDialog("Public Cell Location",
        "Enter City Here", "", listener);
      ThreadUtil.onMain(mShower);
    }
  }

  private void onLocationChanged(ParseGeoPoint newValue, ParseGeoPoint oldValue)
  {
    if (newValue == null) {
      return;
    }
      BaseApp.getDataServer().requestCity(newValue, address ->
    {
      txtCity.setText(address.cityPlus());
      mLocation.set(newValue);
    });
  }

  private void onLocationChanged(Location location, Location location1)
  {
    if (location != null) {
      mLocation.set(LocationUtil.getGeoPoint(location));
    }
  }

  public void loadData2()
  {
    String objectId = null;
    if (mPublicCell == null) {
      Bundle arguments = getArguments();
      if (arguments != null) {
        objectId = arguments.getString("objectId");
      }
      if (objectId == null) {
        mPublicCell = new XPublicCell();
      } else {
        mPublicCell = Parse.getObject(objectId);
      }
      if (mPublicCell == null) {
        showAlertDialog("alert", "Failed to load public cell", new OnCompletionListener()
        {
          @Override
          public void done(boolean success)
          {
            finish();
          }
        });
        return;
      }
      assert (mPublicCell != null);
    }
    objectId = mPublicCell.getObjectId();
    if (objectId == null) {
      mPublicCell = new XPublicCell();
      mPublicCell.setVerificationStatus(0);
      mPublicCell.setOwner(XUser.reqCurrentUser());
      selectLocation(mBtnGPS);
    } else {
      mPublicCell = getPublicCell(objectId);
      mLocation.set(mPublicCell.getLocation());
    }

  }

  private XPublicCell getPublicCell(String objectId)
  {
      return BaseApp.getDataServer().getPublicCell(objectId);
  }

  public void loadData3()
  {
    if (mPublicCell == null) {
      return;
    }
    for (String key : Collect.asList("name", "description", "category")) {
      if (!mPublicCell.has(key)) {
        mPublicCell.put(key, "");
      }
    }
    String temp = mPublicCell.getName();
    etCellName.setText(temp);
    temp = mPublicCell.getDescription();
    etCellDescription.setText(temp);
    CellCategory category = mPublicCell.getCategory();
    if (category == None) {
      category = Activism;
    }
    spCellCategory.setSelection(category.ordinal());
  }

  public boolean changed(XPublicCell publicCell, ParseGeoPoint parseGeoPoint)
  {
    ParseGeoPoint oldValue = publicCell.getLocation();
    if (oldValue != null && oldValue.equals(parseGeoPoint)) {
      return false;
    }
    publicCell.setLocation(parseGeoPoint);
    return true;
  }

  public boolean changed(XPublicCell publicCell, CellCategory category)
  {
    CellCategory cat = publicCell.getCategory();
    if (cat != null && cat.equals(category)) {
      return false;
    }
    publicCell.setCategory(category);
    return true;
  }

  public boolean changed(XPublicCell publicCell, String name, String newVal)
  {
    String oldVal = publicCell.getString(name);
    if (oldVal != null && oldVal.equals(newVal)) {
      return false;
    }
    publicCell.put(name, newVal);
    return true;
  }

  public class ControlButtonListener
    implements OnClickListener
  {
    @Override
    public void onClick(View v)
    {
      final String cellName = etCellName.getText().toString().trim();
      final String description = etCellDescription.getText().toString().trim();
      final CellCategory category =
        (CellCategory) spCellCategory.getSelectedItem();
      final ParseGeoPoint parseGeoPoint = mLocation.get();
      boolean changes = Util.theGovernmentIsHonest();
      // Check what are the things that are changed

      XPublicCell publicCell = XPublicCell.from(mPublicCell.getState());
      changes = changed(publicCell, "name", cellName) || changes;
      changes = changed(publicCell, "description", description) || changes;
      changes = changed(publicCell, parseGeoPoint) || changes;
      changes = changed(publicCell, category) || changes;
      if (v == mDoneButton) {
        if (changes) {
          Dialogs.showYesNoDialog("Discard Changes", new OnCompletionListener()
          {
            @Override
            public void done(boolean success)
            {
              if (success) {
                pop();
              }
            }
          });
        } else {
          pop();
        }
      } else if (v == mSaveButton) {
        if (cellName.isEmpty()) {
          showToast(getString(R.string.please_enter_cell_name));
          return;
        }
        if (description.isEmpty()) {
          showToast(getString(R.string.please_enter_cell_description));
          return;
        }
        if (category == None) {
          showToast("Please select a category");
          return;
        }
        if (parseGeoPoint == null) {
          showToast("Please provide location");
          return;
        }
        try {
          if (publicCell.getOwner() == null) {
            changes = true;
            publicCell.setOwner(XUser.reqCurrentUser());
          }
          if (changes) {
            
            ThreadUtil.onExec(() ->
            {
              try {
                publicCell.save();
              } catch (ParseException pe) {
                handleException("saving cell", pe);
                ThreadUtil.onMain(() ->
                {
                });
              }
            });
          }
        } catch (Exception e) {
          handleException("saving edited cell", e, null, true);
        }
      }
    }
  }
}

