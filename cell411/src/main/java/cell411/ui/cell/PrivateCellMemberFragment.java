package cell411.ui.cell;

import static cell411.utils.ViewType.vtUser;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.Parse;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.HashSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.Rel;
import cell411.model.XPrivateCell;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.services.DataService;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.ui.widget.CircularImageView;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 7/13/2015.
 */
public class PrivateCellMemberFragment
  extends BaseFragment
{
  private final MemberAdapter memberAdapter = new MemberAdapter();
  private final HashSet<String> mMembers = new HashSet<>();
  FragmentFactory mSelectFriendsFactory =
    FragmentFactory.fromClass(SelectFriendsFragment.class);
  private RecyclerView mRecycler;
  private RelativeLayout rlSticky;
  private XPrivateCell mPrivateCell;

  public PrivateCellMemberFragment()
  {
    super(R.layout.fragment_private_cell_members);
  }

  public static int compare(XItem lhs, XItem rhs)
  {
    return compare(lhs.getUser(), rhs.getUser());
  }

  public static int compare(XUser lhs, XUser rhs)
  {
    int res = compare(lhs.getName(), rhs.getName());
    if (res == 0) {
      res = compare(lhs.getObjectId(), rhs.getObjectId());
    }
    return res;
  }

  private static int compare(String lhs, String rhs)
  {
    return String.CASE_INSENSITIVE_ORDER.compare(lhs, rhs);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull final View view,
                            @Nullable final Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    Bundle arguments = getArguments();
    assert arguments != null;
    String objectId = arguments.getString("objectId");
    assert objectId != null;
    mPrivateCell = Parse.getObject(objectId);
    rlSticky = findViewById(R.id.rl_sticky);
    mRecycler = findViewById(R.id.rv_nau_cells);
    mRecycler.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager =
      new LinearLayoutManager(getActivity());
    mRecycler.setLayoutManager(linearLayoutManager);
    mRecycler.setAdapter(memberAdapter);
    FloatingActionButton fabAddFriend = findViewById(R.id.fab_add_friend);
    fabAddFriend.setOnClickListener(v ->
    {
      mSelectFriendsFactory.setObjectId(mPrivateCell.getObjectId());
      push(mSelectFriendsFactory);
    });
    rlSticky.setVisibility(View.GONE);
    LiveQueryService lqs = LiveQueryService.opt();
    assert lqs != null;
    RelationWatcher watcher = lqs.getRelationWatcher();
    Rel rel = watcher.getMemberRel(mPrivateCell);
    rel.addObserver(memberAdapter);
    memberAdapter.update(rel, null);
  }

  public void loadData2()
  {
  }

  public void loadData3()
  {
    memberAdapter.setData();
  }

  @Override
  public boolean onBackPressed()
  {
    pop();
    return super.onBackPressed();
  }

  private int find(ArrayList<XItem> items, XUser user)
  {
    for (int i = 0; i < items.size(); i++) {
      if (items.get(i).getObjectId().equals(user.getObjectId())) {
        return i;
      }
    }
    return -1;
  }

  public class MemberAdapter
    extends RecyclerView.Adapter<ViewHolder>
    implements MyObserver
  {
    private final int VIEW_TYPE_APP_USER = vtUser.ordinal();
    public ArrayList<XItem> mItems = new ArrayList<>();
    private final View.OnClickListener mOnClickListener =
      new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          int position = mRecycler.getChildAdapterPosition(v);
          if (position >= 0 && position < mItems.size()) {
            XItem item = mItems.get(position);
            if (item.getViewType() != vtUser) {
              return;
            }
            XUser user = item.getUser();
            //UserActivity.start(PrivateCellMembersActivity.this, user);
          }
        }
      };

    // Provide a suitable constructor (depends on the kind of data set)
    public MemberAdapter()
    {
    }

    // Create new views (invoked by the layout manager)
    @Override
    @Nonnull
    public PrivateCellMemberFragment.ViewHolder onCreateViewHolder(
      @Nonnull ViewGroup parent, int viewType)
    {
      View v = null;
      if (viewType == VIEW_TYPE_APP_USER) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_friends_new, parent, false);
      }
      // set the view's size, margins, paddings and layout parameters
      PrivateCellMemberFragment.ViewHolder vh =
        new PrivateCellMemberFragment.ViewHolder(this, v, viewType);
      if (viewType == VIEW_TYPE_APP_USER) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(
      @Nonnull final PrivateCellMemberFragment.ViewHolder viewHolder,
      final int position)
    {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if (getItemViewType(position) == VIEW_TYPE_APP_USER) {
        final XItem item = mItems.get(position);
        final XUser user = item.getUser();
        final String name = user.getName();
        viewHolder.txtName.setText(name);
        if (user.getInt("isDeleted") == 1) {
          viewHolder.txtName.setTextColor(
            getColor(R.color.text_disabled_hint_icon));
        } else {
          viewHolder.txtName.setTextColor(getColor(R.color.text_primary));
        }
        //        ImageFactory.ImageListener listener = (bmp) ->
        //        notifyItemChanged(find(mItems, (user)));
        //        viewHolder.imgUser.setImageDrawable(user.getThumbnail());
        //        user.addAvatarObserver();
        viewHolder.imgUser.setImageBitmap(user.getAvatarPic());
      }
    }

    @Override
    public int getItemViewType(int position)
    {
      super.getItemViewType(position);
      return VIEW_TYPE_APP_USER;
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    @Override
    public void update(MyObservable o, Object arg)
    {
      Rel rel = (Rel) o;
      if (rel != null) {
        mMembers.clear();
        mMembers.addAll(rel.getRelatedIds());
        setData();
      }

    }

    public void setData()
    {
      assert ThreadUtil.isMainThread();
      if (!mItems.isEmpty()) {
        notifyItemRangeRemoved(0, mItems.size());
        mItems.clear();
      }
      for (String id : mMembers) {
        XUser user = DataService.get().getUser(id);
        XItem item = new XItem(user);
        mItems.add(item);
      }
      mItems.sort(PrivateCellMemberFragment::compare);
      notifyItemRangeInserted(0, mItems.size());
      rlSticky.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }
  }

  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public class ViewHolder
    extends RecyclerView.ViewHolder
  {
    private final MemberAdapter mMemberAdapter;
    // each data item is just a string in this case
    private CircularImageView imgUser;
    private TextView txtName;

    public ViewHolder(final MemberAdapter memberAdapter, View view, int type)
    {
      super(view);
      mMemberAdapter = memberAdapter;
      if (type == mMemberAdapter.VIEW_TYPE_APP_USER) {
        imgUser = view.findViewById(R.id.avatar);
        txtName = view.findViewById(R.id.name);
      }
    }
  }
}

