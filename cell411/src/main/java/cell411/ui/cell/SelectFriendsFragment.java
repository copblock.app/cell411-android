package cell411.ui.cell;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.CallSuper;

import com.parse.Parse;
import com.parse.model.ParseRelation;
import com.safearx.cell411.R;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.Rel;
import cell411.methods.Dialogs;
import cell411.model.XPrivateCell;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.ui.base.BaseFragment;
import cell411.utils.OnCompletionListener;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 7/13/2015.
 */
public class SelectFriendsFragment
  extends BaseFragment
{
  private final ClickListener mClickListener = new ClickListener();
  private final int mResource = R.layout.cell_friends;
  boolean mPopped = false;
  private Rel mFriendRel;
  private Rel mMembers;
  private FriendListAdapter mAdapter;
  private XPrivateCell mPrivateCell;
  private Button mDoneButton;
  private Button mSaveButton;

  public SelectFriendsFragment()
  {
    super(R.layout.fragment_select_friends);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    System.out.println("CurrentUser: " + XUser.reqCurrentUser());
    Bundle arguments = getArguments();
    assert arguments != null;
    String objectId = arguments.getString("objectId");
    assert objectId != null;
    mPrivateCell = Parse.getObject(objectId);
    assert mPrivateCell != null;
    mDoneButton = findViewById(R.id.done);
    mDoneButton.setOnClickListener(mClickListener);
    mSaveButton = findViewById(R.id.save);
    mSaveButton.setOnClickListener(mClickListener);

    ListView friendView = findViewById(R.id.list_friends);
    mAdapter = new FriendListAdapter(this);
    friendView.setAdapter(mAdapter);
    friendView.setOnItemClickListener(this::onItemClick);
    LiveQueryService service = LiveQueryService.opt();
    RelationWatcher relationWatcher = service.getRelationWatcher();
    assert service != null;
    mPopped = false;
    mFriendRel = relationWatcher.getFriends();
    mFriendRel.addObserver(mAdapter);
    mMembers = relationWatcher.getMemberRel(mPrivateCell);
    mMembers.addObserver(mAdapter);
    ThreadUtil.onMain(() -> mAdapter.update(mFriendRel, null));
    ThreadUtil.onMain(() -> mAdapter.update(mMembers, null));
  }

  public void onItemClick(AdapterView<?> adapterView, View view, int position,
                          long l)
  {
    XItem item = mAdapter.getItem(position);
    item.setSelected(!item.isSelected());
    mAdapter.notifyDataSetChanged();
  }

  @CallSuper
  @Override
  public void onResume()
  {
    super.onResume();
    mPopped = false;
  }

  @CallSuper
  @Override
  public void onPause()
  {
    super.onPause();
  }

  public void saveData()
  {
    try {
      if (ThreadUtil.isMainThread()) {
        
        ThreadUtil.onExec(this::saveData);
      } else {
        TreeSet<String> selectedIds = mAdapter.getSelectedIds();
        System.out.println("SelectedIds: " + selectedIds);
        TreeSet<String> originalIds = new TreeSet<>(mMembers.getRelatedIds());
        System.out.println("OriginalIds: " + originalIds);
        if (!originalIds.equals(selectedIds)) {
          TreeSet<String> mRems = new TreeSet<>(originalIds);
          TreeSet<String> mAdds = new TreeSet<>(selectedIds);
          mRems.removeAll(selectedIds);
          mAdds.removeAll(originalIds);
          if (!(mRems.isEmpty() && mAdds.isEmpty())) {
            ParseRelation<XUser> members = mPrivateCell.getRelation("members");
            for (String id : mRems) {
              members.remove(Parse.getObject(id));
            }
            for (String id : mAdds) {
              members.add(Parse.getObject(id));
            }
            mPrivateCell.save();
            mMembers.setRelatedIds(originalIds);
            mMembers.setRelatedIds(selectedIds);
          }
        }
      }
    } catch (Exception e) {
      System.out.println("ex: " + e);
    }
  }

  private class FriendListAdapter
    extends ArrayAdapter<XItem>
    implements MyObserver
  {
    private final LayoutInflater mInflater;

    public FriendListAdapter(BaseFragment activity)
    {
      super(activity.requireContext(), mResource);
      mInflater = activity.getLayoutInflater();
    }

    List<XItem> asList()
    {
      return new AbstractList<XItem>()
      {
        @Override
        public XItem get(int index)
        {
          return getItem(index);
        }

        @Override
        public int size()
        {
          return getCount();
        }

      };
    }

    public TreeSet<String> getSelectedIds()
    {
      TreeSet<String> res = new TreeSet<>();
      for (int i = 0; i < getCount(); i++) {
        XItem item = getItem(i);
        if (item.isSelected())
          res.add(item.getObjectId());
      }
      return res;
    }

    public ArrayList<XItem> getItems()
    {
      ArrayList<XItem> res = new ArrayList<>();
      for (int i = 0; i < getCount(); i++) {
        res.add(getItem(i));
      }
      return res;
    }

    @Override
    public void update(final MyObservable o, final Object arg)
    {
      if (o == mFriendRel) {
        HashSet<String> mRemIds = new HashSet<>(getIds());
        mRemIds.removeAll(mFriendRel.getRelatedIds());

        HashSet<String> mAddIds = new HashSet<>(mFriendRel.getRelatedIds());

        if (mRemIds.isEmpty() && mAddIds.isEmpty())
          return;

        if (!mRemIds.isEmpty()) {
          for (int i = 0; i < getCount(); i++) {
            XItem item = getItem(i);
            if (mRemIds.contains(item.getObjectId())) {
              remove(item);
              i--;
            }
          }
        }
        if (!mAddIds.isEmpty()) {
          for (String id : mAddIds) {
            XUser user = Parse.getObject(id);
            XItem item = new XItem(user);
            item.setSelected(false);
            add(item);
          }
        }
        sort(this::compare);
      } else if (o == mMembers) {
        HashSet<String> members = new HashSet<>(mMembers.getRelatedIds());
        for (int i = 0; i < getCount(); i++) {
          XItem item = getItem(i);
          item.setSelected(members.contains(item.getObjectId()));
        }
      } else {
        throw new RuntimeException("Unexpected observable: " + o);
      }
      notifyDataSetChanged();
    }

    public ArrayList<String> getIds()
    {
      ArrayList<String> res = new ArrayList<>();
      for (int i = 0; i < getCount(); i++) {
        res.add(getItem(i).getObjectId());
      }
      return res;
    }

    public int compare(final XItem lhs, final XItem rhs)
    {
      System.out.println("compare:  " + lhs.getText() + " to " + rhs.getText());
      if (lhs.isSelected() != rhs.isSelected()) {
        System.out.println(
          "         selected: " + lhs.isSelected() + " " + rhs.isSelected());
        return lhs.isSelected() ? -1 : 1;
      } else if (!lhs.getText().equals(rhs.getText())) {
        int res =
          String.CASE_INSENSITIVE_ORDER.compare(lhs.getText(), rhs.getText());
        System.out.println("         name:" + lhs.getText());
        System.out.println("         name:" + rhs.getText());
        System.out.println("         name:" + res);
        return res;
      } else {
        int res = String.CASE_INSENSITIVE_ORDER.compare(lhs.getObjectId(),
          rhs.getObjectId());
        System.out.println("         objectId:" + res);
        return res;
      }
    }

    public View getView(final int position, View cellView, ViewGroup parent)
    {
      ViewHolder holder;
      if (cellView == null) {
        cellView = mInflater.inflate(mResource, null);
        holder = new ViewHolder(cellView, R.drawable.bg_friend_selected,
          R.drawable.bg_friend_unselected);
      } else {
        holder = (ViewHolder) cellView.getTag();
        holder.imgUser.setImageBitmap(null);
      }
      holder.update(getItem(position));
      return cellView;
    }

  }

  private class ViewHolder
  {
    final private TextView txtUserName;
    final private ImageView imgUser;
    final private ImageView imgTick;
    final private int bg_selected;
    final private int bg_unselected;

    public ViewHolder(View cellView, int bg_friend_selected,
                      int bg_friend_unselected)
    {
      txtUserName = cellView.findViewById(R.id.name);
      imgUser = cellView.findViewById(R.id.avatar);
      imgTick = cellView.findViewById(R.id.img_tick);
      bg_selected = bg_friend_selected;
      bg_unselected = bg_friend_unselected;
      cellView.setTag(this);
    }

    public void update(XItem item)
    {
      ViewHolder holder = this;
      XUser user = item.getUser();
      holder.txtUserName.setText(user.getName());
      holder.imgTick.setVisibility(View.VISIBLE);
      holder.imgTick.setBackgroundResource(getBackground(item.isSelected()));
      Bitmap bitmap = user.getAvatarPic();
      holder.imgUser.setImageBitmap(bitmap);
    }

    private int getBackground(boolean selected)
    {
      return selected ? bg_selected : bg_unselected;
    }

    void thumbnailLoaded(Bitmap bitmap)
    {
      mAdapter.notifyDataSetChanged();
    }
  }

  private class ClickListener
    implements View.OnClickListener
  {
    @Override
    public void onClick(View v)
    {
      if (v == mDoneButton) {
        HashSet<String> orig = new HashSet<>(mMembers.getRelatedIds());
        HashSet<String> curr = new HashSet<>(mAdapter.getSelectedIds());
        if (orig.equals(curr)) {
          pop();
        } else {
          Dialogs.showYesNoDialog("Exit without saving?",
            new OnCompletionListener()
            {
              @Override
              public void done(boolean success)
              {
                if (success) {
                  pop();
                }
              }
            });
        }
      } else if (v == mSaveButton) {
        saveData();
      } else {
        throw new RuntimeException("Unexpected clicked view: " + v);
      }
    }
  }
}


