package cell411.ui.cell;

import static cell411.utils.ViewType.vtNull;
import static cell411.utils.ViewType.vtString;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LQListener;
import cell411.logic.LiveQueryService;
import cell411.logic.Watcher;
import cell411.model.XBaseCell;
import cell411.model.XPublicCell;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.FragmentFactory.TypedFactory;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.ViewType;
import cell411.utils.reflect.XTAG;
import cell411.utils.collect.Collect;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 18-04-2016.
 */
public class PublicCellFragment
  extends BaseFragment
{
  public static final XTAG TAG = new XTAG();
  final TypedFactory<PublicCellMembersFragment> mMembersFactory =
    FragmentFactory.fromClass(PublicCellMembersFragment.class);

  final TypedFactory<PublicCellCreateOrEditFragment> mEditorFactory =
    FragmentFactory.fromClass(PublicCellCreateOrEditFragment.class);
  final WatcherListener mWatcherListener = new WatcherListener();
  final private CellListAdapter mAdapter = new CellListAdapter();
  private Watcher<XPublicCell> mPublicCellWatcher;
  private FloatingActionButton mFabCreate;
  private RecyclerView mRecyclerView;
  private LiveQueryService mService;

  public PublicCellFragment()
  {
    super(R.layout.fragment_public_cells);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mRecyclerView = view.findViewById(R.id.rv_public_cells);
    mRecyclerView.setHasFixedSize(false);
    mRecyclerView.setAdapter(mAdapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    mFabCreate = view.findViewById(R.id.fab);
    mFabCreate.setOnClickListener(this::onCreateClicked);
  }

  private void onCreateClicked(final View view)
  {
    assert view == mFabCreate;
    try {
      XPublicCell cell = new XPublicCell();
      cell.put("owner", XUser.reqCurrentUser());
      cell.put("name", "my new cell");
      cell.put("description", "my new cell -- described");
      
      ThreadUtil.onExec(() ->
      {
        cell.save();
        ThreadUtil.onMain(() ->
            {
              mEditorFactory.setObjectId(cell.getObjectId());
              push(mEditorFactory);
            });
      });
    } catch (Exception e) {
      handleException("creating cell", e);
    }
  }

  @Override
  public void onResume()
  {
    super.onResume();
  }

  @Override
  public void onPause()
  {
    super.onPause();
  }

  public void loadData3()
  {
    mAdapter.update();
  }

  public static class ViewHolder
    extends RecyclerView.ViewHolder
  {
    final View mView;
    final ViewType mViewType;
    final TextView mTextView;
    final TextView txtBtnAction;
    final ImageView imgVerified;
    final ImageView imgChat;

    public ViewHolder(View view, ViewType viewType)
    {
      super(view);
      mView = view;
      mViewType = viewType;
      if (viewType == ViewType.vtPublicCell) {
        mTextView = view.findViewById(R.id.txt_cell_name);
        txtBtnAction = view.findViewById(R.id.txt_btn_action);
        imgChat = view.findViewById(R.id.img_chat);
        imgVerified = view.findViewById(R.id.img_verified);
      } else if (viewType == vtNull) {
        mTextView = view.findViewById(R.id.name);
        txtBtnAction = null;
        imgChat = null;
        imgVerified = null;
      } else if (viewType == vtString) {
        mTextView = view.findViewById(R.id.name);
        mTextView.setVisibility(View.VISIBLE);
        mTextView.setBackgroundColor(0xff000000);
        mTextView.setTextColor(0xffffffff);
        mTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        mTextView.setGravity(Gravity.CENTER);
        imgChat = null;
        imgVerified = null;
        txtBtnAction = null;
      } else {
        throw new RuntimeException("oops!");
      }
    }

    public ViewType getViewType()
    {
      return mViewType;
    }
  }

  class WatcherListener
    implements Runnable,
               LQListener<XPublicCell>
  {
    @Override
    public void change()
    {
      if (ThreadUtil.isMainThread()) {
        run();
      } else {
        ThreadUtil.onMain(this);
      }
    }

    @Override
    public void run()
    {
      mAdapter.update();
    }

  }

  public class CellListAdapter
    extends RecyclerView.Adapter<ViewHolder>
  {
    final List<XItem> mItems = new ArrayList<>();
    final XItem mOwnedTitle = new XItem("owned", "Owned Cells");
    final XItem mJoinedTitle = new XItem("joined", "Joined Cells");

    @Override
    @Nonnull
    public ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent,
                                         int intViewType)
    {
      ViewType viewType = ViewType.valueOf(intViewType);
      View v = null;
      if (viewType == ViewType.vtString) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_public_cell_title, parent, false);
      } else if (viewType == ViewType.vtPublicCell) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_public_cell, parent, false);
      } else if (viewType == vtNull) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_footer, parent, false);
      }
      return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(@Nonnull final ViewHolder viewHolder,
                                 final int position)
    {
      final XItem item = getItem(position);
      ViewType viewType = item.getViewType();
      if (viewType != viewHolder.mViewType) {
        throw new Error(
          "Mismatched viewtypes: " + viewHolder.mViewType + " and " + viewType);
      }
      switch (viewType) {
        case vtPublicCell: {
          final XPublicCell cell = item.getPublicCell();
          if (viewHolder.mTextView != null) {
            viewHolder.mTextView.setText(cell.getName());
          }
          if (viewHolder.txtBtnAction != null) {
            viewHolder.txtBtnAction.setVisibility(View.VISIBLE);
            viewHolder.txtBtnAction.setBackgroundResource(
              R.drawable.bg_cell_leave);
            viewHolder.txtBtnAction.setTextColor(getColor(R.color.red));
          }
          switch (cell.getStatus()) {
            case OWNER:
              if (viewHolder.txtBtnAction != null) {
                viewHolder.txtBtnAction.setText(R.string.delete);
                viewHolder.txtBtnAction.setOnClickListener(
                  view -> CellDialogs.showDeleteCellDialog(getActivity(), cell,
                    success ->
                    {
                      if (success) {
                        showToast("Cell deleted.");
                      } else {
                        showAlertDialog("alert", "Failed to delete cell");
                      }
                    }));
              }
              break;
            case JOINED:
              if (viewHolder.txtBtnAction != null) {
                viewHolder.txtBtnAction.setText(R.string.leave);
                viewHolder.txtBtnAction.setOnClickListener(
                  view -> CellDialogs.showLeaveCellDialog(getActivity(), cell,
                    success ->
                    {
                      if (success) {
                        showToast("Cell deleted.");
                      } else {
                        showAlertDialog("alert", "Failed to delete cell");
                      }
                    }));
              }
              break;
            default:
              if (viewHolder.txtBtnAction != null) {
                viewHolder.txtBtnAction.setText("?");
                viewHolder.txtBtnAction.setOnClickListener(null);
              }
              break;
            case NOT_JOINED:
              if (viewHolder.txtBtnAction != null) {
                viewHolder.txtBtnAction.setText(R.string.join);
                viewHolder.txtBtnAction.setOnClickListener(
                  view -> CellDialogs.joinCell(cell, ex ->
                  {
                    if (ex == null) {
                      showToast(
                        "A request has been sent to the owner of cell " +
                          cell.getName());
                    } else {
                      showAlertDialog("alert", "Cell join failed: " + ex);
                    }
                  }));
              }
              break;
          }
          assert viewHolder.imgVerified != null;
          assert viewHolder.imgChat != null;
          viewHolder.imgVerified.setVisibility(
            (cell.getVerificationStatus() == 1) ? View.VISIBLE :
              View.INVISIBLE);
          viewHolder.imgChat.setVisibility(View.VISIBLE);
          viewHolder.imgChat.setOnClickListener(
            view -> Cell411.req().openChat(cell));
          viewHolder.mView.setOnClickListener(v ->
          {
            int pos = mRecyclerView.getChildAdapterPosition(v);
            XItem it = getItem(pos);
            XPublicCell publicCell = it.getPublicCell();
            mMembersFactory.setObjectId(publicCell.getObjectId());
            push(mMembersFactory);
          });
          break;
        }
        case vtString:
          if (viewHolder.mTextView != null) {
            viewHolder.mTextView.setText(item.getText());
          }
          break;
        case vtNull:
          if (viewHolder.mTextView != null) {
            viewHolder.mTextView.setVisibility(View.INVISIBLE);
          }
          break;
        default:
          throw new RuntimeException();
      }
    }

    @Override
    public int getItemViewType(int position)
    {
      return getItemViewType(getItem(position)).ordinal();
    }

    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    public ViewType getItemViewType(XItem item)
    {
      return item.getViewType();
    }

    public XItem getItem(int position)
    {
      return mItems.get(position);
    }

    public int getColor(int resId)
    {
      final BaseActivity activity = (BaseActivity) getActivity();
      assert activity != null;
      return activity.getColor(resId);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void update()
    {
      Collection<XPublicCell> items = mPublicCellWatcher.getValues();
      mItems.clear();
      if (items == null) {
        mItems.addAll(Collect.asList(new XItem("", "Loading Data ...")));
      } else {
        List<XPublicCell> owned = new ArrayList<>();
        List<XPublicCell> joined = new ArrayList<>();
        for (XPublicCell cell : items) {
          if (!cell.has("name")) {
            cell.put("name", cell.getDescription());
          }
          if (cell.ownedByCurrent()) {
            owned.add(cell);
          } else {
            joined.add(cell);
          }
        }
        owned.sort(XBaseCell::nameCompare);
        joined.sort(XBaseCell::nameCompare);
        mItems.add(mOwnedTitle);
        mItems.addAll(XItem.asList(owned));
        mItems.add(mJoinedTitle);
        mItems.addAll(XItem.asList(joined));

        BaseApp.req();
        ThreadUtil.onMain(this::notifyDataSetChanged, (long) 0);
      }
    }
  }

}
