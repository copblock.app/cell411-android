package cell411.ui.widget;

import androidx.recyclerview.widget.RecyclerView;

import javax.annotation.Nullable;

public abstract class RVAdapter
  extends RecyclerView.AdapterDataObserver
{
  public RVAdapter()
  {
    super();
  }

  @Override
  abstract public void onChanged();

  @Override
  public void onItemRangeChanged(int positionStart, int itemCount)
  {
    onChanged();
  }

  @Override
  public void onItemRangeChanged(int positionStart, int itemCount,
                                 @Nullable Object payload)
  {
    onChanged();
  }

  @Override
  public void onItemRangeInserted(int positionStart, int itemCount)
  {
    onChanged();
  }

  @Override
  public void onItemRangeRemoved(int positionStart, int itemCount)
  {
    onChanged();
  }

  @Override
  public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount)
  {
    onChanged();
  }
}
