package cell411.ui.widget;

import android.view.View;

public interface OpaqueAreaClickListener
{
  void onOpaqueAreaClicked(View v, String TAG);
}

