package cell411.ui.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.model.ParseObject;
import com.safearx.cell411.R;

import java.util.ArrayList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.AlertWatcher;
import cell411.logic.CellWatcher;
import cell411.logic.LQListener;
import cell411.logic.LiveQueryService;
import cell411.model.XAlert;
import cell411.model.XChatRoom;
import cell411.model.XEntity;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.model.util.XItem;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.Util;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 27-03-2017.
 */
public class NewChatFragment
  extends ModelBaseFragment
{
  private final ArrayList<XItem> mItems = new ArrayList<>();
  private final WatcherListener<XAlert> mAlertListener =
    new WatcherListener<>();
  private final WatcherListener<XPublicCell> mPublicCellListener =
    new WatcherListener<>();
  private final WatcherListener<XPrivateCell> mPrivateCellListener =
    new WatcherListener<>();
  private final AlertWatcher mAlertWatcher;
  private final CellWatcher<XPrivateCell> mPrivateCellCellWatcher;
  private final CellWatcher<XPublicCell> mPublicCellCellWatcher;
  private final CellListAdapter mAdapter = new CellListAdapter();
  private final Update mUpdate = new Update();
  private RecyclerView recyclerView;
  private TextView txtEmpty;

  public NewChatFragment()
  {
    super(R.layout.fragment_new_chat);
    LiveQueryService service = LiveQueryService.req();
    mPrivateCellCellWatcher = service.getPrivateCellWatcher();
    mPublicCellCellWatcher = service.getPublicCellWatcher();
    mAlertWatcher = service.getAlertWatcher();
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull final View view,
                            @Nullable final Bundle savedInstanceState)
  {
    getAlertListener().change();

    super.onViewCreated(view, savedInstanceState);
    LinearLayoutManager linearLayoutManager =
      new LinearLayoutManager(requireActivity());

    recyclerView = findViewById(R.id.rv_new_chat);
    txtEmpty = findViewById(R.id.txt_empty);

    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(linearLayoutManager);
    mAdapter.registerAdapterDataObserver(new AdapterListener());
    recyclerView.setAdapter(mAdapter);
  }

  @CallSuper
  public void loadData3()
  {
    new Update().run();
  }

  public WatcherListener<XAlert> getAlertListener()
  {
    return mAlertListener;
  }

  public WatcherListener<XPublicCell> getPublicCellListener()
  {
    return mPublicCellListener;
  }

  public WatcherListener<XPrivateCell> getPrivateCellListener()
  {
    return mPrivateCellListener;
  }

  public AlertWatcher getAlertWatcher()
  {
    return mAlertWatcher;
  }

  public CellWatcher<XPrivateCell> getPrivateCellCellWatcher()
  {
    return mPrivateCellCellWatcher;
  }

  public CellWatcher<XPublicCell> getPublicCellCellWatcher()
  {
    return mPublicCellCellWatcher;
  }

  Update getUpdate()
  {
    return mUpdate;
  }

  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public static class ViewHolder
    extends RecyclerView.ViewHolder
  {
    // each data item is just a string in this case
    //    private final CircularImageView imgCell;
    private final TextView txtCellName;
    //    private final ImageView imgVerified;
    private final ImageView imgChat;
    private final View button;
    private final View distance;

    public ViewHolder(View view)
    {
      super(view);
      //      RDecoder     rDecoder = RDecoder.get();
      //      List<String> keys1    = rDecoder.getKeys(R.id.txt_alert);
      //      List<String> keys2    = rDecoder.getKeys(R.id.img_chat);
      //      List<String> keys3    = rDecoder.getKeys(R.id.txt_btn_action);
      //      List<String> keys4    = rDecoder.getKeys(R.id.txt_distance);

      txtCellName = view.findViewById(R.id.txt_cell_name);
      button = view.findViewById(R.id.txt_btn_action);
      distance = view.findViewById(R.id.txt_distance);
      //      imgCell = view.findViewById(R.id.img_cell);
      imgChat = view.findViewById(R.id.img_chat);
      //      imgVerified = view.findViewById(R.id.img_verified);
    }
  }

  class WatcherListener<X extends ParseObject>
    implements LQListener<X>
  {
    @Override
    public void change()
    {
      getUpdate().run();
    }
  }

  public class CellListAdapter
    extends RecyclerView.Adapter<ViewHolder>
  {
    public final ArrayList<XItem> mList = new ArrayList<>();
    private final View.OnClickListener mOnClickListener =
      new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          
          ThreadUtil.onExec(() ->
          {
            int position = recyclerView.getChildAdapterPosition(v);
            XItem item = mList.get(position);
            XEntity entity = item.getEntity();
            if (entity.getChatRoom() == null) {
              XChatRoom chatRoom = new XChatRoom();
              entity.setChatRoom(chatRoom);
              chatRoom.put("entity", entity.getObjectId());
              entity.save();
            }
            openChat(entity);
          });
        }
      };

    @Override
    @Nonnull
    public NewChatFragment.ViewHolder onCreateViewHolder(
      @Nonnull ViewGroup parent, int viewType)
    {
      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
      View v = inflater.inflate(R.layout.cell_public_cell, parent, false);
      v.setOnClickListener(mOnClickListener);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(
      final @Nonnull NewChatFragment.ViewHolder viewHolder, final int position)
    {
      XItem item = mList.get(position);
      // - get element from your dataset at this position
      // - replace the contents of the view with that element
      viewHolder.itemView.setOnClickListener(mOnClickListener);
      viewHolder.imgChat.setVisibility(View.GONE);
      viewHolder.button.setVisibility(View.GONE);
      viewHolder.distance.setVisibility(View.GONE);
      if (viewHolder.txtCellName != null) {
        viewHolder.txtCellName.setText(item.getText());
      }
    }

    @Override
    public int getItemViewType(int position)
    {
      return mList.get(position).getViewType().ordinal();
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
      return mList.size();
    }

    public void run()
    {
      mAdapter.notifyItemRangeRemoved(0, mList.size());
      mAdapter.mList.clear();
      mItems.clear();
      Util.transform(getAlertWatcher().getValues(), mItems, XItem::new);
      Util.transform(getPrivateCellCellWatcher().getValues(), mItems,
        XItem::new);
      Util.transform(getPublicCellCellWatcher().getValues(), mItems,
        XItem::new);

      for (XItem item : mItems) {
        XEntity entity = item.getEntity();
        if (entity.getChatRoom() != null) {
          continue;
        }
        mAdapter.mList.add(item);
      }
      mAdapter.notifyItemRangeInserted(0, mAdapter.mList.size());
    }
  }

  class Update
    implements Runnable
  {
    @Override
    public void run()
    {
      mAdapter.run();
    }
  }

  class AdapterListener
    extends RecyclerView.AdapterDataObserver
  {
    @Override
    public void onChanged()
    {
      if (mAdapter.getItemCount() == 0) {
        txtEmpty.setVisibility(View.VISIBLE);
      } else {
        txtEmpty.setVisibility(View.GONE);
      }
    }

    @Override
    public void onItemRangeChanged(int positionStart, int itemCount)
    {
      onChanged();
    }

    @Override
    public void onItemRangeChanged(int positionStart, int itemCount,
                                   @Nullable Object payload)
    {
      onChanged();
    }

    @Override
    public void onItemRangeInserted(int positionStart, int itemCount)
    {
      onChanged();
    }

    @Override
    public void onItemRangeRemoved(int positionStart, int itemCount)
    {
      onChanged();
    }

    @Override
    public void onItemRangeMoved(int fromPosition, int toPosition,
                                 int itemCount)
    {
      onChanged();
    }
  }
}

