package cell411.ui.settings;

import static cell411.utils.ViewType.vtUser;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cell411.ui.base.BaseApp;

import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LiveQueryService;
import cell411.logic.RelationWatcher;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.model.util.XItemList;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.OnCompletionListener;
import cell411.utils.ViewType;

public class SpammedUsersFragment
  extends ModelBaseFragment
{
  private final SpammedUserAdapter mAdapter = new SpammedUserAdapter();
  LayoutInflater mInflater;
  private RecyclerView mRecyclerView;
  private RelationWatcher mWatcher;

  public SpammedUsersFragment(int layout)
  {
    super(R.layout.activity_spammed_users);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mRecyclerView = findViewById(R.id.list_friends);
    LiveQueryService lqs = LiveQueryService.opt();
    assert lqs != super.getLocationService();
    mWatcher = lqs.getRelationWatcher();
    mRecyclerView.setAdapter(mAdapter);
  }

  //  private class SpamUsersListAdapter extends ArrayAdapter<XItem>
  //    implements LQListener<XUser>
  //  {
  //    private final List<XItem> mItems = new ArrayList<>();
  //
  //    public SpamUsersListAdapter(Context context, int resource) {
  //      super(context, resource);
  //      mInflater = ((Activity) context).getLayoutInflater();
  //    }
  //
  //    public int getCount() {
  //      return mItems.size();
  //    }
  //
  //    public View getView(final int position, View convertView1, ViewGroup
  //    parent) {
  //      View cellView = convertView1;
  //      ViewHolder viewHolder = null;
  //      final XItem item = getItem(position);
  //      if(item.getViewType()==vtUser) {
  //        final XUser user = item.getUser();
  //        if (cellView == null) {
  //          viewHolder = new ViewHolder();
  //          cellView = mInflater.inflate(R.layout.cell_spammed_user,
  //          parent, false);
  //          viewHolder.txtDisplayName = cellView.findViewById(R.id
  //          .txt_display_name);
  //          viewHolder.imgUser = cellView.findViewById(R.id.img_user);
  //          viewHolder.txtUnSpam = cellView.findViewById(R.id.txt_un_spam);
  //          cellView.setTag(viewHolder);
  //        } else {
  //          viewHolder = (ViewHolder) cellView.getTag();
  //          viewHolder.imgUser.setImageResource(R.drawable.logo);
  //        }
  //        viewHolder.txtDisplayName.setText(user.getName());
  //        viewHolder.imgUser.setImageBitmap(user.getThumbnailPic((bmp) -> {
  //          notifyDataSetChanged();
  //        }));
  //        viewHolder.txtUnSpam.setOnClickListener(view -> unSpamUser(user));
  //      }
  //      return cellView;
  //    }
  //
  //    private void unSpamUser(final XUser user) {
  //      ds()
  //        .flagUser(user, false, success -> {
  //        });
  //    }
  //
  //    @Override
  //    public void done(final List<XUser> objects, final ParseException e) {
  //      if(e!=null) {
  //        handleException("loading data", e);
  //        return;
  //      }
  //      mItems.addAll(XItem.transform(objects));
  //    }
  //
  //    @Override
  //    public void onEvents(final ParseQuery<XUser> query, final
  //    SubscriptionHandler.Event event,
  //                         final XUser object) {
  //
  //    }
  //  }
  private class ViewHolder
    extends RecyclerView.ViewHolder
  {
    private final TextView txtDisplayName;
    private final ImageView imgUser;
    private final TextView txtUnSpam;

    public ViewHolder(View view)
    {
      super(view);
      txtDisplayName = view.findViewById(R.id.txt_display_name);
      imgUser = view.findViewById(R.id.avatar);
      txtUnSpam = view.findViewById(R.id.txt_un_spam);
    }

    public void bind(final XItem item)
    {
      txtDisplayName.setText(item.getText());
      if (item.getViewType() == vtUser) {
        XUser user = item.getUser();
        imgUser.setImageBitmap(user.getAvatarPic());
        txtUnSpam.setOnClickListener(this::unspam);
        txtUnSpam.setTag(item);
      }
    }

    public void unspam(View view)
    {
      if (txtUnSpam != view) {
        return;
      }
      XItem item = (XItem) view.getTag();
      if (item.getViewType() != vtUser) {
        return;
      }
      XUser user = item.getUser();
        BaseApp.getDataServer().flagUser(user, false, new OnCompletionListener()
      {
        @Override
        public void done(final boolean success)
        {
          if (success) {
            showToast("Unflagged %s", user.getName());
          } else {
            showToast("Failed to unflag %s", user.getName());
          }
        }
      });
    }
  }

  private class SpammedUserAdapter
    extends RecyclerView.Adapter<ViewHolder>
    //    implements LQListener<XUser>
  {
    private final XItemList mItems = new XItemList();

    @Nonnull
    @Override
    public ViewHolder onCreateViewHolder(@Nonnull final ViewGroup parent,
                                         final int viewTypeIdx)
    {
      ViewType viewType = ViewType.valueOf(viewTypeIdx);
      switch (viewType) {
        case vtString:
        case vtUser:
          return new ViewHolder(
            mInflater.inflate(R.layout.cell_spammed_user, parent, false));
        default:
          throw new IllegalStateException("Unexpected value: " + viewType);
      }
    }

    @Override
    public void onBindViewHolder(@Nonnull final ViewHolder holder,
                                 final int position)
    {
      holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    //    @Override
    //    public void change(final Watcher<XUser> watcher) {
    //      mItems.addAll(Util.transform(watcher.getData(), XItem::new));
    //    }

    public void notifyItemChanged(final XItem item)
    {
      int index = mItems.indexOf(item);
      if (index >= 0 && index < mItems.size()) {
        notifyItemChanged(index);
      }
    }
  }
}

