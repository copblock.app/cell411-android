package cell411.ui.friend;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import cell411.imgstore.ImageStore;
import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.AggRel;
import cell411.logic.rel.Key;
import cell411.logic.rel.Rel;
import cell411.methods.Dialogs;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.DialogShower;
import cell411.ui.base.FragmentFactory;
import cell411.ui.profile.UserFragment;
import cell411.ui.widget.CircularImageView;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;
import com.parse.ParseQuery;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;

import static cell411.enums.RequestType.FriendRequest;
import static cell411.utils.ViewType.*;

/**
 * Created by Sachin on 18-04-2016.
 */
public class FriendSearchFragment extends BaseFragment implements ValueObserver<String> {
  public static final XTAG TAG = new XTAG();
  private final static String smWaitingForQuery = "Waiting for Query";
  final ObservableValueRW<String> mQuery = new ObservableValueRW<>();
  private final Key mRemovedKey = Key.create("removed", true);
  private final AggRel mRemoved = AggRel.create(mRemovedKey);
  private final Rel mSelfRel;
  private final FragmentFactory mUserFragmentFactory = FragmentFactory.fromClass(UserFragment.class);
  private final TextListener mTextListener = new TextListener();
  private final UserListAdapter mAdapter = new UserListAdapter();
  private QueryRunner<XUser> mRunner;
  private TextView mCount;
  private SearchView mSearchView;

  public FriendSearchFragment() {
    super(R.layout.fragment_search_friend);
    mSelfRel = LiveQueryService.req().getRel("CurrentUser");
    mSelfRel.add(XUser.reqCurrentUser());
  }
  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mSearchView = view.findViewById(R.id.searchview_user);
    mCount = view.findViewById(R.id.count);
    mCount.setText(smWaitingForQuery);
    RecyclerView mRecycler = view.findViewById(R.id.rv_users);
    mRecycler.setAdapter(mAdapter);

    mSearchView.setIconifiedByDefault(false);
    mSearchView.setOnQueryTextListener(mTextListener);
    mSearchView.setOnCloseListener(() -> true);
    mRecycler.setHasFixedSize(true);
    mRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    mSearchView.setQuery(mQuery.get(), false);
    mQuery.addObserver(this);

    LiveQueryService liveQueryService = LiveQueryService.req();
    RelationWatcher relationWatcher = liveQueryService.getRelationWatcher();

    Rel friends = relationWatcher.getFriends();
    AggRel counterParties = relationWatcher.getCounterParties();
    Rel blocksMe = relationWatcher.getBlocksMe();
    Rel myBlocks = relationWatcher.getMyBlocks();
    mRemoved.addRels(friends, counterParties, blocksMe, myBlocks, mSelfRel);
    mRemoved.addObserver(mAdapter);
  }

  public void onPause() {
    super.onPause();
    mRunner = null;
    mAdapter.clear();
  }

  public void onChange(@Nullable final String newValue, @Nullable final String oldValue) {
    if (Util.isNoE(mQuery.get())) {
      mAdapter.clear();
      return;
    }
    // Since we are specifically interested in users to whom we are
    // not connected, we have to run the query ourselves.
    ParseQuery<XUser> query = buildQuery();
    if (mRunner != null ) {
      if( Objects.equals(query, mRunner.getQuery())) {
        return;
      } else {
        ThreadUtil.notify(mRunner,true);
      }
    }
    if (query == null) {
      showAlertDialog("alert", "Unable to create query");
    } else {
      mAdapter.clear();
      mRunner = new QueryRunner<>(query, this);
      ThreadUtil.onExec(mRunner);
    }
  }


  public ParseQuery<XUser> buildQuery() {
    ParseQuery<XUser> query = buildNameQuery();
    if (query != null) {
      query.whereNotContainedIn("objectId", mRemoved.getRelatedIds());
      query.orderByAscending("firstName").addAscendingOrder("lastName").addAscendingOrder("objectId");
    }
    return query;
  }

  ParseQuery<XUser> buildNameQuery() {
    ParseQuery<XUser> query1 = ParseQuery.getQuery("_User");
    ParseQuery<XUser> query2 = ParseQuery.getQuery("_User");
    final String query = mQuery.get();
    if (Util.isNoE(query)) {
      return null;
    }
    String[] toks = query.split("\\s+");
    if (toks.length > 0) {
      toks[0] = baseRegex(toks[0]);
    }
    if (toks.length > 1) {
      toks[1] = baseRegex(toks[1]);
    }
    if (toks.length > 2) {
      String msg = Util.format("Cannot build query from %d tokens", toks.length);
      showAlertDialog("alert", msg);
      return null;
    }
    if (toks.length == 2) {
      // With two tokens, one must match the first name, while
      // the other matches the last name.
      query1.whereMatches("lastName", toks[1], "i");
      query2.whereMatches("firstName", toks[1], "i");
    }
    // With one token, it can match either name.
    query1.whereMatches("firstName", toks[0], "i");
    query2.whereMatches("lastName", toks[0], "i");
    return ParseQuery.or(Arrays.asList(query1, query2));
  }

  private String baseRegex(String str) {
    return "^" + Util.lc(Util.trim(str));
  }

  public QueryRunner<XUser> getRunner() {
    return mRunner;
  }

  public void onUserClick(View v) {
    UserViewHolder vh = (UserViewHolder) v.getTag();
    mUserFragmentFactory.setObjectId(vh.mUser.getObjectId());
    push(mUserFragmentFactory);
  }

  public void onFriendClick(View v) {
    String msg = "Sending Friend Request";
    DialogShower shower = Dialogs.createNoButtonDialog("Sending ...", msg, null);
    ThreadUtil.onMain(shower);

    UserViewHolder vh = (UserViewHolder) v.getTag();
    BaseApp.getDataServer().handleRequest(FriendRequest, vh.mUser, (ex) -> shower.dismiss(), null);
  }

  public int getUserCount() {
    return mAdapter.mUsers.size();
  }

  public <X extends XUser> void addUsers(List<X> list) {
    for (XUser user : list) {
      mAdapter.accept(user);
    }
    updateCounts();
  }

  public int getHighWater() {
    return mAdapter.mHighWater;
  }

  public void queryComplete() {
    mRunner = null;
    String countText = Util.format("%d users loaded %d highWater (Done)",
            mAdapter.mUsers.size(), mAdapter.mHighWater);
    mCount.setText(countText);
  }

  public static class UserViewHolder extends RecyclerView.ViewHolder {
    View mView;
    XUser mUser;
    CircularImageView mImageView;
    TextView mTextView;
    TextView mButton1;
    TextView mButton2;

    UserViewHolder(View view) {
      super(view);
      mView = view;
      mImageView = view.findViewById(R.id.avatar);
      mTextView = view.findViewById(R.id.name);
      mButton1 = view.findViewById(R.id.txt_btn_action);
      mButton2 = view.findViewById(R.id.txt_btn_neg);
      mView.setTag(this);
      mImageView.setTag(this);
      mTextView.setTag(this);
      mButton1.setTag(this);
      mButton2.setTag(this);
      reset();
    }

    public void reset() {
      mUser = null;
      mView.setVisibility(View.VISIBLE);
      mView.setOnClickListener(null);
      mImageView.setVisibility(View.VISIBLE);
      mImageView.setOnClickListener(null);
      mImageView.setImageBitmap(null);
      mImageView.setImageDrawable(null);
      mTextView.setVisibility(View.VISIBLE);
      mTextView.setOnClickListener(null);
      mButton1.setVisibility(View.VISIBLE);
      mButton1.setOnClickListener(null);
      mButton2.setVisibility(View.GONE);
      mButton2.setOnClickListener(null);
    }
  }

  public class UserListAdapter extends Adapter<UserViewHolder> implements MyObserver, Consumer<XUser> {
    final private ArrayList<XUser> mUsers = new ArrayList<>();
    final private ArrayList<XItem> mItems = new ArrayList<>();
    public int mHighWater = 0;

    UserListAdapter() {
      mItems.add(new XItem("nq", "Enter a Query"));
    }

    @Nonnull
    @Override
    public UserViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int viewTypeId) {
      LayoutInflater li = LayoutInflater.from(parent.getContext());
      View v = li.inflate(R.layout.cell_user, parent, false);
      return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@Nonnull UserViewHolder vh, int position) {
      XItem item = get(position);
      int userIndex = -1;
      if(item.getViewType()==vtUser) {
        userIndex = mUsers.indexOf(item.getUser());
        if(mHighWater < userIndex) {
          mHighWater = userIndex;
          ThreadUtil.onExec(() -> ThreadUtil.notify(mRunner, true));
          updateCounts();
        }
      }
      vh.reset();
      if (item.getViewType() == vtUser) {
        final XUser user = item.getUser();
        vh.mUser = user;
        vh.mView.setOnClickListener(FriendSearchFragment.this::onUserClick);
        String name = user.getName();
        name=userIndex+" "+name;
        vh.mTextView.setText(name);
        vh.mTextView.setOnClickListener(FriendSearchFragment.this::onUserClick);
        ImageStore.req().setupImage(user, vh.mImageView);
        vh.mButton1.setOnClickListener(FriendSearchFragment.this::onFriendClick);
      } else if (item.getViewType() == vtNull) {
        vh.mTextView.setVisibility(View.INVISIBLE);
        vh.mImageView.setVisibility(View.GONE);
        vh.mButton1.setVisibility(View.GONE);
      } else if (item.getViewType() == vtString) {
        vh.mTextView.setVisibility(View.VISIBLE);
        vh.mTextView.setText(item.getText());
        vh.mImageView.setVisibility(View.GONE);
        vh.mButton1.setVisibility(View.GONE);
      }
    }

    @Override
    public int getItemViewType(final int position) {
      int res = super.getItemViewType(position);
      if (Util.theGovernmentIsHonest()) System.out.println("res=" + res);
      XItem item = mItems.get(position);
      ViewType type = item.getViewType();
      res = type.ordinal();

      return res;
    }

    @Override
    public int getItemCount() {
      return mItems.size();
    }

    public XItem get(int position) {
      return mItems.get(position);
    }

    @Override
    public void update(MyObservable o, Object arg) {
      Set<String> ids = mRemoved.getRelatedIds();
      synchronized(mItems) {
        int count = mItems.size();
        for(int i=0;i<mItems.size();i++) {
          XItem item = mItems.get(i);
          if (ids.contains(item.getObjectId())) {
            mItems.remove(i);
            notifyItemRemoved(i);
            i--;
          }
        }
        if(mItems.size()!=count)
          ThreadUtil.notify(mRunner,true);
      }
    }

    @Override
    public synchronized void accept(final XUser user) {
      assert ThreadUtil.isMainThread();
      synchronized (mItems) {
        for (int i = 0; i < mItems.size(); i++) {
          XItem item = mItems.get(i);
          if (item.getViewType() != vtUser) {
            mItems.remove(item);
            notifyItemRemoved(i);
          }
        }
        mUsers.add(user);
        XItem item = new XItem(user);
        int index = mItems.size();
        Reflect.announce("adding: " + user);
        Reflect.announce("    at: " + index);
        mItems.add(item);
        notifyItemInserted(index);
      }
    }

    public void clear() {
      mHighWater=0;
      mUsers.clear();
      mItems.clear();
      mCount.setText(smWaitingForQuery);
    }
  }

  private void updateCounts() {
    String countText = Util.format("%d users loaded %d highWater (Running)",
            mAdapter.mUsers.size(), mAdapter.mHighWater);
    mCount.setText(countText);
  }

  private class TextListener implements SearchView.OnQueryTextListener {
    @Override
    public boolean onQueryTextSubmit(String query) {
      Reflect.announce(query);
      mQuery.set(query);
      return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
      Reflect.announce(query);
      if(Util.isNoE(query)) {
        mQuery.set(query);
      }
      return false;
    }
  }

}
