package cell411.ui.friend;

import static cell411.utils.ViewType.vtUser;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.Parse;
import com.parse.http.ParseSyncUtils;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.imgstore.ImageStore;
import cell411.logic.LQListener;
import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.UserWatcher;
import cell411.logic.rel.Rel;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.profile.UserFragment;
import cell411.ui.widget.CircularImageView;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.concurrent.Task;
import cell411.utils.concurrent.ThreadUtil;

/**
 * Created by Sachin on 18-04-2016.
 */
@SuppressLint("NotifyDataSetChanged")
public class FriendFragment
  extends ModelBaseFragment
{
  public static final String TAG = "FriendFragment";
  private final FriendListAdapter mAdapter = new FriendListAdapter(this);
  private RelativeLayout rlNoFriends;
  private RecyclerView mRecycler;
  private TextView mCount;
  private UserWatcher mUserWatcher;
  private final Rel mFriendRel;
  private String mNumItemsFormat;
  private final LiveQueryService mLiveQueryService = LiveQueryService.opt();

  public FriendFragment()
  {
    super(R.layout.fragment_friends);
    mFriendRel=mLiveQueryService.getRelationWatcher().getFriends();
  }


  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mRecycler = view.findViewById(R.id.rv_friends);
    mAdapter.mItems.clear();
    mAdapter.mItems.add(new XItem("Loading", "Loading Data..."));
    mRecycler.setAdapter(mAdapter);
    mCount = view.findViewById(R.id.count);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    mRecycler.setHasFixedSize(true);
    // use a linear layout manager
    mRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    rlNoFriends = view.findViewById(R.id.rl_no_friends);
    rlNoFriends.setVisibility(View.GONE);
    if (mLiveQueryService == null) {
      showAlertDialog("Alert",
        "I don't know why, but there is no live query service.  " +
          "I'm gonna bail.", success -> pop());
    }
    mAdapter.change();
  }

  private void showDeleteFriendDialog(final int position)
  {
    final XItem item = mAdapter.getItem(position);
    if (item.getViewType() != vtUser) {
      return;
    }
    final XUser friend = item.getUser();
    showYesNoDialog("Are you sure?",
      "Remove " + friend.getName() + " as a friend?", success ->
      {
        if(success) {
          ThreadUtil.onExec(()->{
            XUser user = XUser.getCurrentUser();
            user.getRelation("friends").remove(friend);
            user.save();
          });
//          mFriendRel.remove(friend.getObjectId());
//          mAdapter.mItems.remove(position);
//          mAdapter.notifyDataSetChanged();
        } else {
          showAlertDialog("alert", "Canceled");
        }
      });
  }

  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public static class ViewHolder
    extends RecyclerView.ViewHolder
  {
    // each data item is just a string in this case
    private final TextView txtUserName;
    private final CircularImageView imgUser;
    public Button action;

    public ViewHolder(View view)
    {
      super(view);
      txtUserName = view.findViewById(R.id.name);
      imgUser = view.findViewById(R.id.avatar);
    }
  }

  public static class FriendListAdapter
    extends RecyclerView.Adapter<ViewHolder>
    implements LQListener<XUser>,
               MyObserver,
               Runnable
  {
    final FragmentFactory.ClassFactory<UserFragment> mUserFragment =
      FragmentFactory.fromClass(UserFragment.class);
    private final FriendFragment mOwner;
    public ArrayList<XItem> mItems = new ArrayList<>();
    private final View.OnClickListener mOnClickListener = this::onClick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public FriendListAdapter(FriendFragment owner)
    {
      mOwner = owner;
      mItems.add(new XItem("", "Loading Data"));
    }

    public void onClick(View v)
    {
      ViewParent vp = v.getParent();
      while (vp != mOwner.mRecycler) {
        if (vp instanceof View) {
          View asView = (View) vp;
          vp = asView.getParent();
        } else {
          return;
        }
      }
      View p = v;
      while(p.getParent()!= mOwner.mRecycler) {
        p=(View)p.getParent();
      }
      int position = mOwner.mRecycler.getChildAdapterPosition(p);
      XItem item = mOwner.mAdapter.getItem(position);
      if (item.getViewType() != vtUser) {
        return;
      }
      if (v instanceof Button) {
        mOwner.showDeleteFriendDialog(position);
      } else {
        mUserFragment.setObjectId(item.getObjectId());
        mOwner.push(mUserFragment);
      }
    }

    private XItem getItem(int position)
    {
      return mItems.get(position);
    }

    // Create new views (invoked by the layout manager)
    @Nonnull
    @Override
    public FriendFragment.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType)
    {
      LayoutInflater inf = LayoutInflater.from(parent.getContext());
      View v = inf.inflate(R.layout.cell_friend, parent, false);

      ViewHolder vh = new ViewHolder(v);
      vh.action = v.findViewById(R.id.action);
      vh.action.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final @Nonnull FriendFragment.ViewHolder vh,
                                 final int position)
    {
      final XItem item = mItems.get(position);
      if (item.getViewType() == vtUser) {
        final XUser user = item.getUser();
        vh.txtUserName.setText(user.getName());
        ImageStore.req().setupImage(user, vh.imgUser);
        vh.itemView.setOnClickListener(null);
        vh.itemView.setOnClickListener(mOnClickListener);
        Context context = mOwner.getContext();
        assert context != null;
        vh.txtUserName.setTextColor(mOwner.getResources()
          .getColor(R.color.text_primary, context.getTheme()));
        String text = "-Friend";
        vh.action.setText(text);
      } else if (item.getViewType() == ViewType.vtString) {
        Context context = mOwner.getContext();
        assert context != null;
        String string = item.getText();
        vh.txtUserName.setText(string);
        vh.imgUser.setImageBitmap(null);
        vh.itemView.setOnClickListener(null);
        vh.txtUserName.setTextColor(mOwner.getResources()
          .getColor(R.color.text_primary, context.getTheme()));
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    @Override
    public void update(final MyObservable o, final Object arg)
    {
      change();
    }

    @Override
    public void change()
    {
      run();
    }

    public void run()
    {
      if (!ThreadUtil.isMainThread()) {
        ThreadUtil.onMain(this);
        return;
      }
      if (mOwner.mNumItemsFormat == null) {
        mOwner.mNumItemsFormat = mOwner.getString(R.string.num_items);
      }

      TreeSet<XUser> objects = new TreeSet<>(XUser::nameCompare);
      HashSet<XUser> needed = new HashSet<>();
      for (String id : mOwner.mFriendRel.getRelatedIds()) {
        XUser user = Parse.getOrStub(id, XUser.class);
        objects.add(user);
        if (!user.isComplete()) {
          needed.add(user);
        }
      }
      if (!needed.isEmpty()) {
        ParseSyncUtils.fetchAll(needed);
      }
      String text = Util.format(mOwner.mNumItemsFormat, objects.size());
      mOwner.mCount.setText(text);
      ArrayList<XItem> items = new ArrayList<>();
      if (!objects.isEmpty()) {
        for (XUser user : objects) {
          items.add(new XItem(user));
        }
      }
      mItems = items;
      mOwner.rlNoFriends.setVisibility(
        objects.isEmpty() ? View.VISIBLE : View.GONE);
      callNotifyDataSetChanged();
    }

    public void callNotifyDataSetChanged()
    {
      Task<Void> runnable =
        Task.forVirtual(Void.class,this, "notifyDataSetChanged");
      ThreadUtil.onMain(runnable);
    }

  }

}
