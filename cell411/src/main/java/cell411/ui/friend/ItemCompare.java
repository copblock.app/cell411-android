package cell411.ui.friend;

import java.util.Comparator;

import cell411.model.XRequest;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.utils.ViewType;

public class ItemCompare
  implements Comparator<XItem>
{
  public int compare(XItem o1, XItem o2)
  {
    ViewType vt1 = o1.getViewType();
    ViewType vt2 = o2.getViewType();
    if (vt1 != vt2) {
      return compare(vt1.ordinal(), vt2.ordinal());
    }
    switch (vt1) {
      case vtNull:
        return compare(o1.hashCode(), o2.hashCode());
      case vtRequest:
        return compare(o1.getRequest(), o2.getRequest());
      default:
      case vtString:
        return compare(o1.getText(), o2.getText());
    }
  }

  public int compare(int i1, int i2)
  {
    return i1 - i2;
  }

  public int compare(XRequest r1, XRequest r2)
  {
    int comp1 = compare(r1.ownedByCurrent(), r2.ownedByCurrent());
    if (comp1 == 0) {
      if (r1.ownedByCurrent()) {
        comp1 = compare(r1.getSentTo(), r2.getSentTo());
      } else {
        comp1 = compare(r1.getOwner(), r2.getOwner());
      }
    }
    return comp1;
  }

  private int compare(String s1, String s2)
  {
    return String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
  }

  public int compare(boolean b1, boolean b2)
  {
    return (b1 ? -1 : 0) + (b2 ? 1 : 0);
  }

  private int compare(XUser u1, XUser u2)
  {
    return u1.nameCompare(u2);
  }
}
