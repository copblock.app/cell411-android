package cell411.ui.friend;

//import com.github.clans.fab.FloatingActionMenu;
//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.MainThread;



import com.safearx.cell411.R;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.FragmentFactory;
import cell411.ui.base.XSelectFragment;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabFriendFragment
  extends XSelectFragment
{
  public TabFriendFragment()
  {
    super(R.layout.fragment_selectx);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @MainThread
  @Override
  public void onSaveInstanceState(@Nonnull Bundle outState)
  {
    Reflect.announce("onSaveInstanceState");
    super.onSaveInstanceState(outState);
  }

  public List<FragmentFactory> createFactories()
  {
    return Arrays.asList(
      FragmentFactory.fromClass(FriendFragment.class, "Friends"),
      FragmentFactory.fromClass(RequestFragment.class, "Requests"),
      FragmentFactory.fromClass(FriendSearchFragment.class, "Search"));
  }

  //  @Override
  //  public <X extends BaseFragment>
  //  void findFragment(final Class<X> type) {
  //    super.findFragment(type);
  //    if(type==FriendRequestFragment.class){
  //      selectFragment(1);
  //    }
  //  }
}

