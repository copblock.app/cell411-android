package cell411.ui.profile;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.CallSuper;
import androidx.appcompat.app.AlertDialog;

import cell411.ui.base.BaseApp;

import com.parse.Parse;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.io.File;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.imgstore.ImageStore;
import cell411.config.ConfigDepot;
import cell411.model.XUser;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.ip.ImagePickerContract;
import cell411.ui.ip.PicPrefs;
import cell411.ui.widget.XBlinker;
import cell411.utils.ImageUtils;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.Util;
import cell411.utils.io.XLog;
import cell411.utils.reflect.XTAG;
import cell411.utils.reflect.Reflect;

public class ProfileViewFragment
  extends ModelBaseFragment
{
  private static final XTAG TAG = new XTAG();
  private static final FragmentFactory mEditFactory =
    FragmentFactory.fromClass(ProfileEditFragment.class);

  static {
    XLog.i(TAG, "loading class");
  }

  ImagePickerContract mImagePickerContract = new ImagePickerContract();
  Callback mCallback = new Callback();
  final ActivityResultLauncher<PicPrefs> mLauncher = createLauncher();
  private TextView mTxtPhoneNot;
  private ImageView mImgVerified;
  private XBlinker mBlinkingRedSymbol;
  private TextView mTxtBtnAddPhone;
  private ImageView mImgUser;
  private TextView mTxtName;
  private TextView mTxtEmail;
  private TextView mTxtPhone;
  private TextView mTxtBloodGroup;
  private TextView mTxtEmergencyContactName;
  private TextView mTxtEmergencyContactNumber;
  private TextView mTxtAllergies;
  private TextView mTxtOtherMedicalConditions;
  private TextView mTxtProfileCompleteness;
  private TextView mTxtCity;
  private View mEditButton;
  private View mImgEdit;

  public ProfileViewFragment()
  {
    super(R.layout.fragment_profile_view);
  }

  private ActivityResultLauncher<PicPrefs> createLauncher()
  {
    return registerForActivityResult(mImagePickerContract, mCallback);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull final View view,
                            @Nullable final Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

    mImgUser = findViewById(R.id.avatar);
    mImgEdit = findViewById(R.id.img_edit);
    mTxtName = findViewById(R.id.name);
    mTxtEmail = findViewById(R.id.txt_email);
    mTxtCity = findViewById(R.id.txt_city_name);
    mTxtPhone = findViewById(R.id.txt_phone);
    mTxtPhoneNot = findViewById(R.id.txt_not);
    mImgVerified = findViewById(R.id.img_verified);
    mBlinkingRedSymbol = findViewById(R.id.brs);
    mTxtBtnAddPhone = findViewById(R.id.txt_btn_add_phone);
    mTxtProfileCompleteness = findViewById(R.id.txt_profile_completeness);
    mTxtBloodGroup = findViewById(R.id.txt_blood_group);
    mTxtEmergencyContactName = findViewById(R.id.txt_emergency_contact_name);
    mTxtEmergencyContactNumber =
      findViewById(R.id.txt_emergency_contact_number);
    mTxtAllergies = findViewById(R.id.txt_allergies);
    mTxtOtherMedicalConditions =
      findViewById(R.id.txt_other_medical_conditions);
    mEditButton = findViewById(R.id.edit);
    mEditButton.setOnClickListener(this::onClick);
    mImgEdit.setOnClickListener(this::onClick);
    mTxtCity.setText("");
  }

  @CallSuper
  public void loadData1()
  {
    Throwable e = null;
    Cell411.req().sendToast(e);
    XUser user = (XUser) Parse.getCurrentUser();
    ImageStore.req().setupImage(user, mImgUser);
    ParseGeoPoint point = user.getLocation();
    if (point == null) {
      mTxtCity.setText(R.string.no_location_on_file);
    } else {
      mTxtCity.setText(R.string.waiting_for_city);
        BaseApp.getDataServer().requestCity(point, address -> ThreadUtil.onMain(
        () -> setAddress(mTxtCity, address.cityPlus()), (long) 0));
    }
    mTxtName.setText(user.getName());
    mTxtEmail.setText(user.getEmail());
    final String mobileNumber = user.getString("mobileNumber");
    if (Util.isNoE(mobileNumber)) {
      mTxtPhone.setText(R.string.not_available);
      mTxtPhoneNot.setVisibility(View.GONE);
      mBlinkingRedSymbol.setVisibility(View.GONE);
      mImgVerified.setVisibility(View.GONE);
      mTxtBtnAddPhone.setVisibility(View.VISIBLE);
    } else {
      mTxtPhone.setVisibility(View.VISIBLE);
      mImgVerified.setVisibility(View.VISIBLE);
      mTxtPhone.setText(user.getMobileNumber());
      mTxtBtnAddPhone.setVisibility(View.GONE);
    }
    mTxtPhoneNot.setVisibility(View.GONE);
    mBlinkingRedSymbol.setVisibility(View.GONE);
    mImgVerified.setVisibility(View.GONE);
    String bloodType = user.getBloodType();
    if (!Util.isNoE(bloodType) && !bloodType.equals("null")) {
      mTxtBloodGroup.setText(bloodType);
    } else {
      mTxtBloodGroup.setText(R.string.not_available);
    }
    String emergencyContactName = user.getEmergencyContactName();
    if (!Util.isNoE(emergencyContactName) &&
      !emergencyContactName.equals("null"))
    {
      mTxtEmergencyContactName.setText(emergencyContactName);
    }
    String emergencyContactNumber = user.getEmergencyContactNumber();
    if (!Util.isNoE(emergencyContactNumber) &&
      !emergencyContactNumber.equals("null"))
    {
      mTxtEmergencyContactNumber.setText(emergencyContactNumber);
    }
    String allergies = user.getAllergies();
    if (!Util.isNoE(allergies) && !allergies.equals("null")) {
      mTxtAllergies.setText(allergies);
    }
    String otherMedicalConditions = user.getOtherMedicalConditions();
    if (!Util.isNoE(otherMedicalConditions) &&
      !otherMedicalConditions.equals("null"))
    {
      mTxtOtherMedicalConditions.setText(otherMedicalConditions);
    }
    mTxtProfileCompleteness.setText(getProfileCompletePercentage(user));
  }

  private void setAddress(TextView txtCity, String address)
  {
    txtCity.setText(address);
  }

  private String getProfileCompletePercentage(ParseUser user)
  {
    int percentage = 0;
    if (!Util.isNoE(user.getString("firstName"))) {
      percentage += 10;
    }
    if (!Util.isNoE(user.getString("mobileNumber"))) {
      percentage += 10;
    }
    if (!Util.isNoE(user.getString("mobileNumber")) &&
      user.getBoolean("phoneVerified"))
    {
      percentage += 10;
    }
    if ((!Util.isNoE(user.getString("email"))) ||
      user.getUsername().contains("@"))
    {
      percentage += 10;
    }
    if (!Util.isNoE(user.getString("emergencyContactName"))) {
      percentage += 10;
    }
    if (!Util.isNoE(user.getString("emergencyContactNumber"))) {
      percentage += 10;
    }
    if (!Util.isNoE(user.getString("bloodType"))) {
      percentage += 20;
    }
    if (!Util.isNoE(user.getString("allergies"))) {
      percentage += 10;
    }
    if (!Util.isNoE(user.getString("otherMedicalConditions"))) {
      percentage += 10;
    }
    return getString(R.string.profile_setup, percentage);
  }

  public void onClick(View view)
  {
    if (view == mEditButton) {
      push(mEditFactory);
    } else if (view == mImgEdit) {
      captureAndUploadImage();
    } else if (view == null) {
      showAddPhoneDialog();
    }
  }

  public void captureAndUploadImage()
  {
    mImgUser.setImageDrawable(ImageStore.req().getPlaceHolder());
    try {
      mLauncher.launch(new PicPrefs("avatar", "image/*"));
    } catch (Exception e) {
      showAlertDialog("alert", "Shit, it didn't initialize right.\n\n" + e);
    }
  }

  private void showAddPhoneDialog()
  {
    Context context = requireContext();
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle(R.string.dialog_title_add_phone);
    alert.setMessage(R.string.dialog_message_add_phone);
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(
      Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_add_phone, null);
    final EditText etPhone = view.findViewById(R.id.et_phone);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel,
      (dialog, arg1) -> dialog.dismiss());
    alert.setPositiveButton(R.string.dialog_btn_add, (dialog, which) ->
    {
    });
    final AlertDialog dialog = alert.create();
    dialog.show();

    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE)
      .setOnClickListener(v ->
      {
        String phone = etPhone.getText().toString().trim();
        if (phone.isEmpty()) {
          showAlertDialog("alert", R.string.validation_mobile_number);
        }
      });
  }

  class Callback
    implements ActivityResultCallback<Integer>,
               Runnable
  {
    private Integer mResult;

    @Override
    public void onActivityResult(Integer result)
    {
      mResult = result;
      
      ThreadUtil.onExec(this);
    }

    @Override
    public void run()
    {
      try {
        PicPrefs prefs = ImagePickerContract.claimPicPref(mResult);
        Bitmap bitmap = prefs.mBitmap;
        assert bitmap != null;
        assert prefs.mFile == null;
        bitmap = ImageUtils.getCroppedBitmap(bitmap);
        //    bitmap = Bitmap.createScaledBitmap(bitmap,300,300,false);
        XUser user = XUser.reqCurrentUser();
        File avatarDir = ConfigDepot.getAvatarDir();
        File avatarFile =
          new File(avatarDir, "saved." + Util.isoDate() + ".png");
        ImageUtils.saveImage(avatarFile, bitmap, CompressFormat.PNG);
        ImageUtils.queueUpload(avatarFile, user.getObjectId(), "avatar");
        showAlertDialog("alert", "Image Queued");
      } catch (Throwable t) {
        t.printStackTrace();
      }
    }
  }
}

