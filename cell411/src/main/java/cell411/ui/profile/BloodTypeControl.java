package cell411.ui.profile;

import static cell411.enums.BloodType.UNKNOWN;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.gridlayout.widget.GridLayout;

import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;

import cell411.enums.BloodType;

public class BloodTypeControl
  extends GridLayout
  implements View.OnClickListener
{
  BloodTypeData mSelected;
  ArrayList<BloodTypeData> mData;
  Drawable mCellBackgroundSel;
  Drawable mCellBackgroundUns;

  public BloodTypeControl(Context context)
  {
    this(context, null);
  }

  public BloodTypeControl(final Context context, final AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public BloodTypeControl(final Context context, final AttributeSet attrs,
                          final int defStyleAttr)
  {
    super(context, attrs, defStyleAttr);
    mCellBackgroundSel =
      ActivityCompat.getDrawable(context, R.drawable.bg_blood_group_highlight);
    mCellBackgroundUns = ActivityCompat.getDrawable(context,
      R.drawable.bg_blood_group_gray_border);

    mData = new ArrayList<>(Arrays.asList(new BloodTypeData(UNKNOWN),
      new BloodTypeData(BloodType.A_MINUS), new BloodTypeData(BloodType.A_PLUS),
      new BloodTypeData(BloodType.B_MINUS), new BloodTypeData(BloodType.B_PLUS),
      new BloodTypeData(BloodType.AB_MINUS),
      new BloodTypeData(BloodType.AB_PLUS),
      new BloodTypeData(BloodType.O_MINUS),
      new BloodTypeData(BloodType.O_PLUS)));
  }

  @Override
  protected void onMeasure(final int widthSpec, final int heightSpec)
  {
    super.onMeasure(widthSpec, heightSpec);
  }

  @Override
  protected void onLayout(final boolean changed, final int l, final int t,
                          final int r, final int b)
  {
    super.onLayout(changed, l, t, r, b);
  }

  @Override
  public void onViewAdded(final View child)
  {
    super.onViewAdded(child);
    if (child instanceof TextView) {
      System.out.println("Text: " + ((TextView) child).getText());
    }
  }

  @Override
  protected void onSizeChanged(final int w, final int h, final int oldw,
                               final int oldh)
  {
    super.onSizeChanged(w, h, oldw, oldh);
  }

  @Override
  protected void onDraw(final Canvas canvas)
  {
    super.onDraw(canvas);
  }

  public String getValue()
  {
    if (mSelected == null) {
      return "";
    } else {
      return mSelected.mType.altName();
    }
  }

  public void setValue(BloodType type)
  {
    for (BloodTypeData data : mData) {
      if (data.mType == type) {
        mSelected = data;
      }
    }
    for (BloodTypeData data : mData) {
      data.updateUI();
    }
  }

  public void setValue(String type)
  {
    setValue(BloodType.forString(type));
  }

  @Override
  public void onClick(View v)
  {
    mSelected = null;
    BloodTypeData data = getData((d) -> d.mTextView == v);
    if (data == null) {
      return;
    }
    mSelected = data;
    updateUi();
  }

  BloodTypeData getData(Predicate<BloodTypeData> predicate)
  {
    for (BloodTypeData data : mData) {
      if (predicate.test(data)) {
        return data;
      }
    }
    return null;
  }

  public void updateUi()
  {
    mData.forEach(BloodTypeData::updateUI);
  }

  public BloodTypeData getSelected()
  {
    return mSelected;
  }

  class BloodTypeData
  {
    final BloodType mType;
    final TextView mTextView = new TextView(getContext());

    BloodTypeData(BloodType type)
    {
      mType = type;
      mTextView.setOnClickListener(BloodTypeControl.this);
      mTextView.setText(type.altName());
      GridLayout.LayoutParams params =
        new GridLayout.LayoutParams(spec(0, 1, 0),
          spec(mType.ordinal(), 1, 1.0f));
      params.width = 100;
      params.height = 100;
      mTextView.setLayoutParams(params);
      mTextView.setBackground(mCellBackgroundSel);
      mTextView.setTextColor(0xff000000);
      mTextView.setTag(mType);
      mTextView.setGravity(Gravity.CENTER);
      addView(mTextView);
    }

    public void updateUI()
    {
      if (mSelected.mType == mTextView.getTag()) {
        mTextView.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        mTextView.setTextColor(Color.WHITE);
      } else {
        mTextView.setTextColor(Color.parseColor("#999999"));
        mTextView.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
      }
    }
  }

}
