package cell411.ui.alert;

import static com.safearx.cell411.Cell411.TIME_TO_LIVE_FOR_CHAT_ON_ALERTS;
import static cell411.utils.ViewType.vtAlert;
import static cell411.utils.ViewType.vtString;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.Parse;
import com.parse.model.ObjectEvent;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.enums.ProblemType;
import cell411.imgstore.ImageStore;
import cell411.logic.AlertWatcher;
import cell411.logic.LQListener;
import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.Rel;
import cell411.model.XAlert;
import cell411.model.XEntity;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.friend.AddFriendModules;
import cell411.ui.widget.CircularImageView;
import cell411.ui.widget.ProblemTypeInfo;
import cell411.utils.Util;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabAlertFragment
  extends ModelBaseFragment
{
  private final AlertsListAdapter mAdapter = new AlertsListAdapter();
  private final Rel mAlerts;
  private final FragmentFactory mAlertDetailFragFactory =
    FragmentFactory.fromClass(AlertDetailFragment.class);
  private AlertWatcher mAlertWatcher = null;
  private RecyclerView mRecycler;

  public TabAlertFragment()
  {
    super(R.layout.fragment_tab_alerts);
    LiveQueryService liveQueryService = LiveQueryService.opt();
    assert liveQueryService != null;
    setAlertWatcher(liveQueryService.getAlertWatcher());
    RelationWatcher relationWatcher = liveQueryService.getRelationWatcher();
    mAlerts = relationWatcher.getAudienceAlerts();
    mAlerts.addObserver(mAdapter);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    System.out.println(savedInstanceState);
    mRecycler = view.findViewById(R.id.rv_alerts);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    mAdapter.mItems.clear();
    mAdapter.mItems.add(new XItem("", "Loading Data"));
    mRecycler.setAdapter(mAdapter);
    mRecycler.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager =
      new LinearLayoutManager(getActivity());
    mRecycler.setLayoutManager(linearLayoutManager);
    mAdapter.change();
  }

  public void onResume()
  {
    super.onResume();
  }

  public void onPause()
  {
    super.onPause();
  }

  public void loadData2()
  {
  }

  public void loadData3()
  {
  }

  public AlertWatcher getAlertWatcher()
  {
    return mAlertWatcher;
  }

  public void setAlertWatcher(AlertWatcher alertWatcher)
  {
    mAlertWatcher = alertWatcher;
  }

  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public static class ViewHolder
    extends RecyclerView.ViewHolder
  {
    // each data item is just a string in this case
    private TextView txtAlert;
    private TextView txtAlertTime;
    private LinearLayout llBtnFlag;
    private CircularImageView imgUser;
    private ImageView imgChat;
    private ImageView imgAlertType;
    private TextView txtInfo;

    public ViewHolder(View view, int type)
    {
      super(view);
      if (type == vtAlert.ordinal()) {
        txtAlert = view.findViewById(R.id.txt_alert);
        txtAlertTime = view.findViewById(R.id.txt_alert_time);
        llBtnFlag = view.findViewById(R.id.rl_btn_flag);
        imgUser = view.findViewById(R.id.avatar);
        imgChat = view.findViewById(R.id.img_chat);
        imgAlertType = view.findViewById(R.id.img_alert_type);
      } else if (type == vtString.ordinal()) {
        txtInfo = view.findViewById(R.id.txt_info);
      }
    }
  }

  public class AlertsListAdapter
    extends RecyclerView.Adapter<ViewHolder>
    implements LQListener<XAlert>,
               MyObserver
  {
    private final ArrayList<XItem> mItems = new ArrayList<>();

    public AlertsListAdapter()
    {
    }

    // Provide a suitable constructor (depends on the kind of dataset)

    // Create new views (invoked by the layout manager)
    @Nonnull
    @Override
    public TabAlertFragment.ViewHolder onCreateViewHolder(
      @Nonnull ViewGroup parent, int viewType)
    {
      // create a new view
      View v;
      if (viewType == vtAlert.ordinal()) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_alert, parent, false);
      } else if (viewType == vtString.ordinal()) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_footer, parent, false);
      } else {
        throw new IllegalArgumentException(
          "viewType should be ALERT or FOOTER");
      }
      // set the view's size, margins, padding(s) and layout parameters
      TabAlertFragment.ViewHolder vh = new ViewHolder(v, viewType);
      v.setOnClickListener(this::onAlertClicked);
      return vh;
    }

    public void onAlertClicked(View v)
    {
      XItem item = mAdapter.mItems.get(mRecycler.getChildAdapterPosition(v));
      if (item.getViewType() == vtAlert) {
        XAlert alert = item.getAlert();
        mAlertDetailFragFactory.setObjectId(alert.getObjectId());
        push(mAlertDetailFragFactory);
      }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(
      @Nonnull final TabAlertFragment.ViewHolder viewHolder, final int position)
    {
      XItem item = mItems.get(position);
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if (getItemViewType(position) == vtAlert.ordinal()) {
        final XAlert alert = item.getAlert();
        if (alert.getOwner() == null) {
          return;
        }
        String description;
        String issuer;
        if (alert.isSelfAlert()) {
          issuer = "You";
        } else {
          issuer = alert.getOwner().getName();

        }
        ProblemType problemType = alert.getProblemType();
        description = issuer + " issued a " + problemType + " alert";

        viewHolder.txtAlert.setText(description);
        viewHolder.txtAlertTime.setText(
          Util.formatDateTime(alert.getCreatedAt()));
        final BaseActivity activity = (BaseActivity) getActivity();

        XUser user = alert.getOwner();
        ImageStore.req().setupImage(user, viewHolder.imgUser);
        ProblemTypeInfo problemTypeInfo =
          ProblemTypeInfo.valueOf(alert.getProblemType().ordinal());
        viewHolder.imgAlertType.setImageResource(problemTypeInfo.getImageRes());
        viewHolder.imgAlertType.setBackgroundResource(
          problemTypeInfo.getBGDrawable());
        if (alert.isSelfAlert()) {
          viewHolder.llBtnFlag.setVisibility(View.GONE);
        } else {
          viewHolder.llBtnFlag.setVisibility(View.VISIBLE);
          viewHolder.llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
        }
        viewHolder.llBtnFlag.setOnClickListener(
          view -> showFlagAlertDialog(alert));
        if (System.currentTimeMillis() >=
          alert.getCreatedAt().getTime() + TIME_TO_LIVE_FOR_CHAT_ON_ALERTS)
        {
          viewHolder.imgChat.setVisibility(View.GONE);
        } else {
          viewHolder.imgChat.setVisibility(View.VISIBLE);
          viewHolder.imgChat.setOnClickListener(view ->
          {
            ViewParent parent = view.getParent();
            while (!(parent instanceof RecyclerView)) {
              view = (View) parent;
              parent = view.getParent();
            }
            int pos = mRecycler.getChildAdapterPosition(view);
            XItem selItem = mItems.get(pos);
            XEntity entity = (XEntity) selItem.getParseObject();
            Cell411.req().openChat(entity);
          });
        }
      } else {
        viewHolder.txtInfo.setText(item.getText());
      }
    }

    @Override
    public int getItemViewType(int position)
    {
      super.getItemViewType(position);
      return mItems.get(position).getViewType().ordinal();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    void showFlagAlertDialog(final XAlert cell411Alert)
    {
      AddFriendModules.showFlagAlertDialog(getActivity(),
        cell411Alert.getOwner(), null);
    }

    @Override
    public void onEvents(ObjectEvent<XAlert> event)
    {
      LQListener.super.onEvents(event);
    }

    @Override
    public void change()
    {
      ArrayList<XAlert> data = new ArrayList<>();
      for (String id : mAlerts.getRelatedIds()) {
        data.add(Parse.getObject(id));
      }
      if (data.isEmpty()) {
        List<XItem> list = new ArrayList<>();
        list.add(new XItem("", "No Alerts Active"));
        setData(list);
      } else {
        setData(Util.transform(data, XItem::new));
      }
    }

    @Override
    public void update(MyObservable o, Object arg)
    {
      change();
    }

    public void setData(List<XItem> items)
    {
      int minCount = Math.min(mItems.size(), items.size());
      if (mItems.size() > minCount) {
        int maxCount = mItems.size();
        while (mItems.size() > minCount) {
          mItems.remove(minCount);
        }
        notifyItemRangeRemoved(minCount, maxCount);
      }
      if (minCount > 0) {
        for (int i = 0; i < minCount; i++) {
          mItems.set(i, items.get(i));
        }
        notifyItemRangeChanged(0, minCount);
      }
      if (items.size() > minCount) {
        for (int i = minCount; i < items.size(); i++) {
          mItems.add(items.get(i));
        }
        notifyItemRangeInserted(minCount, items.size());
      }
    }
  }

}
