package cell411.ui.alert;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import cell411.enums.ProblemType;
import cell411.model.XUser;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.widget.OpaqueAreaClickListener;
import cell411.ui.widget.PolygonImageView;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;
import com.parse.Parse;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TabMapFragment
  extends ModelBaseFragment
  implements OpaqueAreaClickListener
{
  static int[] mImageIds =
    new int[]{R.id.img_medical, R.id.img_criminal, R.id.img_pulled_over,
      R.id.img_police_interaction, R.id.img_police_arrest,
      R.id.img_panic_button, R.id.img_danger, R.id.img_broken_car,
      R.id.img_being_bullied, R.id.img_general, R.id.img_photo, R.id.img_video,
      R.id.img_fire};
  ProblemType mProblemType;
  FragmentFactory mFragmentFactory =
    FragmentFactory.fromClass(AlertIssuingFragment.class);
  TogglePatrolMode mTogglePatrolMode = new TogglePatrolMode();
  private AnimatorSet mOuterRing;
  private AnimatorSet mInnerRing;
  private RelativeLayout rlRadialMenu;
  private ImageView imgLocationCenter;
  private ImageView imgPatrolMode;
  private int COLOR_ACCENT;
  private int COLOR_GRAY_CCC;
  private ImageView imgUploadImage;
  //  private ImageView imgUploadImage;

  public TabMapFragment()
  {
    super(R.layout.fragment_tab_map);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    View res = super.onCreateView(inflater, container, savedInstanceState);
    Reflect.announce();
    return res;
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    final BaseActivity activity = (BaseActivity) getActivity();
    assert activity != null;
    COLOR_ACCENT = activity.getColor(R.color.colorAccent);
    COLOR_GRAY_CCC = activity.getColor(R.color.gray_ccc);
    rlRadialMenu = view.findViewById(R.id.rl_radial_menu);
    RelativeLayout rlRadialMenuOuterLayer = view.findViewById(R.id.rl_outer);
    RelativeLayout rlRadialMenuInnerLayer = view.findViewById(R.id.rl_inner);
    prepareAnimator(rlRadialMenuOuterLayer, rlRadialMenuInnerLayer);
    imgPatrolMode = view.findViewById(R.id.img_patrol_mode_enabled);
    imgLocationCenter = view.findViewById(R.id.img_location_center_enabled);
    imgUploadImage = view.findViewById(R.id.img_upload_photos);
    imgUploadImage.setOnClickListener(this::onClick);
    for (int id : mImageIds) {
      PolygonImageView poly = rlRadialMenu.findViewById(id);
      poly.setOnOpaqueAreaClickListener(this);
    }
    configToggles();
  }

  @Override
  public void onResume()
  {
    super.onResume();
    if (Util.theGovernmentIsHonest())
      ThreadUtil.onMain(this::animateRadialMenu);
  }

  public void onPause()
  {
    super.onPause();
  }

  public void animateRadialMenu()
  {
    if (mOuterRing != null)
      mOuterRing.start();
    if (mInnerRing != null)
      mInnerRing.start();
  }

  private void prepareAnimator(RelativeLayout rlRadialMenuOuterLayer,
                               RelativeLayout rlRadialMenuInnerLayer)
  {
    try {
      mOuterRing = (AnimatorSet) AnimatorInflater.loadAnimator(BaseApp.req().getCurrentActivity(),
        R.animator.radial_outer_animator);
      mOuterRing.setTarget(rlRadialMenuOuterLayer);
      mInnerRing = (AnimatorSet) AnimatorInflater.loadAnimator(BaseApp.req().getCurrentActivity(),
        R.animator.radial_inner_animator);
      mInnerRing.setTarget(rlRadialMenuInnerLayer);
    } catch (Throwable justDumped) {
      // Say nothing, act natural!
      mOuterRing = null;
      mInnerRing = null;
    }
  }

  public void onClick(View v)
  {
    Reflect.announce(this);
    if (v == imgPatrolMode) {
      
      ThreadUtil.onExec(mTogglePatrolMode);
    } else if (v == imgLocationCenter) {
      toggleLocationCenter();
    }
  }

  public void configToggles()
  {
    XUser user = (XUser) Parse.getCurrentUser();
    if (user == null) {
      return;
    }
    boolean patrolMode = user.getPatrolMode();
    imgPatrolMode.setOnClickListener(this::onClick);
    if (patrolMode) {
      imgPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      imgPatrolMode.setBackgroundResource(
        R.drawable.bg_location_center_enabled);
    } else {
      imgPatrolMode.setBackgroundTintList(
        ColorStateList.valueOf(COLOR_GRAY_CCC));
      imgPatrolMode.setBackgroundResource(
        R.drawable.bg_location_center_disabled);
    }
    imgPatrolMode.setImageResource(R.drawable.img_patrol_mode);
    imgLocationCenter.setOnClickListener(this::onClick);
    imgLocationCenter.setBackgroundTintList(
      ColorStateList.valueOf(COLOR_ACCENT));
    imgLocationCenter.setBackgroundResource(
      R.drawable.bg_location_center_enabled);
    imgLocationCenter.setImageResource(R.drawable.img_loc_center);
    //    imgUploadImage.setOnClickListener(this::onClick);

  }

  private void toggleLocationCenter()
  {
    XUser currentUser = (XUser) Parse.getCurrentUser();
    if(currentUser==null)
      return;
    currentUser.setLocation(BaseApp.req().getLocationService().getParseGeoPoint());
    ThreadUtil.onExec(currentUser::save);
  }

  @Override
  public void onOpaqueAreaClicked(View v, String tag)
  {
    ProblemType problemType = ProblemType.valueOf(tag);
    System.out.println("Wedge tagged " + problemType + " clicked");
    mProblemType = problemType;
    mFragmentFactory.setProblemType(mProblemType);
    push(mFragmentFactory);
  }

  class TogglePatrolMode
    implements Runnable
  {

    @Override
    public void run()
    {
      if (ThreadUtil.isMainThread()) {
        
        ThreadUtil.onExec(this);
      } else {
        final XUser currentUser = (XUser) Parse.getCurrentUser();
        if(currentUser==null)
          return;
        boolean patrolMode = currentUser.getPatrolMode();
        currentUser.setPatrolMode(!patrolMode);
        currentUser.setLocation(BaseApp.req().getLocationService().getParseGeoPoint());
        currentUser.save();
        configToggles();
      }
    }
  }
}
