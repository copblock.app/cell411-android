package cell411.services;

import android.Manifest.permission;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;

import com.parse.model.ParseGeoPoint;
import com.safearx.cell411.Cell411;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import cell411.model.XUser;
import cell411.ui.base.BaseApp;
import cell411.utils.LocationUtil;
import cell411.utils.Util;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.Task;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

@SuppressWarnings("unused")
class LocationServiceImpl
  extends ContextWrapper
  implements LocationService
{
  final static public String LS_MIN_TIME_KEY =
    "LocationService.minDistanceMetres";
  final static public String LS_MIN_DIST_KEY = "LocationService.minTimeMs";
  static final ByteArrayOutputStream baos = new ByteArrayOutputStream();
  // final, static members
  final private static ObservableValueRW<Location> mLocation;
  private static final LocationListener mListener;
  private static final XTAG TAG = new XTAG();
  private static LocationServiceImpl smInstance;

  static {
    mLocation = new ObservableValueRW<>(Location.class, null);
    mListener = new LocationServiceListener();
    mLocation.set(LocationUtil.getLocation(0, 0));
  }

  public void addObserver(ValueObserver<Location> locationValueObserver)
  {
    if(LocationServiceImpl.smInstance == null || LocationServiceImpl.mLocation==null) {
      LocationServiceImpl.mWaitList.add(locationValueObserver);
    } else {
      LocationServiceImpl.mLocation.addObserver(locationValueObserver);
      LocationServiceImpl.smInstance.startWatching();
    }
  }

  public void removeObserver(ValueObserver<Location> locationValueObserver)
  {
    LocationServiceImpl.mLocation.removeObserver(locationValueObserver);
    if (LocationServiceImpl.mLocation.countObservers() == 0 && LocationServiceImpl.smInstance != null) {
      LocationServiceImpl.smInstance.stopWatching();
    }
  }

  public Set<String> getPermRequests()
  {
    Set<String> perms = new HashSet<>();
    perms.add(permission.ACCESS_COARSE_LOCATION);
    perms.add(permission.ACCESS_FINE_LOCATION);
    return perms;
  }
  final Cell411 mAppContext;
  private final Task<Void> mLoadLocationRunnable;
  private final Task<Void> mInitRunnable;
  boolean mIsWatching = false;
  private Future<XUser> mFutureUser;
  private long smLastUpdateMillis = 0;

  {
    mLoadLocationRunnable = Task.forVirtual(Void.class,this, "loadLocation");
    mInitRunnable = Task.forVirtual(Void.class,this, "init");
  }

  {
    smInstance = this;
  }

  public LocationServiceImpl(Cell411 context)
  {
    super(context);
    assert(false);
    mAppContext = context;
    ThreadUtil.onExec(mInitRunnable);
  }

  static ArrayList<ValueObserver<Location>> mWaitList = new ArrayList<>();

  private void stopWatching()
  {
      Context context = BaseApp.getDataServer().getApplicationContext();
    LocationManager lm = context.getSystemService(LocationManager.class);
    try {
      lm.removeUpdates(mListener);
    } catch (SecurityException se) {
      BaseApp.req().handleException("Requesting location updates", se);
    }
  }

  @Override
  public void init()
  {
    boolean done = false;
    ThreadUtil.waitUntil(this,()->BaseApp.opt()!=null,500);
    PrintStream timeLog = System.out;
    try {
      assert !ThreadUtil.isMainThread();
      Future<XUser> userFuture = mAppContext.getFutureUser();
      timeLog.println("Getting CurrentUser");
      System.out.println(baos);
      XUser user = userFuture.get();
      timeLog.println("Starting location service");
      System.out.println(baos);
      ThreadUtil.onExec(mLoadLocationRunnable);
      done = true;
    } catch (Throwable t) {
      t.printStackTrace(timeLog);
      System.out.println(baos);
    } finally {
      if (!done) {
        ThreadUtil.onExec(mInitRunnable);
      }
    }
  }

  @Override
  public void loadLocation()
  {
    Reflect.announce();
    if (BaseApp.opt() == null) {
      Reflect.announce("Deferring:  app()=null");
      ThreadUtil.onExec(mLoadLocationRunnable, 5000);
      return;
    }
    try {
      SharedPreferences latLngPref = getAppPrefs();
      double lat =
        latLngPref.getFloat("Location.Lat", (float) 42.93383800273033);
      double lng =
        latLngPref.getFloat("Location.Lng", (float) -72.27850181930877);
      long locationTime = latLngPref.getLong("Location.Date", 0);
      Location location = LocationUtil.getLocation(lat, lng);
      smLastUpdateMillis = locationTime;
      mLocation.set(location);
    } catch (Exception e) {
      Reflect.announce("Exception: %s", e);
      e.printStackTrace();
    }
  }

  @Override
  public void storeLocation()
  {
    SharedPreferences latLngPref = getAppPrefs();
    SharedPreferences.Editor edit = latLngPref.edit();
    Location location = mLocation.get();
    if (location != null) {
      edit.putFloat("Location.Lat", (float) location.getLatitude());
      edit.putFloat("Location.Lng", (float) location.getLongitude());
      edit.putLong("Location.Time", smLastUpdateMillis);
      edit.apply();
    }
  }

  @Override
  public long locationAge()
  {
    return System.currentTimeMillis() - smLastUpdateMillis;
  }

  private boolean startWatching(String provider)
  {
    Context context = getApplicationContext();
    LocationManager lm = context.getSystemService(LocationManager.class);
    try {
      SharedPreferences prefs = getAppPrefs();
      // Default to 4 updates per hour
      long minDelay =
        prefs.getLong(LS_MIN_TIME_KEY, Util.millis(15, TimeUnit.MINUTES));

      // Default to one update per mile.
      long minDistance = prefs.getLong(LS_MIN_DIST_KEY, 1600);

      lm.requestLocationUpdates(provider, minDelay, minDistance, mListener);
      return true;
    } catch (SecurityException se) {
      BaseApp.req().handleException("Requesting location updates", se);
    }
    return false;
  }

  void startWatching()
  {
    if (mIsWatching) {
      return;
    }
    Reflect.announce();
    BaseApp app = BaseApp.req();
    if (ThreadUtil.isMainThread()) {
      ThreadUtil.onExec((Runnable) this::startWatching, 500);
      return;
    }
    Set<String> missingPermissions = app.getMissingPermissions();
    boolean locOk =
      !missingPermissions.contains(permission.ACCESS_COARSE_LOCATION);
    if (!locOk) {
      locOk = !missingPermissions.contains(permission.ACCESS_FINE_LOCATION);
    }
    if (!locOk) {
      ThreadUtil.onExec((Runnable) this::startWatching, 1000);
      return;
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
      mIsWatching =
        mIsWatching || startWatching(LocationManager.FUSED_PROVIDER);
    }
    mIsWatching = mIsWatching || startWatching(LocationManager.GPS_PROVIDER);
    if (!mIsWatching) {
        BaseApp.getDataServer().showToast(
        "The location manager is not available.  I don't know where we are!");
    }
  }

  @Override
  public ParseGeoPoint getParseGeoPoint()
  {
    return LocationUtil.getGeoPoint(getLocation());
  }

  @Override
  @Nonnull
  public Location getLocation()
  {
    Location res = mLocation.get();
    if (res == null) {
      res = LocationUtil.getLocation(0, 0);
    }
    return res;
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                        String key)
  {
    switch (key) {
      case LS_MIN_DIST_KEY:
      case LS_MIN_TIME_KEY:

    }
  }

  private static class LocationServiceListener
    implements LocationListener
  {
    @Override
    public void onLocationChanged(@Nonnull final Location location)
    {
      mLocation.set(location);
    }

    @Override
    public void onFlushComplete(final int requestCode)
    {
      Reflect.announce("Flush Complete");
    }

    @Override
    public void onProviderEnabled(@Nonnull final String provider)
    {
      Reflect.announce(provider);
    }

    @Override
    public void onProviderDisabled(@Nonnull final String provider)
    {
      Reflect.announce(provider);
    }
  }
}

