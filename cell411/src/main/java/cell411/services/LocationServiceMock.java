package cell411.services;

import android.content.SharedPreferences;
import android.location.Location;

import androidx.annotation.NonNull;

import com.parse.model.ParseGeoPoint;

import java.util.HashSet;
import java.util.Set;

import cell411.utils.LocationUtil;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;

public class LocationServiceMock
  implements LocationService
{
  ObservableValueRW<Location> mLocation = new ObservableValueRW<>();
  @Override
  public void init()
  {
  }

  @Override
  public void loadLocation()
  {

  }

  @Override
  public void storeLocation()
  {

  }

  @Override
  public long locationAge()
  {
    return 0;
  }

  @Override
  public ParseGeoPoint getParseGeoPoint()
  {
    return LocationUtil.getGeoPoint(getLocation());
  }

  @NonNull
  @Override
  public Location getLocation()
  {
    return null;
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                        String key)
  {

  }

  @Override
  public void addObserver(ValueObserver<Location> locationObserver)
  {

  }

  @Override
  public Set<String> getPermRequests()
  {
    return new HashSet<>();
  }

  @Override
  public void removeObserver(ValueObserver<Location> locationChanged)
  {

  }
}
