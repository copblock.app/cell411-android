package com.safearx.cell411;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.POST_NOTIFICATIONS;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_BOOT_COMPLETED;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;
import static android.Manifest.permission.SEND_SMS;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;

import androidx.core.graphics.drawable.IconCompat;

import com.parse.Parse;
import com.parse.model.ParseGeoPoint;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.android.RingtoneData;
import cell411.config.ConfigDepot;
import cell411.imgstore.ImageStoreConnection;
import cell411.imgstore.ImageStore;
import cell411.logic.LiveQueryConnection;
import cell411.logic.LiveQueryService;
import cell411.logic.NotificationAgent;
import cell411.model.XBaseCell;
import cell411.model.XEntity;
import cell411.model.XUser;
import cell411.services.LocationService;
import cell411.services.LocationServiceMock;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.MainActivity;
import cell411.utils.ResultListener;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.func.Func0;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class Cell411
  extends BaseApp
{
  public static final long TIME_TO_LIVE_FOR_CHAT_ON_ALERTS = 86400 * 1000 * 3;
  final static XTAG TAG = new XTAG();
  static {
    announce("Loading Class");
  }

  static void announce(Object obj) {
    XLog.i(TAG, Reflect.announceStr(1,obj));
  }
  final FragmentSelector mFragmentSelector = new FragmentSelector();
  final LocationService mLocationService = new LocationServiceMock();
  final CurrentUserGetter mUserGetter = new CurrentUserGetter();
  private final boolean mDarkModeChanged = false;  final Set<String> mAllPermissions = getAllPermissions();
  LiveQueryConnection mLiveQueryConnection;
  NotificationAgent mNotificationAgent;
  ImageStoreConnection mImageStoreConnection;


  public Cell411()
  {
    super();
    announce("Building Instance");
  }

  RingtoneData mRingtoneeDat;

  @Override
  public ParseGeoPoint getCurrentLocation() {
    return null;
  }

  static public Cell411 opt()
  {
    return (Cell411) BaseApp.opt();
  }

  @Override
  public Set<String> getMemberIds(XBaseCell cell) {
    return null;
  }

  @Override
  public IconCompat getSmallIcon() {
    return null;
  }

  @Nonnull
  static public Cell411 req()
  {
    return (Cell411) BaseApp.req();
  }

  @Override
  public LocationService getLocationService()
  {
    return mLocationService;
  }

  protected Class<Cell411Activity> getMainActivityClass()
  {
    return Cell411Activity.class;
  }
  boolean first=true;
  public BaseActivity getCurrentActivity()
  {
    return super.getCurrentActivity();
  }

  @Override
  public Class<? extends Activity> getActivityClass() {
    return null;
  }

  public Set<String> getAllPermissions()
  {
    if (mAllPermissions != null) {
      return mAllPermissions;
    }

    Set<String> perms = new HashSet<>();

    perms.add(ACCESS_NETWORK_STATE);
    perms.add(CAMERA);
    perms.add(INTERNET);
    perms.add(RECEIVE_BOOT_COMPLETED);
    perms.add(RECORD_AUDIO);
    perms.add(SEND_SMS);
    perms.add(READ_SMS);
    if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) {
      perms.add(POST_NOTIFICATIONS);
    }
    perms.add(REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
    perms.addAll(getLocationService().getPermRequests());
    perms.addAll(LiveQueryService.getPermRequests());
    return perms;
  }

  protected void sendLocationNotification(ParseGeoPoint geoPoint)
  {
    NotificationAgent agent = getNotificationAgent();
    if (agent == null)
      return;
    agent.sendLocationNotification(geoPoint);
  }

  @Override
  protected void setMainActivity(MainActivity baseActivity)
  {
    super.setMainActivity(baseActivity);
    if(baseActivity!=null){
      mFragmentSelector.start();
    }
  }

  public int getPrimaryColor()
  {
    return getResources().getColor(R.color.colorPrimary, getTheme());
  }

  public boolean isDone()
  {
    return false;
  }

  public int getNotificationWidth()
  {
    return getResources().getDimensionPixelSize(
      androidx.core.R.dimen.compat_notification_large_icon_max_width);
  }

  @Override
  public int getLargeIcon() {
    return 0;
  }

  @Override
  public SharedPreferences getPrefs() {
    return null;
  }

  @Override
  public RingtoneData getRingtoneData() {
    return mRingtoneeDat;
  }

  @Override
  public Drawable getUserPlaceholder() {
    return null;
  }

  public int getNotificationHeight()
  {
    return getResources().getDimensionPixelSize(
      androidx.core.R.dimen.compat_notification_large_icon_max_height);
  }

  public NotificationAgent getNotificationAgent()
  {
    if (mNotificationAgent == null) {
      if (LiveQueryService.opt() == null)
        return null;
      mNotificationAgent = new NotificationAgent(LiveQueryService.opt());
    }
    return mNotificationAgent;
  }

  @Nonnull
  public Context createWindowContext(int type, @Nullable Bundle options)
  {
    return super.createWindowContext(type, options);
  }

  public Future<ImageStore> startImageStore()
  {
    Context context = getBaseContext();
    assert context != null;
    if (mImageStoreConnection == null)
      mImageStoreConnection = new ImageStoreConnection(context);
    return ThreadUtil.submit(mImageStoreConnection);
  }

  public Future<LiveQueryService> startLiveQueryService()
  {
    Context context = getBaseContext();
    assert context != null;
    if (mLiveQueryConnection == null)
      mLiveQueryConnection =
        new LiveQueryConnection(this, LiveQueryService.class, LiveQueryService::convert);

    return ThreadUtil.submit(mLiveQueryConnection);
  }
  public LiveQueryService noLiveQueryService() {
    return null;
  }

  public void clearCache()
  {
    LiveQueryService liveQueryService = LiveQueryService.opt();
    if (liveQueryService != null)
      liveQueryService.clear();
  }

  public File getExternalDir(String type)
  {
    File dir =
      new File(getExternalFilesDir(type), "cell411/" + ConfigDepot.getFlavor());

    if (!dir.mkdirs() && !dir.exists()) {
      showAlertDialog("alert",
        "dir " + dir + " does not exist, and creation failed");
      return null;
    }
    return dir;
  }

  public Set<String> getMissingRingtones()
  {
    return new HashSet<>();
  }

  public void openChat(final XEntity cell)
  {
    MainActivity mainActivity = getMainActivity();
    if (mainActivity == null)
      return;
    mainActivity.openChat(cell);
  }

  public void requestPermission(String[] perms,
                                ResultListener<String[]> results)
  {
    Runnable runnable = () -> results.result(perms, null);
    ThreadUtil.onMain(runnable,500);
  }


  public Future<XUser> getFutureUser()
  {
    return ThreadUtil.submit(mUserGetter);
  }

  static class CurrentUserGetter
    implements Callable<XUser>,
               ValueObserver<XUser>,
               Func0<Boolean>

  {
    static int smCount=0;
    int mId = ++smCount;
    public CurrentUserGetter() {
      Reflect.announce();
    }
    @Override
    public Boolean apply()
    {
      return Parse.isInitialized() && Parse.getCurrentUser() != null;
    }

    @Override
    public XUser call()
      throws Exception
    {
      assert !ThreadUtil.isMainThread();
      ThreadUtil.waitUntil(this, this, 15000);
      return XUser.getCurrentUser();
    }

    @Override
    public void onChange(@Nullable final XUser newValue,
                         @Nullable final XUser oldValue)
    {
      notifyAll();
    }
  }


}
