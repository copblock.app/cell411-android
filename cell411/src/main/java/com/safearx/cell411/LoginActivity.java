package com.safearx.cell411;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import cell411.model.XEntity;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.FragmentFrame;
import cell411.ui.base.MainActivity;
import cell411.ui.welcome.StartFragment;

public class LoginActivity
  extends MainActivity
{
  private final FragmentFactory mStartPage;
  {
    mStartPage=FragmentFactory.fromClass(StartFragment.class);
  }
  public LoginActivity() {
    super(R.layout.activity_login);

  }

  public static void start(Activity source)
  {
    Intent intent = new Intent(source,LoginActivity.class);
    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
    source.startActivity(intent);
  }

  @Override
  public void closeDrawer()
  {

  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    onFragmentPopped(null,null);
    getFragmentFrame().push(mStartPage);
  }

  @Override
  public void openChat(XEntity cell)
  {
    throw new RuntimeException("Nobody is Logged In!");
  }

  @Override
  public void onFragmentPopped(FragmentFrame fragmentFrame,
                               FragmentFactory factory)
  {
    super.onFragmentPopped(fragmentFrame, factory);
    if(getFragmentFrame().isEmpty())
      push(mStartPage);
  }

}