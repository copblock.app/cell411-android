package com.safearx.cell411;

import static cell411.utils.concurrent.ThreadUtil.waitUntil;

import android.os.Bundle;

import com.parse.model.ParseUser;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.Nullable;

import cell411.imgstore.ImageStore;
import cell411.model.XEntity;
import cell411.model.XUser;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.ui.friend.FriendSearchFragment;
import cell411.utils.OnCompletionListener;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.concurrent.Task;

public class OnePageActivity
  extends MainActivity
{
  private final Task<Void> mLoadLocationRunnable =
    Task.forVirtual(Void.class,this,
      "addFrag");
  FragmentFactory mFactory =
    FragmentFactory.fromClass(FriendSearchFragment.class);

  public OnePageActivity()
  {
    super(R.layout.one_activity);
  }

  public void addFrag()
  {
    assert !ThreadUtil.isMainThread();
    Cell411.req().initParse();
    if (XUser.getCurrentUser() == null) {
      ParseUser.logIn("dev1@copblock.app", "asdf");
    }
    ThreadUtil.onExec(() ->
  {
    synchronized (AutoCloseable.class) {
      ThreadUtil.waitUntil(AutoCloseable.class, () -> false, 1000);
    }
  });
    waitUntil(this, () ->
    {
      ImageStore is = ImageStore.opt();
      if (is == null)
        return false;
      return ImageStore.opt().isReady();
    }, 1000);
    assert ImageStore.opt().isReady();
    showYesNoDialog("Question", "Are you happy?", new Pusher());
  }

  @Override
  public void closeDrawer()
  {

  }

  @Override
  protected void onCreate(@Nullable final Bundle savedInstanceState)
  {
    ThreadUtil.onExec(() ->
  {
    Future<ImageStore> future = Cell411.req().startImageStore();
    System.out.println("Got future: " + future);
    try {
      future.get();
    } catch (ExecutionException | InterruptedException e) {
      throw Util.rethrow("Waiting for ImageStore", e);
    }
  });
    super.onCreate(savedInstanceState);
    ThreadUtil.onExec(mLoadLocationRunnable);
  }

  @Override
  public void openChat(final XEntity cell)
  {

  }

  class Pusher
    implements OnCompletionListener
  {
    @Override
    public void done(final boolean success)
    {
      push(mFactory);
    }
  }
}
