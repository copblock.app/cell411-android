package com.safearx.cell411;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.parse.Parse;
import com.parse.model.ParseUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.model.XEntity;
import cell411.ui.MainFragment;
import cell411.ui.base.BaseApp;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;

public class Cell411Activity
  extends MainActivity
  implements ValueObserver<ParseUser>
{
  private final Closer mCloser = new Closer();
  boolean mHasBeen = false;
  FragmentFactory mMainFactory = FragmentFactory.fromClass(MainFragment.class);
  private DrawerLayout mDrawer;
  private Cell411NavView mNavigationView;
  private ActionBarDrawerToggle mToggle;
  private Toolbar mActionBar;

  public Cell411Activity()
  {
    super(R.layout.activity_main);
  }

  public static void start(Activity source)
  {
    Intent intent = new Intent(source, Cell411Activity.class);
    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
    source.startActivity(intent);
  }

  @Override
  public void onChange(ParseUser newValue, ParseUser oldValue)
  {
    Reflect.announce();
    if (newValue != null) {
      mHasBeen = true;
    } else if (mHasBeen) {
      finish();
    }
    Reflect.announce();
  }

  public void closeDrawer()
  {
    ThreadUtil.onMain(mCloser, 0);
  }

  @Override
  protected void onCreate(@Nullable final Bundle savedInstanceState)
  {
    if (Parse.getCurrentUser() == null) {
      LoginActivity.start(this);
      finish();
    }
    super.onCreate(savedInstanceState);
    if(mMainFactory==null)
      mMainFactory = FragmentFactory.fromClass(MainFragment.class);
    push(mMainFactory);
    mActionBar = findViewById(R.id.toolbar);
    mDrawer = findViewById(R.id.drawer_layout);
    setSupportActionBar(mActionBar);

    int navOpen = R.string.navigation_drawer_open;
    int navClose = R.string.navigation_drawer_close;
    mToggle =
      new ActionBarDrawerToggle(this, mDrawer, mActionBar, navOpen, navClose);
    mToggle.setDrawerIndicatorEnabled(true);
    for (int i = 0; i < mActionBar.getChildCount(); i++) {
      View view = mActionBar.getChildAt(i);
      if (!(view instanceof TextView)) {
        continue;
      }
      view.setOnClickListener(this::onTitleClick);
      break;
    }

    mDrawer.addDrawerListener(mToggle);
    mToggle.syncState();
    mNavigationView = findViewById(R.id.nav_view);
    mNavigationView.setup(this);
    mCloser.run();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    mMainFactory=null;
  }

  @Override
  protected void onStart() {
    super.onStart();
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  protected void onPostResume() {
    super.onPostResume();
  }

  @Override
  public void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState, @androidx.annotation.Nullable PersistableBundle persistentState) {
    super.onCreate(savedInstanceState, persistentState);
  }

  @Override
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
  }

  private void onTitleClick(View view)
  {
    Reflect.announce("Title Clicked");
    FragmentFactory top = getFragmentFrame().getTopFactory();
    System.out.println(top);
  }

  @Override
  public void openChat(XEntity cell)
  {
    BaseApp app = BaseApp.opt();
    if (app != null)
      app.openChat(cell);
  }

  @Override
  public boolean onChildBackPressed()
  {
    if (mDrawer.isOpen()) {
      mCloser.run();
      return true;
    } else {
      return super.onChildBackPressed();
    }
  }

  @Override
  public void setVisible(boolean visible)
  {
    System.out.println("setVisible(" + visible + ")");
    super.setVisible(visible);
  }

  @Override
  public boolean onOptionsItemSelected(@Nonnull MenuItem item)
  {
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onResume()
  {
    super.onResume();
  }

  @Override
  public ApplicationInfo getApplicationInfo()
  {
    if (getBaseContext() == null) {
      return null;
    }
    return super.getApplicationInfo();
  }

  class Closer
    implements Runnable
  {
    @Override
    public void run()
    {
      if (!ThreadUtil.isMainThread()) {
        ThreadUtil.onMain(this, 200);
      } else {
        mDrawer.close();
      }
    }
  }
}