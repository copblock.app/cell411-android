package com.safearx.cell411;

import static cell411.utils.reflect.Reflect.announce;

import com.parse.Parse;
import com.parse.model.ParseUser;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;

import cell411.config.ConfigDepot;
import cell411.imgstore.ImageStore;
import cell411.logic.LiveQueryService;
import cell411.model.XEntity;
import cell411.model.XUser;
import cell411.ui.base.BaseApp;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.ui.welcome.AccountFragment;
import cell411.ui.welcome.PermissionFragment;
import cell411.ui.welcome.UserConsentFragment;
import cell411.utils.OnCompletionListener;
import cell411.utils.Util;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.Caller;
import cell411.utils.concurrent.ThreadName;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.func.Func0;
import cell411.utils.reflect.Reflect;

public class FragmentSelector
  extends Thread
  implements ValueObserver<Object>,
             ExceptionHandler
{
  final BadState smBadState = new BadState();
  private static final Caller<Boolean> smParseIsInitialized =
    Caller.forStatic(Boolean.class, Parse.class, "isInitialized");
  private static Func0<Boolean> smHasPermissions;
  final AtomicReference<FragmentSelector> mHighlander = new AtomicReference<>();
  private XUser mUser;
  FragmentFactory mPermissionFactory =
    FragmentFactory.fromClass(PermissionFragment.class);
  FragmentFactory mUserConsentFactory =
    FragmentFactory.fromClass(UserConsentFragment.class);
  FragmentFactory mAccountFactory =
    FragmentFactory.fromClass(AccountFragment.class);
  private MainActivity mActivity;

  public FragmentSelector()
  {
    super("FragmentSelector");
    setName("FragmentSelector"+this);
    announce("Fragment Selector Created");
    if (!mHighlander.compareAndSet(null, this))
      throw new RuntimeException("There can be only one!");
    mUser = XUser.getCurrentUser();
  }

  public void start()
  {
    announce("Fragment Selector Starting");
    if (getState() == State.NEW)
      super.start();
  }

  public void run()
  {
    assert currentThread() == this;
    while(Util.theGovernmentIsLying()) {
      try (ThreadName ignored = new ThreadName("FragmentSelector")) {
        waitUntil(this, this::isActivityStarted, 2000);
        mActivity = Cell411.req().getMainActivity();
        assert mActivity != null;
        if (mActivity instanceof Cell411Activity &&
          Parse.getCurrentUser() == null)
        {
          announce("Moving on to second activity.");
          LoginActivity.start(mActivity);
          mActivity.finish();
          ThreadUtil.waitUntil(this,
            ()->getMainActivity() instanceof LoginActivity,1000);
        }
        Parse.addCurrentUserObserver(this);
        tryInitParse();
        waitUntil(this, () -> ConfigDepot.opt() != null, 2000);
        tryPushWait(mPermissionFactory, getHasPermissions());
        while(Parse.getCurrentUser()==null) {
          tryPushWait(mAccountFactory, () -> Parse.getCurrentUser() != null);
        }
        while(Parse.getCurrentUser()!=null) {
          tryPushWait(mUserConsentFactory, FragmentSelector::userHasConsented);
          tryStartLiveQueryService();
          tryStartImageStore();
          tryStartMainActivity();
          waitForLogout();
        }
      }
    }
  }

  public void waitForLogout() {
    try (ThreadName ignored=new ThreadName()) {
      announce("We can nap now");
      System.out.println("CurrentUser:     "+XUser.getCurrentUser());
      System.out.println("CurrentActivity: "+Cell411.req().getMainActivity());
      waitUntil(this, smBadState, 60000);
      ThreadUtil.sleep(1000);
    }
  }

  boolean isActivityStarted()
  {
    return Cell411.req().getMainActivity() != null;
  }

  void tryInitParse()
  {
    try (ThreadName ignore = new ThreadName()) {
      Reflect.announce();
      BaseApp.req().initParse();
      waitUntil(this, smParseIsInitialized, 2000);
    }
  }

  void tryPushWait(FragmentFactory factory, Func0<Boolean> func)
  {
    String name = Util.format("tryPushWait(%s)",factory.getTitle());
    try (ThreadName ignore = new ThreadName(name) ) {
      if (func.apply())
        return;
      assert !(boolean) factory.isAdded();
      mActivity.push(factory, true, true);
      waitUntil(this, ()-> (factory.isAdded() || func.apply()),
        1000, 5000);
      if(!factory.isAdded() && Parse.getCurrentUser()==null) {
        mActivity.push(factory, true, true);
        waitUntil(this, ()-> (factory.isAdded() || func.apply()),
          2000, 5000);
      }
      waitUntil(func, 5000);
      mActivity.popAll();
      waitUntil(this, factory::notAdded, 5000);
    }
  }

  private static Func0<Boolean> getHasPermissions()
  {
    if (smHasPermissions == null) {
      smHasPermissions =
        Caller.forVirtual(Boolean.class, Cell411.req(), "hasPermissions");
    }
    return smHasPermissions;
  }

  private static Boolean userHasConsented()
  {
    Reflect.announce();
    ParseUser parseUser = Parse.getCurrentUser();
    if(parseUser==null || parseUser.getClass()!=XUser.class)
      return false;
    XUser user = (XUser) parseUser;
    return user.getConsented() == Boolean.TRUE;
  }

  void tryStartLiveQueryService()
  {
    try (ThreadName ignored = new ThreadName()) {
      if (LiveQueryService.opt() != null)
        return;
      Future<LiveQueryService> future = Cell411.req().startLiveQueryService();
      LiveQueryService service;
      try {
        service = future.get();
      } catch (ExecutionException | InterruptedException e) {
        throw new RuntimeException(e);
      }
      if (service == null) {
        OnCompletionListener onCompletionListener = success -> ThreadUtil.onExec(this);
        mActivity.showAlertDialog("Warning", "Failed to start LiveQueryService",
          onCompletionListener);
        return;
      }
      waitUntil(service::isReady,2000);
    }
  }

  private void tryStartMainActivity()
  {
    try (ThreadName ignore = new ThreadName()) {
      mUser=XUser.getCurrentUser();
      if(mUser==null)
        return;
      mActivity=getMainActivity();
      if(mActivity instanceof Cell411Activity)
        return;
      Cell411Activity.start(mActivity);
      ThreadUtil.waitUntil(this,
        ()->getMainActivity() instanceof Cell411Activity, 1000, 30000);
    }
  }

  private void waitUntil(Object object, Func0<Boolean> func, long interval,
                         long timeout)
  {
    ThreadUtil.waitUntil(object, func, interval,timeout);
  }
  private void waitUntil(Object object, Func0<Boolean> func, long time)
  {
    ThreadUtil.waitUntil(object, func, time);
  }

  private void waitUntil(Func0<Boolean> func, long interval)
  {
    waitUntil(this, func, interval);
  }

  public void openChat(XEntity cell)
  {
    announce(cell);
  }

  void tryStartImageStore()
  {
    try (ThreadName ignore = new ThreadName()) {
      while (true) {
        try {
          if (ImageStore.opt() != null)
            return;
          ThreadUtil.onMain(() -> mActivity.setDrawerIndicatorEnabled(false));
          Future<ImageStore> future = Cell411.req().startImageStore();
          ImageStore service = future.get();
          if (service == null) {
            throw new NullPointerException("Service is null");
          }
          ImageStore.addReadyObserver(this);
          waitUntil(this, service::isReady, 2000);
          return;
        } catch (Throwable e) {
          mActivity.handleException("staring live query service", e);
        }
      }
    }

  }

  @Override
  public void onChange(@Nullable Object newValue, @Nullable Object oldValue)
  {
    announce();
    ThreadUtil.onExec(() -> ThreadUtil.notify(this, true));
  }

  class BadState
    implements Func0<Boolean>
  {
    private BadState() {

    }
    @Override
    public Boolean apply()
    {
      mActivity=getMainActivity();
      mUser=XUser.getCurrentUser();
      boolean noUser = (mUser==null);
      boolean login = (mActivity instanceof LoginActivity);
      return noUser!=login;
    }
  }

  public MainActivity getMainActivity() {
    return mActivity=Cell411.req().getMainActivity();
  }


}
