I moved the old ios cient into the client-ios subdir of this project.

It should probably live in the root of it eventually, but I'm not sure
what parts of it are required, so a good place to start would be to
cherry-pick from the old dir and move what is required to build here,
and then kill the client-ios dir.

Let me know what you figutre out.

There is a file called Secrets.h, which contains ( as one might
imagine ) keys and such which should not be compromised.  If somebody
starts working on this, let me know, and I'll try to help you get
everything running as best I can.

If the build fails for want to Secrets.h, I'd suggest just creating an
empty file, and when it crys about a symbol, just define that symbol
as an empty string in the empty Secrets.h file.  
